package mosaics.collections.mutable.graphs.oo;

public interface GraphEdge<P,V> {
    public P getKey();
    public GraphNode<P,V> getNode();
}
