package mosaics.collections.mutable.ring;

import mosaics.collections.SeqDouble;
import mosaics.fp.collections.DoubleIterable;
import mosaics.fp.collections.DoubleIterator;
import mosaics.lang.Assert;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.lang.functions.DoubleFunctionLD;

import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;


public interface RingBufferDouble extends SeqDouble {

    public default SeqDouble copy() {
        SeqDouble clone = new ArrayRingBufferDouble( Backdoor.toInt(this.size()), this.lhsIndex() );

        this.iterator().forEach( clone::append );

        return clone;
    }


    public default SeqDouble map( DoubleUnaryOperator op ) {
        return new MappedRingBufferDouble(this, op);
    }

    public default SeqDouble mapWithIndex( DoubleFunctionLD op ) {
        return new MappedWithIndexRingBufferDouble( this, op );
    }

    public default SeqDouble add( SeqDouble rhs ) {
        return new AggregatingRingBufferDouble( this, rhs, (a,b) -> a + b );
    }

    public default SeqDouble sub( SeqDouble rhs ) {
        return new AggregatingRingBufferDouble( this, rhs, (a,b) -> a - b );
    }

    public default SeqDouble shiftLeft( int n ) {
        return new ShiftedRingBufferDouble( this, -n );
    }

    public default SeqDouble shiftRight( int n ) {
        return new ShiftedRingBufferDouble( this, n );
    }

    public default SeqDouble sumPrevious( int n ) {
        return new SumPreviousRingBufferDouble( this, n );
    }
}



class AggregatingRingBufferDouble extends ReadOnlyRingBufferDouble {
    private final SeqDouble     lhs;
    private final SeqDouble     rhs;
    private final DoubleBinaryOperator aggregator;

    public AggregatingRingBufferDouble( SeqDouble lhs, SeqDouble rhs, DoubleBinaryOperator aggregator ) {
        this.lhs        = lhs;
        this.rhs        = rhs;
        this.aggregator = aggregator;
    }

    public double get( long i ) {
        double a = lhs.get(i);
        double b = rhs.get(i);

        return aggregator.applyAsDouble(a, b);
    }


    public long lhsIndex() {
        assertSizesMatch();

        return lhs.lhsIndex();
    }

    public long rhsIndexInc() {
        assertSizesMatch();

        return lhs.rhsIndexInc();
    }

    public long rhsIndexExc() {
        assertSizesMatch();

        return lhs.rhsIndexExc();
    }

    public long size() {
        assertSizesMatch();

        return lhs.size();
    }

    public DoubleIterator iterator() {
        assertSizesMatch();

        return DoubleIterable.combine(lhs.iterator(), rhs.iterator(), aggregator);
    }

    private void assertSizesMatch() {
        Assert.isEqualTo( lhs.size(), rhs.size(), "lhs.size", "rhs.size" );
    }
}

class MappedRingBufferDouble extends ReadOnlyRingBufferDouble {
    private final SeqDouble           wrappedInstance;
    private final DoubleUnaryOperator op;

    MappedRingBufferDouble( SeqDouble wrappedInstance, DoubleUnaryOperator op ) {
        this.wrappedInstance = wrappedInstance;
        this.op = op;
    }

    public double get( long i ) {
        return op.applyAsDouble(wrappedInstance.get(i));
    }

    public long lhsIndex() {
        return wrappedInstance.lhsIndex();
    }

    public long rhsIndexInc() {
        return wrappedInstance.rhsIndexInc();
    }

    public long rhsIndexExc() {
        return wrappedInstance.rhsIndexExc();
    }

    public long size() {
        return wrappedInstance.size();
    }

    public DoubleIterator iterator() {
        return wrappedInstance.iterator().map( op );
    }
}

class MappedWithIndexRingBufferDouble extends ReadOnlyRingBufferDouble {
    private final SeqDouble        wrappedInstance;
    private final DoubleFunctionLD op;

    MappedWithIndexRingBufferDouble( SeqDouble wrappedInstance, DoubleFunctionLD op ) {
        this.wrappedInstance = wrappedInstance;
        this.op              = op;
    }

    public double get( long i ) {
        return op.invoke(i, wrappedInstance.get(i));
    }

    public long lhsIndex() {
        return wrappedInstance.lhsIndex();
    }

    public long rhsIndexInc() {
        return wrappedInstance.rhsIndexInc();
    }

    public long rhsIndexExc() {
        return wrappedInstance.rhsIndexExc();
    }

    public long size() {
        return wrappedInstance.size();
    }

    public DoubleIterator iterator() {
        return new RingBufferIteratorDouble( this );
    }
}


class ShiftedRingBufferDouble extends ReadOnlyRingBufferDouble {
    private SeqDouble orig;
    private int     shiftedBy;

    public ShiftedRingBufferDouble( SeqDouble orig, int shiftedBy ) {
        this.orig      = orig;
        this.shiftedBy = shiftedBy;
    }

    public double get( long i ) {
        long shiftedIndex = i - shiftedBy;

        if ( shiftedIndex < orig.lhsIndex() ) {
            return 0;
        }

        return orig.get(shiftedIndex);
    }

    public long rhsIndexExc() {
        if ( orig.isEmpty() ) {
            return 0;
        }

        return Math.max(0, orig.rhsIndexExc()+shiftedBy);
    }

    public long size() {
        if ( shiftedBy > 0 ) {
            return orig.size();
        } else {
            return orig.size() + shiftedBy;
        }
    }

    public DoubleIterator iterator() {
        return new RingBufferIteratorDouble(this);
    }
}



class SumPreviousRingBufferDouble extends ReadOnlyRingBufferDouble {
    private SeqDouble orig;
    private int       n;

    public SumPreviousRingBufferDouble( SeqDouble orig, int n ) {
        QA.argIsGTZero( n, "n" );

        this.orig = orig;
        this.n    = n;
    }

    public double get( long i ) {
        double acc      = orig.get(i);
        long   minIndex = Math.max(orig.lhsIndex(), i - n + 1);

        for ( long pos=i-1; pos>=minIndex; pos-- ) {
            acc += orig.get(pos);
        }

        return acc;
    }

    public long rhsIndexExc() {
        return orig.rhsIndexExc();
    }

    public long size() {
        return orig.size();
    }

    public DoubleIterator iterator() {
        return new RingBufferIteratorDouble(this);
    }
}


abstract class ReadOnlyRingBufferDouble implements RingBufferDouble {
    public long append( double v ) {
        throw new UnsupportedOperationException("This ring buffer has been set to read only");
    }

    public void set( long i, double v ) {
        throw new UnsupportedOperationException("This ring buffer has been set to read only");
    }

    public boolean equals( Object other ) {
        if ( !(other instanceof SeqDouble) ) {
            return false;
        }

        SeqDouble otherSeq = Backdoor.cast(other);
        return this.contentsCount() == otherSeq.contentsCount() && this.mkString().equals(otherSeq.mkString());
    }

    public int hashCode() {
        return this.mkString().hashCode();
    }
}

class RingBufferIteratorDouble implements DoubleIterator {
    private final double    toIndexExc;
    private final SeqDouble buf;

    private long    i;
    private boolean hasNext;


    RingBufferIteratorDouble( SeqDouble buf ) {
        this(buf, buf.lhsIndex(), buf.rhsIndexExc());
    }

    RingBufferIteratorDouble( SeqDouble buf, long fromIndex, long toIndexExc ) {
        QA.argIsGTE( fromIndex, buf.lhsIndex(), "fromIndex", "buf.lhsIndex" );
        QA.argIsLTE( toIndexExc, buf.rhsIndexExc(), "toIndexExc", "buf.rhsIndexExc");

        this.buf        = buf;
        this.toIndexExc = toIndexExc;

        this.i       = fromIndex;
        this.hasNext = buf.isValidIndex( fromIndex );
    }

    public boolean hasNext() {
        return hasNext;
    }

    public double next() {
        long next = i;

        i      += 1;
        hasNext = i < toIndexExc;

        return buf.get(next);
    }
}
