package mosaics.collections.mutable.ring;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.FPOption;
import mosaics.lang.ArrayUtils;
import mosaics.lang.Assert;
import mosaics.lang.Backdoor;
import mosaics.lang.MathUtils;
import mosaics.lang.QA;
import mosaics.lang.lockable.CloneMixin;


public class RingBuffer<T> extends CloneMixin<RingBuffer<T>> implements FPIterable<T> {
    private       T[] array;
    private final int moduloBitmask;
    private final int bufferSize;

    private long lhs;
    private long rhsExc;


    public RingBuffer( int suggestedSize ) {
        this( Backdoor.cast(Object.class), suggestedSize );
    }
    public RingBuffer( Class<T> elementType, int suggestedSize ) {
        this( elementType, suggestedSize, 0 );
    }

    public RingBuffer( Class<T> elementType, int size, long initialLHS ) {
        this( ArrayUtils.newArray(elementType, MathUtils.roundUpToClosestPowerOf2(size)), size, initialLHS );
    }

    private RingBuffer( T[] array, int bufferSize, long initialLHS ) {
        this.array         = array;
        this.moduloBitmask = array.length - 1;
        this.bufferSize    = bufferSize;         // Array may be of a different size so that we can benefit from the speed of using a moduloBitmask
        this.lhs           = initialLHS;
        this.rhsExc        = initialLHS;
    }

    public T get( long i ) {
        if ( isEmpty() ) {
            throw new IndexOutOfBoundsException("index " + i + " is out of bounds, as the buffer is currently empty");
        }

        if ( i < lhsIndex() || i >= rhsExc ) {
            throw new IndexOutOfBoundsException("index "+i+" is out of bounds, currently supported indexes are "+lhsIndex()+" to "+ rhsIndexInc()+" (inc)");
        }

        int index = wrapIndex(i);

        return array[index];
    }

    public long append( T v ) {
        array[wrapIndex(rhsExc)] = v;

        long index = rhsExc;

        rhsExc = index + 1;
        lhs    = Math.max( rhsExc-size(), lhs );

        return index;
    }

    public void set( long i, T v ) {
        if ( i < lhsIndex() || i >= rhsExc ) {
            throw new IndexOutOfBoundsException("index "+i+" is out of bounds, currently supported indexes are "+lhsIndex()+" to "+ rhsIndexInc()+" (inc)");
        }

        array[wrapIndex(i)] = v;
    }

    public FPOption<T> popLHS() {
        if ( isEmpty() ) {
            return FP.emptyOption();
        }

        int i = wrapIndex( lhs );
        T v = array[i];

        array[i] = null;

        lhs++;

        return FP.option(v);
    }

    public RingBuffer<T> clone() {
        RingBuffer<T> clone = super.clone();

        clone.array = array.clone();

        return clone;
    }

    public boolean isValidIndex( long i ) {
        return MathUtils.isInRangeExc(lhsIndex(), i, rhsIndexExc());
    }

    public long lhsIndex() {
        return lhs;
    }

    public long rhsIndexInc() {
        return Math.max(0,rhsIndexExc()-1);
    }

    public long rhsIndexExc() {
        return rhsExc;
    }

    public int size() {
        return bufferSize;
    }

    /**
     * Short hand for updating the most recently pushed value.
     */
    public void updateRHS( T v ) {
        set( rhsIndexInc(), v );
    }

    @Override
    public boolean isEmpty() {
        return rhsIndexExc() == lhsIndex();
    }

    @SafeVarargs
    public final void appendAll( T... values ) {
        for ( T v : values ) {
            append( v );
        }
    }

    /**
     * Returns true if values have been dropped from the lhs.
     */
    public boolean hasOverflowed() {
        return lhsIndex() > 0;
    }

    /**
     * How many values can be pushed until the buffer will wrap around and start overwriting its
     * oldest (left most) values?
     */
    public long remaining() {
        return isFull() ? 0 : size() - (rhsIndexExc() - lhsIndex());
    }

    public boolean isFull() {
        return rhsIndexExc() - lhsIndex() >= size();
    }

    /**
     * How many values are currently stored within the ring buffer?  Does not include values that
     * have been overwritten when the buffer over flowed.
     */
    public long contentsCount() {
        return Math.min(rhsIndexExc(), size());
    }


    public FPIterator<T> iterator() {
        return new RingBufferIterator<>(this);
    }

    public T getRHS() {
        return getRHSByOffset(0);
    }

    /**
     * Returns the value stored in the furthest right hand position.  Specifying an offset of zero
     * will return the right most position.  An offset of +1 will return the value one position to
     * the left of that.  Negative offsets are not allowed.
     */
    public T getRHSByOffset( int offset ) {
        Assert.argIsGTEZero( offset, "offset" );

        return get(rhsIndexInc() - offset);
    }


    private int wrapIndex( long i ) {
        // & is apx three times faster than using %; it just limits us to buffer sizes of n^2 (and the mask must be n^2-1)
        return Backdoor.toInt(i + array.length) & moduloBitmask;   // NB always doing + then % is faster than using an if block
    }
}



class RingBufferIterator<T> extends FPIterator<T> {
    private final long          toIndexExc;
    private final RingBuffer<T> buf;

    private long    i;
    private boolean hasNext;


    RingBufferIterator( RingBuffer<T> buf ) {
        this(buf, buf.lhsIndex(), buf.rhsIndexExc());
    }

    RingBufferIterator( RingBuffer<T> buf, long fromIndex, long toIndexExc ) {
        QA.argIsGTE( fromIndex, buf.lhsIndex(), "fromIndex", "buf.lhsIndex" );
        QA.argIsLTE( toIndexExc, buf.rhsIndexExc(), "toIndexExc", "buf.rhsIndexExc");

        this.buf        = buf;
        this.toIndexExc = toIndexExc;

        this.i       = fromIndex;
        this.hasNext = buf.isValidIndex( fromIndex );
    }

    protected boolean _hasNext() {
        return hasNext;
    }

    protected T _next() {
        long next = i;

        i      += 1;
        hasNext = i < toIndexExc;

        return buf.get(next);
    }
}
