package mosaics.collections.mutable;

import java.util.stream.Collector;


public class AverageAccumulator {
    public static Collector<Double,AverageAccumulator,Double> newCollector() {
        return Collector.of(
            AverageAccumulator::new,
            AverageAccumulator::append,
            AverageAccumulator::append,
            AverageAccumulator::getAverage
        );
    }

    private double sum;
    private int    count;

    public AverageAccumulator() {
        this(0, 0);
    }

    private AverageAccumulator( double sum, int count ) {
        this.sum   = sum;
        this.count = count;
    }

    public AverageAccumulator append(double v) {
        this.sum   += v;
        this.count += 1;

        return this;
    }

    public AverageAccumulator append(AverageAccumulator other) {
        this.sum   += other.sum;
        this.count += other.count;

        return this;
    }

    public double getAverage() {
        return count == 0 ? 0.0 : sum/count;
    }
}
