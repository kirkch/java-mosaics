package mosaics.io;

public class CorruptedBytesDetectedException extends RuntimeIOException {
    public CorruptedBytesDetectedException( int expectedHashCode, int actualHashCode ) {
        super( "Expected hashCode ("+expectedHashCode+") does not equal actual hashCode ("+actualHashCode+")" );
    }
}
