package mosaics.io.resources;


import lombok.Getter;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.io.IOUtils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;


public class ResourceInputStream {

    private String             url;
    private InputStream        inputStream;
    private FPOption<MimeType> mimeType;
    @Getter
    private FPOption<Charset>  characterSet;

    public ResourceInputStream( String url, InputStream inputStream ) {
        this( url, inputStream, FP.emptyOption(), FP.emptyOption() );
    }

    public ResourceInputStream( String url, InputStream inputStream, FPOption<Charset> characterSet, FPOption<MimeType> mimeType ) {
        this.url          = url;
        this.inputStream  = inputStream;
        this.mimeType     = mimeType;
        this.characterSet = characterSet;
    }

    public InputStreamReader asReader() {
        Charset cs = characterSet.orElseThrow( () -> new IllegalStateException("Cannot convert '"+url+"' to text because it returned binary data") );

        return new InputStreamReader( inputStream, cs );
    }

    /**
     * Reads in the contents of the input stream.  The stream will always be closed by the end of this
     * call.
     */
    public String asString() {
        try {
            return IOUtils.toString(asReader());
        } finally {
            IOUtils.close( inputStream );
        }
    }

    /**
     * Reads in the contents of the input stream.  The stream will always be closed by the end of this
     * call.
     */
    public <T> FPOption<T> decodeUsing( Class<T> targetModelType, ResourceDecoder decoder, MimeType defaultMimeType ) {
        try {
            return decoder.decode( targetModelType, inputStream, characterSet, mimeType.orElse(defaultMimeType) );
        } finally {
            IOUtils.close( inputStream );
        }
    }

    public InputStream asInputStream() {
        return inputStream;
    }

    public FPOption<MimeType> getMimeType() {
        return mimeType;
    }

    public String getURL() {
        return url;
    }

    public ResourceInputStream withDefaults( FPOption<Charset> defaultCharset, FPOption<MimeType> defaultMimeType ) {
        if ( !characterSet.hasValue() ) {
            this.characterSet = defaultCharset;
        }

        if ( mimeType.isEmpty() ) {
            this.mimeType = defaultMimeType;
        }

        return this;
    }
}
