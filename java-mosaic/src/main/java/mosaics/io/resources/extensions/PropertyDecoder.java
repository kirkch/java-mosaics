package mosaics.io.resources.extensions;

import mosaics.fp.collections.FPOption;
import mosaics.io.resources.MimeType;
import mosaics.io.resources.ResourceDecoder;
import mosaics.lang.Backdoor;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Properties;


public class PropertyDecoder implements ResourceDecoder {

    public static final PropertyDecoder INSTANCE = new PropertyDecoder();

    public <T> FPOption<T> decode( Class<T> targetModelType, InputStream inputStream, FPOption<Charset> characterSet, MimeType mimeType ) {
        Properties properties = new Properties();
        if ( !targetModelType.isInstance(properties) ) {
            throw new IllegalArgumentException("Unsupported targetModelType '"+targetModelType.getName()+"'");
        }

        Reader in = new InputStreamReader(inputStream, characterSet.orElse(Backdoor.UTF8));

        try {
            properties.load(in);

            return Backdoor.cast(FPOption.of(properties));
        } catch ( IOException ex ) {
            throw Backdoor.throwException(ex);
        }
    }

    public MimeType getResourceMimeType() {
        return MimeType.PROPERTYFILE;
    }

}
