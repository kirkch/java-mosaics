package mosaics.io.resources.extensions;

import lombok.Value;
import mosaics.fp.LazyVal;
import mosaics.fp.StreamUtils;
import mosaics.fp.collections.FPIterator;
import mosaics.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipFile;


public class ClassPathScanner {
    public static final ClassPathScanner INSTANCE = new ClassPathScanner();

    private final LazyVal<List<ResourceRef>> cache = new LazyVal<>( this::createResourceRefStream );


    /**
     * Returns a stream of every file within the classpath.
     */
    public Stream<ResourceRef> getResources() {
        return cache.fetch().stream();
    }

    @SuppressWarnings("unchecked")
    private List<ResourceRef> createResourceRefStream() {
        return StreamUtils.concat(
            scanClassPath( "java.class.path"),
            scanClassPath( "sun.boot.class.path"),
            scanJavaHomeModules()
        ).collect( Collectors.toList() );
    }

    private Stream<ResourceRef> scanJavaHomeModules( ) {
        File jmodDir = new File(System.getProperty("java.home"),"jmods");
        if ( !jmodDir.exists() ) {
            return Stream.empty();
        }

        return Arrays.stream( Objects.requireNonNull(jmodDir.listFiles()) )
            .flatMap( this::getResourcesFrom );
    }

    private Stream<ResourceRef> scanClassPath( String classPathKey ) {
        String   classPath         = System.getProperty(classPathKey, ".");
        String[] classPathElements = classPath.split(System.getProperty("path.separator"));

        return Arrays.stream( classPathElements ).flatMap(
            this::getResourcesFrom
        );
    }

    private Stream<ResourceRef> getResourcesFrom( File f ){
        return getResourcesFrom( f.getAbsolutePath() );
    }

    private Stream<ResourceRef> getResourcesFrom( String path ){
        File file = new File(path);

        if ( !file.exists() ) {
            return Stream.empty();
        } else if( file.isDirectory() ) {
            return loadAllFromDirectory( file );
        } else if ( path.endsWith(".jmod") ) {
            return loadAllReferencesFromJMod( file );
        } else {
            return loadAllReferencesFromJar( file );
        }
    }


    private Stream<ResourceRef> loadAllReferencesFromJMod( File jarFile ) {
        if ( !jarFile.exists() ) {
            return Stream.empty();
        }

        try {
            ZipFile zf = new ZipFile( jarFile );

            try {
                List<ResourceRef> resourceRefs = FPIterator
                    .wrap( zf.entries() )
                    .filter( ze -> ze.getName().startsWith( "classes/" ) )
                    .map( ze -> new ResourceRef( ze.isDirectory(), ze.getName().substring( "classes/".length() ) ) )
                    .toList();

                return resourceRefs.stream();
            } finally {
                IOUtils.close( zf );
            }
        } catch ( IOException e ) {
            throw new RuntimeException( e );
        }
    }

    private Stream<ResourceRef> loadAllReferencesFromJar( File jarFile ) {
        if ( !jarFile.exists() ) {
            return Stream.empty();
        }


        try{
            ZipFile zf = new ZipFile(jarFile);

            try {
                return FPIterator
                    .wrap( zf.entries() )
                    .map( ze -> new ResourceRef( ze.isDirectory(), ze.getName() ) )
                    .toList().stream();
            } finally {
                IOUtils.close( zf );
            }
        } catch ( IOException e ) {
            throw new RuntimeException( e );
        }
    }

    private Stream<ResourceRef> loadAllFromDirectory( File dir ) {
        int i = dir.getAbsolutePath().length();

        return StreamUtils.breadthFirst(dir, this::fetchChildFilesOf )
            .filter( f -> f.getAbsolutePath().length() > i )
            .map( f -> new ResourceRef(f.isDirectory(), f.getAbsolutePath().substring(i+1)) );
    }

    private Stream<File> fetchChildFilesOf( File f ) {
        File[] children = f.listFiles();

        if ( children == null ) {
            return Stream.empty();
        }

        return Arrays.stream( children );
    }


    @Value
    public static class ResourceRef {
        private boolean isDirectory;
        private String  path;

        public boolean isFile() {
            return !isDirectory;
        }
    }

}
