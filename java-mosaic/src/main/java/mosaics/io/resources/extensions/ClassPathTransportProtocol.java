package mosaics.io.resources.extensions;

import mosaics.collections.mutable.graphs.oo.GraphNode;
import mosaics.collections.mutable.graphs.oo.OOGraphNode;
import mosaics.fp.FP;
import mosaics.fp.LazyVal;
import mosaics.fp.StreamUtils;
import mosaics.fp.collections.FPOption;
import mosaics.io.UriUtils;
import mosaics.io.resources.MimeType;
import mosaics.io.resources.MimeTypes;
import mosaics.io.resources.ResourceInputStream;
import mosaics.io.resources.TransportProtocolHandler;
import mosaics.io.resources.extensions.ClassPathScanner.ResourceRef;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.Function1WithThrows;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class ClassPathTransportProtocol implements TransportProtocolHandler {

    public static final TransportProtocolHandler INSTANCE = new ClassPathTransportProtocol();


    private LazyVal<GraphNode<String,ResourceRef>> allResources = new LazyVal<>( this::loadAllResourceRefs );

    public String getProtocolName() {
        return "classpath";
    }

    public FPOption<ResourceInputStream> fetchURL( String uri ) {
        String             resourcePath = UriUtils.extractPathFrom( uri );
        InputStream        in           = this.getClass().getResourceAsStream(resourcePath);
        FPOption<MimeType> mimeType     = MimeTypes.fromFile(resourcePath);
        FPOption<Charset>  charset      = FP.emptyOption();

        return FP.option(in).map( inputStream -> new ResourceInputStream(uri, inputStream, charset, mimeType) );
    }


    public List<String> fetchChildFiles( String parentUri ) {
        return fetchChildren( parentUri, ResourceRef::isFile );
    }

    public List<String> fetchChildDirectories( String parentUri ) {
        return fetchChildren( parentUri, ResourceRef::isDirectory );
    }

    public List<String> fetchChildFilesRecursively( String parentUrl ) {
        return StreamUtils.breadthFirst( parentUrl, uri -> fetchChildren(uri, ResourceRef::isDirectory).stream() )
            .flatMap( uri -> fetchChildren(uri, ResourceRef::isFile).stream() )
            .collect( Collectors.toList() );
    }

    private List<String> fetchChildren( String parentUri, Function1WithThrows<ResourceRef,Boolean> filter ) {
        GraphNode<String,ResourceRef>            root         = allResources.fetch();
        String[]                                 path         = UriUtils.extractPathArrayFrom( parentUri );
        FPOption<GraphNode<String, ResourceRef>> resourceNode = root.get( path );

        if ( resourceNode.isEmpty() ) {
            return Collections.emptyList();
        }

        return resourceNode.get()
            .getOutEdges()
            .filter( r -> r.getNode().getNodeContents().map(filter).orElse(false) )
            .map( edge -> parentUri + Backdoor.FILE_SEPARATOR+edge.getKey() )
            .toList();
    }

    private GraphNode<String,ResourceRef> loadAllResourceRefs() {
        GraphNode<String,ResourceRef> root = new OOGraphNode<>();

        ClassPathScanner.INSTANCE.getResources().forEach(
            ref -> {
                String[]                      pathElements = ref.getPath().split( Backdoor.FILE_SEPARATOR );
                GraphNode<String,ResourceRef> leaf         = root.getOrCreate( pathElements );

                leaf.setNodeContents( ref );
            }
        );

        return root;
    }

}
