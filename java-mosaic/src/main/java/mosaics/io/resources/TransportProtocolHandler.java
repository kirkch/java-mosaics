package mosaics.io.resources;

import mosaics.fp.collections.FPOption;

import java.util.List;


public interface TransportProtocolHandler {
    public String getProtocolName();

    public FPOption<ResourceInputStream> fetchURL( String uri );

    public List<String> fetchChildDirectories( String parentUri );
    public List<String> fetchChildFiles( String parentUri );
    public List<String> fetchChildFilesRecursively( String parentUri );
}
