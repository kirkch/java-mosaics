package mosaics.io.resources;

import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.lang.ValidationFailure;
import mosaics.utils.ComparatorUtils;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;


@Value
public class MimeTypeAcceptEntry implements Comparable<MimeTypeAcceptEntry> {

//Accept: text/html
//Accept: image/*
//Accept: text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8

    public static Tryable<MimeTypeAcceptEntry> fromSingleAcceptHeaderElement( String text ) {
        // TODO consider moving PullParser into lang then rewritting this method
        //      Pull parser would simplify this method quite nicely by reducing the number of nested if blocks

        if ( StringUtils.isBlank(text) ) {
            return Try.failed( new ValidationFailure("Blank Accept header: '"+text+"'") );
        }

        int slashSeparatorIndex = text.indexOf( '/' );
        int qIndex              = text.indexOf( ";q=" );


        String primary;
        String secondary;
        double qualityValue;


        if ( slashSeparatorIndex < 0 ) {
            if ( qIndex < 0 ) {
                primary = text;
            } else {
                primary = text.substring( 0, qIndex );
            }

            secondary = "*";
        } else {
            primary   = text.substring( 0, slashSeparatorIndex );

            if (qIndex < 0) {
                secondary = text.substring(slashSeparatorIndex + 1);
            } else {
                secondary = text.substring(slashSeparatorIndex + 1, qIndex);
            }
        }


        if ( qIndex < 0 ) {
            qualityValue = 1.0;
        } else {
            String qStr = text.substring( qIndex + 3 );

            try {
                qualityValue = Double.parseDouble( qStr );
            } catch ( NumberFormatException ex ) {
                return Try.failed( new ValidationFailure("Malformed quality value: '"+qStr+"'") );
            }
        }

        return Try.succeeded( new MimeTypeAcceptEntry(primary.trim(),secondary.trim(),qualityValue) );
    }


    private String primaryType;
    private String secondaryType;
    private double qualityValue;


    public int compareTo( MimeTypeAcceptEntry o ) {
        return ComparatorUtils.compareDesc( this.qualityValue, o.qualityValue );
    }

    public boolean matches( MimeType mimeType ) {
        return isMatchPrimary(mimeType) && isMatchSecondary(mimeType);
    }

    public MimeType toMimeType() {
        return MimeType.lookupMimeType( primaryType, secondaryType );
    }

    public String toString() {
        if ( qualityValue >= 1.0 ) {
            return primaryType + "/" + secondaryType;
        } else {
            return primaryType + "/" + secondaryType + ";q=" + qualityValue;
        }
    }


    private boolean isMatchPrimary( MimeType mimeType ) {
        if ( isWildCardPrimary() ) {
            return true;
        }

        return primaryType.equals( mimeType.getPrimary() );
    }

    private boolean isMatchSecondary( MimeType mimeType ) {
        if ( isWildCardSecondary() ) {
            return true;
        }

        return secondaryType.equals( mimeType.getSecondary() );
    }

    private boolean isWildCardPrimary() {
        return "*".equals(primaryType);
    }

    private boolean isWildCardSecondary() {
        return "*".equals(secondaryType);
    }

}
