package mosaics.io.resources;

import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.io.FileUtils;
import mosaics.lang.Backdoor;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;


@SuppressWarnings("unchecked")
public class MimeTypes {
    private static final Map<String,MimeType> ALL_MIMETYPES_BY_EXT = loadMimeTypes();


    public static FPOption<MimeType> fromExtension( String ext ) {
        return FP.option( ALL_MIMETYPES_BY_EXT.get( ext ) );
    }

    public static FPOption<MimeType> fromFile( String f ) {
        return FileUtils.getExtension(f).flatMap(MimeTypes::fromExtension);
    }

    public static FPOption<MimeType> fromFile( File f ) {
        return fromFile(f.getPath());
    }

    private static Map<String,MimeType> loadMimeTypes() {
        Properties p = new Properties();
        try {
            p.load( MimeTypes.class.getResourceAsStream("mimetypes.properties") );
        } catch ( IOException e ) {
            throw Backdoor.throwException(e);
        }

        return p.entrySet().stream()
            .map( e-> MimeType.createAndRegisterMimeType(e.getValue().toString(), e.getKey().toString()) )
            .distinct()
            .flatMap( m -> m.getFileExtensions().stream().map(ext -> new KV(ext,m)) )
            .collect( Collectors.toMap(KV::getKey,KV::getValue) );
    }

    @Value
    private static class KV {
        private String key;
        private MimeType value;
    }
}
