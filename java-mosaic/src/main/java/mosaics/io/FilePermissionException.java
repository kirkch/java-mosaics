package mosaics.io;

public class FilePermissionException extends RuntimeIOException {
    public FilePermissionException( String msg ) {
        super( msg );
    }
}
