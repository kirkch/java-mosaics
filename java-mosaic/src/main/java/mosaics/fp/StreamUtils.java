package mosaics.fp;

import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Function0;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static mosaics.lang.Backdoor.cast;


public class StreamUtils {

    /**
     * Creates a new Stream that generates the next value one at a time on an as needed basis from
     * a generator function.
     *
     * @param fetchNextFunction  a stateful function that will generate the next value in the series.. returning None will end the Stream.
     */
    public static <T> Stream<T> generate( Function0<FPOption<T>> fetchNextFunction ) {
        return generate( fetchNextFunction.invoke().orNull(), p -> fetchNextFunction.invoke(), v -> v );
    }

    /**
     * Creates a new Stream that generates the next value one at a time on an as needed basis from
     * a generator function.
     *
     * @param firstValueNbl      null will create an empty stream
     * @param fetchNextFunction  given the previous value, generate the next value in the series.. returning None will end the Stream.
     */
    public static <T> Stream<T> generate( T firstValueNbl, Function<T,FPOption<T>> fetchNextFunction ) {
        return generate( firstValueNbl, fetchNextFunction::apply, v -> v );
    }


    /**
     * Creates a new Stream that generates the next value one at a time on an as needed basis from
     * a generator function.  This version of the generate methods uses a stream of S to generate the
     * desired Stream&lt;R&gt;.  S is some form of internal state that describes both the value R and
     * maintains extra state used to generate the next value in the series.
     *
     * For example, to generate the fibonacci sequence the next value is created by summing the previous
     * two values.  To model this with this function, S could be a Tuple2<Long>.  fetchNextFunction would
     * return the next pair of values needed to create the next value and toValueF would select the current
     * value from the Tuple.
     *
     * eg to generate the fibonacci sequence:
     *
     * <code>
     * generate(new Tuple2(1,1), t -&gt; new Tuple2(t.getSecond(), t.getFirst()+t.getSecond(), t -&gt; t.getSecond())
     * </code>
     *
     * @param initialState       null will create an empty stream
     * @param fetchNextFunction  given the previous value, generate the next value in the series.. returning None will end the Stream.
     * @param toValueF           converts the internal stream representation into the target value
     */
    public static <S,R> Stream<R> generate( S initialState, Function<S,FPOption<S>> fetchNextFunction, Function<S,R> toValueF ) {
        Objects.requireNonNull(fetchNextFunction);

        Iterator<S> iterator = new Iterator<S>() {
            private FPOption<S> t = FPOption.of(initialState);

            public boolean hasNext() {
                return t.hasValue();
            }

            public S next() {
                S v = t.get();

                t = fetchNextFunction.apply(v);

                return v;
            }
        };

        return StreamSupport.stream(
            Spliterators.spliteratorUnknownSize( iterator, Spliterator.ORDERED | Spliterator.IMMUTABLE),
            false
        ).map(toValueF);
    }

    /**
     * Tool for iterating over a tree using Streams.  fetchNext returns the children of the current node.
     *
     * @param startingNode      the starting node for the walk, and will be returned first
     * @param fetchNextFunction fetches the next nodes to walk that are connected to the specified node
     */
    public static <N> Stream<N> breadthFirst( N startingNode, Function<N,Stream<N>> fetchNextFunction ) {
        Objects.requireNonNull(fetchNextFunction);

        LinkedList<N> queue = new LinkedList<>();
        queue.push(startingNode);

        Iterator<N> iterator = new Iterator<N>() {
            public boolean hasNext() {
                return !queue.isEmpty();
            }

            public N next() {
                N v = queue.removeFirst();

                Stream<N> next = fetchNextFunction.apply(v);
                next.forEach( queue::addLast );

                return v;
            }
        };

        return StreamSupport.stream(
            Spliterators.spliteratorUnknownSize( iterator, Spliterator.ORDERED | Spliterator.IMMUTABLE),
            false
        );
    }

    @SafeVarargs
    public static <T> Stream<T> concat( Stream<T> first, Stream<T>...others ) {
        if ( others == null || others.length == 0 ) {
            return first;
        }

        Stream<T> aggregate = first;
        for ( Stream next : others ) {
            aggregate = Stream.concat(aggregate, cast(next));
        }

        return aggregate;
    }

}
