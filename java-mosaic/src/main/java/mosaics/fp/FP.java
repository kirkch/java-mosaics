package mosaics.fp;

import mosaics.collections.immutable.trees.AVLTreeMap;
import mosaics.collections.immutable.trees.FPTreeMap;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.FPSet;
import mosaics.fp.collections.IntIterable;
import mosaics.fp.collections.RRBVector;
import mosaics.fp.collections.Tuple2;
import mosaics.fp.collections.maps.FPMap;
import mosaics.fp.collections.maps.FPMultiMap;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.VoidFunction1;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;


public class FP {

    public static <T> FPOption<T> option( T v ) {
        return FPOption.of( v );
    }

    public static FPOption<String> optionalString( String v ) {
        return FPOption.ofBlankableString( v );
    }

    public static <T> FPOption<T> emptyOption() {
        return FPOption.none();
    }

    public static <K,V> FPMap<K,V> emptyMap() {
        return FPMap.emptyMap();
    }

    public static <K,V> FPMap<K,V> wrapMap( Map<K,V> map ) {
        return FPMap.wrapMap( map );
    }

    public static <K,V> FPTreeMap<K,V> emptyTree() {
        return AVLTreeMap.empty();
    }

    public static <V> FPIterable<V> emptyIterable() {
        return FPIterable.empty();
    }

    public static <V> FPList<V> toList( List<V> list ) {
        return FPList.wrapList( list );
    }

    @SafeVarargs
    public static <V> FPList<V> toList( V...kvPairs ) {
        return FPList.wrapArray( kvPairs );
    }

    public static <V> FPList<V> emptyList() {
        return FPList.newArrayList();
    }

    public static <K,V> FPMap<K,V> toMap( K k1, V v1 ) {
        return FPMap.wrapMap( k1, v1 );
    }

    public static <K,V> FPMap<K,V> toMap( K k1, V v1, K k2, V v2 ) {
        return FPMap.wrapMap( k1, v1, k2, v2 );
    }

    public static <K,V> FPMap<K,V> toMap( K k1, V v1, K k2, V v2, K k3, V v3 ) {
        return FPMap.wrapMap( k1, v1, k2, v2, k3, v3 );
    }

    public static <K,V> FPMap<K,V> toMap( K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4 ) {
        return FPMap.wrapMap( k1, v1, k2, v2, k3, v3, k4, v4 );
    }

    public static <K,V> FPMap<K,V> toMap( K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5 ) {
        return FPMap.wrapMap( k1, v1, k2, v2, k3, v3, k4, v4, k5, v5 );
    }

    public static <K,V> FPMap<K,V> toMap( K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6 ) {
        return FPMap.wrapMap( k1, v1, k2, v2, k3, v3, k4, v4, k5, v5, k6, v6 );
    }

    public static <K,V> FPMap<K,V> toMap( Object...kvPairs ) {
        return FPMap.wrapMap( kvPairs );
    }

    public static <K,V> FPMultiMap<K,V> emptyMultiMap() {
        return FPMultiMap.emptyMap();
    }

    public static <T> RRBVector<T> emptyVector() {
        return RRBVector.emptyVector();
    }

    public static <T> FPSet<T> toSet( Set<T> set ) {
        return FPSet.wrapSet( set );
    }

    @SafeVarargs
    public static <T> FPSet<T> toSet( T...array ) {
        return FPSet.wrapArray( array );
    }

    public static <T> FPSet<T> emptySet() {
        return FPSet.newSet();
    }

    public static <T> T getOrElse( T a, T b ) {
        if ( a == null ) {
            return b;
        } else {
            return a;
        }
    }

    public static <T> void forEachIgnoresReturnValues( T[] array, Function1<T,?> func ) {
        for ( T v : array ) {
            func.invoke(v);
        }
    }

    public static <T> void forEach( T[] array, VoidFunction1<T> func ) {
        for ( T v : array ) {
            func.invoke(v);
        }
    }

    public static <T> FPIterable<T> wrapSingleton( T v ) {
        return FPIterable.wrapSingleton(v);
    }

    public static <T> FPIterable<T> wrap( Iterable<T>  iterable ) {
        return FPIterable.wrap( iterable );
    }

    public static <T> FPIterable<T> wrapArray( T[] array ) {
        return FPIterable.wrapArray( array );
    }

    public static FPIterable<Integer> wrapArray( int[] array ) {
        return FPIterable.wrapArray( array );
    }

    public static FPIterable<Long> wrapArray( long[] array ) {
        return FPIterable.wrapArray( array );
    }

    public static <T> FPIterator<T> wrap( Iterator<T> iterator ) {
        return FPIterator.wrap( iterator );
    }

    public static <T> FPIterable<T> wrapStream( Stream<T> stream ) {
        return FPIterable.wrapStream( stream );
    }

    @SafeVarargs
    public static <T> FPIterable<T> wrapAll( T...values ) {
        return FPIterable.wrapAll( values );
    }

    @SafeVarargs
    public static <T> RRBVector<T> toVector( T...values ) {
        return RRBVector.toVector( values );
    }

    public static <T> RRBVector<T> toVector( Iterable<T> values ) {
        return RRBVector.toVector( values );
    }

    @SafeVarargs
    public static <T> ConsList<T> toConsList( T...values ) {
        return ConsList.consList( values );
    }

    public static <T> FPIterable<T> repeat( int numTimes, T v ) {
        return FPIterable.repeat( numTimes, v );
    }

    public static <T> FPIterable<T> repeat( int numTimes, Function0<T> v ) {
        return FPIterable.generate( numTimes, v );
    }

    public static <T> FPIterator<T> emptyIterator() {
        return FPIterator.emptyIterator();
    }

    public static <T> ConsList<T> nil() {
        return ConsList.nil();
    }

    public static <T> ConsList<T> consListSingleton( T v ) {
        return ConsList.singleton(v);
    }

    @SafeVarargs
    public static <T> ConsList<T> consList( T...elements ) {
        return ConsList.consList(elements);
    }

    public static <T> FPIterator<T> createIteratorFor( Object x ) {
        return FPIterator.createIteratorFor(x);
    }


    public static <A,B> FPIterable<Tuple2<A,B>> zipArrays( A[] a, B[] b ) {
        return FP.wrapArray(a).zip( FP.wrapArray(b) );
    }

    public static <B> FPIterable<Tuple2<Integer,B>> zipArrays( int[] a, B[] b ) {
        return IntIterable.wrap(a).zip( FP.wrapArray(b) );
    }

    public static FPIterable<Tuple2<Integer,Integer>> zipArrays( int[] a, int[] b ) {
        return IntIterable.wrap(a).zip( IntIterable.wrap(b) );
    }

    public static <A,B> FPIterable<Tuple2<A,B>> zip( Iterable<A> lhs, Iterable<B> rhs ) {
        return wrap(lhs).zip( wrap(rhs) );
    }

    public static FPOption<?> castOrConvertToOption( Object valueOfUnknownType ) {
        if ( valueOfUnknownType instanceof Optional ) {
            return FPOption.ofOptional( Backdoor.cast(valueOfUnknownType) );
        } else if ( valueOfUnknownType instanceof FPOption ) {
            return (FPOption) valueOfUnknownType;
        } else {
            return FPOption.of( valueOfUnknownType );
        }
    }

    /**
     * Creates an option containing the result of invoking the factory iff predicate is true.
     * Else returns NONE.
     */
    public static <T> FPOption<T> option( boolean predicate, Function0<T> factory ) {
        if ( predicate ) {
            return FP.option( factory.invoke() );
        } else {
            return FP.emptyOption();
        }
    }

    public static FPIterable<Integer> range( int fromInc, int toExc ) {
        return FPIterable.generate(
            fromInc >= toExc ? null : fromInc,
            v -> {
                if ( v < toExc-1 ) {
                    return FP.option( v+1 );
                } else {
                    return FP.emptyOption();
                }
            }
        );
    }
}
