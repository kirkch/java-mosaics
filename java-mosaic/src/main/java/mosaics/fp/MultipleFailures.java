package mosaics.fp;

import lombok.EqualsAndHashCode;

import java.util.Arrays;
import java.util.List;


@EqualsAndHashCode
public class MultipleFailures implements Failure {

    public static Failure aggregateFailures( List<Failure> failures ) {
        switch ( failures.size() ) {
            case 0:
                throw new IllegalArgumentException( "No failures found" );
            case 1:
                return failures.get( 0 );
            default:
                return new MultipleFailures( failures );
        }
    }



    private List<Failure> failures;

    public MultipleFailures( Failure...failures ) {
        this( Arrays.asList(failures) );
    }

    public MultipleFailures( List<Failure> failures ) {
        this.failures = failures;
    }

    public Throwable toException() {
        return new MultipleExceptions( FP.wrap(failures).map(Failure::toException).toArray(Throwable.class) );
    }

    public String toString() {
        return "MultipleFailures("+FP.wrap(failures).map(Failure::toString).mkString(", ")+")";
    }

}
