package mosaics.fp.collections;

import mosaics.fp.FP;
import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.Backdoor;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;


/**
 *
 */
public interface ConsList<T> extends FPIterable<T> {

    public static final ConsList NIL = new ConsList() {
        public boolean isEOL() {
            return true;
        }

        public Object getHead() {
            throw new IllegalStateException( "NIL" );
        }

        public ConsList getTail() {
            throw new IllegalStateException( "NIL" );
        }

        @SuppressWarnings("unchecked")
        public ConsList append( Object v ) {
            return new ConsElement(v, this);
        }

        public int hashCode() {
            return 17;
        }

        public boolean equals( Object o ) {
            if ( o == this ) {
                return true;
            } else if ( !(o instanceof ConsList) ) {
                return false;
            }

            ConsList other = (ConsList) o;
            return other.isEOL();
        }

        public String toString() {
            return "Nil";
        }
    };

    @SuppressWarnings("unchecked")
    public static <T> ConsList<T> nil() {
        return NIL;
    }

    @SuppressWarnings("unchecked")
    public static <T> ConsList<T> singleton( T v ) {
        return Backdoor.cast(NIL.append(v));
    }

    @SafeVarargs
    public static <T> ConsList<T> consList( T...args ) {
        return FP.wrapAll(args).fold( nil(), ConsList::append );
    }

    public static <T> ConsList<T> consList( Iterable<T> it ) {
        return FP.wrap(it).fold( nil(), ConsList::append );
    }

    public static ConsList<Integer> consList( int...args ) {
        return FP.wrapArray(args).fold( nil(), ConsList::append );
    }

    public static ConsList<Long> consList( long...args ) {
        return FP.wrapArray(args).fold( nil(), ConsList::append );
    }

    public default ConsList<T> tail() {
        return getTail();
    }

    public boolean isEOL();
    public T getHead();
    public ConsList<T> getTail();
    public ConsList<T> append( T v );

    public default ConsList<T> remove( T valueToRemove ) {
        List<T> filteredList = new LinkedList<>();

        int filteredCount = 0;
        for ( T v : this ) {
            if ( !Objects.equals(v,valueToRemove) ) {
                filteredList.add( v );
            } else {
                filteredCount += 1;
            }
        }

        if ( filteredCount == 0 ) {
            return this;
        }

        ConsList<T> consList = ConsList.nil();
        for ( int i=filteredList.size()-1; i >= 0; i-- ) {
            consList = consList.append( filteredList.get(i) );
        }

        return consList;
    }


    public default FPIterator<T> iterator() {
        return new FPIterator<T>() {
            private ConsList<T> stream = ConsList.this;

            protected boolean _hasNext() {
                return !stream.isEOL();
            }

            protected T _next() {
                T head = this.stream.getHead();

                this.stream = stream.getTail();

                return head;
            }
        };
    }


    /**
     * Slide forward along the list's elements until the specified predicate returns true.
     */
    public default ConsList<T> skipForwardUntil( BooleanFunction1<T> predicate ) {
        ConsList<T> current = this;

        while ( !current.isEOL() ) {
            if ( predicate.invoke(current.getHead()) ) {
                return current;
            }

            current = current.getTail();
        }

        return current;
    }

    public default ConsList<T> reverse() {
        return iterator().fold( nil(), ConsList::append );
    }

    static ConsList<Integer> zeroToInc( int maxInc ) {
        ConsList<Integer> list = nil();

        for ( int i=0; i<=maxInc; i++ ) {
            list.append( i );
        }

        return list;
    }
}

class ConsElement<T> implements ConsList<T> {

    private T           head;
    private ConsList<T> tail;

    public ConsElement( T head, ConsList<T> tail ) {
        this.head = head;
        this.tail = tail;
    }

    public boolean isEOL() {
        return false;
    }

    public T getHead() {
        return head;
    }

    public ConsList<T> getTail() {
        return tail;
    }

    public ConsList<T> append( T v ) {
        return new ConsElement<>( v, this );
    }

    public int hashCode() {
        return tail.iterator().fold( head.hashCode(), (soFar,next) -> soFar*31 + next.hashCode() );
    }

    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append( head );

        tail.iterator().fold(buf, (b,next) -> {
            b.append("::");
            b.append(next);

            return b;
        });

        buf.append( "::Nil" );

        return buf.toString();
    }

    public boolean equals( Object o ) {
        if ( o == this ) {
            return true;
        } else if ( !(o instanceof ConsList) ) {
            return false;
        }

        ConsList<?> other = (ConsList) o;

        return this.zip(other, Objects::equals).isEvery(v -> v);
    }

}
