package mosaics.fp.collections;

import mosaics.collections.mutable.AverageAccumulator;
import mosaics.lang.functions.DoubleFunction0;

import java.util.List;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;


public interface DoubleIterable {

    public static DoubleIterable wrap( double[] array ) {
        return new DoubleIterable() {
            @Override
            public DoubleIterator iterator() {
                return DoubleIterator.wrap( array );
            }
        };
    }

    /**
     * Generate an infinite number series by wrapping a function that maps an index (0..MaxDouble)
     * to another double.
     */
    public static DoubleIterable wrap( DoubleUnaryOperator supplier ) {
        return new DoubleIterable() {
            public DoubleIterator iterator() {
                return new DoubleIterator() {
                    private double i = 0;

                    public boolean hasNext() {
                        return i < Double.MAX_VALUE;
                    }

                    public double next() {
                        double nextIndex = i;

                        i += 1;

                        return supplier.applyAsDouble( nextIndex );
                    }
                };
            }
        };
    }

    /**
     * Create an iterator that combines the zipped values from two separate iterators.
     */
    static DoubleIterator combine( DoubleIterator lhs, DoubleIterator rhs, DoubleBinaryOperator aggregator ) {
        return new DoubleIterator() {
            public boolean hasNext() {
                return lhs.hasNext() && rhs.hasNext();
            }

            public double next() {
                double a = lhs.next();
                double b = rhs.next();

                return aggregator.applyAsDouble(a, b);
            }
        };
    }


    public DoubleIterator iterator();


    public default double sum() {
        return fold(( a, b ) -> a + b);
    }

    /**
     * Combine all of the values using the supplied aggregation function.
     *
     * @param aggregator (soFar,newValue)
     * @return
     */
    public default double fold( DoubleBinaryOperator aggregator ) {
        return iterator().fold(aggregator);
    }

    public default DoubleIterable map( DoubleUnaryOperator mappingFunction ) {
        return new DoubleIterable() {
            public DoubleIterator iterator() {
                DoubleIterator it = DoubleIterable.this.iterator();

                return new DoubleIterator() {
                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    public double next() {
                        double v = it.next();

                        return mappingFunction.applyAsDouble( v );
                    }
                };
            }
        };
    }

    /**
     * For each double v in the series, returns Math.max(v, maxValue).
     */
    public default DoubleIterable max( double maxValue ) {
        return this.map( v -> Math.max(v, maxValue) );
    }

    /**
     * For each double v in the series, returns Math.min(v, minValue).
     */
    public default DoubleIterable min( double minValue ) {
        return this.map(v -> Math.min(v, minValue));
    }

    /**
     * For each double v in the series, returns Math.max(v, maxValue).
     */
    public default DoubleIterable max( DoubleFunction0 maxValueFetcher ) {
        return max(maxValueFetcher.invoke());
    }

    /**
     * For each double v in the series, returns Math.min(v, minValue).
     */
    public default DoubleIterable min( DoubleFunction0 minValueFetcher ) {
        return min(minValueFetcher.invoke());
    }

    public default double average() {
        AverageAccumulator acc = new AverageAccumulator();

        this.iterator().forEach( acc::append );

        return acc.getAverage();
    }

    public default List<Double> toList() {
        return iterator().toList();
    }
}
