package mosaics.fp.collections;

import mosaics.lang.QA;
import mosaics.lang.functions.VoidFunctionL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.LongBinaryOperator;
import java.util.function.LongUnaryOperator;


public interface LongIterator {
    public static LongIterator wrap( long[] array ) {
        return new LongIterator() {
            private int i = 0;

            public boolean hasNext() {
                return i < array.length;
            }

            public long next() {
                return array[i++];
            }
        };
    }



    public boolean hasNext();
    public long next();

    public default LongIterator map( LongUnaryOperator op ) {
        LongIterator original = this;

        return new LongIterator() {
            public boolean hasNext() {
                return original.hasNext();
            }

            public long next() {
                return op.applyAsLong(original.next());
            }
        };
    }

    public default DoubleIterator toDouble() {
        LongIterator original = this;

        return new DoubleIterator() {
            public boolean hasNext() {
                return original.hasNext();
            }

            public double next() {
                return (double) original.next();
            }
        };
    }

    public default void forEach( VoidFunctionL op ) {
        while ( hasNext() ) {
            long v = next();

            op.invoke( v );
        }
    }

    /**
     * Combines each number in the iterator using the specified aggregation function.  The folding
     * inserts a zero for the first value, thus allowing us to support empty lists.  Empty lists
     * always return 0.
     */
    public default long fold( LongBinaryOperator aggregator ) {
        return fold( aggregator, 0 );
    }

    /**
     * Combines each number in the iterator using the specified aggregation function.  The folding
     * uses initialValue for the first value, thus allowing us to support empty lists.  Empty lists
     * always return initialValue.
     */
    public default long fold( LongBinaryOperator aggregator, long initialValue ) {
        long sum = initialValue;
        while ( hasNext() ) {
            sum = aggregator.applyAsLong( sum, next() );
        }

        return sum;
    }

    /**
     * Similar to fold but does not take an initial value.  Insteat it uses the first value from the
     * iterator and errors if the iterator is empty.
     */
    public default long reduce( LongBinaryOperator aggregator ) {
        return fold( aggregator, next() );
    }

    public default LongIterator drop( int n ) {
        while ( n > 0 && hasNext() ) {
            next();
            n--;
        }

        return this;
    }

    public default LongIterator take( int n ) {
        QA.argIsGTEZero( n, "n" );

        LongIterator orig = this;

        return new LongIterator() {
            private int countDown = n;

            public boolean hasNext() {
                return countDown > 0 && orig.hasNext();
            }

            public long next() {
                long v = orig.next();

                countDown -= 1;

                return v;
            }
        };
    }

    public default long min() {
        return hasNext() ? reduce( Math::min ) : 0;
    }

    public default long max() {
        return hasNext() ? reduce( Math::max ) : 0;
    }

    public default List<Long> toList() {
        List<Long> list = new ArrayList<>();

        forEach( list::add );

        return list;
    }

    public default String mkString() {
        StringBuilder buf = new StringBuilder();

        boolean appendComma = false;
        while ( hasNext() ) {
            if ( appendComma ) {
                buf.append( ", " );
            } else {
                appendComma = true;
            }

            buf.append( next() );
        }

        return buf.toString();
    }

    public default FPIterator<long[]> slice( int windowSize ) {
        LongIterator it = this;

        return new FPIterator<long[]>() {
            private long[] next = calculateNext();


            protected boolean _hasNext() {
                return next != null;
            }

            protected long[] _next() {
                long[] next = this.next;

                this.next = calculateNext();
                return next;
            }

            private long[] calculateNext() {
                long[] r = new long[windowSize];
                int    i = 0;

                while ( i < windowSize && it.hasNext() ) {
                    r[i++] = it.next();
                }

                if ( i != windowSize ) {
                    r = Arrays.copyOf(r, i);
                }

                return r;
            }
        };
    }
}
