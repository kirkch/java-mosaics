package mosaics.fp.collections.maps;

import lombok.AllArgsConstructor;
import lombok.Getter;
import mosaics.fp.LazyVal;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.Backdoor;

import java.util.Map;
import java.util.Objects;

import static mosaics.lang.Backdoor.cast;


@AllArgsConstructor
class FPMapRRBVector<K,V> extends BaseFPMap<K,V> {
    private static final Bucket<?,?> EMPTY_BUCKET = new Bucket<>();

    private final Buckets<K,V> buckets;


    public FPMapRRBVector() {
        this( new Buckets<>(31, new RRBVector<>()) );
    }


    public FPOption<V> get( K targetKey ) {
        return buckets.get( targetKey );
    }

    public FPMap<K, V> remove( K targetKey ) {
        Buckets<K,V> updatedBuckets = buckets.remove( targetKey );

        return updatedBuckets == buckets ? this : new FPMapRRBVector<>(updatedBuckets);
    }

    public FPMap<K, V> put( K key, V newValue ) {
        Buckets<K, V> resizedBuckets = buckets;//conditionallyResize();
        Buckets<K,V>  updatedBuckets = resizedBuckets.put( key, newValue );

        return updatedBuckets == buckets ? this : new FPMapRRBVector<>(updatedBuckets);
    }

    public int size() {
        return buckets.size();
    }

    public FPIterable<Map.Entry<K,V>> getEntries() {
        return cast(buckets.getEntries());
    }

    private Buckets<K, V> conditionallyResize() {
        Buckets<K,V> buckets = this.buckets;

        if ( size() > buckets.numBuckets*0.7 )  {
            buckets = buckets.resizeBucketsArray( buckets.numBuckets*2 );
        }

        return buckets;
    }


    @AllArgsConstructor
    private static class Buckets<K,V> {
        private final int                    numBuckets;
        private final RRBVector<Bucket<K,V>> buckets;
        private final LazyVal<Integer>       lazySizeCalc;


        public Buckets( int numBuckets ) {
            this( numBuckets, new RRBVector<>() );
        }

        public Buckets( int numBuckets, RRBVector<Bucket<K,V>> buckets ) {
            this.numBuckets   = numBuckets;
            this.buckets      = buckets;
            this.lazySizeCalc = new LazyVal<>( () -> this.buckets.fold(0, (soFar,bucket) -> soFar+bucket.getSize()) );
        }

        public int size() {
            return lazySizeCalc.fetch();
        }

        public FPOption<V> get( K targetKey ) {
            int targetKeyHashCode = targetKey.hashCode();
            int index             = toIndex( targetKeyHashCode );

            return buckets.getOrElse( index, cast(EMPTY_BUCKET) )
                          .getValue( targetKey, targetKeyHashCode );
        }

//        private int hashedIndexOf( FPMapEntry<K,V> entry ) {
//            return hashedIndexOf( entry.getKeyHashCode() );
//        }

        private int toIndex( int keyHashCode ) {
            return Math.abs(keyHashCode) % numBuckets;
        }

        public FPIterable<FPMapEntry<K, V>> getEntries() {
            return buckets.flatMap( Bucket::getCollisionList );
        }

        public Buckets<K, V> remove( K targetKey ) {
            int keyHashCode = targetKey.hashCode();
            int index       = toIndex( keyHashCode );

            return buckets.get( index )
                .map( bucket -> {
                    Bucket<K,V> updatedBucket = bucket.remove(targetKey, keyHashCode);
                    if ( bucket == updatedBucket ) {
                        return this;
                    }

                    RRBVector<Bucket<K,V>> updatedBuckets = buckets.set(index, updatedBucket);
                    return new Buckets<>(numBuckets,updatedBuckets);
                } )
                .orElse( this );
        }

        public Buckets<K, V> put( K key, V newValue ) {
            int keyHashCode = key.hashCode();
            int index       = toIndex( keyHashCode );

            return buckets.get(index)
                .map( bucket -> {
                    Bucket<K,V> updatedBucket = bucket.put( keyHashCode, key, newValue );

                    return updatedBucket == bucket ? this : new Buckets<>( numBuckets, buckets.set(index,updatedBucket) );
                } )
                .orElse(() -> new Buckets<>(numBuckets,buckets.set(index,new Bucket<K,V>().put(keyHashCode,key,newValue))) );
        }

        public Buckets<K,V> resizeBucketsArray( int newBucketCount ) {
            return this.getEntries().fold(
                new Buckets<>(newBucketCount),
                (soFar,nextEntry) -> soFar.put(nextEntry.getKey(), nextEntry.getValue())
            );
        }
    }

    private static class Bucket<K,V> {
        @Getter private final ConsList<FPMapEntry<K,V>> collisionList;

        private final LazyVal<Integer> lazySizeCalc;

        public Bucket( ConsList<FPMapEntry<K,V>> cl ) {
            this.collisionList = cl;
            this.lazySizeCalc  = new LazyVal<>( () -> Backdoor.toInt(collisionList.count()) );
        }

        public Bucket() {
            this( ConsList.nil() );
        }

        public int getSize() {
            return lazySizeCalc.fetch();
        }

        public FPOption<FPMapEntry<K,V>> getEntry( K key, int keyHashCode ) {
            return collisionList.first(
                entry -> entry.matchesTargetKey(key, keyHashCode)
            );
        }

        public FPOption<V> getValue( K key, int keyHashCode ) {
            return getEntry( key, keyHashCode ).map( FPMapEntry::getValue );
        }

        public Bucket<K, V> remove( K targetKey, int keyHashCode ) {
            return getEntry(targetKey,keyHashCode)
                       .map( matchingEntry -> new Bucket<>(collisionList.remove(matchingEntry)) )
                       .orElse( this );
        }

        public Bucket<K, V> put( int keyHashCode, K key, V newValue ) {
            return getEntry( key, keyHashCode )
                .map( existingEntry -> {
                    if ( Objects.equals(existingEntry.getValue(),newValue) ) {
                        return this;
                    } else {
                        ConsList<FPMapEntry<K, V>> updatedCollisionList = collisionList
                            .remove( existingEntry )
                            .append( new FPMapEntry<>(keyHashCode, key, newValue) );

                        return new Bucket<>(updatedCollisionList);
                    }
                }).orElse(
                    () -> new Bucket<>(collisionList.append(new FPMapEntry<>( keyHashCode, key, newValue )))
                );
        }
    }
}
