package mosaics.fp.collections.maps;

import java.util.Objects;


abstract class BaseFPMap<K,V> implements FPMap<K,V> {
    public boolean equals( Object o ) {
        if ( !(o instanceof FPMap) ) {
            return false;
        }

        FPMap other = (FPMap) o;
        if ( this.size() != other.size() ) {
            return false;
        }

        return Objects.equals(this.getEntries().toSet(),other.getEntries().toSet());
    }

    public int hashCode() {
        return size();
    }

    public String toString() {
        return "{" + getEntries()
            .map(entry -> entry.getKey()+"="+entry.getValue())
            .mkString(", ") + "}";
    }
}
