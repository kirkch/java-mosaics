package mosaics.fp.collections.maps;

import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.Tuple2;
import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function1WithThrows;
import mosaics.lang.functions.Function2WithThrows;
import mosaics.lang.functions.VoidFunction2;
import mosaics.lang.Backdoor;
import mosaics.lang.NotFoundException;
import mosaics.utils.MapUtils;

import java.util.Comparator;
import java.util.Map;
import java.util.Objects;


public interface FPMutableMap<K,V> {
    static final Comparator<Tuple2> KV_COMPARATOR = Comparator.comparing( a -> a.getFirst().toString() );


    public static <K,V> FPMutableMap<K,V> empty() {
        return asMap();
    }

    public static <K,V> FPMutableMap<K,V> newMap() {
        return asMap();
    }

    public static <K,V> FPMutableMap<K,V> asMap( Object...kvPairs ) {
        Map<K,V> map = MapUtils.asMap(kvPairs);

        return wrap( map );
    }

    public static <K,V> FPMutableMap<K,V> wrap( Map<K,V> source ) {
        return new FPMutableMap<K,V>() {
            public FPOption<V> get( K key ) {
                return FPOption.of( source.get(key) );
            }

            public void put( K key, V v ) {
                source.put( key, v );
            }

            public FPIterable<Tuple2<K, V>> getEntries() {
                return FPIterable.wrap( source.entrySet() ).map( e -> new Tuple2<>(e.getKey(),e.getValue()));
            }

            public FPIterable<V> getValues() {
                return FPIterable.wrap(source.values());
            }

            public int size() {
                return source.size();
            }

            public FPMutableMap<K,V> materialize() {
                return this;
            }

            public String toString() {
                return getEntries().sort( Backdoor.cast(KV_COMPARATOR)).map( kv -> "("+kv.getFirst()+"->"+kv.getSecond()+")" ).toString();
            }
        };
    }


    public FPOption<V> get( K key );
    public void put( K key, V v );
    public FPIterable<Tuple2<K,V>> getEntries();
    public FPIterable<V> getValues();
    public int size();

    /**
     * Ensures that the map has been fully created.  When chaining operations across IterableX and
     * MapX the calls are often lazy and thus do not cause any changes to the map straight away.  A
     * call to this method will cause the chain of operations to be immediately executed and the final
     * 'materialized' map will be created and returned.
     */
    public FPMutableMap<K,V> materialize();


    public default boolean isEmpty() {
        return size() == 0;
    }

    public default boolean containsKey( K key ) {
        return get(key).hasValue();
    }

    public default V getMandatory( K key ) {
        return get(key).get();
    }

    /**
     * Specialised version of put that will error if key already has a value associated with it.
     */
    public default void putOnce( K key, V newValue ) {
        if ( this.containsKey(key) ) {
            V existingValue = getMandatory(key);

            throw new IllegalArgumentException("Failed to register '"+newValue+"' because '"+existingValue+"' is already registered under '"+key+"'");
        }

        this.put( key, newValue );
    }

    public default V get( K key, V defaultValue ) {
        return get(key).orElse( defaultValue );
    }

    /**
     * Return the value associated with the key or throws a NotFoundException with the specified
     * message
     *
     * @param errorMessage  %s within the message will be substituted for the key name
     */
    public default V getOrThrow( K key, String errorMessage ) {
        return getOrThrow(key, errorMessage, Objects.toString(key));
    }

    public default V getOrThrow( K key, String errorMessage, String...args ) {
        return get(key).orElseThrow(() -> new NotFoundException(errorMessage, args));
    }

    public default V computeIfAbsent( K key, Function1<K, V> factoryMethod ) {
        synchronized (this) {
            FPOption<V> v = get( key );

            if ( v.hasValue() ) {
                return v.get();
            }

            V newV = factoryMethod.invoke( key );
            put( key, newV );

            return newV;
        }
    }

    public default FPMutableMap<K,V> filterValues( BooleanFunction1<V> predicate ) {
        return getEntries().filter(kv -> predicate.invoke(kv.getSecond())).toMap();
    }

    public default <V2> FPMutableMap<K,V2> map( Function2WithThrows<K,V,V2> mappingFunction ) {
        FPMutableMap<K,V> source = this;

        return new FPMutableMap<K,V2>() {
            public FPOption<V2> get( K key ) {
                return source.get(key).map( v -> mappingFunction.invoke(key,v) );
            }

            public void put( K key, V2 v2 ) {
                throw new UnsupportedOperationException();
            }

            public FPIterable<Tuple2<K, V2>> getEntries() {
                return source.getEntries().map( kv -> new Tuple2<>(kv.getFirst(), mappingFunction.invoke(kv.getFirst(), kv.getSecond())) );
            }

            public FPIterable<V2> getValues() {
                return getEntries().map(kv -> kv.getSecond());
            }

            public int size() {
                return source.size();
            }

            public FPMutableMap<K,V2> materialize() {
                return getEntries().toMap();
            }

            public String toString() {
                return getEntries().sort(Backdoor.cast(KV_COMPARATOR)).map( kv -> "("+kv.getFirst()+"->"+kv.getSecond()+")" ).toString();
            }
        };
    }

    public default <V2> FPMutableMap<K,V2> mapValues( Function1WithThrows<V,V2> mappingFunction ) {
        FPMutableMap<K,V> source = this;

        return new FPMutableMap<K,V2>() {
            public FPOption<V2> get( K key ) {
                return source.get(key).map( mappingFunction );
            }

            public void put( K key, V2 v2 ) {
                throw new UnsupportedOperationException();
            }

            public FPIterable<Tuple2<K, V2>> getEntries() {
                return source.getEntries().map( kv -> new Tuple2<>(kv.getFirst(), mappingFunction.invoke(kv.getSecond())) );
            }

            public FPIterable<V2> getValues() {
                return source.getValues().map( mappingFunction );
            }

            public int size() {
                return source.size();
            }

            public FPMutableMap<K,V2> materialize() {
                return getEntries().toMap();
            }

            public String toString() {
                return getEntries().sort(Backdoor.cast(KV_COMPARATOR)).map( kv -> "("+kv.getFirst()+"->"+kv.getSecond()+")" ).toString();
            }
        };
    }

    public default <V2> FPMutableMap<K,V2> flatMapValues( Function1WithThrows<V,FPIterable<V2>> mappingFunction ) {
        FPMutableMap<K,V> source = this;

        return new FPMutableMap<K,V2>() {
            public FPOption<V2> get( K key ) {
                return source.get(key).flatMap( Backdoor.cast(mappingFunction) );
            }

            public void put( K key, V2 v2 ) {
                throw new UnsupportedOperationException();
            }

            public FPIterable<Tuple2<K, V2>> getEntries() {
                return source.getEntries().flatMap( kv -> mappingFunction.invoke(kv.getSecond()).map(v -> new Tuple2<>(kv.getFirst(), v)) );
            }

            public FPIterable<V2> getValues() {
                return source.getValues().flatMap( Backdoor.cast(mappingFunction) );
            }

            public int size() {
                return source.size();
            }

            public FPMutableMap<K,V2> materialize() {
                return getEntries().toMap();
            }

            public String toString() {
                return getEntries().sort(Backdoor.cast(KV_COMPARATOR)).map( kv -> "("+kv.getFirst()+"->"+kv.getSecond()+")" ).toString();
            }
        };
    }


    public default FPMutableMap<K, V> chainWith( FPMutableMap<K, V> secondaryMap ) {
        FPMutableMap<K,V> primaryMap = this;

        return new FPMutableMap<K,V>() {
            public FPOption<V> get( K key ) {
                return primaryMap.get(key).or(() -> secondaryMap.get(key));
            }

            public void put( K key, V v ) {
                primaryMap.put(key, v);
            }

            public FPIterable<Tuple2<K, V>> getEntries() {
                return primaryMap.getEntries().and( secondaryMap.getEntries() ).dedup( Tuple2::getFirst );
            }

            public FPIterable<V> getValues() {
                return primaryMap.getValues().and( secondaryMap.getValues() );
            }

            public int size() {
                return primaryMap.size() + secondaryMap.size();
            }

            public FPMutableMap<K,V> materialize() {
                return getEntries().toMap();
            }

            public String toString() {
                return getEntries().sort(Backdoor.cast(KV_COMPARATOR)).map( kv -> "("+kv.getFirst()+"->"+kv.getSecond()+")" ).toString();
            }
        };
    }

    public default void forEach( VoidFunction2<K,V> callback ) {
        this.getEntries().forEach( kv -> callback.invoke( kv.getFirst(), kv.getSecond() ) );
    }

    public default FPMap<K,V> toFPMap() {
        return getEntries().toFPMap();
    }
}
