package mosaics.fp.collections;

import mosaics.lang.QA;
import mosaics.lang.functions.VoidFunctionD;
import mosaics.lang.QA;
import mosaics.lang.functions.VoidFunctionD;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;


public interface DoubleIterator {
    public static DoubleIterator wrap( double[] array ) {
        return new DoubleIterator() {
            private int i = 0;

            public boolean hasNext() {
                return i < array.length;
            }

            public double next() {
                return array[i++];
            }
        };
    }



    public boolean hasNext();
    public double next();

    public default DoubleIterator map( DoubleUnaryOperator op ) {
        DoubleIterator original = this;

        return new DoubleIterator() {
            public boolean hasNext() {
                return original.hasNext();
            }

            public double next() {
                return op.applyAsDouble(original.next());
            }
        };
    }

    public default void forEach( VoidFunctionD op ) {
        while ( hasNext() ) {
            double v = next();

            op.invoke( v );
        }
    }

    /**
     * Combines each number in the iterator using the specified aggregation function.  The folding
     * inserts a zero for the first value, thus allowing us to support empty lists.  Empty lists
     * always return 0.
     */
    public default double fold( DoubleBinaryOperator aggregator ) {
        return fold( aggregator, 0 );
    }

    /**
     * Combines each number in the iterator using the specified aggregation function.  The folding
     * uses initialValue for the first value, thus allowing us to support empty lists.  Empty lists
     * always return initialValue.
     */
    public default double fold( DoubleBinaryOperator aggregator, double initialValue ) {
        double sum = initialValue;
        while ( hasNext() ) {
            sum = aggregator.applyAsDouble( sum, next() );
        }

        return sum;
    }

    /**
     * Similar to fold but does not take an initial value.  Insteat it uses the first value from the
     * iterator and errors if the iterator is empty.
     */
    public default double reduce( DoubleBinaryOperator aggregator ) {
        return fold( aggregator, next() );
    }

    public default DoubleIterator drop( int n ) {
        while ( n > 0 && hasNext() ) {
            next();
            n--;
        }

        return this;
    }

    public default DoubleIterator take( int n ) {
        QA.argIsGTEZero( n, "n" );

        DoubleIterator orig = this;

        return new DoubleIterator() {
            private int countDown = n;

            public boolean hasNext() {
                return countDown > 0 && orig.hasNext();
            }

            public double next() {
                double v = orig.next();

                countDown -= 1;

                return v;
            }
        };
    }

    public default double min() {
        return hasNext() ? reduce( Math::min ) : 0;
    }

    public default double max() {
        return hasNext() ? reduce( Math::max ) : 0;
    }

    public default List<Double> toList() {
        List<Double> list = new ArrayList<>();

        forEach( list::add );

        return list;
    }

    public default String mkString() {
        StringBuilder buf = new StringBuilder();

        boolean appendComma = false;
        while ( hasNext() ) {
            if ( appendComma ) {
                buf.append( ", " );
            } else {
                appendComma = true;
            }

            buf.append( next() );
        }

        return buf.toString();
    }

    public default FPIterator<double[]> slice( int windowSize ) {
        DoubleIterator it = this;

        return new FPIterator<double[]>() {
            private double[] next = calculateNext();


            protected boolean _hasNext() {
                return next != null;
            }

            protected double[] _next() {
                double[] next = this.next;

                this.next = calculateNext();
                return next;
            }

            private double[] calculateNext() {
                double[] r = new double[windowSize];
                int    i = 0;

                while ( i < windowSize && it.hasNext() ) {
                    r[i++] = it.next();
                }

                if ( i != windowSize ) {
                    r = Arrays.copyOf(r, i);
                }

                return r;
            }
        };
    }
}
