package mosaics.fp.collections;

import lombok.Value;
import mosaics.lang.functions.VoidFunction1;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;
import java.util.function.Function;


/**
 * Generic depth first and breadth first binary tree iterators.
 */
public class TreeIterators {
    public static <V> FPIterator<V> inorderDepthFirstIteratorOpt( FPOption<V> initial, Function<V,FPOption<V>> getLHSFrom, Function<V,FPOption<V>> getRHSFrom ) {
        return initial.map( v -> inorderDepthFirstIterator(v, getLHSFrom, getRHSFrom) )
                      .orElse( FPIterator::emptyIterator );
    }

    /**
     * An 'inorder depth first walk' goes lhs-root-rhs.  If the tree is in ascending order
     * then the result will also be in ascending order.  If the tree is in descending order
     * then the result will also be in descending order.
     */
    public static <V> FPIterator<V> inorderDepthFirstIterator( V initial, Function<V,FPOption<V>> getLHSFrom, Function<V,FPOption<V>> getRHSFrom ) {
        MyCollection<V> stack = new MyStack<>();

        stack.push( initial );

        return inorderIterator( stack, getLHSFrom, getRHSFrom );
    }

    public static <V> FPIterator<V> breadthFirstIteratorOpt( FPOption<V> initial, Function<V,FPOption<V>> getLHSFrom, Function<V,FPOption<V>> getRHSFrom ) {
        return initial.map( v -> breadthFirstIterator(v, getLHSFrom, getRHSFrom) )
                      .orElse( FPIterator::emptyIterator );
    }

    /**
     * A breadth first walk goes node->lhs->rhs.
     */
    public static <V> FPIterator<V> breadthFirstIterator( V initial, Function<V,FPOption<V>> getLHSFrom, Function<V,FPOption<V>> getRHSFrom ) {
        MyCollection<V> queue = new MyQueue<>();

        queue.push( initial );

        return preorderIteratorL2R( queue, getLHSFrom, getRHSFrom );
    }

    private static <V> FPIterator<V> preorderIteratorL2R( MyCollection<V> visitableNodes, Function<V, FPOption<V>> getLHSFrom, Function<V, FPOption<V>> getRHSFrom ) {
        return treeWalkingIterator( visitableNodes, currentValue -> {
            visitableNodes.push( currentValue, true );
            visitableNodes.push( getLHSFrom.apply( currentValue ) );
            visitableNodes.push( getRHSFrom.apply( currentValue ) );
        } );
    }

    private static <V> FPIterator<V> inorderIterator( MyCollection<V> visitableNodes, Function<V, FPOption<V>> getLHSFrom, Function<V, FPOption<V>> getRHSFrom ) {
        return treeWalkingIterator( visitableNodes, currentValue -> {
            visitableNodes.push( getRHSFrom.apply( currentValue ) );
            visitableNodes.push( currentValue, true );
            visitableNodes.push( getLHSFrom.apply( currentValue ) );
        } );
    }

    private static <V> FPIterator<V> treeWalkingIterator( MyCollection<V> visitableNodes, VoidFunction1<V> updateStack ) {
        return new FPIterator<>() {
            protected boolean _hasNext() {
                return visitableNodes.hasContents();
            }

            protected V _next() {
                // NB each node in the visitableNodes has two states, visited and not visited.
                //    when the next node has been visited, the iterator exits and returns the value
                //    when the next node has not been visited, then the nodes rhs, itself (now visited) and
                //    lastly its lhs are pushed to the visitableNodes.  This maintains the 'inorder depth first'
                //    search order..
                while ( true ) {
                    Ref<V> nextRef = visitableNodes.pop();
                    V poppedValue = nextRef.getValue();

                    if ( nextRef.visited ) {
                        return poppedValue;
                    } else {
                        updateStack.invoke( poppedValue );
                    }
                }
            }
        };
    }

    private static interface MyCollection<T> {
        public boolean hasContents();
        public void push( Ref<T> ref );
        public Ref<T> pop();

        public default void push( FPOption<T> v ) {
            v.ifPresent( this::push );
        }

        public default void push( T v ) {
            push( v, false );
        }

        public default void push( T v, boolean visited ) {
            push( new Ref<>(v,visited) );
        }
    }

    private static class MyStack<T> implements MyCollection<T> {
        private final Stack<Ref<T>> stack = new Stack<>();

        public boolean hasContents() {
            return !stack.isEmpty();
        }

        public void push( Ref<T> ref ) {
            stack.push( ref );
        }

        public Ref<T> pop() {
            return stack.pop();
        }
    }

    private static class MyQueue<T> implements MyCollection<T> {
        private final Deque<Ref<T>> deque = new LinkedList<>();

        public boolean hasContents() {
            return !deque.isEmpty();
        }

        public void push( Ref<T> ref ) {
            deque.addFirst( ref );
        }

        public Ref<T> pop() {
            return deque.removeLast();
        }
    }

    @Value
    private static class Ref<V> {
        private V       value;
        private boolean visited;
    }
}
