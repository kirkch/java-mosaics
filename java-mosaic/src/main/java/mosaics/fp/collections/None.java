package mosaics.fp.collections;

import mosaics.fp.FP;
import mosaics.lang.Backdoor;
import mosaics.lang.TryUtils;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function0WithThrows;
import mosaics.lang.functions.Function1WithThrows;
import mosaics.lang.functions.VoidFunction0WithThrows;
import mosaics.lang.functions.VoidFunction1;
import mosaics.lang.functions.VoidFunction1WithThrows;

import java.util.Collections;
import java.util.List;


/**
 *
 */
public class None<T> extends FPOption<T> {
    public static final None<?> INSTANCE = new None<>();

    public T get() {
        throw new IllegalStateException( "no value" );
    }

    public boolean hasValue() {
        return false;
    }

    public boolean isEmpty() {
        return true;
    }

    public boolean isBlank() {
        return true;
    }

    public <B> FPOption<B> map( Function1WithThrows<T, ? extends B> mappingFunction ) {
        return Backdoor.cast(this );
    }

    public <B> FPOption<B> flatMap( Function1WithThrows<T, FPIterable<B>> mappingFunction ) {
        return Backdoor.cast(this );
    }

    public T orElse( T value ) {
        return value;
    }

    public T orElse( Function0WithThrows<T> fetcher ) {
        return TryUtils.invokeAndRethrowException( fetcher::invoke );
    }

    public FPOption<T> or( FPOption<T> alt ) {
        return alt;
    }

    public FPOption<T> or( Function0<FPOption<T>> fetcher ) {
        return fetcher.invoke();
    }

    public FPIterable<T> toIterable() {
        return FPIterable.empty();
    }

    public List<T> toList() {
        return Collections.emptyList();
    }

    public <X extends Throwable> T orElseThrow( Function0<X> exceptionFactory ) {
        throw Backdoor.throwException( exceptionFactory.invoke() );
    }

    public <X extends Throwable> FPOption<T> orThrow( Function0<X> exceptionFactory ) {
        throw Backdoor.throwException( exceptionFactory.invoke() );
    }

    public void ifPresent( VoidFunction1WithThrows<T> op ) {}

    public void ifEmpty( VoidFunction0WithThrows op ) {
        op.invokeQuietly();
    }

    public <B> FPOption<B> ifElse( Function1WithThrows<T,B> ifPresentFunc, Function0WithThrows<B> ifEmptyFunc ) {
        return FP.option( ifEmptyFunc.invokeSilently() );
    }

    public T orNull() {
        return null;
    }

    public FPIterable<T> and( FPOption<T> other ) {
        return other.toIterable();
    }

    public FPIterator<T> iterator() {
        return FPIterator.emptyIterator();
    }

    public void forEach( VoidFunction1<T> f ) {}

    public String toString() {
        return "None";
    }

    public int hashCode() {
        return 17;
    }

    public boolean equals( Object o ) {
        return o instanceof None;
    }
}
