package mosaics.fp.collections;


import mosaics.fp.FP;
import mosaics.lang.Backdoor;

import java.util.Arrays;
import java.util.Objects;

import static mosaics.lang.Backdoor.cast;


/**
 * An immutable sequence (List).  Stores values using an implementation of a relaxed Radix Balanced
 * Vector using a tree structure.  Each tree node contains up to n elements where n is a multiple of
 * 2;  usually 32.  Values are stored in the leaf nodes.  The inner nodes are connect the other
 * nodes together into a tree structure.  To locate index 'i'; the bits of the index are broken
 * up into a binary path.  If the branching factor is 32, then the index is broken up into blocks
 * of 5 bits (2^5 is 32).  This constitutes a path through the tree.   For example, in a tree
 * of depth 2.  The index 134 would be store in element 4 of the first node and element 6 of the leaf
 * node.  This is because 134 in binary is '1000 0110'; broken into groups of 5 gives us '00100' '00110'
 * or 4 and 6.
 */
public class RRBVector<T> implements FPIterable<T> {

    public static <T> RRBVector<T> toVector( Iterable<T> list ) {
        RRBVector<T> vector = RRBVector.emptyVector();

        for  ( T v : list ) {
            vector = vector.add( v );
        }

        return vector;
    }

    @SafeVarargs
    public static <T> RRBVector<T> toVector( T...values ) {
        return toVector( Arrays.asList(values) );
    }


    private static final RRBVector EMPTY_INSTANCE = new RRBVector();

    public static <T> RRBVector<T> emptyVector() {
        return cast(EMPTY_INSTANCE);
    }


    private static long LEAF_NODE_MASK = 0b11111;

    private static int BRANCH_FACTOR = 32;

    /**
     * indexed by the 'height' of the tree..
     */
    private static long[] PATH_MASKS = new long[] {
        0,
        LEAF_NODE_MASK,
        LEAF_NODE_MASK << 5,
        LEAF_NODE_MASK << 10,
        LEAF_NODE_MASK << 15,
        LEAF_NODE_MASK << 20,
        LEAF_NODE_MASK << 25,
        LEAF_NODE_MASK << 30,
        LEAF_NODE_MASK << 35,
        LEAF_NODE_MASK << 40,
        LEAF_NODE_MASK << 45,
        LEAF_NODE_MASK << 50,
        LEAF_NODE_MASK << 55,
        LEAF_NODE_MASK << 60
    };


    private int      height;
    private long     size;
    private Object[] rootNode;



    public RRBVector() {
        this(1, 0, new Object[BRANCH_FACTOR]);
    }

    private RRBVector( int height, long size, Object[] rootNode ) {
        this.height      = height;
        this.size        = size;
        this.rootNode    = rootNode;
    }


    public RRBVector<T> add( T newValue ) {
        return set( size, newValue );
    }

    @SafeVarargs
    public final RRBVector<T> addAll( T... values ) {
        return addAll( Arrays.asList(values) );
    }

    public RRBVector<T> addAll( Iterable<T> collection ) {
        RRBVector<T> vector = this;

        for ( T v : collection ) {
            vector = vector.add( v );
        }

        return vector;
    }

    public long indexOf( T targetValue ) {
        long i = 0;

        for ( T candidate : this ) {
            if ( Objects.equals(candidate,targetValue) ) {
                return i;
            }

            i += 1;
        }

        return -1;
    }

    public RRBVector<T> remove( T valueToRemove ) {
        long indexToRemove = indexOf( valueToRemove );
        if (indexToRemove == -1) {
            return this;
        }

        // shift all values along by one (this can get expensive fast) -- todo look into batch shifts
        RRBVector<T> updatedVector = this;
        for ( long i=indexToRemove+1; i<size; i++ ) {
            updatedVector = updatedVector.set( i-1, this.get(i) );
        }

        return updatedVector.set( size-1, FP.emptyOption() );
    }

    public Object[] toArray() {
        int      intSize = Backdoor.toInt( size() );
        Object[] array   = new Object[intSize];

        for ( int i=0; i<intSize; i++ ) {
            array[i] = get(i);
        }

        return array;
    }

    /**
     * Returns the max index+1 even if all of the other slots hold null.
     * @return
     */
    public long size() {
        return size;
    }

    public RRBVector<T> clear( long i ) {
        return set( i, FP.emptyOption() );
    }

    public RRBVector<T> set( long i, T v ) {
        return set( i, FP.option(v) );
    }

    public RRBVector<T> set( long i, FPOption<T> v ) {
        if ( i == Long.MAX_VALUE || i < 0 ) {
            throw new IndexOutOfBoundsException( i + " is out of bounds" );
        }

        // if necessary, first grow the height of the tree
        int      currentHeight = height;
        Object[] currentRoot   = this.rootNode;
        long     newSize       = Math.max(i+1,this.size);

        if ( this.size == i+1 && v.isEmpty() ) {
            newSize = this.size-1;
        }

        while( newSize > maxSizeForTreesOfHeight(currentHeight) ) {
            Object[] newRoot = new Object[BRANCH_FACTOR];
            newRoot[0] = currentRoot;

            currentHeight += 1;
            currentRoot    = newRoot;
        }

        // now set the value within the newly grown tree
        Object[] newRootNode = recursiveSet(currentHeight, currentRoot, i, v.orNull());

        return new RRBVector<>( currentHeight, newSize, newRootNode );
    }

    public FPOption<T> get( long i ) {
        return selectLeafNodeThatContains(i)
            .flatMap( node -> FP.option(cast(node[toNodeIndex(1,i)])) );
    }

    public T getOrElse( long i, T defaultValue ) {
        return get(i).orElse( defaultValue );
    }

    public FPOption<T> getHead() {
        return size() == 0 ? FPOption.none() : get(0);
    }

    /**
     * Walk along the tree to the leaf node that contains the specified vector index.
     */
    private FPOption<Object[]> selectLeafNodeThatContains( long i ) {
        if ( i >= size ) {
            return FP.emptyOption();
        }

        Object[] node = rootNode;
        for ( int h=height; h>1; h-- ) {
            int nodeIndex = toNodeIndex(h,i);

            if ( node == null ) {
                return FP.emptyOption();
            }

            node = (Object[]) node[nodeIndex];
        }

        return FP.option(node);
    }

    public long maxSizeForTreesOfHeight( int h ) {
        return Backdoor.toLong( Math.pow(BRANCH_FACTOR,h) );
    }

    public String toString() {
        StringBuilder buf = new StringBuilder();

        for ( long i=0; i<size(); i++ ) {
            if ( i > 0 ) {
                buf.append( ", " );
            }

            T v = get( i ).orNull();

            buf.append(v);
        }

        return buf.toString();
    }

    public int hashCode() {
        int hashCode = 17;

        for ( long i=0; i<size(); i++ ) {
            hashCode = (hashCode << 3) + Objects.hashCode(get(i));
        }

        return hashCode;
    }

    public boolean equals( Object o ) {
        if ( o == null || o.getClass() != RRBVector.class ) {
            return false;
        }

        RRBVector other = (RRBVector) o;

        if ( this.size != other.size ) {
            return false;
        }

        for ( long i=0; i<size(); i++ ) {
            if ( !Objects.equals(this.get(i), other.get(i)) ) {
                return false;
            }
        }

        return true;
    }

    /**
     * Given the height within the RRB Tree that a node exists at, return the index of this node
     * that contains the next node or value for the specified vectorIndex.
     *
     * Effectively this is the main logic for walking the RRB path.  It selects the 5 bits of the
     * vectorIndex and shifts them along so that they fall within the range of 0-31.  Which is the
     * index range of a single node.
     */
    private int toNodeIndex( int nodeHeight, long vectorIndex ) {
        int offset = (nodeHeight-1)*5;

        return Backdoor.toInt( (vectorIndex & PATH_MASKS[nodeHeight]) >> offset );
    }

    /**
     * Given a node within the RRB tree, recursively descend to the leaf.  Create a new leaf with
     * the new value and then unwind the stack placing the new child node within a clone of each
     * node.  The result at the root is a mutated copy of the original with the new value added.
     */
    private Object[] recursiveAdd( int height, Object[] node, long i, T v ) {
        if ( node == null ) {
            node = new Object[BRANCH_FACTOR];
        }

        int nodeIndex = toNodeIndex(height,i);

        if ( isLeafNode(height) ) {
            return setValueWithinNode( node, nodeIndex, v );
        } else {
            Object[] childNode = recursiveAdd( height - 1, cast(node[nodeIndex]), i, v );

            return setValueWithinNode( node, nodeIndex, childNode );
        }
    }

    private boolean isLeafNode( int height ) {
        return height == 1;
    }

    private Object[] setValueWithinNode( Object[] array, int arrayIndex, Object v ) {
        Object[] clone = Arrays.copyOf(array, BRANCH_FACTOR );

        clone[arrayIndex] = v;

        return clone;
    }

    /**
     * Given a node within the RRB tree, recursively descend to the leaf.  Create a new leaf with
     * the new value and then unwind the stack placing the new child node within a clone of each
     * node.  The result at the root is a mutated copy of the original with the new value added.
     */
    private Object[] recursiveSet( int height, Object[] node, long i, T v ) {
        if ( node == null ) {
            node = new Object[BRANCH_FACTOR];
        }

        int nodeIndex = toNodeIndex(height,i);

        if ( isLeafNode(height) ) {
            return setValueWithinNode( node, nodeIndex, v );
        } else {
            Object[] childNode = recursiveAdd( height - 1, cast(node[nodeIndex]), i, v );

            return setValueWithinNode( node, nodeIndex, childNode );
        }
    }

    public long count() {
        return size();
    }


    public FPIterator<T> iterator() {
        return new FPIterator<T>() {
            private int i = 0;

            public boolean _hasNext() {
                return i < size;
            }

            public T _next() {
                return get(i++).orNull();
            }
        }.filter( Objects::nonNull );
    }

    public boolean hasContents() {
        return size > 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

}
