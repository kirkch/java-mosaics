package mosaics.fp.collections;

/**
 *
 */
public interface HasKey<K> {
    public K getKey();
}
