package mosaics.fp.collections;

import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function1;

import java.util.HashSet;
import java.util.Set;


/**
 *
 */
public class DFS {

    public static <T> FPIterable<T> dfs( T origin, Function1<T,FPIterable<T>> childrenFetcher ) {
        return dfsMulti( FPList.wrapArray(origin), childrenFetcher );
    }

    public static <T> FPIterable<T> dfsMulti( FPIterable<T> origins, Function1<T,FPIterable<T>> childrenFetcher ) {
        return new FPIterable<T>() {
            public String toString() {
                return this.mkString();
            }

            public FPIterator<T> iterator() {
                return new FPIterator<T>() {
                    private LIFO<T> stack             = new LIFO<>(origins);
                    private Set<T>  alreadyVisitedSet = new HashSet<>();

                    protected synchronized boolean _hasNext() {
                        stack.dropWhile( alreadyVisitedSet::contains );

                        return !stack.isEmpty();
                    }

                    protected synchronized T _next() {
                        T next = stack.pop();

                        FPIterable<T> children = childrenFetcher.invoke( next );
                        children.forEach( stack::push );

                        alreadyVisitedSet.add( next );

                        return next;
                    }
                };
            }
        };
    }

}
