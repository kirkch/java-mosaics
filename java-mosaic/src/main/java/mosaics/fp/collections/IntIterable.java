package mosaics.fp.collections;

import mosaics.lang.functions.IntFunction0;
import mosaics.lang.functions.IntFunction0;

import java.util.Iterator;
import java.util.function.IntBinaryOperator;
import java.util.function.IntUnaryOperator;


public interface IntIterable {

    /**
     * Generate an infinite number series by wrapping a function that maps an index (0..MaxInt)
     * to another int.
     */
    public static IntIterable wrap( IntUnaryOperator supplier ) {
        return new IntIterable() {
            public IntIterator iterator() {
                return new IntIterator() {
                    private int i = 0;

                    public boolean hasNext() {
                        return i < Integer.MAX_VALUE;
                    }

                    public int next() {
                        int nextIndex = i;

                        i += 1;

                        return supplier.applyAsInt( nextIndex );
                    }
                };
            }
        };
    }

    public static IntIterable wrap( int[] array ) {
        return wrap( i -> array[i] );
    }


    public IntIterator iterator();


    public default int sum() {
        return fold( (a,b) -> a+b );
    }

    public default int[] toIntArray() {
        return iterator().toIntArray();
    }

    /**
     * Combine all of the values using the supplied aggregation function.
     *
     * @param aggregator (soFar,newValue)
     * @return
     */
    public default int fold( IntBinaryOperator aggregator ) {
        IntIterator it = iterator();

        int sum = 0;
        while ( it.hasNext() ) {
            sum = aggregator.applyAsInt( sum, it.next() );
        }

        return sum;
    }

    public default IntIterable map( IntUnaryOperator mappingFunction ) {
        return new IntIterable() {
            public IntIterator iterator() {
                IntIterator it = IntIterable.this.iterator();

                return new IntIterator() {
                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    public int next() {
                        int v = it.next();

                        return mappingFunction.applyAsInt( v );
                    }
                };
            }
        };
    }

    /**
     * For each int v in the series, returns Math.max(v, maxValue).
     */
    public default IntIterable max( int maxValue ) {
        return this.map( v -> Math.max(v,maxValue) );
    }

    /**
     * For each int v in the series, returns Math.min(v, minValue).
     */
    public default IntIterable min( int minValue ) {
        return this.map( v -> Math.min(v,minValue) );
    }

    /**
     * For each int v in the series, returns Math.max(v, maxValue).
     */
    public default IntIterable max( IntFunction0 maxValueFetcher ) {
        return max( maxValueFetcher.invoke() );
    }

    /**
     * For each int v in the series, returns Math.min(v, minValue).
     */
    public default IntIterable min( IntFunction0 minValueFetcher ) {
        return min( minValueFetcher.invoke() );
    }

    /**
     * Combines two iterators into a single iterator of tuples where each element from
     * each iterator is aligned with the other iterator.  For example:
     *
     * LHS:  a,b,c,d
     * RHS:  1,2,3,4,5
     *
     * becomes: (a,1), (b,2), (c,3), (d,4), (null,5)
     */
    public default <B> FPIterable<Tuple2<Integer,B>> zip( Iterable<B> rhs ) {
        return new FPIterable.BaseIterable<Tuple2<Integer,B>>() {
            public FPIterator<Tuple2<Integer, B>> iterator() {
                return new FPIterator<Tuple2<Integer, B>>() {
                    private IntIterator lhsIt = IntIterable.this.iterator();
                    private Iterator<B> rhsIt = rhs.iterator();

                    public boolean _hasNext() {
                        return lhsIt.hasNext() || rhsIt.hasNext();
                    }

                    public Tuple2<Integer, B> _next() {
                        Integer a = lhsIt.hasNext() ? lhsIt.next() : null;
                        B       b = rhsIt.hasNext() ? rhsIt.next() : null;

                        return new Tuple2<>(a,b);
                    }
                };
            }
        };
    }

    /**
     * Combines two iterators into a single iterator of tuples where each element from
     * each iterator is aligned with the other iterator.  For example:
     *
     * LHS:  a,b,c,d
     * RHS:  1,2,3,4,5
     *
     * becomes: (a,1), (b,2), (c,3), (d,4), (null,5)
     */
    public default FPIterable<Tuple2<Integer,Integer>> zip( IntIterable rhs ) {
        return new FPIterable.BaseIterable<Tuple2<Integer,Integer>>() {
            public FPIterator<Tuple2<Integer, Integer>> iterator() {
                return new FPIterator<Tuple2<Integer, Integer>>() {
                    private IntIterator lhsIt = IntIterable.this.iterator();
                    private IntIterator rhsIt = rhs.iterator();

                    public boolean _hasNext() {
                        return lhsIt.hasNext() || rhsIt.hasNext();
                    }

                    public Tuple2<Integer, Integer> _next() {
                        Integer a = lhsIt.hasNext() ? lhsIt.next() : null;
                        Integer b = rhsIt.hasNext() ? rhsIt.next() : null;

                        return new Tuple2<>(a,b);
                    }
                };
            }
        };
    }

}
