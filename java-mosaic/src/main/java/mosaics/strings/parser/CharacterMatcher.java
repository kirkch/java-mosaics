package mosaics.strings.parser;

import mosaics.fp.collections.FPOption;


/**
 *
 */
public interface CharacterMatcher {

    /**
     * Returns the number of characters matched.  Zero if nothing was matched.  Any characters matched
     * will scroll the iterator forwards.
     */
    public int matchCount( CharacterIterator it );

    public default FPOption<String> matchPull( CharacterIterator it ) {
        int count = it.peek( () -> matchCount(it) );

        return count == 0 ? FPOption.none() : FPOption.of( it.pull(count) );
    }

    public default boolean peek( CharacterIterator it ) {
        int numCharactersMatched = it.peek( () -> matchCount( it ) );

        return numCharactersMatched > 0;
    }

    public default CharacterMatcher withDescription( String text ) {
        CharacterMatcher orig = this;

        return new CharacterMatcher() {
            public int matchCount( CharacterIterator it ) {
                return orig.matchCount( it );
            }

            public String toString() {
                return text;
            }
        };
    }

    /**
     * Automatically include whitespace on either side of the matcher.
     */
    public default CharacterMatcher trim() {
        CharacterMatcher wsMatcher   = CharacterMatchers.whitespace();
        CharacterMatcher origMatcher = this;

        return new CharacterMatcher() {
            public int matchCount( CharacterIterator it ) {
                int wsLHSCount       = wsMatcher.matchCount( it );
                int origMatcherCount = origMatcher.matchCount( it );
                int wsRHSCount       = wsMatcher.matchCount( it );

                if ( origMatcherCount == 0 ) {
                    return 0;
                }

                return wsLHSCount + origMatcherCount + wsRHSCount;
            }

            public String toString() {
                return "trim("+origMatcher+")";
            }
        };
    }

    /**
     * Return a matcher that will match characters up to but not including the point where this
     * matcher will match or the end of the stream.
     *
     * The default implement works by testing the original matcher and then scrolling one character
     * to test again and thus does not scale well (O(n^2)).  Where applicable override this method
     * with a more faster implementation.
     */
    public default CharacterMatcher invertMatcher() {
        CharacterMatcher origMatcher = this;

        return new CharacterMatcher() {
            public int matchCount( CharacterIterator it ) { // very expensive on larger iterators
                int numCharactersSinceStart = 0;

                while ( it.hasNext() && !origMatcher.peek( it ) ) {
                    it.next();

                    numCharactersSinceStart++;
                }

                return numCharactersSinceStart;
            }

            public String toString() {
                return "invert("+origMatcher+")";
            }
        };
    }

    /**
     * Create a new matcher that will first match this matcher and then the supplied matcher.  Both
     * matchers must match for the new matcher to match.
     */
    @SuppressWarnings("Convert2Lambda")
    public default CharacterMatcher and( CharacterMatcher b ) {
        CharacterMatcher a = this;

        return new CharacterMatcher() {
            public int matchCount( CharacterIterator it ) {
                return it.pull( () -> {
                    int countA = a.matchCount( it );
                    if ( countA == 0 ) {
                        return 0;
                    }

                    int countB = b.matchCount( it );
                    if ( countB == 0 ) {
                        return 0;
                    }

                    return countA + countB;
                } );
            }

            public String toString() {
                return "("+a + " && " + b+")";
            }
        };
    }

    /**
     * Create a new matcher that will first match this matcher and then optionally the supplied matcher.
     */
    @SuppressWarnings("Convert2Lambda")
    public default CharacterMatcher andOptionally( CharacterMatcher b ) {
        CharacterMatcher a = this;

        return new CharacterMatcher() {
            public int matchCount( CharacterIterator it ) {
                return it.pull( () -> {
                    int countA = a.matchCount( it );
                    if ( countA == 0 ) {
                        return 0;
                    }

                    int countB = b.matchCount( it );

                    return countA + countB;
                } );
            }

            public String toString() {
                return "("+a + " &&Opt " + b+")";
            }
        };
    }

    /**
     * Create a new matcher that will first match this matcher OR (exclusive) the supplied matcher.
     * One of matchers must match for the new matcher to match.
     */
    @SuppressWarnings("Convert2Lambda")
    public default CharacterMatcher or( CharacterMatcher b ) {
        CharacterMatcher a = this;

        return new CharacterMatcher() {
            public int matchCount( CharacterIterator it ) {
                return it.pull( () -> {
                    int countA = a.matchCount( it );
                    if ( countA > 0 ) {
                        return countA;
                    }

                    int countB = b.matchCount( it );
                    if ( countB > 0 ) {
                        return countB;
                    }

                    return 0;
                } );
            }

            public String toString() {
                return "(" + a + " || " + b + ")";
            }
        };
    }
}
