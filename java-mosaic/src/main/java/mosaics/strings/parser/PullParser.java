package mosaics.strings.parser;

import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.lang.functions.BooleanFunctionC;

import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;


/**
 * An incremental parser.  Parses against a CharSequence using the CharacterMatcher requested.
 */
public class PullParser {

    private final CharacterIterator in;
    private       CharacterMatcher  skipMatcher          = CharacterMatchers.matchNothing();
    private       CharacterMatcher  upToEndOfLineMatcher = CharacterMatchers.upToEndOfLineMatcher();
    private       CharacterPosition pos;


    public PullParser( String resourceName, String[] in ) {
        this( resourceName, String.join(Backdoor.NEWLINE,in) );
    }

    public PullParser( String resourceName, String in ) {
        this( resourceName, CharacterIterators.stringIterator(in) );
    }


    public PullParser( String resourceName, InputStream in, String cs ) throws UnsupportedEncodingException {
        this( resourceName, CharacterIterators.streamIterator(in,cs) );
    }

    public PullParser( String resourceName, InputStream in, Charset cs ) throws UnsupportedEncodingException {
        this( resourceName, CharacterIterators.streamIterator(in,cs) );
    }

    public PullParser( String resourceName, Reader in ) {
        this( resourceName, CharacterIterators.readerIterator(in) );
    }

    public PullParser( String resourceName, CharacterIterator in ) {
        QA.argNotNull( resourceName, "resourceName" );
        QA.argNotNull( in, "in" );

        this.pos          = new CharacterPosition(resourceName);
        this.in           = in;
    }

    public CharacterPosition getPosition() {
        return pos;
    }

    /**
     * Automatically skip any characters matched by the specified matcher.  These characters are
     * skipped between calls to other parse methods on this class, and not during the execution of
     * a CharacterMatcher.
     */
    public void setSkipMatcher( CharacterMatcher matcher ) {
        QA.argNotNull(matcher, "matcher");

        this.skipMatcher = matcher;
    }

    public FPOption<String> pull( char c ) {
        return pullOptional( CharacterMatchers.constant(c) );
    }

    public FPOption<String> pullRegExp( String pattern ) {
        return pullOptional( CharacterMatchers.regexp(pattern) );
    }

    /**
     * Use the specified matcher to match characters at the current location.  If it succeeds then
     * the current location will scroll on to after the matched characters, if it fails then the
     * current location will remain the same.  Either way, the result of the match will be returned
     * in the result.
     */
    public FPOption<String> pullOptional( CharacterMatcher matcher ) {
        runSkipMatcher();

        FPOption<String> matchResult = matcher.matchPull( in );

        advance( matchResult );

        return matchResult;
    }

    public FPOption<String> pullOptionalNoAutoSkip( CharacterMatcher matcher ) {
        FPOption<String> matchResult = matcher.matchPull( in );

        advance( matchResult );

        return matchResult;
    }


    public void pullMandatory( char c ) {
        runSkipMatcher();

        if ( in.hasNext() ) {
            char actual = in.next();

            if ( actual == c ) {
                advance( actual );

                return;
            }

            throw new ParseException( pos, "Expected '" + c + "' but found '" + actual + "'" );
        } else {
            throw new ParseException( pos, "Expected '" + c + "' but found EOF" );
        }
    }

    public void pullMandatory( String txt ) {
        pullMandatory( CharacterMatchers.constant(txt) );
    }

//    public FPOption<String> pullUpto( CharacterMatcher matcher ) {
//        matcher.invertMatcher()
//
//
//        StringBuilder buf = new StringBuilder();
//
//        while ( !peek(matcher) ) {
//
//        }
//    }

    public String pullMandatory( CharacterMatcher matcher ) {
        FPOption<String> opt = pullOptional(matcher);

        if ( !opt.hasValue() ) {
            throwParseException( matcher );
        }

        return opt.get();
    }

    public String pullMandatoryNoAutoSkip( CharacterMatcher matcher ) {
        FPOption<String> opt = pullOptionalNoAutoSkip(matcher);

        if ( !opt.hasValue() ) {
            throwParseException( matcher );
        }

        return opt.get();
    }

    public boolean skipWhitespace() {
        return skip(CharacterMatchers.whitespace()) > 0;
    }

    public int skip( int numCharactersToSkip ) {
        String txt = in.pull( numCharactersToSkip );

        return advance( txt );
    }

    public int skipWhile( BooleanFunctionC predicate ) {
        FPOption<String> txt = in.pullWhile( predicate );

        return advance( txt );
    }

    public int skip( CharacterMatcher matcher ) {
        FPOption<String> txt = matcher.matchPull( in );

        return advance( txt );
    }

    public int skipAll( CharacterMatcher matcher ) {
        int totalSkipCount = 0;

        int numCharsSkipped = this.skip( matcher );
        while ( numCharsSkipped > 0 ) {
            totalSkipCount += numCharsSkipped;

            numCharsSkipped = this.skip( matcher );
        }

        return totalSkipCount;
    }

    public void mandatorySkip( CharacterMatcher matcher ) {
        int numCharactersSkipped = skip(matcher);

        if ( numCharactersSkipped == 0 ) {
            throwParseException( matcher );
        }
    }

    public boolean peek( char c ) {
        runSkipMatcher();

        return in.hasNext() && in.peek() == c;
    }

    public boolean peek( String txt ) {
        return peek( CharacterMatchers.constant(txt) );
    }

    public boolean peek( CharacterMatcher matcher ) {
        runSkipMatcher();

        return matcher.peek(in);
    }

    public char peek() {
        return in.peek();
    }

    private void runSkipMatcher() {
        FPOption<String> match = skipMatcher.matchPull( in );

        while ( match.hasValue() ) {
            advance( match );

            match = skipMatcher.matchPull( in );
        }
    }

    public String pullToEndOfLine() {
        FPOption<String> txt = upToEndOfLineMatcher.matchPull(in);

        advance( txt );

        return txt.orElse( "" );
    }

    public void skipToEndOfLine() {
        pullToEndOfLine();
    }

    public boolean hasMore() {
        return in.hasNext();
    }

    public boolean hasMore( int numChars ) {
        return in.hasNext(numChars);
    }

    public boolean isEmpty() {
        return in.isEmpty();
    }

    public char next() {
        return in.next();
    }


    /**
     * Breaks the remaining text up into chunks around the specified matcher.  Returning only the
     * parts of the string that are either side of the specified delimiter.
     */
    public FPIterator<String> split( CharacterMatcher delimiterMatcher ) {
        PullParser       parser          = this;
        CharacterMatcher invertedMatcher = delimiterMatcher.invertMatcher();

        parser.skipAll( delimiterMatcher );

        return new FPIterator<String>() {
            protected boolean _hasNext() {
                return parser.hasMore();
            }

            protected String _next() {
                String match = parser.pullMandatory( invertedMatcher );

                parser.skipAll( delimiterMatcher );

                return match;
            }
        };
    }

    public FPIterator<String> split( CharacterMatcher delimiterMatcher, char escapeChar ) {
        PullParser parser = this;

        parser.skipAll( delimiterMatcher );

        return new FPIterator<String>() {
            protected boolean _hasNext() {
                return parser.hasMore();
            }

            protected String _next() {
                StringBuilder buf = new StringBuilder();

                while ( parser.hasMore() ) {
                    if ( parser.peek(delimiterMatcher) ) {
                        break;
                    }

                    char c = parser.next();

                    if ( c == escapeChar && parser.hasMore() ) {
                        if ( parser.peek(delimiterMatcher) ) {
                            buf.append( parser.next() );
                        } else {
                            buf.append(escapeChar);
                            buf.append(parser.next());
                        }
                    } else {
                        buf.append( c );
                    }
                }

                parser.skipAll( delimiterMatcher );
                return buf.toString();
            }
        };
    }


    private void throwParseException( CharacterMatcher matcher ) {
        if ( in.hasNext() ) {
            throw new ParseException( pos, "Expected '" + matcher + "' but found '" + in.pullRemaining() + "'" );
        } else {
            throw new ParseException( pos, "Expected '" + matcher + "' but found EOF" );
        }
    }


    private int advance( char c ) {
        this.pos = pos.walkCharacters( c );

        return 1;
    }

    private int advance( FPOption<String> matchResult ) {
        return matchResult.hasValue() ? this.advance( matchResult.get() ) : 0;
    }

    private int advance( String txt ) {
        this.pos = pos.walkCharacters( txt );

        return txt.length();
    }

    public boolean peekAndPull( char c ) {
        runSkipMatcher();

        if ( in.peek() == c ) {
            in.next();
            advance( c );

            return true;
        } else {
            return false;
        }
    }

    public boolean peekAndPull( CharacterMatcher m ) {
        return pullOptional(m).hasValue();
    }

    public long getCharOffset() {
        return pos.getCharOffset();
    }
}
