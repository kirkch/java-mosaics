package mosaics.strings.parser;

import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import lombok.AllArgsConstructor;
import lombok.Value;


/**
 * Captures the column and line number of a position within a CharSequence.
 */
@Value
@AllArgsConstructor
public class CharacterPosition {
    /**
     * The number of columns represented by a tab character.
     */
    private static final int TAB_WIDTH = 2;


    private final String resourceName;
    private final int    lineNumber;
    private final int    columnNumber;
    private final long   charOffset;

    public CharacterPosition( String resourceName ) {
        this(resourceName, 1,1,0);
    }


    /**
     * The line number of where this matcher started matching from.
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * The column number of where this matcher started matching from.
     */
    public int getColumnNumber() {
        return columnNumber;
    }

    /**
     * The character offset from the start of the stream where this matcher started to match from.
     */
    public long getCharacterOffset() {
        return charOffset;
    }


    public CharacterPosition min( CharacterPosition other ) {
        if ( other == null ) {
            return this;
        }

        return this.charOffset <= other.charOffset ? this : other;
    }

    public CharacterPosition max( CharacterPosition other ) {
        if ( other == null ) {
            return this;
        }

        return this.charOffset >= other.charOffset ? this : other;
    }

    /**
     * Create a new CharPosition based on walking over the supplied characters.
     *
     * @param numCharacters how many characters to consume
     * @param it the characters to walk over
     */
    public CharacterPosition walkCharacters( int numCharacters, CharacterIterator it ) {
        return it.peek( () -> {
            int countDown = numCharacters;

            int  line   = this.lineNumber;
            int  col    = this.columnNumber;
            long offset = this.charOffset;

            while ( countDown > 0 && it.hasNext() ) {
                char c = it.next();

                offset++;

                if ( c == '\t' ) {
                    col += TAB_WIDTH;
                } else if ( c == '\n' ) {
                    col = 1;
                    line++;
                } else {
                    col++;
                }
            }

            return new CharacterPosition( resourceName, line, col, offset );
        });
    }

    /**
     * Create a new CharPosition based on walking over the supplied characters.
     *
     * @param characters the characters to walk over
     */
    public CharacterPosition walkCharacters( CharSequence characters ) {
        if ( characters.length() == 0 ) {
            return this;
        }

        int  line   = this.lineNumber;
        int  col    = this.columnNumber;
        long offset = this.charOffset;

        for ( int i=0; i<characters.length(); i++ ) {
            char c = characters.charAt( i );

            offset++;

            if ( c == '\t' ) {
                col += TAB_WIDTH;
            } else if ( c == '\n' ) {
                col = 1;
                line++;
            } else {
                col++;
            }
        }

        return new CharacterPosition( resourceName, line, col, offset );
    }

    public CharacterPosition walkCharacters( char c ) {
        int  line   = this.lineNumber;
        int  col    = this.columnNumber;
        long offset = this.charOffset + 1;

        if ( c == '\t' ) {
            col += TAB_WIDTH;
        } else if ( c == '\n' ) {
            col = 1;
            line++;
        } else {
            col++;
        }

        return new CharacterPosition( resourceName, line, col, offset );
    }

    /**
     * Create a new CharPosition based on walking over the supplied characters.
     *
     * @param numCharacters how many characters to consume
     * @param characters the characters to walk over
     */
    public CharacterPosition walkCharacters( int numCharacters, CharSequence characters ) {
        QA.argIsGTEZero(numCharacters, "numCharacters");

        if ( numCharacters == 0 ) {
            return this;
        }

        int line   = this.lineNumber;
        int col    = this.columnNumber;
        int offset = Backdoor.toInt(this.charOffset);
        int maxExc = Math.min(offset+numCharacters,characters.length());

        for ( int i=offset; i<maxExc; i++ ) {
            char c = characters.charAt( i );

            offset++;

            if ( c == '\t' ) {
                col += TAB_WIDTH;
            } else if ( c == '\n' ) {
                col = 1;
                line++;
            } else {
                col++;
            }
        }

        return new CharacterPosition( resourceName, line, col, offset );
    }

    public CharacterPosition setCharacterOffset( long newStreamOffset ) {
        return new CharacterPosition( resourceName, this.lineNumber, this.columnNumber, newStreamOffset );
    }

    public String toString() {
        return resourceName + " ["+lineNumber + "," + columnNumber + "," + charOffset+"]";
    }

}
