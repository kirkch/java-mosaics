package mosaics.strings.parser;

import mosaics.fp.LazyVal;
import mosaics.lang.ArrayUtils;
import mosaics.lang.QA;
import mosaics.lang.functions.BooleanFunction0;
import mosaics.lang.functions.Function0;
import lombok.Value;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CharacterMatchers {
    public static CharacterMatcher constant( char targetChar ) {
        return new ConstantCharMatcher( targetChar );
    }

    public static CharacterMatcher constant( String targetStr ) {
        return new ConstantMatcher( targetStr );
    }

    public static CharacterMatcher constantIgnoreCase( char c ) {
        return new ConstantCharMatcherIgnoreCase( c );
    }

    public static CharacterMatcher constantIgnoreCase( String c ) {
        return new ConstantStringMatcherIgnoreCase( c );
    }

    public static CharacterMatcher matchNothing() {
        return MatchNothingMatcher.INSTANCE;
    }

    public static CharacterMatcher upToEndOfLineMatcher() {
        return UpToEndOfLineMatcher.INSTANCE;
    }

    /**
     * Includes line feeds and carridge returns
     */
    public static CharacterMatcher whitespace() {
        return WhitespaceMatcher.INSTANCE;
    }

    public static CharacterMatcher nonWhitespace() {
        return NonWhitespaceMatcher.INSTANCE;
    }

    public static CharacterMatcher regexp( String regexp ) {
        return new JDKRegexpMatcher( Pattern.compile(regexp) );
    }

    public static CharacterMatcher regexp( Pattern regexp ) {
        return new JDKRegexpMatcher( regexp );
    }

    public static CharacterMatcher javaVariableName() {
        return JavaVariableNameMatcher.INSTANCE;
    }

    public static CharacterMatcher javaVariableType() {
        return JavaVariableTypeMatcher.INSTANCE;
    }

    public static CharacterMatcher integer() {
        return IntegerMatcher.INSTANCE;
    }

    /**
     *
     * When support commas is false:  [+-]?\d+
     * When support commas is true: then \d+ is changed to support 12,332,111 etc
     */
    public static CharacterMatcher integer( boolean supportCommas ) {
        return supportCommas ? CommaIntegerMatcher.INSTANCE : IntegerMatcher.INSTANCE;
    }

    public static CharacterMatcher digits() {
        return DigitsOnlyMatcher.INSTANCE;
    }

    public static CharacterMatcher floatingPointNumber() {
        return floatingPointNumber( false );
    }

    public static CharacterMatcher floatingPointNumber( boolean supportScientificFormat ) {
        if (supportScientificFormat) {
            return FloatingPointNumberMatcher.INSTANCE.andOptionally(
                CharacterMatchers.constantIgnoreCase('e').and(integer(false)));
        } else {
            return FloatingPointNumberMatcher.INSTANCE;
        }
    }

    public static CharacterMatcher everything() {
        return EverythingMatcher.INSTANCE;
    }

    public static CharacterMatcher everythingExcept( char c ) {
        return new EverythingExceptMatcher(c);
    }

    public static CharacterMatcher everythingExcept( String...constants ) {
        CharacterMatcher[] matchers = ArrayUtils.map( CharacterMatcher.class, constants, CharacterMatchers::constant );

        return new EverythingExceptOneOfMatcher(matchers);
    }

    public static CharacterMatcher consumeUptoNCharacters( int c ) {
        return new ConsumeUptoNCharactersMatcher(c);
    }

    public static CharacterMatcher delimitedText( char c ) {
        return new DelimitedTextMatcher(c,c);
    }

    public static CharacterMatcher delimitedText( char pre, char post ) {
        return new DelimitedTextMatcher(pre,post);
    }

    /**
     * Creates a matcher for sequences of items, such as lists or maps.
     *
     * @param prefixMatcher The opening matcher, eg for an array '['
     * @param elementMatcher Matches the main value that is repeated, for example '0-9'
     * @param separatorMatcher elements can be repeated if they are separated by this value, eg ','
     * @param postfix The end of the sequence, eg for an array ']'
     */
    public static CharacterMatcher sequence( CharacterMatcher prefixMatcher, CharacterMatcher elementMatcher, CharacterMatcher separatorMatcher, CharacterMatcher postfix ) {
        return sequence( prefixMatcher, () -> elementMatcher, separatorMatcher, postfix );
    }

    /**
     * Creates a matcher for sequences of items, such as lists or maps.
     *
     * @param prefixMatcher The opening matcher, eg for an array '['
     * @param elementMatcherFactory Matches the main value that is repeated, for example '0-9'.  A factory method is used
     *                              so that the matcher may go recursive.  Consider a sequence that could accept an array of arrays.
     * @param separatorMatcher elements can be repeated if they are separated by this value, eg ','
     * @param postfixMatcher The end of the sequence, eg for an array ']'
     */
    public static CharacterMatcher sequence( CharacterMatcher prefixMatcher, Function0<CharacterMatcher> elementMatcherFactory, CharacterMatcher separatorMatcher, CharacterMatcher postfixMatcher ) {
        return new SequenceCharacterMatcher( prefixMatcher, elementMatcherFactory, separatorMatcher, postfixMatcher );
    }


    private static class ConstantMatcher implements CharacterMatcher {
        private String targetStr;

        public ConstantMatcher( String targetStr ) {
            QA.notNull(targetStr, "targetStr");

            this.targetStr = targetStr;
        }

        public String toString() {
            return targetStr;
        }

        public int matchCount( CharacterIterator it ) {
            if ( !it.hasNext(targetStr.length()) ) {
                return 0;
            }

            return it.pull( () -> {
                int targetStrLength = targetStr.length();

                for ( int i=0; i<targetStrLength; i++ ) {
                    if ( it.next() != targetStr.charAt(i) ) {
                        return 0;
                    }
                }

                return targetStrLength;
            });
        }
    }


    private static class ConstantCharMatcher implements CharacterMatcher {
        private char targetChar;

        public ConstantCharMatcher( char targetChar ) {
            this.targetChar = targetChar;
        }

        public String toString() {
            return Character.toString(targetChar);
        }

        public int matchCount( CharacterIterator it ) {
            if ( it.isEmpty() ) {
                return 0;
            }

            char actual = it.peek();
            if ( actual != targetChar ) {
                return 0;
            }

            it.next();

            return 1;
        }
    }


    private static class ConstantCharMatcherIgnoreCase implements CharacterMatcher {
        private char targetCharLC;
        private char targetCharUC;

        public ConstantCharMatcherIgnoreCase( char targetChar ) {
            this.targetCharLC = Character.toLowerCase( targetChar );
            this.targetCharUC = Character.toUpperCase( targetChar );
        }

        public String toString() {
            return targetCharLC + "|" + targetCharUC;
        }

        public int matchCount( CharacterIterator it ) {
            if ( it.isEmpty() ) {
                return 0;
            }

            char actual = it.peek();

            if ( actual != targetCharLC && actual != targetCharUC ) {
                return 0;
            }

            it.next();

            return 1;
        }
    }


    private static class ConstantStringMatcherIgnoreCase implements CharacterMatcher {
        private char[] targetLC;
        private char[] targetUC;

        public ConstantStringMatcherIgnoreCase( String targetStr ) {
            this.targetLC = targetStr.toLowerCase().toCharArray();
            this.targetUC = targetStr.toUpperCase().toCharArray();
        }

        public String toString() {
            return targetLC + "|" + targetUC;
        }

        public int matchCount( CharacterIterator it ) {
            if ( !it.hasNext(targetLC.length) ) {
                return 0;
            }

            return it.pull( () -> {
                int targetStrLength = targetLC.length;

                for ( int i=0; i<targetStrLength; i++ ) {
                    char next = it.next();

                    if ( next != targetLC[i] && next != targetUC[i] ) {
                        return 0;
                    }
                }

                return targetStrLength;
            });
        }
    }

    private static class MatchNothingMatcher implements CharacterMatcher {
        public static final CharacterMatcher INSTANCE = new MatchNothingMatcher();

        private MatchNothingMatcher() {}

        public int matchCount( CharacterIterator it ) {
            return 0;
        }
    }

    private static class UpToEndOfLineMatcher implements CharacterMatcher {
        public static final CharacterMatcher INSTANCE = new UpToEndOfLineMatcher();

        private UpToEndOfLineMatcher() {}

        public int matchCount( CharacterIterator it ) {
            int count = it.pullCount( c -> c != '\r' && c != '\n' );

            count += it.conditionalNext('\r');
            count += it.conditionalNext('\n');

            return count;
        }
    }

    private static class WhitespaceMatcher implements CharacterMatcher {
        public static final CharacterMatcher INSTANCE = new WhitespaceMatcher();

        public static boolean isNotWhitespace( char c ) {
            return !isWhitespace( c );
        }

        public static boolean isWhitespace( char c ) {
            switch(c) {
                case ' ':
                case '\n':
                case '\r':
                case '\t':
                    return true;
                default:
                    return false;
            }
        }

        private WhitespaceMatcher() {}

        public String toString() {
            return "whitespace";
        }

        public int matchCount( CharacterIterator it ) {
            return it.countWhile( WhitespaceMatcher::isWhitespace );
        }
    }

    private static class NonWhitespaceMatcher implements CharacterMatcher {
        public static final CharacterMatcher INSTANCE = new NonWhitespaceMatcher();

        private NonWhitespaceMatcher() {}

        public String toString() {
            return "non-whitespace";
        }

        public int matchCount( CharacterIterator it ) {
            return it.countWhile( WhitespaceMatcher::isNotWhitespace );
        }
    }


    private static class IntegerMatcher extends FSMCharacterMatcher {
        public static CharacterMatcher INSTANCE = new IntegerMatcher();

        public static final NotMatchedYet  MATCHED_FIRST_LETTER = new NotMatchedYet( IntegerMatcher::matchNextDigit );
        public static final CandidateMatch MATCH_DIGITS         = new CandidateMatch( IntegerMatcher::matchNextDigit );

        // A: matchInitialCharacter
        // +|-    ->  MATCHED_FIRST_LETTER(notValidYet, B)
        // digit  ->  MATCH_DIGITS(candidate, B)

        // B: matchNextDigit
        // digit  -> MATCH_DIGITS(candidate, B)


        public IntegerMatcher( ) {
            super( IntegerMatcher::matchInitialCharacter );
        }

        public String toString() {
            return "INTEGER";
        }

        private static FSMStep matchInitialCharacter( char c ) {
            if ( c == '+' || c == '-' ) {
                return MATCHED_FIRST_LETTER;
            } else if ( c >= '0' && c <= '9' ) {
                return MATCH_DIGITS;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMStep matchNextDigit( char c ) {
            if ( c >= '0' && c <= '9' ) {
                return MATCH_DIGITS;
            } else {
                return NO_MATCH;
            }
        }
    }

    /**
     * Matches +123.1111 -12.332 3.14 3.0 10
     */
    private static class FloatingPointNumberMatcher extends FSMCharacterMatcher {
        public static CharacterMatcher INSTANCE = new FloatingPointNumberMatcher();

        public static final NotMatchedYet  MATCHED_FIRST_LETTER       = new NotMatchedYet( FloatingPointNumberMatcher::matchNextDigit );
        public static final CandidateMatch MATCH_INTEGER_DIGITS       = new CandidateMatch( FloatingPointNumberMatcher::matchNextDigit );
        public static final NotMatchedYet  MATCH_FIRST_FRACTION_DIGIT = new NotMatchedYet( FloatingPointNumberMatcher::matchNextFractionDigit );
        public static final CandidateMatch MATCH_NEXT_FRACTION_DIGIT  = new CandidateMatch( FloatingPointNumberMatcher::matchNextFractionDigit );

        // A: matchInitialCharacter
        // +|-    ->  MATCHED_FIRST_LETTER(notValidYet, B)
        // digit  ->  MATCH_DIGITS(candidate, B)

        // B: matchNextDigit
        // digit  -> MATCH_DIGITS(candidate, B)
        // .      -> MATCH_FIRST_FRACTION_DIGIT(noValidYet)

        // C: matchNextFractionDigit
        // digit -> MATCH_NEXT_FRACTION_DIGIT(candidate)


        public FloatingPointNumberMatcher( ) {
            super( FloatingPointNumberMatcher::matchInitialCharacter );
        }

        public String toString() {
            return "FloatNumber";
        }

        private static FSMStep matchInitialCharacter( char c ) {
            if ( c == '+' || c == '-' ) {
                return MATCHED_FIRST_LETTER;
            } else if ( c >= '0' && c <= '9' ) {
                return MATCH_INTEGER_DIGITS;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMStep matchNextDigit( char c ) {
            if ( c >= '0' && c <= '9' ) {
                return MATCH_INTEGER_DIGITS;
            } else if ( c == '.' ) {
                return MATCH_FIRST_FRACTION_DIGIT;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMStep matchNextFractionDigit( char c ) {
            if ( c >= '0' && c <= '9' ) {
                return MATCH_NEXT_FRACTION_DIGIT;
            } else {
                return NO_MATCH;
            }
        }
    }

    private static class DigitsOnlyMatcher implements CharacterMatcher {
        public static final CharacterMatcher INSTANCE = new DigitsOnlyMatcher();

        private DigitsOnlyMatcher() {}

        public String toString() {
            return "digits";
        }

        public int matchCount( CharacterIterator it ) {
            return it.countWhile( this::isDigit );
        }

        private boolean isDigit( char c ) {
            return c >= '0' && c <= '9';
        }
    }

    private static class CommaIntegerMatcher extends FSMCharacterMatcher {
        public static final CharacterMatcher INSTANCE = new CommaIntegerMatcher();

        private static final FSMStep EXPECT_FIRST_DIGIT                = new NotMatchedYet( CommaIntegerMatcher::expectFirstDigit );
        private static final FSMStep EXPECT_SECOND_DIGIT               = new CandidateMatch( CommaIntegerMatcher::expectSecondDigit );
        private static final FSMStep EXPECT_THIRD_DIGIT                = new CandidateMatch( CommaIntegerMatcher::expectThirdDigit );

        private static final FSMStep OPTIONAL_NEW_BLOCK_OR_JUST_DIGITS = new CandidateMatch( CommaIntegerMatcher::optionalNewBlockOrJustDigits );

        private static final FSMStep EXPECT_JUST_DIGITS                = new CandidateMatch( CommaIntegerMatcher::expectJustDigits );

        private static final FSMStep EXPECT_3DIGITS                    = new NotMatchedYet( CommaIntegerMatcher::expect3Digits );
        private static final FSMStep EXPECT_2DIGITS                    = new NotMatchedYet( CommaIntegerMatcher::expect2Digits );
        private static final FSMStep EXPECT_1DIGITS                    = new NotMatchedYet( CommaIntegerMatcher::expect1Digits );

        private static final FSMStep EXPECT_OPTIONAL_NEW_BLOCK         = new CandidateMatch( CommaIntegerMatcher::expectOptionalNewBlock );



        // MATCH_INITIAL_CHARACTER
        // +|-    ->  (notValidYet, EXPECT_FIRST_DIGIT)
        // digit  ->  (candidate, EXPECT_SECOND_DIGIT)

        // EXPECT_FIRST_DIGIT:
        // digit  -> (candidate, EXPECT_SECOND_DIGIT)

        // EXPECT_SECOND_DIGIT:
        // digit  -> (candidate, EXPECT_THIRD_DIGIT)
        // comma  -> (notValidYet, EXPECT3DIGITS)

        // EXPECT_THIRD_DIGIT:
        // digit  -> (candidate, OPTIONAL_NEW_BLOCK_OR_JUST_DIGITS)
        // comma  -> (notValidYet, EXPECT3DIGITS)

        // OPTIONAL_NEW_BLOCK_OR_JUST_DIGITS:
        // digit  -> (candidate, EXPECT_JUST_DIGITS)
        // comma  -> (notValidYet, EXPECT3DIGITS)

        // EXPECT_JUST_DIGITS:
        // digit  -> (candidate, EXPECT_JUST_DIGITS)

        // EXPECT3DIGITS:
        // digit -> (notValidYet, EXPECT2DIGITS)

        // EXPECT2DIGITS:
        // digit -> (notValidYet, EXPECT1DIGITS)

        // EXPECT1DIGITS:
        // digit -> (valid, OPTIONAL_NEW_BLOCK)

        // OPTIONAL_NEW_BLOCK:
        // comma -> (notValidYet, EXPECT3DIGITS)


        public CommaIntegerMatcher( ) {
            super( CommaIntegerMatcher::matchInitialCharacter );
        }


        private static FSMCharacterMatcher.FSMStep matchInitialCharacter( char c ) {
            if ( c == '+' || c == '-' ) {
                return EXPECT_FIRST_DIGIT;
            } else if ( c >= '0' && c <= '9' ) {
                return EXPECT_SECOND_DIGIT;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMCharacterMatcher.FSMStep expectFirstDigit( char c ) {
            if ( c >= '0' && c <= '9' ) {
                return EXPECT_SECOND_DIGIT;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMCharacterMatcher.FSMStep expectSecondDigit( char c ) {
            if ( c == ',' ) {
                return EXPECT_3DIGITS;
            } else if ( c >= '0' && c <= '9' ) {
                return EXPECT_THIRD_DIGIT;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMCharacterMatcher.FSMStep expectThirdDigit( char c ) {
            if ( c == ',' ) {
                return EXPECT_3DIGITS;
            } else if ( c >= '0' && c <= '9' ) {
                return OPTIONAL_NEW_BLOCK_OR_JUST_DIGITS;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMCharacterMatcher.FSMStep optionalNewBlockOrJustDigits( char c ) {
            if ( c == ',' ) {
                return EXPECT_3DIGITS;
            } else if ( c >= '0' && c <= '9' ) {
                return EXPECT_JUST_DIGITS;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMCharacterMatcher.FSMStep expectJustDigits( char c ) {
            if ( c >= '0' && c <= '9' ) {
                return EXPECT_JUST_DIGITS;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMCharacterMatcher.FSMStep expect3Digits( char c ) {
            if ( c >= '0' && c <= '9' ) {
                return EXPECT_2DIGITS;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMCharacterMatcher.FSMStep expect2Digits( char c ) {
            if ( c >= '0' && c <= '9' ) {
                return EXPECT_1DIGITS;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMCharacterMatcher.FSMStep expect1Digits( char c ) {
            if ( c >= '0' && c <= '9' ) {
                return EXPECT_OPTIONAL_NEW_BLOCK;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMCharacterMatcher.FSMStep expectOptionalNewBlock( char c ) {
            if ( c == ',' ) {
                return EXPECT_3DIGITS;
            } else if ( c >= '0' && c <= '9' ) {
                return FAILED_MATCH;
            } else {
                return NO_MATCH;
            }
        }

        public String toString() {
            return "comma-int";
        }

    }

    private static class JavaVariableNameMatcher implements CharacterMatcher {
        public static final CharacterMatcher INSTANCE = new JavaVariableNameMatcher();

        private JavaVariableNameMatcher() {}

        public String toString() {
            return "JavaVariableNameMatcher";
        }

        public int matchCount( CharacterIterator it ) {
            return it.pull( () -> {
                if ( it.isEmpty() || !isValidFirstChar( it.next() ) ) {
                    return 0;
                }

                return 1 + it.countWhile( this::isValidChar );
            });
        }


        private boolean isValidFirstChar( char c ) {
            return isBetween(c, 'a', 'z') || isBetween(c, 'A', 'Z') || c == '_';
        }

        private boolean isValidChar( char c ) {
            return isBetween(c, 'a', 'z') || isBetween(c, 'A', 'Z') || isBetween(c, '0', '9')
                || c == '_' || c == '$';
        }

        private boolean isBetween( char c, char min, char maxInc ) {
            return c >= min && c <= maxInc;
        }
    }

    private static class JavaVariableTypeMatcher implements CharacterMatcher {
        public static final CharacterMatcher INSTANCE = new JavaVariableTypeMatcher();

        private JavaVariableTypeMatcher() {}

        public String toString() {
            return "JavaVariableTypeMatcher";
        }


        public int matchCount( CharacterIterator it ) {
            return it.pull( () -> {
                if ( it.isEmpty() || !isValidFirstChar( it.next() ) ) {
                    return 0;
                }

                return 1 + it.countWhile( this::isValidChar );
            });
        }

        private boolean isValidFirstChar( char c ) {
            return isBetween(c, 'a', 'z') || isBetween(c, 'A', 'Z') || c == '_';
        }

        private boolean isValidChar( char c ) {
            return isBetween(c, 'a', 'z') || isBetween(c, 'A', 'Z') || isBetween(c, '0', '9')
                || c == '_' || c == '.';
        }

        private boolean isBetween( char c, char min, char maxInc ) {
            return c >= min && c <= maxInc;
        }
    }

    private static class JDKRegexpMatcher implements CharacterMatcher {
        private Pattern pattern;

        public JDKRegexpMatcher( Pattern pattern ) {
            this.pattern = pattern;
        }

        public String toString() {
            return pattern.toString();
        }

        public int matchCount( CharacterIterator it ) {
            return it.pull( () -> {
                Matcher m = pattern.matcher( it.pullUpTo(200) );

                return m.lookingAt() ? m.end() : 0;
            } );
        }
    }

    private static class EverythingExceptMatcher implements CharacterMatcher {
        private final char terminatingChar;

        public EverythingExceptMatcher( char c ) {
            terminatingChar = c;
        }

        public int matchCount( CharacterIterator it ) {
            return it.countWhile( c -> c != terminatingChar );
        }

        public String toString() {
            return "EverythingExcept("+terminatingChar+")";
        }
    }

    @Value
    private static class DelimitedTextMatcher implements CharacterMatcher {
        private final char openingDelimitingChar;
        private final char closingDelimitingChar;


        public int matchCount( CharacterIterator it ) {
            if ( it.peek() != openingDelimitingChar ) {
                return 0;
            }

            return it.pull( () -> {
                it.skip( 1 );

                int count = 0;

                while ( it.hasNext() ) {
                    char next = it.next();

                    if ( next == closingDelimitingChar ) {
                        return count + 2;
                    } else if ( next == '\\' ) {
                        if ( it.hasNext() ) {
                            it.skip( 1 );

                            count += 2; // skip the next character
                        } else {
                            return 0;  // \\ found at end of stream, need the delimiter to be at the end
                        }
                    } else {
                        count += 1;
                    }
                }

                return 0;
            } );
        }

        public String toString() {
            return "DelimitedText("+openingDelimitingChar+", "+closingDelimitingChar+")";
        }
    }

    private static class EverythingMatcher implements CharacterMatcher {
        public static CharacterMatcher INSTANCE = new EverythingMatcher();

        public int matchCount( CharacterIterator it ) {
            return it.countWhile( c -> true );
        }

        public String toString() {
            return "Everything()";
        }
    }

    private static class EverythingExceptOneOfMatcher implements CharacterMatcher {
        private CharacterMatcher[] exclusions;

        public EverythingExceptOneOfMatcher( CharacterMatcher[] exclusions ) {
            this.exclusions = exclusions;
        }

        public int matchCount( CharacterIterator it ) {
            return it.countWhile( new BooleanFunction0() {
                public boolean invoke() {
                    for ( CharacterMatcher e : exclusions ) {
                        it.markPosition();
                        try {
                            if ( e.matchCount( it ) > 0 ) {
                                return false;
                            }
                        } finally {
                            it.returnToLastMark();
                        }
                    }

                    return true;
                }
            } );
        }

        public String toString() {
            return "EverythingExceptOneOf"+ Arrays.asList(exclusions);
        }

    }

    private static class ConsumeUptoNCharactersMatcher implements CharacterMatcher {
        private final int numCharactersToConsume;

        public ConsumeUptoNCharactersMatcher( int numCharacters ) {
            this.numCharactersToConsume = numCharacters;
        }

        public int matchCount( CharacterIterator it ) {
            return it.pull( () -> it.pullUpTo(numCharactersToConsume).length() );
        }

        public String toString() {
            return "ConsumeUpToNCharacters("+numCharactersToConsume+")";
        }
    }

    private static class SequenceCharacterMatcher implements CharacterMatcher {
        private CharacterMatcher          prefixMatcher;
        private LazyVal<CharacterMatcher> elementMatcherFactory;
        private CharacterMatcher          separatorMatcher;
        private CharacterMatcher          postfixMatcher;

        private SequenceCharacterMatcher( CharacterMatcher prefixMatcher, Function0<CharacterMatcher> elementMatcherFactory, CharacterMatcher separatorMatcher, CharacterMatcher postfixMatcher ) {
            this.prefixMatcher         = prefixMatcher.trim();
            this.elementMatcherFactory = new LazyVal<>( () -> elementMatcherFactory.invoke().trim() );
            this.separatorMatcher      = separatorMatcher.trim();
            this.postfixMatcher        = postfixMatcher.trim();
        }


        public int matchCount( CharacterIterator it ) {
            int prefixCount = prefixMatcher.matchCount( it );
            if ( prefixCount == 0 ) {
                return 0;
            }

            CharacterMatcher elementMatcher = elementMatcherFactory.fetch();

            int elementCount     =  0;
            int prevElementCount = -1;
            while ( !postfixMatcher.peek(it) && prevElementCount != elementCount ) {
                prevElementCount = elementCount;

                if ( elementCount > 0 ) {
                    int separatorCount = separatorMatcher.matchCount( it );
                    if ( separatorCount == 0 ) {
                        return 0;
                    }

                    elementCount += separatorCount;

                    int count = elementMatcher.matchCount(it);
                    if ( count == 0 ) {
                        return 0;
                    }

                    elementCount += count;
                } else {
                    elementCount += elementMatcher.matchCount(it);
                }
            }

            int postfixCount = postfixMatcher.matchCount( it );
            if ( postfixCount == 0 ) {
                return 0;
            }

            return prefixCount + elementCount + postfixCount;
        }
    }
}
