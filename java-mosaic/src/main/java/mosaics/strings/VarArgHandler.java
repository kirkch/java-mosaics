package mosaics.strings;

import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPSet;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.functions.FunctionI;
import mosaics.lang.functions.FunctionIOO;
import mosaics.lang.reflection.ReflectionException;
import lombok.AllArgsConstructor;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Given n arguments, combine them into a single object.  Used by StringifiedJavaMethod.
 */
interface VarArgHandler {
    public Object aggregate( Object[] args, int fromInc );
}

@SuppressWarnings("rawtypes")
class ArrayVarArgHandler extends BaseVarArgHandler {
    public ArrayVarArgHandler( Class elementType, StringCodec codec ) {
        super(
            codec,
            len -> Array.newInstance(elementType, len),
            (i, array, v) -> {Array.set( array, i, v ); return array;},
            "ArrayVarArgHandler("+elementType.getSimpleName()+")"
        );
    }
}

@SuppressWarnings("rawtypes")
class ListVarArgHandler extends BaseVarArgHandler {
    @SuppressWarnings("unchecked")
    public ListVarArgHandler( StringCodec codec ) {
        super(
            codec,
            ArrayList::new,
            (i, list, v) -> {((List) list).add(v); return list;},
            "ListVarArgHandler("+codec.getType().getShortName()+")"
        );
    }
}

@SuppressWarnings("rawtypes")
class SetVarArgHandler extends BaseVarArgHandler {
    @SuppressWarnings("unchecked")
    public SetVarArgHandler( StringCodec codec ) {
        super(
            codec,
            HashSet::new,
            (i, set, v) -> {((Set) set).add(v); return set;},
            "SetVarArgHandler("+codec.getType().getShortName()+")"
        );
    }
}

@SuppressWarnings("rawtypes")
class FPListVarArgHandler extends BaseVarArgHandler {
    @SuppressWarnings("unchecked")
    public FPListVarArgHandler( StringCodec codec ) {
        super(
            codec,
            FPList::newArrayList,
            (i, list, v) -> {((FPList) list).add(v); return list;},
            "FPListVarArgHandler("+codec.getType().getShortName()+")"
        );
    }
}

@SuppressWarnings("rawtypes")
class RRBVectorVarArgHandler extends BaseVarArgHandler {
    @SuppressWarnings("unchecked")
    public RRBVectorVarArgHandler( StringCodec codec ) {
        super(
            codec,
            initialLen -> RRBVector.emptyVector(),
            (i, list, v) -> ((RRBVector) list).add(v),
            "RRBVectorVarArgHandler("+codec.getType().getShortName()+")"
        );
    }
}

@SuppressWarnings("rawtypes")
class FPSetVarArgHandler extends BaseVarArgHandler {
    @SuppressWarnings("unchecked")
    public FPSetVarArgHandler( StringCodec codec ) {
        super(
            codec,
            initialLen -> FPSet.newSet(),
            (i, set, v) -> ((FPSet) set).add(v),
            "FPSetVarArgHandler("+codec.getType().getShortName()+")"
        );
    }
}


@AllArgsConstructor
@SuppressWarnings("rawtypes")
abstract class BaseVarArgHandler implements VarArgHandler {
    private final StringCodec codec;
    private final FunctionI   factory;
    private final FunctionIOO setter;
    private final String      toStringText;

    @SuppressWarnings("unchecked")
    public Object aggregate( Object[] args, int fromInc ) {
        int   numArgs = args.length - fromInc;
        Object agg    = factory.invoke(numArgs);

        for ( int i=0; i<numArgs; i++ ) {
            Object decodedValue = decodeArg( args[fromInc + i] );

            agg = setter.invoke( i, agg, decodedValue );
        }

        return agg;
    }

    public String toString() {
        return toStringText;
    }

    private Object decodeArg( Object arg ) {
        try {
            return codec.decode( (String) arg ).getResult();
        } catch ( IllegalArgumentException | IllegalStateException ex ) {
            throw ReflectionException.recast("Malformed value '"+arg+"' within vararg", ex);
        }
    }
}
