package mosaics.strings.templates;

import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.parser.ParseException;


public class MissingParameterException extends ParseException {

    public MissingParameterException( CharacterPosition pos, String parameterName ) {
        super( pos, "Missing value for '" + parameterName + "'" );
    }

}
