package mosaics.strings.templates;

import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.parser.ParseException;


public class MissingPropertyException extends ParseException {

    public MissingPropertyException( CharacterPosition pos, Class clazz, String propertyName ) {
        super( pos, clazz.getName() + " is missing property '" + propertyName + "'" );
    }

}
