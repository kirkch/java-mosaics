package mosaics.strings.templates;

import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.parser.ParseException;


public class MissingModifierException extends ParseException {

    public MissingModifierException( CharacterPosition pos, String modifierName ) {
        super( pos, "Missing modifier for '" + modifierName + "'" );
    }

}
