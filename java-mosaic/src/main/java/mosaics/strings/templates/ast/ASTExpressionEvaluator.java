package mosaics.strings.templates.ast;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Function1;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaProperty;
import mosaics.strings.StringUtils;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.MissingParameterException;
import mosaics.strings.templates.MissingPropertyException;
import mosaics.utils.SetUtils;

import java.util.List;
import java.util.Optional;
import java.util.Set;


@SuppressWarnings("unchecked")
public class ASTExpressionEvaluator implements TemplateASTWalker {
    private static final Set<String> FALSE_VALUES = SetUtils.asSet("false", "f", "0", "no", "n");

    private Function1<String, FPOption<Object>> dataFetcher;
    private FPOption<Object>                    result;


    public ASTExpressionEvaluator( Function1<String, FPOption<Object>> dataFetcher ) {
        this.dataFetcher = dataFetcher;
    }

    public FPOption<Object> getResult() {
        return result;
    }

    public boolean isTrue() {
        return isTrue(result);
    }

    public void walkPropertyAST( CharacterPosition pos, String parameterName, FPOption<String> parameterType, List<String> chainedProperties, List<String> applyModifiers, FPOption<String> defaultValue ) {
        result = fetchParameter( dataFetcher, pos, parameterName, chainedProperties, false )
            .or( (FPOption) defaultValue );

        if ( parameterType.hasValue() && parameterType.get().equals("boolean") ) {
            result = result.map( v -> !StringUtils.isBlank(v.toString()) && !FALSE_VALUES.contains(v.toString().toLowerCase()) );
        }
    }

    public void walkAndAST( CharacterPosition pos, List<TemplateAST> predicates ) {
        result = FP.option( true );

        for ( TemplateAST predicate : predicates ) {
            ASTExpressionEvaluator nestedEvaluator = new ASTExpressionEvaluator(dataFetcher);

            predicate.walkAST(nestedEvaluator);

            this.result = nestedEvaluator.getResult();

            if ( !isTrue() ) {
                return;
            }
        }
    }


    public void walkTextAST( CharacterPosition pos, String text ) {
        throw new UnsupportedOperationException();
    }

    public void walkIfBlockAST( CharacterPosition pos, TemplateAST predicate, FPOption<TemplateAST> ifTrueBlock, FPOption<TemplateAST> elseBlock ) {
        throw new UnsupportedOperationException();
    }

    public void walkForEachAST( CharacterPosition pos, String variableName, TemplateAST expression, TemplateAST block ) {
        throw new UnsupportedOperationException();
    }



    public static boolean isTrue( Function1<String, FPOption<Object>> dataFetcher, TemplateAST predicate ) {
        ASTExpressionEvaluator evaluator = new ASTExpressionEvaluator( dataFetcher );

        predicate.walkAST( evaluator );

        return evaluator.isTrue();
    }

    public static boolean isTrue( FPOption<Object> value ) {
        if ( value.isEmpty() ) {
            return false;
        }

        Object v = value.get();
        if ( v instanceof Boolean ) {
            return (Boolean) v;
        } else {
            return false;
        }
    }


    @SuppressWarnings("unchecked")
    public static FPOption<Object> fetchParameter( Function1<String, FPOption<Object>> dataFetcher, CharacterPosition pos, String parameterName, List<String> propertyPath, boolean errorIfMissing ) {
        FPOption<Object> parameterValue = dataFetcher.invoke(parameterName);

        parameterValue = unpackOptionalValue( parameterValue );

        if ( parameterValue.isEmpty() ) {
            if ( errorIfMissing ) {
                throw new MissingParameterException( pos, parameterName );
            } else {
                return FP.emptyOption();
            }
        }

        for ( String propertyName : propertyPath ) {
            Class<?>                              type      = parameterValue.get().getClass();
            JavaClass                             javaClass = JavaClass.of( type );
            FPOption<JavaProperty<Object,Object>> property  = javaClass.getProperty(propertyName);

            if ( !property.hasValue() ) {
                if ( errorIfMissing ) {
                    throw new MissingPropertyException( pos, javaClass.getJdkClass(), propertyName );
                } else {
                    return FP.emptyOption();
                }
            }

            parameterValue = property.get().getValueFrom(parameterValue.get());
            parameterValue = unpackOptionalValue( parameterValue );

            if ( !parameterValue.hasValue() ) {
                if ( errorIfMissing ) {
                    throw new MissingParameterException( pos, propertyName );
                } else {
                    return FP.emptyOption();
                }
            }
        }

        return parameterValue;
    }

    private static FPOption<Object> unpackOptionalValue( FPOption<Object> value ) {
        if ( value.hasValue() ) {
            if ( value.get() instanceof FPOption ) {
                FPOption v = (FPOption) value.get();

                if ( v.hasValue() ) {
                    return v;
                } else {
                    return FPOption.none();
                }
            } else if ( value.get() instanceof Optional ) {
                Optional v = (Optional) value.get();

                if ( v.isPresent() ) {
                    return FPOption.of( v.get() );
                } else {
                    return FPOption.none();
                }
            }
        }

        return value;
    }

}
