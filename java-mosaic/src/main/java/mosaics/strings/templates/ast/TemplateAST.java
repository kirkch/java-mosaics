package mosaics.strings.templates.ast;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.strings.parser.CharacterPosition;


public abstract class TemplateAST {

    /**
     * Return the position where this AST was parsed from.
     */
    public abstract CharacterPosition getPos();

    /**
     * A double dispatch method that identifies this AST to a callback interface.
     */
    public abstract void walkAST( TemplateASTWalker walker );

    /**
     * Ask this AST node to clean out dead nodes.  If it returns empty then it is saying that it
     * is redundant and can be removed, for example an empty block.
     */
    public abstract FPOption<TemplateAST> optimiseSelf( TemplateContext ctx );

    public <T extends TemplateAST> FPIterable<TemplateAST> collectChildrenOfType( Class<T> type ) {
        if ( type.isAssignableFrom(this.getClass()) ) {
            return FP.wrapSingleton( this );
        }

        return FPIterable.empty();
    }


    /**
     * Gives two nodes the opportunity to merge and become one.
     *
     * @return none if the two nodes cannot be merged and thus must remain separate else the merged
     *   node will be returned in a Some.
     */
    public FPOption<TemplateAST> mergeWith( TemplateAST other ) {
        return FP.emptyOption();
    }


    protected FPOption<TemplateAST> internOptimisedVersion( FPOption<TemplateAST> optimisedAlt ) {
        if ( optimisedAlt.isEmpty() ) {
            return optimisedAlt;
        }

        return internOptimisedVersion( optimisedAlt.get() );
    }

    protected FPOption<TemplateAST> internOptimisedVersion( TemplateAST optimisedAlt ) {
        if ( optimisedAlt.equals(this) ) {
            return FP.option( this );
        } else {
            return FP.option( optimisedAlt );
        }
    }

}
