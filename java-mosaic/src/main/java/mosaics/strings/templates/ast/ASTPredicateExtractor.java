package mosaics.strings.templates.ast;

import mosaics.strings.parser.CharacterPosition;
import lombok.Getter;
import mosaics.fp.collections.FPOption;

import java.util.ArrayList;
import java.util.List;


public class ASTPredicateExtractor {

    /**
     * Walk the specified AST and identify every predicate used in an if statement.
     */
    public static List<TemplateAST> extractPredicatesFrom( TemplateAST rawAST ) {
        PredicateFinderASTWalker walker = new PredicateFinderASTWalker();

        rawAST.walkAST( walker );

        return walker.getPredicates();
    }



    private static class PredicateFinderASTWalker implements TemplateASTWalker {
        @Getter private List<TemplateAST> predicates = new ArrayList<>();

        public void walkTextAST( CharacterPosition pos, String text ) {}

        public void walkPropertyAST( CharacterPosition pos, String parameterName, FPOption<String> parameterType, List<String> chainedProperties, List<String> applyModifiers, FPOption<String> defaultValue ) { }

        public void walkIfBlockAST( CharacterPosition pos, TemplateAST predicate, FPOption<TemplateAST> ifTrueBlock, FPOption<TemplateAST> elseBlock ) {
            predicates.add( predicate );

            ifTrueBlock.ifPresent( block -> block.walkAST(this) );
            elseBlock.ifPresent( block -> block.walkAST(this) );
        }

        public void walkForEachAST( CharacterPosition pos, String variableName, TemplateAST expression, TemplateAST block ) {
            block.walkAST( this );
        }

        public void walkAndAST( CharacterPosition pos, List<TemplateAST> predicates ) {
            predicates.forEach( block -> block.walkAST(this) );
        }
    }

}
