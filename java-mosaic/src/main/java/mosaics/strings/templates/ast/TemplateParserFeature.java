package mosaics.strings.templates.ast;

/**
 * Set of TemplateParser features that may be enabled/disabled.  By default all features are enabled.
 */
public enum TemplateParserFeature {
    /**
     * Support for #for may be disabled.
     */
    FOREACH_SUPPORT
}
