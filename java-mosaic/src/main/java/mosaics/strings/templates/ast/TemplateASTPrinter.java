package mosaics.strings.templates.ast;

import mosaics.strings.parser.CharacterPosition;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;
import mosaics.fp.collections.FPOption;
import mosaics.io.xml.TagWriter;
import mosaics.utils.ListUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Value
@AllArgsConstructor
public class TemplateASTPrinter implements TemplateASTWalker {

    public static String toString( TemplateAST root ) {
        StringBuilder buf = new StringBuilder();

        root.walkAST( new TemplateASTPrinter(buf) );

        return buf.toString();
    }

    public static String toStringWithPositions( TemplateAST root ) {
        StringBuilder buf = new StringBuilder();

        root.walkAST( new TemplateASTPrinter(buf).withPositionAttributes(true) );

        return buf.toString();
    }

          private TagWriter out;
    @With private boolean   positionAttributes;


    public TemplateASTPrinter( Appendable out ) {
        this( TagWriter.xmlWriter(out), false );
    }


    public void walkTextAST( CharacterPosition pos, String text ) {
        out.tag( "Text" );

        if ( positionAttributes ) {
            out.attr( "pos", formatPos(pos) );
        }

        out.bodyText( text );

        out.closeTag( "Text" );
    }

    public void walkPropertyAST( CharacterPosition pos, String parameterName, FPOption<String> parameterType, List<String> chainedProperties, List<String> applyModifiers, FPOption<String> defaultValue ) {
        Map<String,String> attributes = new HashMap<>();

        appendPosAttribute( pos, attributes );
        attributes.put( "name", parameterName );

        if ( !chainedProperties.isEmpty() ) {
            attributes.put( "chainedProperties", ListUtils.toString(chainedProperties,",") );
        }

        parameterType.ifPresent( pt -> attributes.put("type", pt) );

        defaultValue.ifPresent( v -> attributes.put("defaultValue", v) );

        out.tag( "Property", attributes );
    }

    public void walkIfBlockAST( CharacterPosition pos, TemplateAST predicate, FPOption<TemplateAST> ifTrueBlock, FPOption<TemplateAST> elseBlock ) {
        out.tag("If" );
        if ( positionAttributes ) {
            out.attr( "pos", formatPos(pos) );
        }

        out.tag("Predicate");

        predicate.walkAST( this );

        out.closeTag( "Predicate" );

        optionallyVisitChild( "OnTrue", ifTrueBlock );
        optionallyVisitChild( "OnFalse", elseBlock );

        out.closeTag( "If" );
    }

    public void walkForEachAST( CharacterPosition pos, String variableName, TemplateAST expression, TemplateAST block ) {
        out.tag( "ForEach" );

        out.attr( "var", variableName );

        if ( positionAttributes ) {
            out.attr( "pos", formatPos(pos) );
        }

        visitChild( "Expression", expression );
        block.walkAST( this );

        out.closeTag( "ForEach" );
    }

    public void walkAndAST( CharacterPosition pos, List<TemplateAST> predicates ) {
        out.tag("And");
        if ( positionAttributes ) {
            out.attr( "pos", formatPos(pos) );
        }

        predicates.forEach( ast -> ast.walkAST(this) );
        out.closeTag("And");
    }

    private void optionallyVisitChild( String tagName, FPOption<TemplateAST> childOpt ) {
        childOpt.ifPresent( child -> visitChild(tagName,child) );
    }

    private void visitChild( String tagName, TemplateAST child ) {
        out.tag( tagName );
        child.walkAST( this );
        out.closeTag( tagName );
    }

    private void appendPosAttribute( CharacterPosition pos, Map<String, String> attributes ) {
        if ( positionAttributes ) {
            attributes.put(
                "pos",
                formatPos( pos )
            );
        }
    }

    private String formatPos( CharacterPosition pos ) {
        return String.format("%d,%d,%d",pos.getLineNumber(),pos.getColumnNumber(),pos.getCharacterOffset());
    }

}
