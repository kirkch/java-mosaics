package mosaics.strings.templates.ast.nodes;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.strings.StringUtils;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import lombok.EqualsAndHashCode;
import lombok.Value;


@Value
@EqualsAndHashCode(callSuper = false)
public class TextAST extends TemplateAST {
    private CharacterPosition pos;
    private String            text;

    public void walkAST( TemplateASTWalker walker ) {
        walker.walkTextAST( pos, text );
    }

    public FPOption<TemplateAST> optimiseSelf( TemplateContext ctx ) {
        if ( StringUtils.isEmpty(text) ) {
            return FP.emptyOption();
        }

        return FP.option( this );
    }

    public FPOption<TemplateAST> mergeWith( TemplateAST o ) {
        if ( !(o instanceof TextAST) ) {
            return FP.emptyOption();
        }

        TextAST other = (TextAST) o;
        return FP.option(
            new TextAST( this.pos, this.text + other.text )
        );
    }
}
