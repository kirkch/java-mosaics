package mosaics.strings.templates.ast;

import lombok.SneakyThrows;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Function1;
import mosaics.strings.parser.CharacterPosition;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;


/**
 * Provides if and for behaviour to the ASTWalker.
 */
public abstract class TemplateExecuteASTWalker implements TemplateASTWalker, Cloneable {
    private Function1<String, FPOption<Object>> dataFetcher;


    protected TemplateExecuteASTWalker( Function1<String, FPOption<Object>> dataFetcher ) {
        this.dataFetcher = dataFetcher;
    }


    public TemplateExecuteASTWalker withNewDataValue( String key, Object value ) {
        return this.withDataFetcher( candidateKey -> {
            if ( Objects.equals(candidateKey,key) ) {
                return FP.option( value );
            } else {
                return dataFetcher.invoke( key );
            }
        } );
    }

    public TemplateExecuteASTWalker withDataFetcher( Function1<String,FPOption<Object>> newDataFetcher ) {
        TemplateExecuteASTWalker clone = this.clone();

        clone.dataFetcher = newDataFetcher;

        return clone;
    }

    public void walkIfBlockAST( CharacterPosition pos, TemplateAST predicate, FPOption<TemplateAST> ifTrueBlock, FPOption<TemplateAST> elseBlock ) {
        FPOption<Object> v = evaluateExpression(predicate);

        if ( ASTExpressionEvaluator.isTrue(v) ) {
            ifTrueBlock.ifPresent( ast -> ast.walkAST(this) );
        } else {
            elseBlock.ifPresent( ast -> ast.walkAST(this) );
        }
    }

    public void walkForEachAST( CharacterPosition pos, String variableName, TemplateAST expression, TemplateAST block ) {
        FPOption<Object> v = evaluateExpression(expression);

        if ( v.hasValue() ) {
            Iterable iterable;

            Object value = v.get();
            if ( value instanceof Iterable ) {
                iterable = (Iterable) value;
            } else if ( value instanceof Map ) {
                iterable = ((Map) value).entrySet();
            } else if ( value instanceof Stream ) {
                iterable = ((Stream) value)::iterator;
            } else {
                iterable = null;
            }

            repeatBlockForEachElementInIterable( iterable, variableName, block );
        }
    }

    protected Function1<String, FPOption<Object>> getDataFetcher() {
        return dataFetcher;
    }

    protected FPOption<Object> evaluateExpression( TemplateAST expression ) {
        ASTExpressionEvaluator evaluator = new ASTExpressionEvaluator(dataFetcher);

        expression.walkAST( evaluator );

        return evaluator.getResult();
    }

    @SuppressWarnings("unchecked")
    private void repeatBlockForEachElementInIterable( Iterable iterable, String variableName, TemplateAST block ) {
        if ( iterable == null ) {
            return;
        }

        iterable.forEach( obj -> block.walkAST(withNewDataValue(variableName,obj)) );
    }

    @SneakyThrows
    protected TemplateExecuteASTWalker clone() {
        return (TemplateExecuteASTWalker) super.clone();
    }
}

