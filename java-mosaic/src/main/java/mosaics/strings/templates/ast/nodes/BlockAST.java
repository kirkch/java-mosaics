package mosaics.strings.templates.ast.nodes;

import mosaics.fp.FP;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static mosaics.lang.Backdoor.cast;


@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BlockAST extends TemplateAST {
    private CharacterPosition  pos;
    private List<TemplateAST> children;


    public BlockAST( CharacterPosition pos, TemplateAST...children ) {
        this( pos, Arrays.asList(children) );
    }

    public void walkAST( TemplateASTWalker walker ) {
        walker.walkBlockAST( pos, children );
    }


    public BlockAST conditionallyAppend( FPOption<TemplateAST> astOption ) {
        if ( astOption.hasValue() ) {
            return append( astOption.get() );
        } else {
            return this;
        }
    }

    public BlockAST append( TemplateAST child ) {
        List<TemplateAST> newChildren = new ArrayList<>( children );

        newChildren.add( child );

        return new BlockAST( pos, newChildren );
    }

    public FPOption<TemplateAST> optimiseSelf( TemplateContext ctx ) {
        List<TemplateAST> optimisedChildren = FP.wrap(children)
            .fold( FP.<TemplateAST>nil(), (soFar,next) -> optimiseChild(ctx,soFar,next) )
            .reverse()
            .toList();

        if ( optimisedChildren.isEmpty() ) {
            return FP.emptyOption();
        } else if ( optimisedChildren.size() == 1 ) {
            return FP.option( optimisedChildren.get(0) );
        }

        return internOptimisedVersion( new BlockAST(pos,optimisedChildren) );
    }

    public <T extends TemplateAST> FPIterable<TemplateAST> collectChildrenOfType( Class<T> type ) {
        if ( type.isAssignableFrom(this.getClass()) ) {
            return FP.wrapSingleton( this );
        }

        return cast(FP.wrap(children).filter(type));
    }

    private ConsList<TemplateAST> optimiseChild( TemplateContext ctx, ConsList<TemplateAST> childrenSoFar, TemplateAST nextChild ) {
        if ( childrenSoFar.isEmpty() ) {
            return nextChild.optimiseSelf(ctx).toConsList();
        }

        FPOption<TemplateAST> optimisedChildOpt = nextChild.optimiseSelf(ctx);
        if ( optimisedChildOpt.isEmpty() ) {
            return childrenSoFar;
        }

        FPOption<TemplateAST> mergedASTOption = childrenSoFar.getHead().mergeWith(optimisedChildOpt.get());
        if ( mergedASTOption.hasValue() ) {
            return childrenSoFar.getTail().append( mergedASTOption.get() );
        } else {
            return childrenSoFar.append( optimisedChildOpt.get() );
        }
    }
}
