package mosaics.strings.templates.ast;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import mosaics.strings.StringCodecs;

import java.util.HashMap;
import java.util.Map;


/**
 * Provides optional type information for a template as it is compiled.
 */
public class TemplateContext {

    private StringCodecs                    stringCodecs;

    private Map<String,FPOption<JavaClass>> parameters          = new HashMap<>();
    private boolean                         acceptAllParameters;


    public TemplateContext() {
        this( StringCodecs.DEFAULT_CODECS );
    }

    public TemplateContext( StringCodecs codecs ) {
        this.stringCodecs = codecs;
    }


    public boolean acceptsParameter( String parameterName ) {
        return acceptAllParameters || parameters.containsKey(parameterName);
    }

    public FPOption<JavaClass> getParameterType( String parameterName ) {
        return parameters.get( parameterName );
    }

    public FPOption<JavaClass> getTypeByLabel( String label ) {
        return getStringCodecByAlias(label).map( StringCodec::getType );
    }

    public FPOption<StringCodec> getStringCodecByAlias( String alias ) {
        return FP.option( stringCodecs.getCodecByAlias(alias) );
    }


    public TemplateContext acceptsAllParameters( boolean flag ) {
        this.acceptAllParameters = flag;

        return this;
    }

    public TemplateContext withParameter( String parameterName ) {
        parameters.put( parameterName, FP.emptyOption() );

        return this;
    }

    public TemplateContext withParameter( String parameterName, JavaClass parameterType ) {
        parameters.put( parameterName, FP.option(parameterType) );

        return this;
    }


}
