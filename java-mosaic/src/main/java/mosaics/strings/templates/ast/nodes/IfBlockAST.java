package mosaics.strings.templates.ast.nodes;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;


@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class IfBlockAST extends TemplateAST {
    private CharacterPosition      pos;
    private TemplateAST           predicate;
    private FPOption<TemplateAST> ifTrueBlock;
    private FPOption<TemplateAST> elseBlock;


    public IfBlockAST( CharacterPosition pos, TemplateAST predicate, TemplateAST ifTrueBlock ) {
        this( pos, predicate, FP.option(ifTrueBlock), FP.emptyOption() );
    }

    public IfBlockAST withElseBlock( TemplateAST elseBlock ) {
        return new IfBlockAST( pos, predicate, ifTrueBlock, FP.option(elseBlock) );
    }


    public void walkAST( TemplateASTWalker walker ) {
        walker.walkIfBlockAST( pos, predicate, ifTrueBlock, elseBlock );
    }

    public FPOption<TemplateAST> optimiseSelf( TemplateContext ctx ) {
        FPOption<TemplateAST> optimisedIfTrueBlock = ifTrueBlock.flatMap( ast -> ast.optimiseSelf(ctx) );
        FPOption<TemplateAST> optimisedElseBlock   = elseBlock.flatMap( ast -> ast.optimiseSelf(ctx) );

        if ( optimisedIfTrueBlock.isEmpty() && optimisedElseBlock.isEmpty() ) {
            return FP.emptyOption();
        }

        FPOption<TemplateAST> optimisedIfBlock = predicate.optimiseSelf(ctx).map( optimisedPredicate ->
            (TemplateAST) new IfBlockAST(
                pos,
                optimisedPredicate,
                optimisedIfTrueBlock,
                optimisedElseBlock
            )
        ).or( optimisedElseBlock );

        return internOptimisedVersion( optimisedIfBlock );
    }

    public <T extends TemplateAST> FPIterable<TemplateAST> collectChildrenOfType( Class<T> type ) {
        if ( type.isAssignableFrom(this.getClass()) ) {
            return FP.wrapSingleton( this );
        }

        return predicate.collectChildrenOfType(type)
            .and( ifTrueBlock.map(block -> block.collectChildrenOfType(type)).orElse(FPIterable.empty()) )
            .and( elseBlock.map(block -> block.collectChildrenOfType(type)).orElse(FPIterable.empty()) );
    }
}
