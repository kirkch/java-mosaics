package mosaics.strings.templates;

import mosaics.cache.Cache;
import mosaics.cache.EmptyCache;
import mosaics.strings.StringFormatters;
import mosaics.strings.parser.CharacterIterator;
import mosaics.strings.parser.CharacterIterators;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.parser.ReaderCharacterIterator;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTParser;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.ReflectionUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;


@Value
@AllArgsConstructor
public class TextTemplateParser {

    @With private TemplateASTParser astParser;
    @With private StringFormatters  stringFormatters;

    @With private Cache<String,TextTemplate> cache;


    public TextTemplateParser() {
        this( new TemplateASTParser(), StringFormatters.DEFAULT, new EmptyCache<>() );
    }


    public TextTemplate parse( File templateFile ) {
        try {
            CharacterIterator in = new ReaderCharacterIterator( new FileReader( templateFile ) );

            return new TextTemplateParser().parse( templateFile.getName(), in );
        } catch ( IOException ex ) {
            throw Backdoor.throwException( ex );
        }
    }

    public TextTemplate parse( String templateName, String templateDefinition ) {
        return parse( templateName, CharacterIterators.stringIterator(templateDefinition) );
    }

    public TextTemplate parse( String templateName, Reader templateDefinition ) {
        return parse( templateName, CharacterIterators.readerIterator(templateDefinition) );
    }

    public TextTemplate parse( String templateName, CharacterIterator in ) {
        QA.argNotNull( "templateName", templateName );

        return cache.get( templateName, key -> doParse(templateName, in) );
    }

    private TextTemplate doParse( String templateName, CharacterIterator in ) {
        QA.argNotNull( "templateName", templateName );

        FPOption<TemplateAST> templateAST = astParser.parse( templateName, in );

        TemplateAST ast = templateAST.orElse( () -> new TemplateAST() {
            public CharacterPosition getPos() {
                return new CharacterPosition( "" );
            }

            public void walkAST( TemplateASTWalker walker ) { }

            public FPOption<TemplateAST> optimiseSelf( TemplateContext ctx ) {
                return FP.option(this);
            }
        } );

        return new TextTemplate( ast, stringFormatters );
    }

    public TextTemplate parseResource( String resourcePath ) {
        JavaClass   caller = ReflectionUtils.getCallersClass();
        InputStream in     = caller.getResourceAsStream(resourcePath).get();

        return parse( resourcePath, in );
    }

    private TextTemplate parse( String templateName, InputStream in ) {
        return parse( templateName, new InputStreamReader(in, Backdoor.UTF8) );
    }

}
