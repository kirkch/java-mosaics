package mosaics.strings;

import lombok.SneakyThrows;
import mosaics.lang.TryUtils;

import java.io.IOException;


/**
 * Instances of this interface convert an object to some form of textual form.
 */
public interface PrettyPrinter<T> {
    public void writeTo( T obj, Appendable out ) throws IOException;

    @SneakyThrows
    public default void writeToSilent( T obj, Appendable out ) {
        writeTo( obj, out );
    };

    public default String toString( T obj ) {
        StringBuilder buf = new StringBuilder(400);

        TryUtils.invokeAndRethrowException( () -> writeTo(obj, buf) );

        return buf.toString();
    }
}
