package mosaics.strings.codecs;

import lombok.EqualsAndHashCode;
import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.maps.FPMutableMap;
import mosaics.fp.collections.Tuple2;
import mosaics.lang.ArrayUtils;
import mosaics.lang.QA;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.MalformedValueFailure;
import mosaics.strings.StringCodec;


@EqualsAndHashCode(exclude = {"validValues","validValuesStr"})
public class EnumCodec<T extends Enum<T>> implements StringCodec<T> {
    private final JavaClass              type;
    private final Class<T>               enumClass;

    private final FPMutableMap<String,T> validValues;
    private final String                 validValuesStr;


    public EnumCodec( Class<T> c ) {
        QA.isTrueArg( c.isEnum(), "%s must be an Enum", c );

        this.type           = JavaClass.of(c);
        this.enumClass      = c;
        this.validValues    = FP.wrapArray(c.getEnumConstants()).map(v -> new Tuple2<>(v.name().toLowerCase(),v)).toMap();
        this.validValuesStr = ArrayUtils.toString(c.getEnumConstants(), ",");
    }

    public boolean supportsNull() {
        return false;
    }

    public String encode( T obj ) {
        return obj.name();
    }

    public Tryable<T> decode( String text ) {
        return validValues.get( text.toLowerCase() )
            .map( Try::succeeded )
            .orElse( () -> Try.failed( new MalformedValueFailure(text,validValuesStr) ) );
    }

    public JavaClass getType() {
        return type;
    }
}
