package mosaics.strings.codecs;

import lombok.Value;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import mosaics.utils.SetUtils;

import java.util.Set;


@Value
public class BooleanCodec implements StringCodec<Boolean> {
    public static final BooleanCodec BOOLEAN_PRIMITIVE_CODEC = new BooleanCodec( JavaClass.BOOLEAN_PRIMITIVE );
    public static final BooleanCodec BOOLEAN_OBJECT_CODEC    = new BooleanCodec( JavaClass.BOOLEAN_OBJECT );

    private static final Set<String> TRUE_VALUES = SetUtils.asImmutableSet("true", "t", "y", "yes", "on", "1");


    private final JavaClass type;

    private BooleanCodec( JavaClass type ) {
        this.type = type;
    }


    public boolean supportsNull() {
        return false;
    }

    public String encode( Boolean v ) {
        if ( v == null ) {
            return null;
        }

        return v ? "true" : "false";
    }

    public boolean decode( String v, boolean defaultValue ) {
        if ( isBlank(v) ) {
            return defaultValue;
        }

        return TRUE_VALUES.contains(v.toLowerCase());
    }

    private boolean isBlank( String v ) {
        return v == null || v.trim().length() == 0;
    }

    public Tryable<Boolean> decode( String v ) {
        if ( v == null && type.isObject() ) {
            return Try.succeeded( null );
        }

        boolean r = !isBlank(v) && TRUE_VALUES.contains(v.toLowerCase());

        return Try.succeeded( r );
    }

    public String toString() {
        return "BooleanCodec";
    }
}
