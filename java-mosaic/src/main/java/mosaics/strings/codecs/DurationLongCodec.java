package mosaics.strings.codecs;

import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import lombok.Value;


@Value
public class DurationLongCodec implements StringCodec<Long> {
    public static final StringCodec<Long> INSTANCE = new DurationLongCodec();


    public boolean supportsNull() {
        return false;
    }

    @SuppressWarnings("UnnecessaryUnboxing")
    public String encode( Long v ) {
        if ( v == null ) {
            return "";
        } else if ( v == 0L ) {
            return "0s";
        }

        long numHours   = Math.abs( v.longValue()/(60*60*1000));
        long numMinutes = Math.abs( v.longValue()/1000/60 ) - numHours*60;
        long numSeconds = Math.abs( v.longValue()/1000 ) - numHours*60*60 - numMinutes*60;
        long numMillis  = Math.abs( v.longValue() ) - numHours*60*60*1000 - numMinutes*60*1000 - numSeconds*1000;

        boolean requiresSeparator = false;

        StringBuilder buf = new StringBuilder();

        if ( v < 0 ) {
            buf.append('-');
        }

        if ( numHours > 0 ) {
            buf.append( numHours );
            buf.append( 'h' );

            requiresSeparator = true;
        }

        if ( numMinutes > 0 ) {
            if ( requiresSeparator ) {
                buf.append(' ');
            }

            buf.append( numMinutes );
            buf.append( 'm' );

            requiresSeparator = true;
        }

        if ( numSeconds > 0 || numMillis > 0 ) {
            if ( requiresSeparator ) {
                buf.append(' ');
            }

            buf.append(numSeconds);

            if ( numMillis > 0 ) {
                buf.append('.');

                if ( numMillis < 10 ) {
                    buf.append("00");
                } else if ( numMillis < 100 ) {
                    buf.append('0');
                }

                buf.append(numMillis);
            }

            buf.append("s");
        }

        return buf.toString();
    }

    public Tryable<Long> decode( String v ) {
        if ( v == null ) {
            return Try.failed( new NumberFormatException("For input string: null") );
        }

        return Try.invoke(() -> {
            long millis = 0L;
            long isNeg = v.charAt(0) == '-' ? -1 : 1;

            for ( String part : v.split(" ") ) {
                try {
                    int lastCharIndex = part.length() - 1;
                    double num = Math.abs(Double.parseDouble(part.substring(0, lastCharIndex)));

                    switch ( part.charAt(lastCharIndex) ) {
                        case 'h':
                            millis += ((long) num) * 60 * 60 * 1000;
                            break;
                        case 'm':
                            millis += ((long) num) * 60 * 1000;
                            break;
                        case 's':
                            double x = num * 1000 + 0.5;
                            millis += x;
                            break;
                        default:
                            throw new NumberFormatException("For input string: \"" + v + "\"");
                    }
                } catch ( NumberFormatException ex ) {
                    throw new NumberFormatException("For input string: \"" + v + "\"");
                }
            }

            return millis * isNeg;
        } );
    }

    public JavaClass getType() {
        return JavaClass.LONG_PRIMITIVE;
    }

    public String toString() {
        return "DurationPrimitiveLongCodec";
    }
}
