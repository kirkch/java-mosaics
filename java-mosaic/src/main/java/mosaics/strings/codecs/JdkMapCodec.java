package mosaics.strings.codecs;

import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import mosaics.utils.ComparatorUtils;

import java.util.Map;


@Value
public class JdkMapCodec<K,V> implements StringCodec<Map<K,V>> {
    private JavaClass      type;
    private StringCodec<K> keyCodec;
    private StringCodec<V> valueCodec;

    public String encode( Map<K, V> obj ) {
        if ( obj == null ) {
            return "";
        }


        StringBuilder buf = new StringBuilder();

        buf.append( '{' );

        String separator = "";
        for ( Map.Entry<K,V> entry : FP.wrap(obj.entrySet()).sort( ComparatorUtils.comparing(Map.Entry::getKey)) ) {
            buf.append( separator );
            buf.append( keyCodec.encode(entry.getKey()) );
            buf.append( "=" );
            buf.append( valueCodec.encode(entry.getValue()) );

            separator = ", ";
        }

        buf.append( '}' );

        return buf.toString();
    }

    public Tryable<Map<K, V>> decode( String text ) {
        //todo
        return null;
    }

    public boolean supportsNull() {
        return true;
    }
}
