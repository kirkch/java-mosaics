package mosaics.strings.codecs;

import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import lombok.Value;


@Value
public class JdkStringInstanceCodec implements StringCodec<String> {
    public static final StringCodec<String> INSTANCE = new JdkStringInstanceCodec();


    public boolean supportsNull() {
        return false;
    }

    public String encode( String v ) {
        return v;
    }

    public Tryable<String> decode( String v ) {
        return Try.succeeded(v);
    }

    public JavaClass getType() {
        return JavaClass.STRING;
    }

    public String toString() {
        return "StringCodec";
    }
}
