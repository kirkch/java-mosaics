package mosaics.strings;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.TryUtils;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaMethod;
import mosaics.lang.reflection.ReflectionException;
import mosaics.strings.codecs.BooleanCodec;
import mosaics.strings.codecs.CashCodec;
import mosaics.strings.codecs.CharacterCodec;
import mosaics.strings.codecs.DTMCodecLong;
import mosaics.strings.codecs.DefaultStringCodecFactory;
import mosaics.strings.codecs.DurationLongCodec;
import mosaics.strings.codecs.DynamicStringCodec;
import mosaics.strings.codecs.FPOptionCodec;
import mosaics.strings.codecs.JdkDurationCodec;
import mosaics.strings.codecs.JdkMapCodec;
import mosaics.strings.codecs.JdkOptionalCodec;
import mosaics.strings.codecs.JdkSetCodec;
import mosaics.strings.codecs.JdkStringInstanceCodec;
import mosaics.strings.codecs.MillisCodec;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * A set of objects that know how to parse strings into typed objects.
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class StringCodecs {

    private final Map<String,StringCodec<?>> codecsByFQN;
    private final Map<String,StringCodec<?>> codecsByAlias;



    public StringCodecs() {
        this.codecsByFQN   = new ConcurrentHashMap<>();
        this.codecsByAlias = new ConcurrentHashMap<>();
    }

    public StringCodecs( StringCodecs parent ) {
        this.codecsByAlias = new ConcurrentHashMap<>( parent.codecsByAlias );
        this.codecsByFQN   = new ConcurrentHashMap<>( parent.codecsByFQN );
    }

    public <T> FPOption<StringCodec<T>> getCodecFor( Class<T> c ) {
        return getCodecFor( c.getName(), c );
    }

    @SuppressWarnings("unchecked")
    public FPOption<StringCodec> getCodecFor( JavaClass type ) {
        if ( type.isFPOption() && isClassWithSingleReifiedClassTypeParameter(type) ) {
            JavaClass   innerType  = type.getClassGenerics().firstOrNull();
            StringCodec innerCodec = getCodecFor(innerType).get();

            return FPOption.of( new FPOptionCodec(innerCodec, type) );
        } else if ( type.isJdkOptional() && isClassWithSingleReifiedClassTypeParameter(type) ) {
            JavaClass innerType = type.getClassGenerics().firstOrNull();
            StringCodec innerCodec = getCodecFor( innerType ).get();

            return FPOption.of( new JdkOptionalCodec( type, innerCodec ) );
        } else if ( type.isJdkMap() ) {
            JavaClass   keyType    = type.getClassGenerics().firstOrNull();
            JavaClass   valueType  = type.getClassGenerics().secondOrNull();
            StringCodec keyCodec   = getCodecFor( keyType ).get();
            StringCodec valueCodec = getCodecFor( valueType ).get();

            return FP.option( new JdkMapCodec<>( type, keyCodec, valueCodec ) );
        } else if ( type.isJdkSet() ) {
            JavaClass   valueType  = type.getClassGenerics().firstOrNull();
            StringCodec valueCodec = getCodecFor( valueType ).get();

            return FP.option( new JdkSetCodec<>( type, valueCodec ) );
        } else {
            return getCodecFor( (Class) type.getJdkClass() );
        }
    }

    static boolean isClassWithSingleReifiedClassTypeParameter(JavaClass jc) {
        return jc.getClassGenerics().count() == 1 && jc.getClassGenerics().filterNotNull().count() == 1;
    }

    public <T> StringCodecs withAliasedCodec( String alias, JavaClass type ) {
        return withCustomCodec( alias, getCodecFor(type).first().get() );
    }

    public <T> StringCodec<T> getCodecByAlias( String name ) {
        return (StringCodec<T>) codecsByAlias.get( name );
    }

    public <T> StringCodecs withCodec( StringCodec<T> codec ) {
        return withCodec( (Class<T>) codec.getType().getJdkClass(), codec );
    }

    public <T> StringCodecs withCodec( String shortName, StringCodec<T> codec ) {
        StringCodecs clone = new StringCodecs( this );

        clone.codecsByFQN.put( shortName, codec );
        clone.codecsByFQN.put( codec.getType().getFullName(), codec );

        return clone;
    }

    /**
     * Converts the parameters of a method to type String.  When the method is invoked with string
     * args, those strings will be decoded into their original types and then the original method
     * will be invoked.
     *
     * @return as the original method except all parameters are of type String
     *
     * @throws CodecNotFoundException thrown when any of the parameter types lack a StringCodec
     */
    public JavaMethod stringify( JavaMethod originalMethod ) {
        return StringifiedJavaMethod.stringify( originalMethod, this::getCodecFor );
    }


    private <T> StringCodecs withCustomCodec( String alias, StringCodec<T> codec ) {
        return withCustomCodec( alias, FP.option(codec) );
    }

    private <T> StringCodecs withCustomCodec( String alias, FPOption<StringCodec<T>> codec ) {
        StringCodecs clone = new StringCodecs( this );

        clone.codecsByAlias.put( alias, codec.orNull() );

        return clone;
    }

    private <T> StringCodecs withAliasedCodec( String alias, Class<T> type ) {
        return withCustomCodec( alias, getCodecFor(type) );
    }

    private <T> StringCodecs withCodec( Class<T> type, StringCodec<T> codec ) {
        StringCodecs clone = new StringCodecs( this );

        clone.codecsByFQN.put( type.getName(), codec );

        return clone;
    }

    @SuppressWarnings("unchecked")
    private <T> FPOption<StringCodec<T>> getCodecFor( String typeName, Class c ) {
        try {
            StringCodec<T> codec = (StringCodec<T>) codecsByFQN.computeIfAbsent( typeName, k ->
                TryUtils.invokeAndRethrowException( () -> {
                    JavaClass   type         = JavaClass.of(c);
                    StringCodec dynamicCodec = new DynamicStringCodec(type,this);

                    if ( type.isJdkMap() ) {
                        return new JdkMapCodec<>( type, dynamicCodec, dynamicCodec );
                    } else if ( type.isJdkSet() ) {
                        return new JdkSetCodec<>( type, dynamicCodec );
                    } else {
                        return DefaultStringCodecFactory.createCodecFor( c );
                    }
                } )
            );

            return FP.option( codec );
        } catch ( ReflectionException ex ) {
            return FP.emptyOption();
        }
    }


    // NB has to be declared at the bottom of this file due to ordering issues.. the constructor
    // depends upon the values of some of the static fields.
    public static final StringCodecs DEFAULT_CODECS = new StringCodecs()
        .withCodec( JdkStringInstanceCodec.INSTANCE )
        .withCodec( JdkDurationCodec.INSTANCE )
        .withCodec( BooleanCodec.BOOLEAN_PRIMITIVE_CODEC )
        .withCodec( BooleanCodec.BOOLEAN_OBJECT_CODEC )
        .withCodec( CharacterCodec.CHARACTER_PRIMITIVE_CODEC )
        .withCodec( CharacterCodec.CHARACTER_OBJECT_CODEC )
        .withCodec( Byte.TYPE,       DefaultStringCodecFactory.createCodecFor( JavaClass.BYTE_OBJECT, JavaClass.BYTE_PRIMITIVE, (byte) 0) )
        .withCodec( Short.TYPE,      DefaultStringCodecFactory.createCodecFor( JavaClass.SHORT_OBJECT, JavaClass.SHORT_PRIMITIVE, (short) 0 ) )
        .withCodec( Integer.TYPE,    DefaultStringCodecFactory.createCodecFor( JavaClass.INTEGER_OBJECT, JavaClass.INTEGER_PRIMITIVE, 0 ) )
        .withCodec( Long.TYPE,       DefaultStringCodecFactory.createCodecFor( JavaClass.LONG_OBJECT, JavaClass.LONG_PRIMITIVE, 0L ) )
        .withCodec( Float.TYPE,      DefaultStringCodecFactory.createCodecFor( JavaClass.FLOAT_OBJECT, JavaClass.FLOAT_PRIMITIVE, 0.0f ) )
        .withCodec( Double.TYPE,     DefaultStringCodecFactory.createCodecFor( JavaClass.DOUBLE_OBJECT, JavaClass.DOUBLE_PRIMITIVE, 0.0 ) )
        .withCodec( Byte.class,      DefaultStringCodecFactory.createCodecFor( Byte.class ) )
        .withCodec( Integer.class,   DefaultStringCodecFactory.createCodecFor( Integer.class ) )
        .withCodec( Long.class,      DefaultStringCodecFactory.createCodecFor( Long.class ) )
        .withCodec( Float.class,     DefaultStringCodecFactory.createCodecFor( Float.class ) )
        .withCodec( Double.class,    DefaultStringCodecFactory.createCodecFor( Double.class ) )

        .withAliasedCodec("Boolean",    Boolean.class)
        .withAliasedCodec( "boolean",   Boolean.TYPE )
        .withAliasedCodec( "Character", Character.class )
        .withAliasedCodec( "char",      Character.TYPE )
        .withAliasedCodec( "Byte",      Byte.class )
        .withAliasedCodec( "Short",     Short.class )
        .withAliasedCodec( "short",     Short.TYPE )
        .withAliasedCodec( "Integer",   Integer.class )
        .withAliasedCodec( "int",       Integer.TYPE)
        .withAliasedCodec( "Long",      Long.class )
        .withAliasedCodec( "long",      Long.TYPE )
        .withAliasedCodec( "Float",     Float.class )
        .withAliasedCodec( "float",     Float.TYPE )
        .withAliasedCodec( "Double",    Double.class )
        .withAliasedCodec( "double",    Double.TYPE )
        .withAliasedCodec( "String",    String.class )

        .withCustomCodec("DTM", DTMCodecLong.DEFAULT_CODEC )
        .withCustomCodec("dtm", DTMCodecLong.DEFAULT_CODEC )
//        .withCustomCodec( "1dp", DPCodec.DP1_CODEC )
//        .withCustomCodec( "3dp", DPCodec.DP3_CODEC )
        .withCustomCodec( "millis", MillisCodec.INSTANCE )
        .withCustomCodec( "duration", DurationLongCodec.INSTANCE )

        .withCustomCodec( "cash", CashCodec.INSTANCE )
        .withCustomCodec( "gbp", CashCodec.INSTANCE);

}
