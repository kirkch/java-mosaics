package mosaics.strings;

public interface StringFormatter<T> {
    public String format( T obj );
}
