package mosaics.strings;

public class CodecNotFoundException extends RuntimeException {
    public CodecNotFoundException( String codec ) {
        super( "Unable to find StringCodec: '"+codec+"'" );
    }

    public final int hashCode() {
        return getMessage().hashCode();
    }

    public final boolean equals( Object o ) {
        if ( !(o instanceof CodecNotFoundException) ) {
            return false;
        }

        CodecNotFoundException other = (CodecNotFoundException) o;
        return this.getMessage().equals(other.getMessage());
    }
}
