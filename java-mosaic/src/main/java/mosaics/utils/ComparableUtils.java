package mosaics.utils;

import mosaics.lang.functions.Function1;


public class ComparableUtils {

    public static <E extends Comparable<E>> E min( E a, E b ) {
        if ( a.compareTo(b) <= 0 ) {
            return a;
        } else {
            return b;
        }
    }

    public static <T,E extends Comparable<E>> T min( T a, T b, Function1<T,E> compareByFunc ) {
        if ( compareByFunc.invoke(a).compareTo(compareByFunc.invoke(b)) <= 0 ) {
            return a;
        } else {
            return b;
        }
    }

    public static <E extends Comparable<E>> E max( E a, E b ) {
        if ( a.compareTo(b) >= 0 ) {
            return a;
        } else {
            return b;
        }
    }

    public static <T,E extends Comparable<E>> T max( T a, T b, Function1<T,E> compareByFunc ) {
        if ( compareByFunc.invoke(a).compareTo(compareByFunc.invoke(b)) >= 0 ) {
            return a;
        } else {
            return b;
        }
    }

}
