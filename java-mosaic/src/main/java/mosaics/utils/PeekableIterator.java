package mosaics.utils;

import java.util.Iterator;
import java.util.Optional;


/**
 * An iterator that supports looking at the next value without scrolling on to the next value.
 */
@SuppressWarnings("OptionalGetWithoutIsPresent")
public class PeekableIterator<T> implements Iterator<T> {
    private final Iterator<T> wrappedIterator;
    private Optional<T> next;

    public PeekableIterator( Iterator<T> wrappedIterator ) {
        this.wrappedIterator = wrappedIterator;
        this.next            = updateNext();
    }

    /**
     * Return the next value without changing the state of the iterator.  Only call if hasNext()
     * returns true.
     */
    public T peek() {
        return next.get();
    }

    public boolean hasNext() {
        return next.isPresent();
    }

    public T next() {
        T nextValue = next.get();

        next = updateNext();

        return nextValue;
    }

    private Optional<T> updateNext() {
        return wrappedIterator.hasNext() ? Optional.of(wrappedIterator.next()) : Optional.empty();
    }
}
