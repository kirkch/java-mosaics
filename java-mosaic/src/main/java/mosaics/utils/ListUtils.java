package mosaics.utils;

import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.Tuple2;
import mosaics.lang.ArrayUtils;
import mosaics.lang.QA;
import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function2;
import mosaics.lang.functions.FunctionC;
import mosaics.lang.functions.VoidFunction1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;


@SuppressWarnings("unchecked")
public class ListUtils {

    public static <T> List<T> copyToList( Iterable<T> it ) {
        List<T> list = new ArrayList<>();

        for ( T v : it ) {
            list.add( v );
        }

        return list;
    }

    public static <T> boolean removeFirst( List<T> list, BooleanFunction1 predicate ) {
        ListIterator<T> it = list.listIterator();

        while ( it.hasNext() ) {
            T e = it.next();

            if ( predicate.invoke(e) ) {
                it.remove();

                return true;
            }
        }

        return false;
    }

    public static String toString( List list, String separator ) {
        StringBuilder buf = new StringBuilder(100);

        toString( buf, list, separator );

        return buf.toString();
    }

    public static void toString( StringBuilder buf, List list, String separator ) {
        boolean printSeparator = false;
        for ( Object o : list ) {
            if ( printSeparator ) {
                buf.append( separator );
            } else {
                printSeparator = true;
            }

            buf.append( o );
        }
    }

    public static <T> FPOption<T> firstMatch( List<T> list, Function1<T,Boolean> predicateFunction ) {
        for ( T e : list ) {
            if ( predicateFunction.invoke(e) ) {
                return FPOption.of( e );
            }
        }

        return FPOption.none();
    }

    public static <A,B> FPOption<B> matchAndMapFirstResult( List<A> list, Function1<A,FPOption<B>> matchAndMapFunction ) {
        for ( A e : list ) {
            FPOption<B> r = matchAndMapFunction.invoke(e);

            if ( r.hasValue() ) {
                return r;
            }
        }

        return FPOption.none();
    }

    public static <B> List<B> map( CharSequence str, FunctionC<B> mappingFunction ) {
        List<B> results = new ArrayList<>( str.length() );

        for ( int i=0; i<str.length(); i++ ) {
            results.add( mappingFunction.invoke(str.charAt(i)) );
        }

        return results;
    }

    public static <A,B> List<B> mapCollection( Collection<A> list, Function1<A,B> mappingFunction ) {
        List<B> results = new ArrayList<>( list.size() );

        for ( A v : list ) {
            results.add( mappingFunction.invoke(v) );
        }

        return results;
    }

    public static <A,B> List<B> mapIterable( Iterable<A> list, Function1<A,B> mappingFunction ) {
        List<B> results = new ArrayList<>();

        for ( A v : list ) {
            results.add( mappingFunction.invoke(v) );
        }

        return results;
    }

    public static <T> List<T> flatten( List<T>...lists ) {
        return flatten( Arrays.asList(lists) );
    }

    public static <T, C extends List<T>> List<T> flatten( List<C> list ) {
        List<T> results = new ArrayList<>(list.size()*2);

        if ( list != null ) {
            for ( List<T> sublist : list ) {
                results.addAll( sublist );
            }
        }

        return results;
    }

    public static <T> List<T> filter( List<T> list, BooleanFunction1<T> predicate ) {
        if ( list == null || list.isEmpty() ) {
            return Collections.EMPTY_LIST;
        }

        List<T> results = new ArrayList<>( list.size() );

        for ( T v : list ) {
            if ( predicate.invoke(v) ) {
                results.add(v);
            }
        }

        if ( results.isEmpty() ) {
            return Collections.EMPTY_LIST;
        }

        return results;
    }

    /**
     * Perform op on every value within the list starting with the specified
     * firstValue.  For example, given the list [1,2,3] and the firstValue 0 then
     * the result would be: 0 op 1 op 2 op 3.  If the list was [], then the result
     * would be the first value, which is 0 in this example.
     */
    public static <T,V> V fold( Iterable<T> list, V firstValue, Function2<V,T,V> op ) {
        V valueSoFar = firstValue;

        for ( T e : list ) {
            valueSoFar = op.invoke(valueSoFar, e);
        }

        return valueSoFar;
    }

    public static <T,V> V foldArray( T[] array, V firstValue, Function2<V, T, V> op ) {
        return fold( Arrays.asList(array), firstValue, op );
    }

    public static <T,G> Map<G,List<T>> groupBy( List<T> list, Function1<T,G> groupByValueFunction ) {
        Map<G,List<T>> result = new HashMap();

        if ( list != null ) {
            for ( T v : list ) {
                G groupedBy = groupByValueFunction.invoke(v);

                MapUtils.appendToMultiMap( result, groupedBy, v );
            }
        }

        return result;
    }


    public static <T> int deepHashCode( Iterable<T> it ) {
        return it == null ? 0 : deepHashCode( it.iterator() );
    }

    public static <T> int deepHashCode( Iterator<T> it ) {
        int hashCodeSoFar = 17;

        while ( it.hasNext() ) {
            hashCodeSoFar = 31*hashCodeSoFar + Objects.hashCode( it.next() );
        }

        return hashCodeSoFar;
    }

    public static boolean iterableEquals( Iterable<?> o1, Object o2 ) {
        if ( o1 == o2 ) {
            return true;
        } else if ( !(o2 instanceof Iterable) || o1 == null ) {
            return false;
        }

        Iterable<?> other = (Iterable) o2;
        Iterator<?> it1   = o1.iterator();
        Iterator<?> it2   = other.iterator();

        while ( it1.hasNext() ) {
            if ( !it2.hasNext() ) {
                return false;
            }

            Object a = it1.next();
            Object b = it2.next();

            if ( !Objects.equals(a,b) ) {
                return false;
            }
        }

        return !it2.hasNext();
    }


    /**
     * Groups all of the elements of collection that match the predicate into
     * the first list of the returned Pair.  Everything else goes into the second.
     */
    public static <T> Tuple2<List<T>, List<T>> partition( Iterable<T> collection, BooleanFunction1<T> predicate ) {
        List<T> matches = new ArrayList<T>();
        List<T> misses  = new ArrayList<T>();

        for ( T v : collection ) {
            if ( predicate.invoke(v) ) {
                matches.add(v);
            } else {
                misses.add(v);
            }
        }

        return new Tuple2( matches, misses );
    }

    public static Object toArraySupportsPrimitives( Class type, List list ) {
        int numElements = list.size();

        Object array = Array.newInstance(type, numElements);

        int i=0;
        for ( Object e : list ) {
            Array.set( array, i++, e );
        }

        return array;
    }

    /**
     * NB generics do not support primitives.. consider using toArraySupportsPrimitives()
     */
    public static <T> T[] toArray( Class<T> type, List<T> list ) {
        int numElements = list.size();

        T[] array = ArrayUtils.newArray(type, numElements);

        int i=0;
        for ( T e : list ) {
            array[i++] = e;
        }

        return array;
    }

    public static <T> void permutations( List<T> list, int chooseN, VoidFunction1<List<T>> callback ) {
        permutations( Collections.EMPTY_LIST, list, chooseN, callback );
    }

    private static <T> void permutations( List<T> soFar, List<T> remaining, int chooseN, VoidFunction1<List<T>> callback ) {
        if ( soFar.size() == chooseN || remaining.isEmpty() ) {
            if ( !soFar.isEmpty() ) {
                callback.invoke( soFar );
            }
        } else {
            for ( int i=0; i<remaining.size(); i++ ) {
                List<T> nextSoFar     = new ArrayList<>( soFar );
                List<T> nextRemaining = new ArrayList<>( remaining );

                T nextValue = nextRemaining.remove( i );
                nextSoFar.add( nextValue );

                permutations( nextSoFar, nextRemaining, chooseN, callback );
            }
        }
    }

    public static <T> List<T> asLinkedList( T...elements ) {
        List<T> list = new LinkedList<>();

        Collections.addAll( list, elements );

        return list;
    }

    /**
     * Call action on every element of collection, traversing right to left.
     */
    public static <T> void forEachReversed( List<T> list, VoidFunction1<T> action ) {
        QA.argNotNull(list, "list");

        ListIterator<T> it = list.listIterator(list.size());

        while ( it.hasPrevious() ) {
            T next = it.previous();

            action.invoke( next );
        }
    }

    /**
     * Remove all if the predicate returns true.
     */
    public static <T> void removeAll( List<T> list, BooleanFunction1<T> predicate ) {
        ListIterator<T> it = list.listIterator();

        while ( it.hasNext() ) {
            T v = it.next();

            if ( predicate.invoke(v) ) {
                it.remove();
            }
        }
    }

    public static List<String> ensureSize( List<String> list, int targetSize, String paddingValue ) {
        while ( list.size() < targetSize ) {
            list.add( paddingValue );
        }

        return list;
    }
}
