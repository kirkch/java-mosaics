package mosaics.utils;

import mosaics.lang.Backdoor;
import mosaics.lang.time.DTMUtils;

import java.io.IOException;


public class DurationUtils {

    public static String toStringMillis( long millis ) {
        StringBuilder buf = new StringBuilder();

        appendMillis( millis, buf );

        return buf.toString();
    }

    public static String toStringNanos( long nanos ) {
        StringBuilder buf = new StringBuilder();

        appendNanos( nanos, buf );

        return buf.toString();
    }

    public static void appendMillis( long millis, Appendable out ) {
        try {
            long remainder = millis;
            DurationPP pp = new DurationPP(out);

            remainder = pp.appendUnit( remainder, "d", DTMUtils.DAY_MILLIS );
            remainder = pp.appendUnit( remainder, "h", DTMUtils.HOUR_MILLIS );
            remainder = pp.appendUnit( remainder, "m", DTMUtils.MINUTE_MILLIS );
            remainder = pp.appendUnit( remainder, "s", DTMUtils.SECOND_MILLIS );
            pp.appendUnit( remainder, "ms", 1 );

            pp.done("ms");
        } catch ( IOException ex ) {
            throw Backdoor.throwException( ex );
        }
    }

    public static void appendNanos( long nanos, Appendable out ) {
        try {
            long remainder = nanos;
            DurationPP pp = new DurationPP(out);

            remainder = pp.appendUnit( remainder, "d", DTMUtils.DAY_MILLIS*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND );
            remainder = pp.appendUnit( remainder, "h", DTMUtils.HOUR_MILLIS*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND );
            remainder = pp.appendUnit( remainder, "m", DTMUtils.MINUTE_MILLIS*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND );
            remainder = pp.appendUnit( remainder, "s", DTMUtils.SECOND_MILLIS*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND );
            remainder = pp.appendUnit( remainder, "ms", Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND );
            pp.appendUnit( remainder, "ns", 1 );

            pp.done("ns");
        } catch ( IOException ex ) {
            throw Backdoor.throwException( ex );
        }
    }


    private static class DurationPP {
        private Appendable out;
        private String     separator = "";

        public DurationPP( Appendable out ) {
            this.out = out;
        }

        public long appendUnit( long remainder, String unitLabel, long quantityPerUnit ) throws IOException {
            long unitCount    = remainder / quantityPerUnit;
            long newRemainder = remainder - unitCount * quantityPerUnit;

            if ( unitCount > 0 ) {
                out.append( separator );
                out.append( Long.toString( unitCount ) );
                out.append( unitLabel );

                separator = " ";
            }

            return newRemainder;
        }

        public void done( String unit ) throws IOException {
            if ( separator.length() == 0 ) {
                out.append( "0" );
                out.append( unit );
            }
        }
    }
}
