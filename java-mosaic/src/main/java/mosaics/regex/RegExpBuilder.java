package mosaics.regex;

import lombok.AllArgsConstructor;

import java.util.regex.Pattern;


/**
 * A convenience helper for creating RegExps.
 */
public class RegExpBuilder {

    public static RegExpFragment escape( String txt ) {
        return new RegExpFragment( Pattern.quote(txt) );
    }

    public static RegExpFragment re( String txt ) {
        return new RegExpFragment(txt);
    }

    public static RegExp createRegExp( RegExpFragment...fragments ) {
        StringBuilder buf = new StringBuilder();

        for ( RegExpFragment f : fragments ) {
            f.appendTo( buf );
        }

        return new RegExp( buf.toString() );
    }


    private StringBuilder buf = new StringBuilder();


    public RegExpBuilder appendRegExp( String pattern ) {
        buf.append( pattern );

        return this;
    }

    public RegExpBuilder appendStatic( String text ) {
        buf.append( Pattern.quote(text) );

        return this;
    }

    public RegExp build() {
        String pattern = buf.toString();

        buf.setLength( 0 );

        return new RegExp( pattern );
    }


    @AllArgsConstructor
    public static class RegExpFragment {
        private String txt;

        public void appendTo( StringBuilder buf ) {
            buf.append( txt );
        }
    }


}
