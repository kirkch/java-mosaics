package mosaics.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import mosaics.json.gson.AnnotatedGsonCodecTypeAdapterFactory;
import mosaics.json.gson.ClassNamePolymorphicGsonTypeAdapter;
import mosaics.json.gson.FPTypeFactory;
import mosaics.json.gson.Jdk15RecordTypeFactory;
import mosaics.json.gson.JdkTypeFactory;
import mosaics.json.gson.MappedPolymorphicGsonTypeAdapter;
import mosaics.lang.Backdoor;
import net.dongliu.gson.GsonJava8TypeAdapterFactory;

import java.io.IOException;
import java.util.Map;

import static mosaics.json.gson.PostProcessingTypeAdapterFactory.defaultValuePostProcessor;
import static mosaics.lang.Backdoor.cast;


@SuppressWarnings("ConstantConditions")
public class JsonCodecBuilder {
    private GsonBuilder gsonBuilder = createBaseGsonBuild();


    public JsonCodecBuilder identifyInheritanceTreeByClassName( Class<?> baseType ) {
        this.gsonBuilder = gsonBuilder.registerTypeAdapter( baseType, new ClassNamePolymorphicGsonTypeAdapter<>() );

        return this;
    }

    public JsonCodecBuilder identifyInheritanceTreeByClassName( Class<?> baseType, String propertyName ) {
        ClassNamePolymorphicGsonTypeAdapter<Object> typeAdapter = new ClassNamePolymorphicGsonTypeAdapter<>()
            .withJsonPropertyName( propertyName );

        this.gsonBuilder = gsonBuilder.registerTypeAdapter( baseType, typeAdapter );

        return this;
    }

    @SuppressWarnings({"unchecked","rawtypes"})
    public JsonCodecBuilder identifyInheritanceTreeByPreRegisteredIdentifiers( Class<?> baseType, Map<Class,String> mappings ) {
        this.gsonBuilder = gsonBuilder.registerTypeAdapter( baseType, new MappedPolymorphicGsonTypeAdapter(mappings) );

        return this;
    }

    @SuppressWarnings({"unchecked","rawtypes"})
    public JsonCodecBuilder identifyInheritanceTreeByPreRegisteredIdentifiers( Class<?> baseType, String propertyName, Map<Class,String> mappings ) {
        MappedPolymorphicGsonTypeAdapter typeAdapter = new MappedPolymorphicGsonTypeAdapter( mappings )
            .withTypeJsonPropertyName( propertyName );

        this.gsonBuilder = gsonBuilder.registerTypeAdapter( baseType, typeAdapter );

        return this;
    }

    public JsonCodecBuilder withPrettyPrinting() {
        this.gsonBuilder = gsonBuilder.setPrettyPrinting();

        return this;
    }


    public JsonCodec create() {
        return new JsonCodec( gsonBuilder.create() );
    }


    static GsonBuilder createBaseGsonBuild() {
        // NB When Gson searches these factories, it reverses the list.  So it starts at the bottom and
        // works upwards of the order declared here.
        return new GsonBuilder()
            .registerTypeAdapterFactory( defaultValuePostProcessor() )
            .registerTypeAdapterFactory( new JdkTypeFactory() )
            .registerTypeAdapterFactory( new GsonJava8TypeAdapterFactory() )
            .registerTypeAdapterFactory( new Jdk15RecordTypeFactory() )
            .registerTypeAdapterFactory( new FPTypeFactory() )
            .registerTypeAdapterFactory( new AnnotatedGsonCodecTypeAdapterFactory() )
            .registerTypeAdapterFactory( new TypeAdapterFactory() {
                public <T> TypeAdapter<T> create( Gson gson, TypeToken<T> type ) {
                    if ( !type.toString().startsWith("java.lang.Class") ) {
                        return null;
                    }

                    return cast(new TypeAdapter<Class<?>>() {
                        public void write( JsonWriter out, Class value ) throws IOException {
                            if ( value == null ) {
                                out.nullValue();
                            } else {
                                out.value( value.getName() );
                            }
                        }

                        public Class<?> read( JsonReader in ) throws IOException {
                            if ( in.peek() == JsonToken.NULL ) {
                                return null;
                            }

                            String fqn = in.nextString();

                            try {
                                return Class.forName( fqn );
                            } catch ( ClassNotFoundException e ) {
                                throw Backdoor.throwException( e );
                            }
                        }
                    });
                }
            } )
            .disableHtmlEscaping();
    }
}
