package mosaics.json.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import lombok.Value;
import mosaics.cache.Cache;
import mosaics.cache.PermCache;
import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Factory;
import mosaics.lang.functions.VoidFunction1;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaField;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;


/**
 * Run actions on pojos after they have just been created by Gson.
 */
public class PostProcessingTypeAdapterFactory implements TypeAdapterFactory {
    public static TypeAdapterFactory defaultValuePostProcessor() {
        return new PostProcessingTypeAdapterFactory(
            new DefaultNullFieldsAction()
        );
    }


    private final List<VoidFunction1<Object>> postProcessors;

    @SafeVarargs
    private PostProcessingTypeAdapterFactory( VoidFunction1<Object>...postProcessors ) {
        this.postProcessors = Arrays.asList(postProcessors);
    }


    public <T> TypeAdapter<T> create( Gson gson, TypeToken<T> typeToken ) {
        TypeAdapter<T> delegateAdapter = gson.getDelegateAdapter(this, typeToken);

        return new TypeAdapter<>() {
            public void write( JsonWriter out, T value ) throws IOException {
                delegateAdapter.write(out, value);
            }

            public T read( JsonReader in ) throws IOException {
                T value = delegateAdapter.read(in);

                postProcessors.forEach( p -> p.invoke(value) );

                return value;
            }
        };
    }


    /**
     * Scans the fields on a pojo for a null value.  When a null value is found for a supported
     * type, then this action will set the field to a default value.
     */
    private final static class DefaultNullFieldsAction implements VoidFunction1<Object> {
        private final Cache<Class<?>, List<EnsureFieldHasValueAction>> fieldsToDefaultCache = new PermCache<>();

        public void invoke( Object pojo ) {
            if ( pojo == null ) {
                return;
            }

            getFieldActionsFor(pojo).forEach( action -> action.invoke(pojo) );
        }

        private List<EnsureFieldHasValueAction> getFieldActionsFor( Object o ) {
            return fieldsToDefaultCache.get( o.getClass(), this::createFieldActionsFor );
        }

        private List<EnsureFieldHasValueAction> createFieldActionsFor( Class<?> clazz ) {
            return JavaClass.of(clazz).getAllInstanceFields()
                .filterNot( f -> f.getType().isPrimitive() )
                .flatMap( this::createDefaultFieldActionFor )
                .toList();
        }

        private FPOption<EnsureFieldHasValueAction> createDefaultFieldActionFor( JavaField f ) {
            return createDefaultValueFactoryFor(f).map(s -> new EnsureFieldHasValueAction(f,s));
        }

        /**
         * For the specified field, returns a factory method that will create a 'default value' for
         * that field.  For example, a field of type List will receive an an instance of an empty list.<p/>
         *
         * If we do not know of a suitable default value for the field, then no factory method will
         * be returned.
         */
        private FPOption<Factory<Object>> createDefaultValueFactoryFor( JavaField f ) {
            return f.getType().getDefaultValueFactory();
        }

        /**
         * Ensure that a specified field on the newly created pojo is not null.  If it is found
         * to be null then the fields value will be set to a default value.
         */
        @Value
        private static class EnsureFieldHasValueAction implements VoidFunction1<Object> {
            private JavaField       field;
            private Factory<Object> newObjectFactory;

            public void invoke( Object arg ) {
                if ( field.getValueFrom(arg) == null ) {
                    Object newValue = newObjectFactory.invoke();

                    field.setValueOn( arg, newValue );
                }
            }
        }
    }
}
