package mosaics.json.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import lombok.SneakyThrows;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaRecord;
import mosaics.lang.reflection.JavaRecordPart;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Jdk15RecordTypeFactory implements TypeAdapterFactory {
    public <T> TypeAdapter<T> create( Gson gson, TypeToken<T> typeToken ) {
        try {
            JavaClass jc = JavaClass.of( typeToken.getType() );

            if ( jc.isNotRecord() ) {
                return null;
            }

            return new RecordTypeAdapter<>( gson, jc );
        } catch ( UnsupportedOperationException ex ) {
            return null;  // this can happen if JavaClass.of() does not recognise the Type being past in
        }
    }

    private static class RecordTypeAdapter<T> extends TypeAdapter<T> {
        private final Gson       gson;
        private final JavaRecord javaRecord;

        public RecordTypeAdapter( Gson gson, JavaClass rawType ) {
            this.gson       = gson;
            this.javaRecord = JavaRecord.of( rawType );
        }

        @SneakyThrows
        public void write( JsonWriter out, T value ) throws IOException {
            out.beginObject();

            for ( JavaRecordPart part : javaRecord ) {
                Object fieldValue = part.getValueFrom( value );

                writeValueTo( out, part, fieldValue );
            }

            out.endObject();
        }

        @SneakyThrows
        public T read( JsonReader in ) throws IOException {
            Map<String, Object> values = readJsonInAsAMap( in );

            return javaRecord.createInstance( values );
        }

        private void writeValueTo( JsonWriter out, JavaRecordPart part, Object v ) throws IOException {
            if ( v != null ) {
                TypeAdapter<Object> adapter = getAdapterFor( part );

                out.name( part.getName() );
                adapter.write( out, v );
            }
        }

        private Map<String, Object> readJsonInAsAMap( JsonReader in ) throws IOException {
            in.beginObject();

            Map<String, Object> results = new HashMap<>();
            while ( in.hasNext() ) {
                String                   label                = in.nextName();
                FPOption<JavaRecordPart> javaRecordPartOption = javaRecord.getJavaRecordPart( label );

                if ( javaRecordPartOption.hasValue() ) {
                    Object value = readInNextValue( in, javaRecordPartOption.get() );

                    if ( value != null ) {
                        results.put( label, value );
                    }
                }
            }

            in.endObject();

            return results;
        }

        private Object readInNextValue( JsonReader in, JavaRecordPart javaRecordPart ) throws IOException {
            TypeAdapter<?> typeAdapter = getAdapterFor( javaRecordPart );

            return typeAdapter.read( in );
        }

        private TypeAdapter<Object> getAdapterFor( JavaRecordPart part ) {
            return part.getComponentPartType().isRecord() ? new RecordTypeAdapter<>( gson, part.getComponentPartType() ) : Backdoor.cast( gson.getAdapter( TypeToken.get( part.getJdkType() ) ) );
        }
    }
}