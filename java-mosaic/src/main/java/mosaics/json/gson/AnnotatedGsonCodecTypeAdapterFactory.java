package mosaics.json.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import mosaics.fp.collections.FPOption;
import mosaics.json.GsonCodec;
import mosaics.lang.reflection.JavaClass;

import static mosaics.lang.Backdoor.cast;


public class AnnotatedGsonCodecTypeAdapterFactory implements TypeAdapterFactory {
    public <T> TypeAdapter<T> create( Gson gson, TypeToken<T> typeToken ) {
        JavaClass jc = JavaClass.of(typeToken.getRawType());

        FPOption<GsonCodec> annotation = jc.getAnnotation( GsonCodec.class );

        return cast(
            annotation.ifElse(
                this::fetchTypeAdapterFor,
                () -> null
            ).orNull()
        );
    }

    private TypeAdapter fetchTypeAdapterFor( GsonCodec gsonCodec ) {
        JavaClass jc = JavaClass.of( gsonCodec.value() );

        return jc.newInstance();
    }
}
