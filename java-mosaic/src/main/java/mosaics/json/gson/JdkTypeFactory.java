package mosaics.json.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.internal.bind.TreeTypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import mosaics.lang.reflection.JavaClass;

import java.io.IOException;


public class JdkTypeFactory implements TypeAdapterFactory {

    @Override
    @SuppressWarnings("unchecked")
    public <T> TypeAdapter<T> create( Gson gson, TypeToken<T> typeToken) {
        Class<T> rawType = (Class<T>) typeToken.getRawType();

        if ( !JavaClass.of(rawType).isPartOfTheCoreJavaLibs() && (rawType.isSealed() || rawType.isInterface()) ) {
            return TreeTypeAdapter.newFactoryWithMatchRawType(typeToken, new ClassNamePolymorphicGsonTypeAdapter<>()).create( gson, typeToken );
        } else {
            return null;
        }
    }

}
