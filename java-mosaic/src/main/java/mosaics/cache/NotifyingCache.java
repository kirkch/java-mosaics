package mosaics.cache;

import lombok.AllArgsConstructor;
import mosaics.fp.collections.FPOption;
import mosaics.lang.clock.Clock;


@AllArgsConstructor
public class NotifyingCache<K,V> implements Cache<K,V> {
    private final Clock            clock;
    private final Cache<K,V>       wrappedCache;
    private final CacheListener<K> cacheListener;


    public FPOption<V> get( K key ) {
        long startNanos = clock.currentTimeNanos();
        FPOption<V> result = wrappedCache.get( key );

        double durationMillis = clock.currentDurationNanosAsMillis(startNanos);

        result.ifPresent( v -> cacheListener.cacheHit(key,durationMillis) );
        result.ifEmpty( () -> cacheListener.cacheMiss(key,durationMillis) );

        return result;
    }

    public void remove( K key ) {
        double durationMillis = clock.timeMillis( () -> wrappedCache.remove(key) );

        cacheListener.valueRemoved( key, durationMillis );
    }

    public void put( K key, V value ) {
        double durationMillis = clock.timeMillis( () -> wrappedCache.put(key, value) );

        cacheListener.valuePut( key, durationMillis );
    }

    public void clear() {
        double durationMillis = clock.timeMillis( wrappedCache::clear );

        cacheListener.cacheCleared( durationMillis );
    }

    public int size() {
        long startNanos = clock.currentTimeNanos();
        int  cacheSize  = wrappedCache.size();

        double durationMillis = clock.currentDurationNanosAsMillis( startNanos );

        cacheListener.cacheSize( cacheSize, durationMillis );

        return cacheSize;
    }
}
