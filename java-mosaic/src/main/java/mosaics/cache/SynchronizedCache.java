package mosaics.cache;

import mosaics.fp.collections.FPOption;


public class SynchronizedCache<K,V> implements Cache<K,V> {
    private Cache<K,V> wrappedCache;


    public SynchronizedCache() {
        this( new PermCache<>() );
    }

    public SynchronizedCache( Cache<K, V> wrappedCache ) {
        this.wrappedCache = wrappedCache;
    }


    public synchronized FPOption<V> get( K key ) {
        return wrappedCache.get( key );
    }

    public synchronized void remove( K key ) {
        wrappedCache.remove( key );
    }

    public synchronized void put( K key, V value ) {
        wrappedCache.put( key, value );
    }

    public synchronized void clear() {
        wrappedCache.clear();
    }

    public synchronized int size() {
        return wrappedCache.size();
    }
}
