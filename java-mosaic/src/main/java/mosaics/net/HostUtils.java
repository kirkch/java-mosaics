package mosaics.net;

import mosaics.fp.FP;
import mosaics.fp.LazyVal;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.threads.ThreadUtils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.UUID;


public class HostUtils {

    private static String JVM_RUNTIME_INSTANCE_ID = UUID.randomUUID().toString();

    private static LazyVal<FPOption<String>> HOSTNAME_REF = new LazyVal<>( () -> {
        try {
            return FP.option(InetAddress.getLocalHost().getHostName());
        } catch ( UnknownHostException ex ) {
            return FP.emptyOption();
        }
    } );

    private static LazyVal<FPOption<String>> HOSTIP_REF = new LazyVal<>( () -> {
        try {
            return FP.option(InetAddress.getLocalHost().getHostAddress());
        } catch ( UnknownHostException ex ) {
            return FP.emptyOption();
        }
    } );


    public static URI toURI( String uri ) {
        return Backdoor.invokeAndRethrowException(() -> new URI(uri));
    }

    public static URL toURL( String uri ) {
        return Backdoor.invokeAndRethrowException(() -> new URL(uri));
    }

    public static String getRuntimeJvmId() {
        return JVM_RUNTIME_INSTANCE_ID;
    }

    public static FPOption<String> getHostName() {
        return HOSTNAME_REF.fetch();
    }

    public static FPOption<String> getHostIp() {
        return HOSTIP_REF.fetch();
    }

//    public static void spinUntilAvailable( String host, int port ) {
//        spinUntilAvailable( 60_000, 1_000, host, port );
//    }

    public static void spinUntilAvailable( long timeoutMillis, long retryIntervalMillis, String host, int port ) {
        ThreadUtils.spinUntilTrue( timeoutMillis, retryIntervalMillis, () -> HostUtils.isAvailable(host,port) );
    }

    public static boolean isAvailable( String host, int port ) {
        try {
            new Socket(host, port).close();

            return true;
        } catch ( IOException e ) {
            return false;
        }
    }

//    public static int findUnusedLocalPort() throws IOException {
//        try (ServerSocket serverSocket = new ServerSocket(0)) {
//            return serverSocket.getLocalPort();
//        }
//    }
//
//    public static int selectRandomPort() {
//        try ( ServerSocket s = new ServerSocket(0) ) {
//            return s.getLocalPort();
//        } catch ( IOException ex ) {
//            throw Backdoor.throwException( ex );
//        }
//    }
//
//    public static int[] selectTwoRandomPorts() {
//        try (
//            ServerSocket s1 = new ServerSocket( 0 );
//            ServerSocket s2 = new ServerSocket( 0 )
//        ) {
//            return new int[] {s1.getLocalPort(), s2.getLocalPort()};
//        } catch ( IOException ex ) {
//            throw Backdoor.throwException( ex );
//        }
//    }
//
//    public static int[] selectThreeRandomPorts() {
//        try (
//            ServerSocket s1 = new ServerSocket( 0 );
//            ServerSocket s2 = new ServerSocket( 0 );
//            ServerSocket s3 = new ServerSocket( 0 )
//        ) {
//            return new int[] {s1.getLocalPort(), s2.getLocalPort(), s3.getLocalPort()};
//        } catch ( IOException ex ) {
//            throw Backdoor.throwException( ex );
//        }
//    }

}
