package mosaics.xml.ast;

import mosaics.fp.collections.FPOption;
import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.VoidFunction2;
import lombok.NonNull;
import lombok.Value;


/**
 * Represents an XML tag's attribute (key/value pair).
 *
 * @param <R> the type of the source POJO (which contains the attribute)
 */
@Value
public class XmlAttributeAST<R> implements XmlAttrAST<R> {

    @NonNull private String              attributeName;
    @NonNull private Function1<R,String> attributeValueFetcher;


    public boolean hasContents( R pojo ) {
        return fetchAttributeValue(pojo).hasValue();
    }

    public void writeTo( R pojo, VoidFunction2<Object,TagWriter> nestedSerializer, TagWriter out ) {
        FPOption<String> attributeValue = fetchAttributeValue(pojo);

        attributeValue.ifPresent( v -> out.attr(attributeName,v) );
    }


    private FPOption<String> fetchAttributeValue( R pojo ) {
        String attrValue = attributeValueFetcher.invoke( pojo );

        return FPOption.ofBlankableString( attrValue );
    }

}
