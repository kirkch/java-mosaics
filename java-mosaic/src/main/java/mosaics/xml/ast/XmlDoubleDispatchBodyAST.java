package mosaics.xml.ast;

import mosaics.fp.collections.FPOption;
import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.VoidFunction2;
import mosaics.lang.reflection.JavaProperty;
import mosaics.lang.reflection.ReflectionUtils;
import lombok.Value;


@Value
public class XmlDoubleDispatchBodyAST<S,T> implements XmlBodyAST<S> {

    private JavaProperty<S,T> property;


    public boolean hasContents( S pojo ) {
        FPOption<T> valueFrom = property.getValueFrom( pojo );

        return valueFrom.map(ReflectionUtils::hasValue).orElse( false );
    }

    public void writeTo( S pojo, VoidFunction2<Object, TagWriter> nestedSerializer, TagWriter out ) {
        property.getValueFrom(pojo).ifPresent( v -> {
            nestedSerializer.invoke(v,out);
        } );
    }

}
