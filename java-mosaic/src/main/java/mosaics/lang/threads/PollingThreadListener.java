package mosaics.lang.threads;

import mosaics.fp.Failure;


public interface PollingThreadListener<T> {
    public void threadStarted();

    public void objectReceived( T o );

    public void pollFailure( Failure f );
    public void dispatchFailure( T o, Failure f );

    public void threadFinished();
}
