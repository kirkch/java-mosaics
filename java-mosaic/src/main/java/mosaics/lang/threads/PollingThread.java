package mosaics.lang.threads;

import mosaics.fp.ExceptionFailure;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.VoidFunctionL;
import mosaics.lang.lifecycle.Subscription;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.IntToLongFunction;



public class PollingThread {

    public static <T> Subscription createPollingDaemonThread(
        String                               pollingThreadName,
        Function0<FPOption<T>> pollF,
        PollingThreadListener<T>             callbacks
    ) {
        return createPollingDaemonThread(
            pollingThreadName,
            pollF,
            callbacks,
            RetryDelay::exponentialBackoffSeries,
            Backdoor::sleep,
            10
        );
    }

    /**
     * Creates a thread that will dispatch a value after it detects that a value has arrived.  The
     * thread will spin on pollF asking it for a value, if there is no value then the polling thread
     * will sleep for pollingIntervalMillis.  If on the other hand there is a value, then it will
     * invoke the dispatcherF and then immediately try to fetch a new value.  The thread will continue
     * polling until the Subscription is cancelled.
     */
    public static <T> Subscription createPollingDaemonThread(
        String                               pollingThreadName,
        Function0<FPOption<T>>               pollF,
        PollingThreadListener<T>             callbacks,
        IntToLongFunction                    calcRetryDelayF,
        VoidFunctionL                        sleepF,
        long                                 pollingIntervalMillis
    ) {
        AtomicBoolean requestShutdownFlag = new AtomicBoolean();

        Thread pollingThread = new DaemonThread( pollingThreadName ) {
            public void run() {
                callbacks.threadStarted();

                try {
                    int attemptCount = 0;

                    while ( !requestShutdownFlag.get() ) {
                        FPOption<T> pollResult;

                        try {
                            pollResult = pollF.invoke();

                            if ( pollResult.hasValue() ) {
                                try {
                                    callbacks.objectReceived( pollResult.get() );

                                    attemptCount = 0;
                                } catch ( Exception ex ) {
                                    try {
                                        callbacks.dispatchFailure( pollResult.get(), new ExceptionFailure( ex ) );
                                    } catch ( Exception ex2 ) {
                                        // ignore as one exception has already been reported
                                    }

                                    pollResult = FP.emptyOption();

                                    attemptCount++;
                                }
                            } else {
                                sleepF.invoke( pollingIntervalMillis );

                                attemptCount = 0;
                            }
                        } catch ( Exception ex ) {
                            try {
                                callbacks.pollFailure( new ExceptionFailure( ex ) );
                            } catch ( Exception ex2 ) {
                                // ignore as one exception has already been reported
                            }

                            pollResult = FP.emptyOption();

                            attemptCount++;
                        }

                        if ( attemptCount == 0 && pollResult.isEmpty() ) {
                            sleepFor( pollingIntervalMillis, sleepF );
                        } else if ( attemptCount > 0 ) {
                            long errorDelayMillis = calcRetryDelayF.applyAsLong( attemptCount );
                            if ( errorDelayMillis > 0 ) {
                                sleepFor( errorDelayMillis, sleepF );
                            }
                        }
                    }
                } finally {
                    callbacks.threadFinished();
                }
            }

            private void sleepFor( long errorDelayMillis, VoidFunctionL sleepF ) {
                try {
                    sleepF.invoke( errorDelayMillis );
                } catch ( Throwable ex ) {

                }
            }
        };

        pollingThread.start();

        return new Subscription(
            () -> {
                requestShutdownFlag.set(true);

                ThreadUtils.spinUntilTrue( 60_000, () -> !pollingThread.isAlive() );
            },
            pollingThread::isAlive
        );
    }

}
