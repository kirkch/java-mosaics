package mosaics.lang;


/**
 * Unable to separate two candidate matches.  Based upon a matching criteria, two candidate matches
 * are equally valid.
 */
public class AmbiguousMatchException extends RuntimeException {

    private Object criteria;
    private Object match1;
    private Object match2;


    public AmbiguousMatchException( Object criteria, Object match1, Object match2 ) {
        this( "Ambiguous match.  '"+criteria+"' is unable to separate '"+match1+"' from '"+match2+"'" );

        this.criteria = criteria;
        this.match1   = match1;
        this.match2   = match2;
    }

    public AmbiguousMatchException( String message ) {
        super( message );
    }

    public AmbiguousMatchException( String message, Throwable cause ) {
        super( message, cause );
    }

    public Object getMatchCriteria() {
        return criteria;
    }

    public Object getMatch1() {
        return match1;
    }

    public Object getMatch2() {
        return match2;
    }
}
