package mosaics.lang.reflection;

import mosaics.strings.StringUtils;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Function0WithThrows;
import mosaics.lang.functions.Function2;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static mosaics.lang.Backdoor.cast;


/**
 * Represents a Java Bean Property.  It offers the ability to get, set and chain property values
 * together.
 */
public abstract class JavaProperty<S,T> {
    
    public static <T> JavaProperty<T,T> wrap( JavaClass type, String rootName ) {
        return new RootProperty<>( type, rootName );
    }

    static String dropGetterLabelPrefixFrom( String name ) {
        return StringUtils.lowercaseFirstLetter(StringUtils.dropWhile(name, Character::isLowerCase));
    }


    @SuppressWarnings("StaticInitializerReferencesSubClass")
    private static final FPList<Function2<JavaClass,String,FPOption<JavaProperty>>> SEARCH_PATH = FPList.wrapArray(
        MethodJavaProperty::fetchPropertyFrom,
        (type,propertyName) -> MethodJavaProperty.fetchPropertyFrom(type, "get"+capitalize(propertyName)),
        (type,propertyName) -> MethodJavaProperty.fetchPropertyFrom(type, "is"+capitalize(propertyName)),
        (type,propertyName) -> MethodJavaProperty.fetchPropertyFrom(type, "has"+capitalize(propertyName)),
        FieldJavaProperty::fetchPropertyFrom
    );


    static <A,B> FPOption<JavaProperty<A,B>> getPropertyFor( Class<A> objectType, String propertyName ) {
        return getPropertyFor( JavaClass.of(objectType), propertyName );
    }

    @SuppressWarnings("unchecked")
    static <A,B> FPOption<JavaProperty<A,B>> getPropertyFor( JavaClass objectType, String propertyName ) {
        if ( objectType.isOptional() ) {
            return getPropertyFor( objectType.getClassGenerics().first().orElse(JavaClass.OBJECT), propertyName );
        } else if ( propertyName.contains(".") ) {
            return cast(
                FP.wrapArray(propertyName.split("\\."))
                    .foldWhile(FP.option(JavaProperty.wrap(objectType,"root")), JavaProperty::getProperty)
            );
        } else {
            return cast( SEARCH_PATH.firstOpt( f -> f.invoke( objectType, propertyName ) ) );
        }
    }


    public abstract String getName();

    /**
     * The type of the object that this property is declared upon.
     */
    public abstract JavaClass getSourceType();

    /**
     * The type of the value stored within this property.
     */
    public abstract JavaClass getValueType();

    public Class<?> getJdkValueType() {
        return getValueType().getJdkClass();
    }
//    public TypeMeta getTypeMeta() {
//        JavaClass valueType = getValueType();
//
//        String[] generics = Arrays.stream( valueType.getGenerics() )
//            .map(JavaClass::getFullName)
//            .toArray( String[]::new );
//
//        return new TypeMeta( valueType.toJDK(), generics );
//    }

    public abstract FPOption<T> getValueFrom( S o );

    public abstract boolean isPublic();

    public abstract boolean hasSetter();

    public abstract boolean isDirectFieldAccessEnabled();

    /**
     * By default properties will aim to use setter methods.  However when a setter method is not available
     * should setValueOn error or go direct to the underlying field if one exists?  It breaks encapsulation
     * to go direct to the field, so the default is to error however it can be useful (if naughty) to
     * let us bypass and go direct.  That is the purpose of this method.
     */
    public abstract JavaProperty<S,T> withDirectFieldAccessEnabled( boolean f );

    public abstract void setValueOn( S o, T v );

    public abstract void unsetOn( S o );

    public abstract BoundJavaProperty<T> bindTo( S o );

    public abstract FPOption<JavaDoc> getJavaDoc();

    public abstract <A extends Annotation> FPOption<A> getAnnotation( Class<A> annotationType );

    public boolean isSetOn( S o ) {
        return getValueFrom( o ).hasValue();
    }


    /**
     * Returns true if any of the properties within the chain of properties are of type Option or
     * Optional.
     */
    public abstract boolean isOptional();

    public boolean isMandatory() {
        return !isOptional();
    }

    /**
     * If this property contains no value then set it to a default value.  Where the default value is
     * given by the values no arg constructor.
     */
    public void init( S o ) {
        JavaClass valueType = getValueType();

        if ( valueType.isPrimitive() || valueType.isEnum() || getValueFrom(o).hasValue() ) {
            return;
        } else if ( valueType.isFPOption() || valueType.isOptional() ) {
            if ( getValueFrom(o).hasValue() || valueType.getClassGenerics().count() == 0 ) {
                return;
            }

            JavaClass genericType = valueType.getClassGenerics().first().orElse( JavaClass.OBJECT );
            Object newValue = genericType.newInstance();

            setValueOn( o, cast(newValue) );
        } else if ( hasSetter() ) {
            T newValue;

            if (valueType.hasZeroArgConstructor()) {
                newValue = valueType.newInstance();
            } else if ( isDirectFieldAccessEnabled() ) {
                newValue = valueType.allocateInstance();
            } else {
                throw new MissingConstructorException( valueType );
            }

            setValueOn( o, newValue );
        }
    }

    public T getValueOrElseFrom( S o, T defaultValue ) {
        return getValueFrom(o).orElse( defaultValue );
    }

    public T getValueOrElseFrom( S o, Function0WithThrows<T> defaultValueFetcher ) {
        return getValueFrom(o).orElse( defaultValueFetcher );
    }

    public <T2> FPOption<JavaProperty<S,T2>> getProperty( String nextPropertyName ) {
        FPOption<JavaProperty<T,T2>> nextPropertyOpt = getPropertyFor( getValueType(), nextPropertyName );

        return nextPropertyOpt.map( nextProperty -> new ChainedProperty<>(this, nextProperty) );
    }

    public <T2> FPOption<JavaProperty<S,T2>> getProperty( JavaClass expectedPropertyType, String nextPropertyName ) {
        return getProperty( nextPropertyName );
    }

    public <T2> FPOption<JavaProperty<S,T2>> getProperty( Class<T2> expectedPropertyType, String nextPropertyName ) {
        return getProperty( nextPropertyName );
    }

    @SuppressWarnings("unchecked")
    public JavaProperty<S,Object> getProperties( List<String> propertyNames ) {
        return FP.wrap(propertyNames)
            .fold(
                (JavaProperty<S,Object>) this,
                (soFar,next) -> soFar.getProperty(next).orElseThrow(() -> new MissingPropertyException(soFar.getSourceType().getFullName(),next))
            );
    }

    @SuppressWarnings("unchecked")
    public FPIterable<JavaProperty<T,Object>> getProperties() {
        return getValueType().getProperties().map( childProperty -> new ChainedProperty(this, childProperty) );
    }

    public boolean isPrimitive() {
        return getValueType().isPrimitive();
    }

    public boolean isString() {
        return getValueType().isString();
    }



    private static String capitalize( String s ) {
        if ( s == null || s.length() == 0 ) {
            return s;
        }

        char c = s.charAt( 0 );
        if ( !Character.isLowerCase(c) ) {
            return s;
        }

        return Character.toUpperCase(c) + s.substring(1);
    }

    public void throwIfNoSetter() {
        if ( !hasSetter() ) {
            throw new IllegalStateException( this.getSourceType().getFullName() + "." + this + " has no setter" );
        }
    }
}


@SuppressWarnings("unchecked")
class FieldJavaProperty<S,T> extends JavaProperty<S,T> {

    public static FPOption<JavaProperty> fetchPropertyFrom( JavaClass clazz, String propertyName ) {
        return clazz.getField(propertyName).map( FieldJavaProperty::new );
    }


    private final JavaField field;

    private FieldJavaProperty( JavaField field ) {
        this.field = field;
    }

    public JavaClass getSourceType() {
        return field.getDeclaringClass();
    }

    public JavaClass getValueType() {
        return field.getType();
    }

    public boolean isPublic() {
        return field.isPublic();
    }

    public FPOption<T> getValueFrom( S o ) {
        Object valueFrom = field.getValueFrom( o );

        if ( valueFrom instanceof FPOption ) {
            FPOption option = (FPOption) valueFrom;

            return option;
        } else if ( valueFrom instanceof Optional ) {
            Optional optional = (Optional) valueFrom;

            return FPOption.ofOptional(optional);
        } else {
            return FPOption.of( cast(valueFrom) );
        }
    }

    public boolean hasSetter() {
        return true;
    }

    public boolean isDirectFieldAccessEnabled() {
        return true;
    }

    public JavaProperty<S,T> withDirectFieldAccessEnabled( boolean f ) {
        return this;
    }

    public void setValueOn( S o, T v ) {
        if ( getValueType().isFPOption() && (v != null && !(v instanceof FPOption)) ) {
            field.setValueOn( o, FPOption.of( v ) );
        } else if ( getValueType().isJdkOptional() && (v != null && !(v instanceof Optional)) ) {
            field.setValueOn( o, Optional.of(v) );
        } else {
            field.setValueOn( o, v );
        }
    }

    public void unsetOn( S o ) {
        if ( getValueType().isFPOption() ) {
            field.setValueOn( o, FPOption.none() );
        } else if ( getValueType().isJdkOptional() ) {
            field.setValueOn( o, Optional.empty() );
        } else if ( getValueType().isPrimitive() ){
            field.setValueOn( o, 0 );
        } else {
            field.setValueOn( o, null );
        }
    }

    public boolean isOptional() {
        return getValueType().isOptional();
    }

    public BoundJavaProperty<T> bindTo( S o ) {
        return new BoundJavaProperty<T>(this, o);
    }

    public FPOption<JavaDoc> getJavaDoc() {
        return field.getJavaDoc();
    }

    public <T extends Annotation> FPOption<T> getAnnotation( Class<T> annotationType ) {
        return field.getAnnotation(annotationType);
    }

    public String getName() {
        return field.getName();
    }

    public String toString() {
        return field.getName();
    }

    public boolean equals( Object o ) {
        if ( o == null || !(o instanceof FieldJavaProperty) ) {
            return false;
        } else if ( o == this ) {
            return true;
        }

        FieldJavaProperty other = (FieldJavaProperty) o;
        return this.field.equals(other.field);
    }

    public int hashCode() {
        return field.hashCode();
    }
}


@Value
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("unchecked")
class RootProperty<T> extends JavaProperty<T,T> {

    private JavaClass type;
    private String       name;


    public JavaClass getSourceType() {
        return type;
    }

    public JavaClass getValueType() {
        return type;
    }

    public boolean isPublic() {
        return true;
    }

    public FPOption<T> getValueFrom( T o ) {
        return FP.option( o );
    }

    public boolean hasSetter() {
        return true;
    }

    public boolean isDirectFieldAccessEnabled() {
        return true;
    }

    public JavaProperty<T,T> withDirectFieldAccessEnabled( boolean f ) {
        return this;
    }

    public void setValueOn( T o, T v ) {
        throw new UnsupportedOperationException();
    }

    public void unsetOn( T o ) {
        throw new UnsupportedOperationException();
    }

    public boolean isOptional() {
        return getValueType().isOptional() || getValueType().isFPOption();
    }

    public BoundJavaProperty<T> bindTo( T o ) {
        return new BoundJavaProperty<T>(this, o);
    }

    public FPOption<JavaDoc> getJavaDoc() {
        return type.getJavaDoc();
    }

    public <T extends Annotation> FPOption<T> getAnnotation( Class<T> annotationType ) {
        return type.getAnnotation(annotationType);
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return getName();
    }
}


@AllArgsConstructor
@SuppressWarnings("unchecked")
class MethodJavaProperty<S,T> extends JavaProperty<S,T> {

    public static FPOption<JavaProperty> fetchPropertyFrom( JavaClass clazz, String methodName ) {
        FPOption<JavaMethod> getterMethodOpt = clazz.getMethod( methodName );
        if ( !getterMethodOpt.hasValue() ) {
            return FPOption.none();
        }

        JavaMethod           getterMethod = getterMethodOpt.get();
        FPOption<JavaMethod> setterMethod = getterMethod.getMatchingSetter();
        FPOption<JavaField>  field        = getterMethod.getMatchingField();

        return FPOption.of(
            new MethodJavaProperty( getterMethod, setterMethod, field, false )
        );
    }


    private JavaMethod           getterMethod;
    private FPOption<JavaMethod> setterMethod;
    private FPOption<JavaField>  underlyingField;
    private boolean              directFieldAccessEnabled;


    public JavaClass getSourceType() {
        return cast(getterMethod.getDeclaringClass());
    }

    public JavaClass getValueType() {
        return getterMethod.getReturnType();
    }

    public boolean isPublic() {
        return getterMethod.isPublic();
    }

    public FPOption<T> getValueFrom( S o ) {
        Object valueFrom = getterMethod.invokeAgainst( o );

        return FPOption.of( cast(valueFrom) );
    }

    public boolean hasSetter() {
        return setterMethod.hasValue() || (directFieldAccessEnabled && underlyingField.hasValue());
    }

    public boolean isDirectFieldAccessEnabled() {
        return directFieldAccessEnabled;
    }

    public JavaProperty<S,T> withDirectFieldAccessEnabled( boolean f ) {
        if ( f == directFieldAccessEnabled ) {
            return this;
        }

        return new MethodJavaProperty<>( getterMethod, setterMethod, underlyingField, f );
    }

    public void setValueOn( S o, T v ) {
        if ( !hasSetter() ) {
            throw new MissingSetterException(this);
        }

        Object newValue;

        if ( getValueType().isFPOption() && !JavaClass.of(v).isFPOption() ) {
            newValue = FPOption.of(v);
        } else if ( getValueType().isJdkOptional() && !JavaClass.of(v).isJdkOptional() ) {
            newValue = Optional.of(v);
        } else {
            newValue = v;
        }

        if (setterMethod.hasValue()) {
            setterMethod.ifPresent( m -> m.invokeAgainst(o, newValue) );
        } else {
            underlyingField.ifPresent( f -> f.setValueOn(o,newValue) );
        }
    }

    public void unsetOn( S o ) {
        Object newValue;

        if ( getValueType().isFPOption() ) {
            newValue = FPOption.none();
        } else if ( getValueType().isOptional() ) {
            newValue = Optional.empty();
        } else if ( getValueType().isPrimitive() ){
            if ( getValueType().isBoolean() ) {
                newValue = false;
            } else {
                newValue = 0;
            }
        } else {
            newValue = null;
        }

        setterMethod.get().invokeAgainst( o, newValue );
    }

    public boolean isOptional() {
        return getValueType().isOptional() || getValueType().isFPOption();
    }

    public BoundJavaProperty<T> bindTo( S o ) {
        return new BoundJavaProperty<T>(this, o);
    }

    public FPOption<JavaDoc> getJavaDoc() {
        return getterMethod.getJavaDoc()
            .or( () -> underlyingField.flatMap(JavaField::getJavaDoc) );
    }

    public <T extends Annotation> FPOption<T> getAnnotation( Class<T> annotationType ) {
        return getterMethod.getAnnotation(annotationType)
            .or( () -> underlyingField.flatMap(f -> f.getAnnotation(annotationType)));
    }

    public String getName() {
        return dropGetterLabelPrefixFrom(getterMethod.getName());
    }

    public String toString() {
        return dropGetterLabelPrefixFrom(getterMethod.getName());
    }

    public boolean equals( Object o ) {
        if ( o == null || !(o instanceof MethodJavaProperty) ) {
            return false;
        } else if ( o == this ) {
            return true;
        }

        MethodJavaProperty other = (MethodJavaProperty) o;
        return this.getterMethod.equals(other.getterMethod);
    }

    public int hashCode() {
        return getterMethod.hashCode();
    }

}


@AllArgsConstructor
@SuppressWarnings("unchecked")
class ChainedProperty<A,B,C> extends JavaProperty<A,C> {

    private JavaProperty<A,B> prop1;
    private JavaProperty<B,C> prop2;

    public JavaClass getSourceType() {
        return prop1.getSourceType();
    }

    public JavaClass getValueType() {
        return prop2.getValueType();
    }

    public FPOption<C> getValueFrom( A o ) {
        return prop1.getValueFrom(o).flatMap( prop2::getValueFrom );
    }

    public boolean isPublic() {
        return prop1.isPublic() && prop2.isPublic();
    }

    public boolean hasSetter() {
        return prop2.hasSetter();
    }


    public boolean isDirectFieldAccessEnabled() {
        return prop2.isDirectFieldAccessEnabled();
    }

    public JavaProperty<A,C> withDirectFieldAccessEnabled( boolean f ) {
        return new ChainedProperty<>( prop1.withDirectFieldAccessEnabled(f), prop2.withDirectFieldAccessEnabled(f) );
    }

    public void setValueOn( A o, C v ) {
        FPOption<B> prop1Value = prop1.getValueFrom( o );

        if ( prop1Value.isEmpty() ) {
            prop1.init( o );

            prop1Value = prop1.getValueFrom( o );
        }

        prop2.setValueOn( prop1Value.get(), v );
    }

    public void unsetOn( A o ) {
        prop1.getValueFrom(o).ifPresent( b -> prop2.unsetOn(b) );
    }

    @Override
    public void init( A o ) {
        FPOption<B> b = prop1.getValueFrom( o );

        if ( b.isEmpty() ) {
            prop1.init(o);

            b = prop1.getValueFrom( o );
        }

        if ( b.hasValue() ) {
            prop2.init( b.get() );
        }
    }

    public boolean isOptional() {
        return prop1.isOptional() || prop2.isOptional();
    }

    public BoundJavaProperty<C> bindTo( A o ) {
        return new BoundJavaProperty<>(this, o);
    }

    public FPOption<JavaDoc> getJavaDoc() {
        // NB overrides scan L2R, while with no override we want the right most property else nothing.
        return findOverriddenJavaDoc(this.getName()).or(prop2::getJavaDoc);
    }

    public <T extends Annotation> FPOption<T> getAnnotation( Class<T> annotationType ) {
        return prop2.getAnnotation( annotationType );
    }

    public String getName() {
        return prop1 + "." + prop2;
    }

    public String toString() {
        return prop1 + "." + prop2;
    }

    public boolean equals( Object o ) {
        if ( o == null || !(o instanceof ChainedProperty) ) {
            return false;
        } else if ( o == this ) {
            return true;
        }

        ChainedProperty other = (ChainedProperty) o;
        return this.prop1.equals(other.prop1) && this.prop2.equals(other.prop2);
    }

    public int hashCode() {
        return Objects.hash(prop1, prop2);
    }



    /**
     * Scan the chain of JavaProperties left to right looking for an override
     * (ie @param a.b.c description).
     */
    private FPOption<JavaDoc> findOverriddenJavaDoc(String path) {
        List<JavaProperty<?,?>> chain         = flattenChain();
        String                  remainingPath = path;

        for ( JavaProperty<?,?> nextProp : chain ) {
            FPOption<JavaDoc> override = hasOverride( remainingPath, nextProp );

            if ( override.hasValue() ) {
                return override;
            }

            remainingPath = remainingPath.substring( remainingPath.indexOf('.')+1 );
        }

        return FP.emptyOption();
    }

    private FPOption<JavaDoc> hasOverride( String path, JavaProperty<?,?> prop ) {
        FPOption<JavaDoc> javaDocText = prop.getJavaDoc();
        if ( javaDocText.hasValue() ) {
            String tailPath = path.substring( path.indexOf('.')+1 );

            FPOption<JavaDocTag> override = javaDocText.get().getTag( "param", tailPath );

            if ( override.hasValue() ) {
                return override.map( tag -> new JavaDoc(tag.getText()) );
            }
        }

        return FP.emptyOption();
    }

    // flattenChain is recursive.. we are assuming that the property chain will not be > 100 deep
    private List<JavaProperty<?,?>> flattenChain() {
        List<JavaProperty<?,?>> list = new ArrayList<>();

        list.addAll( flattenChain(prop1) );
        list.addAll( flattenChain(prop2) );

        return list;
    }

    private Collection<JavaProperty<?, ?>> flattenChain( JavaProperty<?,?> prop ) {
        if ( prop instanceof ChainedProperty chainedProperty ) {
            return chainedProperty.flattenChain();
        } else {
            return List.of(prop);
        }
    }
}
