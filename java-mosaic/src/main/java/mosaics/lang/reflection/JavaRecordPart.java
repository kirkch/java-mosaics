package mosaics.lang.reflection;

import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Factory;

import java.lang.reflect.RecordComponent;
import java.lang.reflect.Type;

public class JavaRecordPart {
    private final RecordComponent recordComponent;
    private final int             index;
    private final JavaMethod      accessor;
    private final JavaClass       recordComponentType;

    JavaRecordPart( RecordComponent recordComponent, int index ) {
        this.recordComponent     = recordComponent;
        this.index               = index;
        this.accessor            = JavaMethod.of(recordComponent.getAccessor());
        this.recordComponentType = JavaClass.of(recordComponent.getAccessor().getGenericReturnType());
    }

    public Object getValueFrom( Object value ) {
        return accessor.invokeAgainst( value );
    }

    public String getName() {
        return recordComponent.getName();
    }

    /**
     * Returns the index position of this part starting from 0 as declared upon the record.
     */
    public int getIndex() {
        return index;
    }

    public JavaClass getComponentPartType() {
        return recordComponentType;
    }

    public Type getJdkType() {
        return recordComponent.getAccessor().getGenericReturnType();
    }

    public FPOption<Object> getDefaultValue() {
        return recordComponentType.getDefaultValueFactory().map( Factory::invoke );
    }
}
