package mosaics.lang.reflection;

import mosaics.lang.QA;

import java.lang.reflect.Executable;


public class CallerDetails {
    private Executable caller;

    public CallerDetails( Executable caller ) {
        QA.argNotNull(caller, "caller");

        this.caller = caller;
    }

    public JavaClass getDeclaringClass() {
        return JavaClass.of(caller.getDeclaringClass());
    }

    public String getName() {
        return caller.getName();
    }

    public String getShortClassNameAndMethodName() {
        return caller.getDeclaringClass().getSimpleName() + "#" + caller.getName();
    }
}
