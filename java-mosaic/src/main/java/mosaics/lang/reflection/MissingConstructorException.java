package mosaics.lang.reflection;

import mosaics.lang.ArrayUtils;

public class MissingConstructorException extends ReflectionException {
    public MissingConstructorException( JavaClass jc, Class...targetArgTypes ) {
        super( "Unable to find matching constructor for " + jc.getFullName() + "(" + ArrayUtils.toString(targetArgTypes,", ") + ")" );
    }
}
