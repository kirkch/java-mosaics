package mosaics.lang.reflection;

import lombok.Value;
import lombok.With;
import mosaics.fp.collections.FPOption;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * Describes an argument to a constructor or a method.
 */
@With
@Value
public class JavaParameter {
    public static JavaParameter of( JavaExecutable executable, int index, Parameter param, Map<String, JavaClass> generics ) {
        return new JavaParameter(
            executable, index,
            JavaClass.of(param.getParameterizedType(),generics), param.getName(), Arrays.asList(param.getAnnotations())
        );
    }

    private JavaExecutable   source;
    private int              index;
    private JavaClass        type;
    private String           name;
    private List<Annotation> annotations;

    public <A extends Annotation> boolean hasAnnotation( Class<A> annotationClass ) {
        return annotations.stream().anyMatch( a -> a.annotationType() == annotationClass );
    }

    public Class<?> getJdkType() {
        return type.getJdkClass();
    }

    public FPOption<JavaDoc> getJavaDoc() {
        return JavaDocFetcher.INSTANCE.fetchJavaDocFor( this );
    }

    public String toString() {
        return type.getFullName() + " " + name;
    }
}
