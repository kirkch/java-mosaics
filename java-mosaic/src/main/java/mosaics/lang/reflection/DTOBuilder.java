package mosaics.lang.reflection;


import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.i18n.I18nContext;
import mosaics.lang.Backdoor;
import mosaics.lang.reflection.converters.TypeConverterFactory;
import mosaics.strings.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;


/**
 * The DTOBuilder supports the creation of DTOs one property at a time.
 * Including nested DTOs.<p/>
 *
 * Declare properties one at a time using string property paths eg 'a.b.c' then call
 * build() and the builder will create the DTO and any referenced dtos that are required.
 *
 * The DTOBuilder supports both mutable and immutable DTOs.  Mutable dtos support setter methods.
 * Immutable dtos support either withX() methods and/or the lombok @Builder annotation.
 */
public abstract class DTOBuilder<T> {
    public static <T> DTOBuilder<T>  createDTOBuilderFor( Class<T> dtoClass ) {
        return createDTOBuilderFor( JavaClass.of(dtoClass) );
    }

    public static <T> DTOBuilder<T> createDTOBuilderFor( JavaClass dtoClass ) {
        return createDTOBuilderFor( dtoClass, I18nContext.UTC );
    }

    private static <T> DTOBuilder<T> createDTOBuilderFor( JavaClass dtoClass, I18nContext i18nContext ) {
        if ( LombokBuilderDTOBuilder.doesClassSupportLombokBuilders(dtoClass) ) {
            return new LombokBuilderDTOBuilder<>( dtoClass, i18nContext );
        } else {
            return new ReflectiveDTOBuilder<>( dtoClass, i18nContext );
        }
    }

    protected final JavaClass          dtoClass;
    protected final I18nContext        i18nContext;

    protected final Map<String,Object> properties = new HashMap<>();


    public DTOBuilder( JavaClass dtoClass, I18nContext i18nContext ) {
        this.dtoClass    = dtoClass;
        this.i18nContext = i18nContext;
    }

    public abstract DTOBuilder<T> withProperty( String path, Object value );
    public abstract T build();



    private static void throwIfNoZeroArgConstructor( JavaClass dtoClass ) {
        dtoClass.getConstructor().orElseThrow(
            () -> new IllegalArgumentException(
                dtoClass.getFullName() + " has no public zero arg constructor"
            )
        );
    }

    /**
     * Setters are to be used on mutable DTOs, With methods go on immutable DTOs.
     * Mixing the two together onto the same class creates confusion as to which paradigm
     * the DTO belongs to.
     */
    private static void throwIfThereAreBothSettersAndWithMethods( JavaClass dtoClass ) {
        boolean hasAtLeastOneSetter = dtoClass.getProperties().hasFirst( p -> hasSetterMethodFor(dtoClass,p) );
        boolean hasAtLeastOneWither = dtoClass.getProperties().hasFirst( p -> hasWithMethodFor(dtoClass,p) );

        if ( hasAtLeastOneSetter && hasAtLeastOneWither ) {
            throw new IllegalArgumentException(
                dtoClass.getFullName() + " has both setter methods and with methods declared.. DTOBuilder"
                + " does not support mixing setter methods and with methods because it is unclear whether"
                + " the DTO is immutable or not."
            );
        }
    }

    private static boolean hasWithMethodFor( JavaClass dtoClass, JavaProperty<Object, Object> p ) {
        String methodName = "with" + StringUtils.capitalizeFirstCharacter(p.getName());

        return dtoClass.getMethod( methodName, p.getJdkValueType() ).hasValue();
    }

    private static boolean hasSetterMethodFor( JavaClass dtoClass, JavaProperty<Object, Object> p ) {
        String methodName = "set" + StringUtils.capitalizeFirstCharacter(p.getName());

        return dtoClass.getMethod( methodName, p.getJdkValueType() ).hasValue();
    }

    protected Object convertValueToTargetPropertyType( String path, Object value ) {
        JavaProperty<Object, Object> property          = getNextProperty( path );
        JavaClass                    propertyValueType = property.getValueType();

        try {
            Tryable<Object> convertionResult = TypeConverterFactory.convert(
                i18nContext, value, JavaClass.of( value ), propertyValueType
            );

            return convertionResult.getResult();
        } catch ( Throwable ex ) {
            throw new IllegalArgumentException(
                "Unable to convert '" + value + "' of type '" + value.getClass().getName()
                    + "' to property '"+property.getName() + "' of type '"
                    + property.getValueType().getFullName() + "'",
                ex
            );
        }
    }

    protected JavaProperty<Object,Object> getNextProperty( String path ) {
        throwIfNoPropertyCalled( path );

        return dtoClass.getProperty( path ).get();
    }

    private void throwIfNoPropertyCalled( String path ) {
        dtoClass.getProperty(path).orElseThrow(
            () -> new IllegalArgumentException(dtoClass.getFullName()+" does not have a property called '"+path+"'")
        );
    }

    /**
     * Internal property setter that walks the specified path.  Creating nested DTOBuilders
     * until the leaf of the path is reached where the final value is stored.
     */
    protected void setProperty( int i, String[] path, Object convertedValue ) {
        String nextProp = path[i];

        if ( i == path.length-1 ) {
            properties.put( nextProp, convertedValue );
        } else if ( properties.containsKey(nextProp) ) {
            DTOBuilder nextBuilder = (DTOBuilder) properties.get( nextProp );

            nextBuilder.setProperty( i+1, path, convertedValue );
        } else {
            JavaProperty<Object,Object> nextProperty = getNextProperty( nextProp );
            DTOBuilder<Object>          nextBuilder  = DTOBuilder.createDTOBuilderFor( nextProperty.getValueType() );

            properties.put( nextProp, nextBuilder );

            nextBuilder.setProperty( i+1, path, convertedValue );
        }
    }

    /**
     * Supports get/set/with methods on mutable dtos.
     */
    private static class ReflectiveDTOBuilder<T> extends DTOBuilder<T> {
        public ReflectiveDTOBuilder( JavaClass dtoClass, I18nContext i18nContext ) {
            super( dtoClass, i18nContext );

            throwIfNoZeroArgConstructor( dtoClass );
            throwIfThereAreBothSettersAndWithMethods( dtoClass );
        }


        @Override
        public DTOBuilder<T> withProperty( String path, Object value ) {
            Object convertedType = convertValueToTargetPropertyType( path, value );

            setProperty( 0, path.split( "\\." ), convertedType );

            return this;
        }

        @Override
        public T build() {
            final AtomicReference<T> objRef = new AtomicReference<>(dtoClass.newInstance());

            properties.forEach( (propertyName,value) -> {
                T updatedObj = setPropertyOn( objRef.get(), propertyName, value );

                objRef.set( updatedObj );
            } );

            return objRef.get();
        }

        private T setPropertyOn( T obj, String propertyName, Object value ) {
            if ( value instanceof DTOBuilder<?> childDtoBuilder ) {
                Object child = childDtoBuilder.build();

                return setPropertyUsingJavaProperties( obj, propertyName, child );
            } else {
                JavaClass propType = dtoClass.getProperty( propertyName ).get().getValueType();
                FPOption<JavaMethod> withMethod = dtoClass.getMethod( "with" + StringUtils.capitalizeFirstCharacter(propertyName), propType.getJdkClass() );

                if ( withMethod.hasValue() ) {
                    return Backdoor.cast( withMethod.get().invokeAgainst(obj, value) );
                } else {
                    return setPropertyUsingJavaProperties( obj, propertyName, value );
                }
            }
        }

        private T setPropertyUsingJavaProperties( T obj, String propertyName, Object value ) {
            JavaClass            propType   = dtoClass.getProperty( propertyName ).get().getValueType();
            FPOption<JavaMethod> withMethod = dtoClass.getMethod( "with" + StringUtils.capitalizeFirstCharacter(propertyName), propType.getJdkClass() );

            if ( withMethod.hasValue() ) {
                return Backdoor.cast( withMethod.get().invokeAgainst(obj, value) );
            } else {
                try {
                    dtoClass.getProperty( propertyName ).get().setValueOn( obj, value );
                } catch ( IllegalArgumentException ex ) {
                    throw new IllegalArgumentException(
                        "Property '"+propertyName+"' on '"+obj.getClass().getName()+"' does not accept"
                            + " values of type '" + value.getClass().getName() + "'",
                        ex
                    );
                }

                return obj;
            }
        }
    }

    private static class LombokBuilderDTOBuilder<T> extends DTOBuilder<T> {
        public static boolean doesClassSupportLombokBuilders( JavaClass dtoClass ) {
            return dtoClass.getStaticMethod("builder").hasValue();
        }


        public LombokBuilderDTOBuilder( JavaClass dtoClass, I18nContext i18nContext ) {
            super( dtoClass, i18nContext );
        }

        @Override
        public DTOBuilder<T> withProperty( String path, Object value ) {
            Object convertedType = convertValueToTargetPropertyType( path, value );

            setProperty( 0, path.split( "\\." ), convertedType );

            return this;
        }

        /**
         * Supports immutable objects using lombok generated builders.  Or any builder that
         * follows the same pattern as lombok.
         *
         * 1. create the lombok builder dtoClass.builder()
         * 2. set properties
         * 3. invoke builder.build()
         */
        public T build() {
            Object    builder      = dtoClass.getStaticMethod( "builder" ).get().invokeStaticAgainst();
            JavaClass builderClass = JavaClass.of( builder );

            properties.forEach( (propertyName,value) -> {
                if ( value instanceof DTOBuilder<?> childBuilder ) {
                    value = childBuilder.build();
                }

                JavaMethod setterMethod = builderClass.getFirstMethod( propertyName, 1 )
                    .orElseThrow( () -> new IllegalArgumentException(builderClass.getFullName()+" does not have setter on its builder called '"+propertyName+"';  is it missing the lombok @Default annotation?") );

                setterMethod.invokeAgainst(builder,value);
            } );

            return Backdoor.cast(
                builderClass.getMethod("build").get().invokeAgainst( builder )
            );
        }
    }
}
