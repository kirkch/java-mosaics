package mosaics.lang.reflection;

import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.utils.SetUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;
import java.util.StringJoiner;

import static mosaics.strings.StringUtils.dropWhile;


public interface JavaMethod extends JavaExecutable {
    public static JavaMethod of(Method method) {
        return new JavaMethodJdk(method);
    }

    public String getName();

    public FPIterable<JavaParameter> getParameters();

    public int getModifiers();

    public FPIterable<Annotation> getAnnotations();

    public JavaClass getReturnType();

    /**
     *
     * @return None when this method is 'synthetic'.  eg curried from another method.
     */
    public FPOption<Method> getJdkMethod();

    public <T extends Annotation> FPOption<T> getAnnotation( Class<T> annotationType );

    public <S> Object invokeAgainst( S o, Object...args );

    public Object invokeStaticAgainst( Object...args );

    public JavaClass getDeclaringClass();


    public default FPIterable<JavaClass> getParameterTypes() {
        return getParameters().map( JavaParameter::getType );
    }

    public default FPIterable<Class<?>> getParameterTypesJdk() {
        return getParameterTypes().map( JavaClass::getJdkClass );
    }

    public default int getParameterCount() {
        return Backdoor.toInt(getParameters().count());
    }

    /**
     * Returns 'package.className#methodName(paramType paramName*)'.
     */
    public default String getFullSignature() {
        StringJoiner sj = new StringJoiner(", ", getName() + "(", ")");

        getParameters().map( p -> p.getType() + " " + p.getName() ).forEach( sj::add );

        return getDeclaringClass().getFullName()+"#"+sj;
    }

    /**
     * Returns 'methodName(paramType*)'.
     */
    public default String getShortSignature() {
        StringJoiner sj = new StringJoiner(",", getName() + "(", ")");

        getParameters().map( p -> p.getType().toString() ).forEach( sj::add );

        return sj.toString();
    }

    public default boolean isGetterMethod() {
        String  methodName     = this.getName();
        boolean hasValidName   = methodName.startsWith("get")
            || methodName.startsWith("is")
            || methodName.startsWith("has");

        boolean hasValidReturn = !this.hasVoidReturn();
        boolean hasValidParams = this.getParameterCount() == 0;

        return hasValidName && hasValidReturn && hasValidParams;
    }

    public default boolean isDefault() {
        return getJdkMethod().map( Method::isDefault ).orElse( false );
    }

    public default boolean hasVoidReturn() {
        return this.getReturnType().isVoid();
    }


    private String toPropertyName() {
        String methodName   = getName();
        String propertyName = methodName;

        if ( methodName.startsWith("get") || methodName.startsWith("is") || methodName.startsWith("has")) {
            propertyName = dropWhile( methodName, Character::isLowerCase );
        }

        return Character.toLowerCase(propertyName.charAt(0)) + propertyName.substring(1);
    }

    public default FPOption<JavaMethod> getMatchingSetter() {
        JavaClass    jc               = getDeclaringClass();
        String       propertyName     = toPropertyName();
        String       propertyNameUC   = Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
        Set<String>  candidateNames   = SetUtils.asSet("has" + propertyNameUC, "set" + propertyNameUC, "is" + propertyNameUC);
        JavaClass    getterReturnType = getReturnType();

        return jc.getAllPublicInstanceMethods()
            .first( m -> m.getParameters().count() == 1 && m.getParameterTypes().first().get().equals(getterReturnType) && candidateNames.contains(m.getName()) );
    }

    // todo this method is suspicious.. it assumes that JavaMethod is a getter
    public default FPOption<JavaField> getMatchingField() {
        JavaClass jc               = getDeclaringClass();
        String    propertyName     = toPropertyName();
        JavaClass methodReturnType = getReturnType();

        return jc.getAllFields()
            .first(
                candidateField -> candidateField.getName().equals(propertyName)
                    && candidateField.getType().equals(methodReturnType)
            );
    }

    public default <R> boolean matches( Class<R> returnType, String targetMethodName, Class[] targetMethodParameterTypes ) {
        return this.getName().equals(targetMethodName)
            && Arrays.equals(getParameterTypesJdk().toArray(), targetMethodParameterTypes);
    }

    public default FPOption<JavaDoc> getJavaDoc() {
        return JavaDocFetcher.INSTANCE.fetchJavaDocFor( this );
    }
}
