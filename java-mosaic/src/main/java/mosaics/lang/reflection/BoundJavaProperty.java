package mosaics.lang.reflection;

import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Function0WithThrows;
import mosaics.lang.Backdoor;


public class BoundJavaProperty<T> {

    private JavaProperty<Object, T> property;
    private Object source;

    <S> BoundJavaProperty( JavaProperty<S,T> property, S source ) {
        this.property = Backdoor.cast(property);
        this.source   = source;
    }

    public JavaClass getValueType() {
        return property.getValueType();
    }

    public FPOption<T> getValue() {
        return property.getValueFrom( source );
    }

    public T getValueOrElse( T defaultValue ) {
        return property.getValueOrElseFrom( source, defaultValue );
    }

    public T getValueOrElse( Function0WithThrows<T> defaultValueFetcher ) {
        return property.getValueOrElseFrom( source, defaultValueFetcher );
    }

    public boolean hasSetter() {
        return property.hasSetter();
    }

    public void setValue( T v ) {
        property.setValueOn( source, v );
    }

    public void unset() {
        property.unsetOn( source );
    }

    public boolean isSet() {
        return property.isSetOn( source );
    }

    public void init() {
        property.init( source );
    }

    public String toString() {
        return property.toString();
    }

}
