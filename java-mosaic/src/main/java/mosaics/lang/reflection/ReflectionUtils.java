package mosaics.lang.reflection;

import lombok.SneakyThrows;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.maps.FPMap;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.lang.StackWalker.Option.RETAIN_CLASS_REFERENCE;


@SuppressWarnings( "unchecked" )
public class ReflectionUtils {

    private static final JavaMethod CLONE_METHOD = JavaClass.OBJECT.getMethod( "clone" ).get();

    public static <T extends Cloneable> T clone( T obj ) {
        return Backdoor.cast( CLONE_METHOD.invokeAgainst(obj) );
    }

    public static boolean isZeroOrEmpty( Object v ) {
        if ( v == null ) {
            return true;
        } else if ( v instanceof Boolean ) {
            return v.equals( Boolean.FALSE );
        } else if ( v instanceof Byte ) {
            return (Byte) v == 0;
        } else if ( v instanceof Character ) {
            return (Character) v == 0;
        } else if ( v instanceof Short ) {
            return (Short) v == 0;
        } else if ( v instanceof Integer ) {
            return (Integer) v == 0;
        } else if ( v instanceof Long ) {
            return (Long) v == 0;
        } else if ( v instanceof Float ) {
            return (Float) v == 0;
        } else if ( v instanceof Double ) {
            return (Double) v == 0;
        } else if ( v.getClass().isArray() ) {
            return Array.getLength( v ) == 0;
        } else if ( v instanceof Iterable ) {
            return !((Iterable) v).iterator().hasNext();
        } else if ( v instanceof Map ) {
            return ((Map) v).size() == 0;
        } else if ( v instanceof Iterator ) {
            return !((Iterator) v).hasNext();
        } else if ( v instanceof Optional ) {
            return !((Optional) v).isPresent();
        } else if ( v instanceof FPMap ) {
            return ((FPMap) v).isEmpty();
        } else {
            String str = v.toString();

            return str.trim().length() == 0;
        }
    }

    public static Stream<Field> allFieldsFor( Class c ) {
        return walkInheritanceTreeFor(c).flatMap( k -> Arrays.stream(k.getDeclaredFields()) );
    }

    /**
     * Returns the class that invoked the method that called this method.  In other words, the method
     * who called this method wants to know who invoked it.<p/>
     *
     * For example:<p/>
     *
     * A.main called B.foo<p/>
     *
     * within B.foo, it wants to know who called it.  So it calls ReflectionUtils.getCallersClass,
     * and it returns A.
     *
     *
     */
    public static JavaClass getCallersClass() {
        try {
            Class<?> c = Class.forName( new Exception().getStackTrace()[2].getClassName() );

            return JavaClass.of(c);
        } catch ( ClassNotFoundException e ) {
            throw Backdoor.throwException( e );
        }
    }

    public static CallerDetails getCallersMethod() {
        try {
            StackTraceElement stackTraceElement = new Exception().getStackTrace()[2];
            Class             callerClass       = Class.forName(stackTraceElement.getClassName());
            String            methodName        = stackTraceElement.getMethodName();

            if ( methodName.equals("<init>") ) {
                return new CallerDetails( callerClass.getConstructors()[0] );
            } else {
                return new CallerDetails( _findFirstMethodByName( callerClass,methodName) );
            }
        } catch ( ClassNotFoundException e ) {
            throw Backdoor.throwException(e);
        }
    }

    private static Method _findFirstMethodByName( Class c, String methodName ) {
        for ( Method m : c.getDeclaredMethods() ) {
            if ( m.getName().equals(methodName) ) {
                return m;
            }
        }

        return null;
    }

    public static Stream<Class> walkInheritanceTreeFor( Class c ) {
        return iterate( c, k -> Optional.ofNullable(k.getSuperclass()) );
    }

    public static <T> Stream<T> iterate( T seed, Function<T,Optional<T>> fetchNextFunction ) {
        Objects.requireNonNull(fetchNextFunction);

        Iterator<T> iterator = new Iterator<T>() {
            private Optional<T> t = Optional.ofNullable(seed);

            public boolean hasNext() {
                return t.isPresent();
            }

            public T next() {
                T v = t.get();

                t = fetchNextFunction.apply(v);

                return v;
            }
        };

        return StreamSupport.stream(
            Spliterators.spliteratorUnknownSize( iterator, Spliterator.ORDERED | Spliterator.IMMUTABLE),
            false
        );
    }

    public static void setField( Object instance, Field field, Object v ) {
        field.setAccessible( true );

        try {
            field.set( instance, v );
        } catch ( IllegalAccessException e ) {
            throw new RuntimeException( e );
        }
    }

    @SneakyThrows
    public static Field getField( Class c, String fieldName ) {
        return c.getDeclaredField( fieldName );
    }

    @SneakyThrows
    public static Object getFieldValue( Object instance, String fieldName ) {
        Field f = getField( instance.getClass(), fieldName );

        return getFieldValue( instance, f );
    }

    @SneakyThrows
    public static Object getFieldValue( Object instance, Field field ) {
        field.setAccessible( true );

        return field.get( instance );
    }

    public static <T> T getPrivateFieldOrNoArgMethod( Object o, String name ) {
        Class c = o.getClass();

        while ( c != null ) {
            try {
                Field f = c.getDeclaredField( name );
                f.setAccessible( true );

                return Backdoor.cast( f.get(o) );
            } catch ( Exception e ) {
                try {
                    Method m = c.getDeclaredMethod(name);
                    m.setAccessible(true);

                    return (T) m.invoke(o);
                } catch ( Exception e1 ) {
                    c = c.getSuperclass();
                }
            }
        }

        throw ReflectionException.recast( new NoSuchFieldException(name) );
    }

//    public static ParameterMeta uneraseParameter( Parameter parameter ) {
//        Method              owningMethod = (Method) parameter.getDeclaringExecutable();
//        Class<?>            owningClass  = owningMethod.getDeclaringClass();
//        Optional<ClassMeta> classMetaOpt = ClassMetaLoader.loadClassMetaFor( owningClass );
//
//        Optional<ParameterMeta> parameterMetaOpt = classMetaOpt
//            .flatMap( cm -> cm.getMethodMetaFor(owningMethod) )
//            .flatMap( mm -> mm.getParameterMetaFor(parameter) );
//
//        return parameterMetaOpt.orElseGet( () -> new ParameterMeta(parameter.getType(),parameter.getName()) );
//    }

    private static boolean isAnonymousInnerClass( Object tc ) {
        return tc != null && tc.getClass().getEnclosingClass() != null;
//        try {
//            return tc != null && tc.getClass().getDeclaredField("this$0") != null;
//        } catch ( NoSuchFieldException e ) {
//            return false;
//        }
    }

    public static <T> boolean hasValue( T v ) {
        if ( v instanceof Boolean ) {
            return (Boolean) v;
        } else if ( v instanceof String ) {
            return ((String) v).trim().length() > 0;
        } else if ( v instanceof Optional ) {
            return ((Optional) v).isPresent();
        } else if ( v instanceof FPOption ) {
            return ((FPOption) v).hasValue();
        } else if ( v instanceof Iterable ) {
            return ((Iterable) v).iterator().hasNext();
        } else if ( v instanceof Map ) {
            return !((Map) v).isEmpty();
        } else if ( v instanceof FPMap ) {
            return ((FPMap) v).size() > 0;
        }

        return v != null;
    }

    private static boolean hasAnnotation( Object tc, Class annotationType ) {
        return tc.getClass().getAnnotation(annotationType) != null;
    }

    public static Object getContainingObjectForAnonymousInnerClass( Object tc ) {
        int depthCount = countEnclosingClassDepth(tc.getClass());
        return getFieldValue( tc, "this$"+(depthCount-1) );
    }

    private static int countEnclosingClassDepth( Class<?> c ) {
        int depthCount = 0;

        while (c.getEnclosingClass() != null ) {
            depthCount += 1;

            c = c.getEnclosingClass();
        }

        return depthCount;
    }

    /**
     * Parent classes and all of their interfaces.
     */
    public static FPIterable<JavaClass> classHierarchyIterable( Class c ) {
        return walkClassHierarchy(c).map(JavaClass::of);
    }

    private static FPIterable<Class> walkClassHierarchy( Class c ) {
        return new FPIterable<Class>() {
            public FPIterator<Class> iterator() {
                Deque<Class> queue = new LinkedList<>();
                queue.push(c);

                return new FPIterator<Class>() {
                    public boolean _hasNext() {
                        return !queue.isEmpty();
                    }

                    public Class _next() {
                        Class next = queue.removeLast();

                        queue.addAll( Arrays.asList(next.getInterfaces()) );

                        Class parent = next.getSuperclass();
                        if ( parent != null ) {
                            queue.addFirst( parent );
                        }

                        return next;
                    }
                };
            }
        };
    }

    /**
     * Can a be casted to b?
     *
     * @param a The type of the object in question
     * @param b The type of the interface that we are checking
     */
    public static <R, T> boolean isCastableTo( Class<R> a, Class<T> b ) {
        return b.isAssignableFrom( a );
    }

    public static boolean isCastableTo( Type a, Type b ) {
        return toClass(b).isAssignableFrom( toClass(a) );
    }

    public static <T> Class<T> toClass( Type type ) {
        if ( type instanceof Class ) {
            return (Class<T>) type;
        } else if ( type instanceof ParameterizedType t ) {
            return toClass( t.getRawType() );
        }

        try {
            return (Class<T>) Class.forName( type.getTypeName() );
        } catch ( ClassNotFoundException e ) {
            throw new IllegalArgumentException( type + " cannot be converted to a Class", e );
        }
    }

    private static final Map<Type,Object> DEFAULT_VALUES = Map.of(
        boolean.class, false,
        byte.class, (byte) 0,
        char.class, (char) 0,
        short.class, (short) 0,
        int.class, 0,
        long.class, 0L,
        float.class, 0f,
        double.class, 0d
    );

    private static final Set<Object> DEFAULT_FIELD_VALUES = Set.copyOf(DEFAULT_VALUES.values());

    public static boolean isUnset( Object obj, Field f ) {
        Object currentFieldValue = getFieldValue( obj, f );

        return currentFieldValue == null || DEFAULT_FIELD_VALUES.contains( currentFieldValue );
    }

    public static void clearField( Field f, Object o ) {
        setField( o, f, DEFAULT_VALUES.get(f.getType()) );
    }

    public static StackWalker.StackFrame getCallersStackFrame() {
        StackWalker stackWalker = StackWalker.getInstance(RETAIN_CLASS_REFERENCE);
        return stackWalker.walk(
            frames ->
                frames
                    .skip(2)
                    .findFirst()
                    .get()
        );
    }

    public static boolean isStatic( Field field ) {
        return Modifier.isStatic( field.getModifiers() );
    }

    public static boolean isInstanceField( Field field ) {
        return !isStatic( field );
    }

    public static boolean isNotSyntheticField( Field field ) {
        return !field.isSynthetic();
    }

    public static boolean isBoolean( Type type ) {
        return isBoolean( toClass(type) );
    }

    public static boolean isBoolean( Class type ) {
        return type == boolean.class || type == Boolean.class;
    }

    public static boolean isByte( Type type ) {
        return isByte( toClass(type) );
    }

    public static boolean isByte( Class type ) {
        return type == byte.class || type == Byte.class;
    }

    public static boolean isShort( Type type ) {
        return isShort( toClass(type) );
    }

    public static boolean isShort( Class type ) {
        return type == short.class || type == Short.class;
    }

    public static boolean isInteger( Type type ) {
        return isInteger( toClass(type) );
    }

    public static boolean isInteger( Class type ) {
        return type == int.class || type == Integer.class;
    }

    public static boolean isLong( Type type ) {
        return isLong( toClass(type) );
    }

    public static boolean isLong( Class type ) {
        return type == long.class || type == Long.class;
    }

    public static boolean isCharacter( Type type ) {
        return isCharacter( toClass(type) );
    }

    public static boolean isCharacter( Class type ) {
        return type == char.class || type == Character.class;
    }

    public static boolean isFloat( Type type ) {
        return isFloat( toClass(type) );
    }

    public static boolean isFloat( Class type ) {
        return type == float.class || type == Float.class;
    }

    public static boolean isDouble( Type type ) {
        return isDouble( toClass(type) );
    }

    public static boolean isDouble( Class type ) {
        return type == double.class || type == Double.class;
    }

    public static <T> Object newArray(Class componentType, int arrayLength, Supplier<T> factory) {
        Object array = Array.newInstance(componentType, arrayLength);

        for ( int i=0; i<arrayLength; i++ ) {
            Object newValue = factory.get();

            Array.set( array, i, newValue );
        }

        return array;
    }

    public static List<Type> getTypeParameters( Type type ) {
        if ( type instanceof ParameterizedType t ) {
            return Arrays.asList(t.getActualTypeArguments());
        } else {
            return Collections.emptyList();
        }
    }

    public static boolean isArray( Type type ) {
        return toClass(type).isArray();
    }

    public static boolean isEnum( Type type ) {
        return toClass(type).isEnum();
    }

    public static boolean isJdkList( Type type ) {
        return isCastableTo( type, List.class );
    }

    public static boolean isJdkSet( Type type ) {
        return isCastableTo( type, Set.class );
    }

    public static boolean isJdkMap( Type type ) {
        return isCastableTo( type, Map.class );
    }
}
