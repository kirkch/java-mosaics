package mosaics.lang.reflection.converters;

import lombok.AllArgsConstructor;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.i18n.I18nContext;
import mosaics.lang.reflection.JavaClass;

import static mosaics.lang.Backdoor.cast;


@Value
@AllArgsConstructor
public class NoOpConverter<S,D> implements TypeConverter<S,D> {
    public static FPOption<TypeConverter> tryToCreateCompatibleConverterFor( JavaClass from, JavaClass to ) {
        return FP.option( from.isAssignableTo(to), () -> new NoOpConverter<>(from,to) );
    }

    private JavaClass sourceType;
    private JavaClass destType;

    public NoOpConverter( JavaClass t ) {
        this( cast(t), cast(t) );
    }

    public Tryable<D> convert( I18nContext ctx, S value ) {
        return Try.succeeded(cast(value));
    }

    public Tryable<S> reverse( I18nContext ctx, D value ) {
        return Try.succeeded(cast(value));
    }

}
