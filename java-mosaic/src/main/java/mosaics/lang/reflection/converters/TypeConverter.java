package mosaics.lang.reflection.converters;

import mosaics.fp.Tryable;
import mosaics.i18n.I18nContext;
import mosaics.lang.reflection.JavaClass;


/**
 * Convert between two specified types.
 */
public interface TypeConverter<S,D> {
    public JavaClass getSourceType();
    public JavaClass getDestType();

    /**
     * Convert the specified source value into the destination type.
     */
    public Tryable<D> convert( I18nContext ctx, S value );

    /**
     * Convert the destination type to the source type.
     */
    public Tryable<S> reverse( I18nContext ctx, D value );

    /**
     * Reverse the direction of this type converter.  Source becomes destination, and destination
     * becomes source.
     */
    public default TypeConverter<D,S> reverse() {
        TypeConverter<S,D> orig = this;

        return new TypeConverter<D, S>() {
            public JavaClass getSourceType() {
                return orig.getDestType();
            }

            public JavaClass getDestType() {
                return orig.getSourceType();
            }

            public Tryable<S> convert( I18nContext ctx, D value ) {
                return orig.reverse(ctx,value);
            }

            public Tryable<D> reverse( I18nContext ctx, S value ) {
                return orig.convert(ctx,value);
            }

            public TypeConverter<S, D> reverse() {
                return orig;
            }
        };
    }
}
