package mosaics.lang.reflection.converters;

import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.i18n.I18nContext;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaConstructor;

import static mosaics.lang.Backdoor.cast;


class DefaultStringConstructorTypeConverter<T> implements TypeConverter<String,T> {
    // NB only supports String -> Class with single String constructor because
    // the TypeConverterFactory will handle Class -> String reversal for us
    static FPOption<TypeConverter> createDefaultConverterFor( JavaClass sourceType, JavaClass destType ) {
        if ( !isSupportedSourceType(sourceType) || !isSupportedDestType(destType) ) {
            return FP.emptyOption();
        }

        return FP.option( new DefaultStringConstructorTypeConverter<>(destType) );
    }


    private JavaClass       destType;
    private JavaConstructor stringConstructor;

    private DefaultStringConstructorTypeConverter( JavaClass destType ) {
        this.destType          = destType;
        this.stringConstructor = destType.getConstructor( String.class ).get();
    }


    public JavaClass getSourceType() {
        return JavaClass.STRING;
    }

    public JavaClass getDestType() {
        return destType;
    }

    public Tryable<T> convert( I18nContext ctx, String value ) {
        return Try.invoke( () -> cast(stringConstructor.newInstance(value)) );
    }

    public Tryable<String> reverse( I18nContext ctx, T value ) {
        return Try.invoke( value::toString );
    }


    private static boolean isSupportedSourceType( JavaClass sourceType ) {
        return JavaClass.STRING.equals( sourceType );
    }

    private static boolean isSupportedDestType( JavaClass destType ) {
        return destType.getConstructor( String.class ).hasValue();
    }
}
