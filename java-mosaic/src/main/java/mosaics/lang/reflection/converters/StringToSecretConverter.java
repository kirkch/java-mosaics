package mosaics.lang.reflection.converters;

import lombok.EqualsAndHashCode;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.i18n.I18nContext;
import mosaics.lang.Secret;
import mosaics.lang.reflection.JavaClass;


@EqualsAndHashCode
public class StringToSecretConverter implements TypeConverter<String, Secret> {
    public static final StringToSecretConverter INSTANCE = new StringToSecretConverter();

    public JavaClass getSourceType() {
        return JavaClass.STRING;
    }

    public JavaClass getDestType() {
        return JavaClass.of(Secret.class);
    }

    public Tryable<Secret> convert( I18nContext ctx, String value ) {
        return Try.invoke( () -> new Secret(value) );
    }

    public Tryable<String> reverse( I18nContext ctx, Secret value ) {
        return Try.invoke( value::getValue );
    }
}
