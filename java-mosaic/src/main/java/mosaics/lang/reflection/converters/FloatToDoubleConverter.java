package mosaics.lang.reflection.converters;

import lombok.Value;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.i18n.I18nContext;
import mosaics.lang.reflection.JavaClass;

import java.text.ParseException;


@Value
public class FloatToDoubleConverter implements TypeConverter<Float,Double> {

    private JavaClass sourceType;
    private JavaClass destType;


    public Tryable<Double> convert( I18nContext ctx, Float value ) {
        if ( value == null ) {
            return handleNullCase( destType.isNullable() );
        }

        return Try.succeeded( value.doubleValue() );
    }

    public Tryable<Float> reverse( I18nContext ctx, Double value ) {
        if ( value == null ) {
            return handleNullCase( sourceType.isNullable() );
        }

        return Try.succeeded( value.floatValue() );
    }


    private <T> Tryable<T> handleNullCase( boolean isNullable ) {
        if ( isNullable ) {
            return Try.succeeded( null );
        } else {
            return Try.failed( new ParseException( null, 0 ) );
        }
    }

}
