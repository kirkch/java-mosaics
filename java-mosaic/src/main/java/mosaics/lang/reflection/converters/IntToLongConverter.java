package mosaics.lang.reflection.converters;

import lombok.Value;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.i18n.I18nContext;
import mosaics.lang.reflection.JavaClass;

import java.text.ParseException;


@Value
public class IntToLongConverter implements TypeConverter<Integer,Long> {

    private JavaClass sourceType;
    private JavaClass    destType;


    public Tryable<Long> convert( I18nContext ctx, Integer value ) {
        if ( value == null ) {
            return handleNullCase( destType.isNullable() );
        }

        return Try.succeeded( value.longValue() );
    }

    public Tryable<Integer> reverse( I18nContext ctx, Long value ) {
        if ( value == null ) {
            return handleNullCase( sourceType.isNullable() );
        }

        if ( value.longValue() > Integer.MAX_VALUE || value.longValue() < Integer.MIN_VALUE ) {
            return Try.failed( "%s is out of range for an int", value.longValue() );
        }

        return Try.succeeded( value.intValue() );
    }


    private <T> Tryable<T> handleNullCase( boolean isNullable ) {
        if ( isNullable ) {
            return Try.succeeded( null );
        } else {
            return Try.failed( new ParseException( null, 0 ) );
        }
    }

}
