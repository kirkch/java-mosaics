package mosaics.lang.reflection.converters;

import lombok.AllArgsConstructor;
import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.i18n.I18nContext;
import mosaics.lang.reflection.JavaClass;

import static mosaics.lang.Backdoor.cast;


@AllArgsConstructor
public class EnumStringTypeConverter<T extends Enum<T>> implements TypeConverter<String,T> {
    @SuppressWarnings("rawtypes")
    public static FPOption<TypeConverter> createEnumTypeConverterFor( JavaClass source, JavaClass dest ) {
        if ( source.isString() && dest.isEnum() ) {
            return FP.option( new EnumStringTypeConverter(dest) );
        } else if ( source.isEnum() && dest.isString() ) {
            return FP.option( new EnumStringTypeConverter(source).reverse() );
        } else {
            return FP.emptyOption();
        }
    }

    private final JavaClass enumType;



    public JavaClass getSourceType() {
        return JavaClass.STRING;
    }

    public JavaClass getDestType() {
        return enumType;
    }


    public Tryable<T> convert( I18nContext ctx, String value ) {
        return Try.invoke( () -> cast(Enum.valueOf(cast(enumType.getJdkClass()), value)) );
    }

    public Tryable<String> reverse( I18nContext ctx, T value ) {
        return Try.succeeded( value.name() );
    }
}
