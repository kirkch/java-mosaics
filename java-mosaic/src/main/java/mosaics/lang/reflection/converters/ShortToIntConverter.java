package mosaics.lang.reflection.converters;

import lombok.Value;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.i18n.I18nContext;
import mosaics.lang.reflection.JavaClass;

import java.text.ParseException;


@Value
public class ShortToIntConverter implements TypeConverter<Short,Integer> {

    private JavaClass sourceType;
    private JavaClass destType;


    public Tryable<Integer> convert( I18nContext ctx, Short value ) {
        if ( value == null ) {
            return handleNullCase( destType.isNullable() );
        }

        return Try.succeeded( value.intValue() );
    }

    public Tryable<Short> reverse( I18nContext ctx, Integer value ) {
        if ( value == null ) {
            return handleNullCase( sourceType.isNullable() );
        }

        if ( value.intValue() > Short.MAX_VALUE || value.intValue() < Short.MIN_VALUE ) {
            return Try.failed( "%s is out of range for a short", value.intValue() );
        }

        return Try.succeeded( value.shortValue() );
    }


    private <T> Tryable<T> handleNullCase( boolean isNullable ) {
        if ( isNullable ) {
            return Try.succeeded( null );
        } else {
            return Try.failed( new ParseException( null, 0 ) );
        }
    }

}
