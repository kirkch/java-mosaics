package mosaics.lang.reflection.converters;


import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import lombok.Value;

import java.util.Optional;


@Value
public class JdkOptionalTypeWrapper<T> implements TypeWrapper<T,Optional<T>> {

    private JavaClass wrappedType;


    public Tryable<Optional<T>> wrap( T value ) {
        return Try.succeeded( Optional.ofNullable(value) );
    }

    public Tryable<T> unwrap( Optional<T> value ) {
        if ( value == null || ! value.isPresent() ) {
            if ( getElementType().isNullable() ) {
                return Try.succeeded( null );
            } else {
                return Try.failed( "%s is not nullable", getElementType() );
            }
        }

        return Try.succeeded( value.get() );
    }

}
