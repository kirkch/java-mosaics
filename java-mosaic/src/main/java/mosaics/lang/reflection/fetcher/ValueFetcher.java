package mosaics.lang.reflection.fetcher;


import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Function1;
import mosaics.i18n.I18nContext;


/**
 * Retrieve a value from a source.  This may involve walking a series of properties and/or type
 * conversions and handling default values.
 */
public interface ValueFetcher<T> {
    public FPOption<T> fetchFrom( I18nContext ctx, Function1<String, ?> source );
}
