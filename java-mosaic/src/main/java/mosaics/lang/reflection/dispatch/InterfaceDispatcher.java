package mosaics.lang.reflection.dispatch;

import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.AmbiguousMatchException;
import mosaics.lang.Assert;
import mosaics.lang.Backdoor;
import mosaics.lang.NotFoundException;
import mosaics.lang.QA;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.VoidFunction1;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaMethod;
import mosaics.utils.ComparatorUtils;
import mosaics.utils.MapUtils;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;


/**
 * Given an object, finds a method on an interface to invoke that will handle it.
 */
@SuppressWarnings("unchecked")
public class InterfaceDispatcher<T> {

    public static <T,H> void dispatchEachTo( Iterator<T> it, H handler ) {
        InterfaceDispatcher<H> dispatcher = new InterfaceDispatcher<>( handler );

        it.forEachRemaining( dispatcher::dispatch );
    }


    private final T         destination;
    private final JavaClass destinationType;

    private final Map<Class,FPOption<Function1>> cachedMethods = MapUtils.createWithDefault(this::createDispatchFunctionFor);


    public InterfaceDispatcher( T destination ) {
        QA.argNotNull( destination, "destination" );

        this.destination     = destination;
        this.destinationType = JavaClass.of( destination.getClass() );
    }

    /**
     * Invokes a public method on destination that takes V as an argument.
     */
    public <V,R> R dispatch( V obj ) {
        Class<V>                 targetArgType = (Class<V>) obj.getClass();
        FPOption<Function1<V,R>> dispatcher    = fetchDispatchFunctionFor(targetArgType);

        if ( dispatcher.isEmpty() ) {
            throw new NotFoundException("Found no target method that takes '" + targetArgType.getName() + "' as a parameter on '" + destinationType.getFullName() + "'");
        }

        return dispatcher.get().invoke(obj);
    }

    public <V,R> R dispatch( V obj, Function1<V,R> defaultDispatcherFunc ) {
        Class<V>                targetArgType = (Class<V>) obj.getClass();
        FPOption<Function1<V, R>> dispatcherOpt = fetchDispatchFunctionFor( targetArgType );
        Function1<V,R>          dispatcher    = dispatcherOpt.orElse( defaultDispatcherFunc );

        return dispatcher.invoke( obj );
    }

    public <V> void dispatchVoid( V obj, VoidFunction1<V> defaultDispatcherFunc ) {
        Class<V>                     targetArgType = (Class<V>) obj.getClass();
        FPOption<Function1<V, Object>> dispatcherOpt = fetchDispatchFunctionFor( targetArgType );

        if ( dispatcherOpt.isEmpty() ) {
            defaultDispatcherFunc.invoke( obj );
        } else {
            dispatcherOpt.get().invoke( obj );
        }
    }

    /**
     * Returns a dynamically built function that when called will pass its argument to a method on
     * destination.
     */
    public <A,R> FPOption<Function1<A,R>> fetchDispatchFunctionFor( Class<A> targetArgType ) {
        return Backdoor.cast( cachedMethods.get(targetArgType) );
    }



    private FPOption<Function1> createDispatchFunctionFor( Class targetArgType ) {
        return scanForDestinationMethod(JavaClass.of(targetArgType)).map(m -> (arg -> m.invokeAgainst(destination, arg)));
    }

    private FPOption<JavaMethod> scanForDestinationMethod( JavaClass targetArgType ) {
        FPIterable<JavaMethod> allMatchingMethods = destinationType.getAllPublicInstanceMethods().filter(
            m -> isCandidateDestinationMethodFor(m, targetArgType)
        );

        FPIterable<JavaMethod> sortedMethods = allMatchingMethods.sort(new AssignmentDistanceComparator(targetArgType));

        throwIfAmbiguousMatch( targetArgType, sortedMethods );

        return sortedMethods.first();
    }

    private void throwIfAmbiguousMatch( JavaClass targetArgType, FPIterable<JavaMethod> sortedMethods ) {
        if ( sortedMethods.count() >= 2 ) {
            JavaClass a = sortedMethods.firstOrNull().getParameterTypes().firstOrNull();
            JavaClass b = sortedMethods.secondOrNull().getParameterTypes().firstOrNull();

            if ( a.equals(b) ) {
                throw new AmbiguousMatchException( targetArgType, sortedMethods.firstOrNull(), sortedMethods.secondOrNull() );
            }
        }
    }

    private boolean isCandidateDestinationMethodFor( JavaMethod candidateMethod, JavaClass targetArgType ) {
        if (
            candidateMethod.getDeclaringClass().isInterface() ||
            candidateMethod.getDeclaringClass().isPartOfTheCoreJavaLibs() ||
            candidateMethod.getParameterCount() != 1
        ) {
            return false;
        }

        JavaClass paramType = candidateMethod.getParameterTypes().firstOrNull();

        return targetArgType.isAssignableTo( paramType );
    }


    private static final class AssignmentDistanceComparator implements Comparator<JavaMethod> {
        private final JavaClass targetArgType;

        public AssignmentDistanceComparator( JavaClass targetArgType ) {
            this.targetArgType = targetArgType;
        }

        public int compare( JavaMethod m1, JavaMethod m2 ) {
            Assert.isEqualTo( 1, m1.getParameterCount(), "m1 has the wrong number of args" );
            Assert.isEqualTo( 1, m2.getParameterCount(), "m2 has the wrong number of args" );

            JavaClass a = m1.getParameterTypes().firstOrNull();
            JavaClass b = m2.getParameterTypes().firstOrNull();

            int d1 = targetArgType.assignmentDistanceFrom(a);
            int d2 = targetArgType.assignmentDistanceFrom(b);

            return ComparatorUtils.compareAsc(d1, d2);
        }
    };

}
