package mosaics.lang;

import mosaics.fp.Tryable;
import mosaics.regex.RegExp;

import java.io.File;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;


/**
 * A suite of argument and state validations.  Use to enforce api contracts
 * with only one line per contract.  QA is short for Quality Assurance and
 * the checks on this class are only invoked when -ea is passed to the JVM,
 * otherwise HotSpot will strip them out of the optimised assembler code.
 */
@SuppressWarnings("UnusedDeclaration")
public class QA {

    private static final float  DEFAULT_FLOAT_TOLERANCE = 0e-7f;
    private static final double DEFAULT_DOUBLE_TOLERANCE = 0e-14f;


    public static void argIsDirectory( File dir ) {
        if ( !dir.isDirectory() ) {
            throwIllegalArgumentException( "%s is not a directory" );
        }
    }

// argIsPowerOf2

    public static void argIsPowerOf2( int v, String argName ) {
        if ( !MathUtils.isPowerOf2(v) ) {
            throwIllegalArgumentException("%s (%s) is not a power of two", v, argName);
        }
    }

    public static void argIsPowerOf2( long v, String argName ) {
        if ( !MathUtils.isPowerOf2(v) ) {
            throwIllegalArgumentException("%s (%s) is not a power of two", v, argName);
        }
    }

// isZero

    public static void isZero( byte v, String message, Object...args ) {
        if ( v != 0 ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isZero( char v, String message, Object...args ) {
        if ( v != 0 ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isZero( short v, String message, Object...args ) {
        if ( v != 0 ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isZero( int v, String message, Object...args ) {
        if ( v != 0 ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isZero( long v, String message, Object...args ) {
        if ( v != 0 ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isZero( float v, String message, Object...args ) {
        isZero( v, DEFAULT_FLOAT_TOLERANCE, message, args );
    }

    public static void isZero( float v, float tolerance, String message, Object...args ) {
        if ( Math.abs(v) > tolerance ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isZero( double v, String message, Object...args ) {
        isZero( v, DEFAULT_DOUBLE_TOLERANCE, message, args );
    }

    public static void isZero( double v, double tolerance, String message, Object...args ) {
        if ( Math.abs(v) > tolerance ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

// isNotZero

    public static void isNotZero( byte v, String message, Object...args ) {
        if ( v == 0 ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isNotZero( char v, String message, Object...args ) {
        if ( v == 0 ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isNotZero( short v, String message, Object...args ) {
        if ( v == 0 ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isNotZero( int v, String message, Object...args ) {
        if ( v == 0 ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isNotZero( long v, String message, Object...args ) {
        if ( v == 0 ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isNotZero( float v, String message, Object...args ) {
        isNotZero( v, DEFAULT_FLOAT_TOLERANCE, message, args );
    }

    public static void isNotZero( float v, float tolerance, String message, Object...args ) {
        if ( Math.abs(v) <= tolerance ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void isNotZero( double v, String message, Object...args ) {
        isNotZero( v, DEFAULT_DOUBLE_TOLERANCE, message, args );
    }

    public static void isNotZero( double v, double tolerance, String message, Object...args ) {
        if ( Math.abs(v) <= tolerance ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

// isGTE

    public static void argIsGTE( byte a, byte b, String argName ) {
        if ( a < b ) {
            throwIllegalArgumentException("%s (%s) must be >= %s", argName, a, b);
        }
    }

    public static void argIsGTE( short a, short b, String argName ) {
        if ( a < b ) {
            throwIllegalArgumentException("%s (%s) must be >= %s", argName, a, b);
        }
    }

    public static void argIsGTE( int a, int b, String argName ) {
        if ( a < b ) {
            throwIllegalArgumentException("%s (%s) must be >= %s", argName, a, b);
        }
    }

    public static void argIsGTE( long a, long b, String argName ) {
        if ( a < b ) {
            throwIllegalArgumentException("%s (%s) must be >= %s", argName, a, b);
        }
    }

    public static void argIsGTE( long a, long b, String argNameA, String argNameB ) {
        if ( a < b ) {
            throwIllegalArgumentException("%s (%s) must be >= %s (%s)", argNameA, a, argNameB, b);
        }
    }

    public static void argIsGTE( float a, float b, String argName ) {
        argIsGTE( a, b, argName, DEFAULT_FLOAT_TOLERANCE );
    }

    public static void argIsGTE( float a, float b, String argName, float tolerance ) {
        if ( a < b ) {
            throwIllegalArgumentException("%s (%s) must be >= %s", argName, a, b);
        }
    }

    public static void argIsGTE( double a, double b, String argName ) {
        argIsGTE( a, b, argName, DEFAULT_DOUBLE_TOLERANCE );
    }

    public static void argIsGTE( double a, double b, String argName, double tolerance ) {
        if ( a < b ) {
            throwIllegalArgumentException("%s (%s) must be >= %s", argName, a, b);
        }
    }

    public static <T extends Comparable<T>> void argIsGTE( T a, T b, String argName ) {
        if ( a.compareTo(b) < 0 ) {
            throwIllegalArgumentException("%s (%s) must be >= %s", argName, a, b);
        }
    }

    public static void isGTE( byte a, byte b, String message, Object...args ) {
        isTrueState(a >= b, IllegalStateException.class, message, args);
    }

    public static void isGTE( short a, short b, String message, Object...args ) {
        isTrueState(a >= b, IllegalStateException.class, message, args);
    }

    public static void isGTE( int a, int b, String message, Object...args ) {
        isTrueState(a >= b, IllegalStateException.class, message, args);
    }

    public static void isGTE( long a, long b, String message, Object...args ) {
        isTrueState(a >= b, IllegalStateException.class, message, args);
    }

    public static void isGTE( float a, float b, String message, Object...args ) {
        isGTE( a, b, DEFAULT_FLOAT_TOLERANCE, message, args );
    }

    public static void isGTE( float a, float b, float tolerance, String message, Object...args ) {
        isTrueState(a - tolerance >= b, IllegalStateException.class, message, args);
    }

    public static void isGTE( double a, double b, String message, Object...args ) {
        isGTE( a, b, DEFAULT_DOUBLE_TOLERANCE, message, args );
    }

    public static void isGTE( double a, double b, double tolerance, String message, Object...args ) {
        isTrueState(a - tolerance >= b, IllegalStateException.class, message, args);
    }

    public static <T extends Comparable<T>> void GTE( T a, T b, String message, Object...args ) {
        isTrueState(a.compareTo(b) >= 0, IllegalStateException.class, message, args);
    }


// isGTZero

    public static void argIsGTZero( byte a, String argName ) {
        if ( a <= 0 ) {
            throwIllegalArgumentException("%s (%s) must be > 0", argName, a);
        }
    }

    public static void argIsGTZero( short a, String argName ) {
        if ( a <= 0 ) {
            throwIllegalArgumentException("%s (%s) must be > 0", argName, a);
        }
    }

    public static void argIsGTZero( int a, String argName ) {
        if ( a <= 0 ) {
            throwIllegalArgumentException("%s (%s) must be > 0", argName, a);
        }
    }

    public static void argIsGTZero( long a, String argName ) {
        if ( a <= 0 ) {
            throwIllegalArgumentException("%s (%s) must be > 0", argName, a);
        }
    }

    public static void argIsGTZero( float a, String argName ) {
        argIsGTZero( a, argName, DEFAULT_FLOAT_TOLERANCE );
    }

    public static void argIsGTZero( float a, String argName, float tolerance ) {
        if ( a+tolerance <= 0 ) {
            throwIllegalArgumentException("%s (%s) must be > 0", argName, a);
        }
    }

    public static void argIsGTZero( double a, String argName ) {
        argIsGTZero( a, argName, DEFAULT_DOUBLE_TOLERANCE );
    }

    public static void argIsGTZero( double a, String argName, double tolerance ) {
        if ( a+tolerance <= 0 ) {
            throwIllegalArgumentException("%s (%s) must be > 0", argName, a);
        }
    }


    public static void isGTZero( byte a, String message, Object...args ) {
        isTrueState(a > 0, IllegalStateException.class, message, args);
    }

    public static void isGTZero( short a, String message, Object...args ) {
        isTrueState(a > 0, IllegalStateException.class, message, args);
    }

    public static void isGTZero( int a, String message, Object...args ) {
        isTrueState(a > 0, IllegalStateException.class, message, args);
    }

    public static void isGTZero( long a, String message, Object...args ) {
        isTrueState(a > 0, IllegalStateException.class, message, args);
    }

    public static void isGTZero( float a, String message, Object...args ) {
        isGTZero( a, DEFAULT_FLOAT_TOLERANCE, message, args );
    }

    public static void isGTZero( float a, float tolerance, String message, Object...args ) {
        isTrueState(a - tolerance > 0, IllegalStateException.class, message, args);
    }

    public static void isGTZero( double a, String message, Object...args ) {
        isGTZero( a, DEFAULT_DOUBLE_TOLERANCE, message, args );
    }

    public static void isGTZero( double a, double tolerance, String message, Object...args ) {
        isTrueState(a - tolerance > 0, IllegalStateException.class, message, args);
    }



// isGTEZero

    public static void argIsGTEZero( byte a, String argName ) {
        if ( a < 0 ) {
            throwIllegalArgumentException("%s (%s) must be >= 0", argName, a);
        }
    }

    public static void argIsGTEZero( short a, String argName ) {
        if ( a < 0 ) {
            throwIllegalArgumentException("%s (%s) must be >= 0", argName, a);
        }
    }

    public static void argIsGTEZero( int a, String argName ) {
        if ( a < 0 ) {
            throwIllegalArgumentException("%s (%s) must be >= 0", argName, a);
        }
    }

    public static void argIsGTEZero( long a, String argName ) {
        if ( a < 0 ) {
            throwIllegalArgumentException("%s (%s) must be >= 0", argName, a);
        }
    }

    public static void argIsGTEZero( float a, String argName ) {
        argIsGTEZero( a, argName, DEFAULT_FLOAT_TOLERANCE );
    }

    public static void argIsGTEZero( float a, String argName, float tolerance ) {
        if ( a+tolerance < 0 ) {
            throwIllegalArgumentException("%s (%s) must be >= 0", argName, a);
        }
    }

    public static void argIsGTEZero( double a, String argName ) {
        argIsGTEZero( a, argName, DEFAULT_DOUBLE_TOLERANCE );
    }

    public static void argIsGTEZero( double a, String argName, double tolerance ) {
        if ( a+tolerance < 0 ) {
            throwIllegalArgumentException("%s (%s) must be >= 0", argName, a);
        }
    }

    public static void isGTEZero( byte a, String message, Object...args ) {
        isTrueState(a >= 0, IllegalStateException.class, message, args);
    }

    public static void isGTEZero( short a, String message, Object...args ) {
        isTrueState(a >= 0, IllegalStateException.class, message, args);
    }

    public static void isGTEZero( int a, String message, Object...args ) {
        isTrueState(a >= 0, IllegalStateException.class, message, args);
    }

    public static void isGTEZero( long a, String message, Object...args ) {
        isTrueState(a >= 0, IllegalStateException.class, message, args);
    }

    public static void isGTEZero( float a, String message, Object...args ) {
        isGTEZero( a, DEFAULT_FLOAT_TOLERANCE, message, args );
    }

    public static void isGTEZero( float a, float tolerance, String message, Object...args ) {
        isTrueState(a + tolerance >= 0, IllegalStateException.class, message, args);
    }

    public static void isGTEZero( double a, String message, Object...args ) {
        isGTEZero( a, DEFAULT_DOUBLE_TOLERANCE, message, args );
    }

    public static void isGTEZero( double a, double tolerance, String message, Object...args ) {
        isTrueState(a + tolerance >= 0, IllegalStateException.class, message, args);
    }



// isGT

    public static void argIsGT( byte a, byte b, String argName ) {
        if ( a <= b ) {
            throwIllegalArgumentException("%s (%s) must be > %s", argName, a, b);
        }
    }

    public static void argIsGT( short a, short b, String argName ) {
        if ( a <= b ) {
            throwIllegalArgumentException("%s (%s) must be > %s", argName, a, b);
        }
    }

    public static void argIsGT( int a, int b, String argName ) {
        if ( a <= b ) {
            throwIllegalArgumentException("%s (%s) must be > %s", argName, a, b);
        }
    }

    public static void argIsGT( byte a, byte b, String argNameA, String argNameB ) {
        if ( a <= b ) {
            throwIllegalArgumentException("%s (%s) must be > %s (%s)", argNameA, a, argNameB, b);
        }
    }

    public static void argIsGT( int a, int b, String argNameA, String argNameB ) {
        if ( a <= b ) {
            throwIllegalArgumentException("%s (%s) must be > %s (%s)", argNameA, a, argNameB, b);
        }
    }

    public static void argIsGT( long a, long b, String argNameA, String argNameB ) {
        if ( a <= b ) {
            throwIllegalArgumentException("%s (%s) must be > %s (%s)", argNameA, a, argNameB, b);
        }
    }

    public static <T extends Throwable> void argIsGT( int a, int b, String argName, Class<T> exceptionType ) {
        if ( a <= b ) {
            throwIllegalArgumentException("%s (%s) must be > %s", argName, a, b);
        }
    }

    public static void argIsGT( long a, long b, String argName ) {
        if ( a <= b ) {
            throwIllegalArgumentException("%s (%s) must be > %s", argName, a, b);
        }
    }

    public static void argIsGT( float a, float b, String argName ) {
        argIsGT( a, b, argName, DEFAULT_FLOAT_TOLERANCE );
    }

    public static void argIsGT( float a, float b, String argName, float tolerance ) {
        if ( a <= b ) {
            throwIllegalArgumentException("%s (%s) must be > %s", argName, a, b);
        }
    }

    public static void argIsGT( double a, double b, String argName ) {
        argIsGT( a, b, argName, DEFAULT_DOUBLE_TOLERANCE );
    }

    public static void argIsGT( double a, double b, String argName, double tolerance ) {
        if ( a+tolerance <= b ) {
            throwIllegalArgumentException("%s (%s) must be > %s", argName, a, b);
        }
    }

    public static <T extends Comparable<T>> void argIsGT( T a, T b, String argName ) {
        if ( a.compareTo(b) <= 0 ) {
            throwIllegalArgumentException("%s (%s) must be > %s", argName, a, b);
        }
    }

    public static void isGT( byte a, byte b, String message, Object...args ) {
        isTrueState(a > b, IllegalStateException.class, message, args);
    }

    public static void isGT( short a, short b, String message, Object...args ) {
        isTrueState(a > b, IllegalStateException.class, message, args);
    }

    public static void isGT( int a, int b, String message, Object...args ) {
        isTrueState(a > b, IllegalStateException.class, message, args);
    }

    public static void isGT( long a, long b, String message, Object...args ) {
        isTrueState(a > b, IllegalStateException.class, message, args);
    }

    public static void isGT( float a, float b, String message, Object...args ) {
        isGT( a, b, DEFAULT_FLOAT_TOLERANCE, message, args );
    }

    public static void isGT( float a, float b, float tolerance, String message, Object...args ) {
        isTrueState(a - tolerance > b, IllegalStateException.class, message, args);
    }

    public static void isGT( double a, double b, String message, Object...args ) {
        isGT( a, b, DEFAULT_DOUBLE_TOLERANCE, message, args );
    }

    public static void isGT( double a, double b, double tolerance, String message, Object...args ) {
        isTrueState(a - tolerance > b, IllegalStateException.class, message, args);
    }

    public static <T extends Comparable<T>> void isGT( T a, T b, String message, Object...args ) {
        isTrueState(a.compareTo(b) > 0, IllegalStateException.class, message, args);
    }

// isLT

    public static void argIsLT( byte a, byte b, String argName ) {
        if ( a >= b ) {
            throwIllegalArgumentException("%s (%s) must be < %s", argName, a, b);
        }
    }

    public static void argIsLT( short a, short b, String argName ) {
        if ( a >= b ) {
            throwIllegalArgumentException("%s (%s) must be < %s", argName, a, b);
        }
    }

    public static void argIsLT( int a, int b, String argName ) {
        if ( a >= b ) {
            throwIllegalArgumentException("%s (%s) must be < %s", argName, a, b);
        }
    }

    public static void argIsLT( long a, long b, String argName ) {
        if ( a >= b ) {
            throwIllegalArgumentException("%s (%s) must be < %s", argName, a, b);
        }
    }

    public static void argIsLT( long a, long b, String argName1, String argName2 ) {
        if ( a >= b ) {
            throwIllegalArgumentException("%s (%s) must be < %s (%s)", argName1, a, argName2, b);
        }
    }

    public static void argAIsLTArgB( long a, long b, String argNameA, String argNameB ) {
        if ( a >= b ) {
            throwIllegalArgumentException("%s (%s) must be < %s (%s)", argNameA, a, argNameB, b);
        }
    }

    public static void argIsLT( float a, float b, String argName ) {
        argIsLT( a, b, argName, DEFAULT_FLOAT_TOLERANCE );
    }

    public static void argIsLT( float a, float b, String argName, float tolerance ) {
        if ( a+tolerance >= b ) {
            throwIllegalArgumentException("%s (%s) must be < %s", argName, a, b);
        }
    }

    public static void argIsLT( double a, double b, String argName ) {
        argIsLT( a, b, argName, DEFAULT_DOUBLE_TOLERANCE );
    }

    public static void argIsLT( double a, double b, String argName, double tolerance ) {
        if ( a-tolerance >= b ) {
            throwIllegalArgumentException("%s (%s) must be < %s", argName, a, b);
        }
    }

    public static <T extends Comparable<T>> void argIsLT( T a, T b, String argName ) {
        if ( a.compareTo(b) >= 0 ) {
            throwIllegalArgumentException("%s (%s) must be < %s", argName, a, b);
        }
    }

    public static void isLT( byte a, byte b, String message, Object...args ) {
        isTrueState(a < b, IllegalStateException.class, message, args);
    }

    public static void isLT( short a, short b, String message, Object...args ) {
        isTrueState(a < b, IllegalStateException.class, message, args);
    }

    public static void isLT( int a, int b, String message, Object...args ) {
        isTrueState(a < b, IllegalStateException.class, message, args);
    }

    public static void isLT( long a, long b, String message, Object...args ) {
        if ( a - b >= 0 ) { // condition added here as this method was shown to be slow without it in benchmarks
            isTrueState(a < b, IllegalStateException.class, message, args);
        }
    }

    public static void isLT( float a, float b, String message, Object...args ) {
        isLT( a, b, DEFAULT_FLOAT_TOLERANCE, message, args );
    }

    public static void isLT( float a, float b, float tolerance, String message, Object...args ) {
        isTrueState(a - tolerance < b, IllegalStateException.class, message, args);
    }

    public static void isLT( double a, double b, String message, Object...args ) {
        isLT( a, b, DEFAULT_DOUBLE_TOLERANCE, message, args );
    }

    public static void isLT( double a, double b, double tolerance, String message, Object...args ) {
        isTrueState(a - tolerance < b, IllegalStateException.class, message, args);
    }

    public static <T extends Comparable<T>> void LT( T a, T b, String message, Object...args ) {
        isTrueState(a.compareTo(b) < 0, message, args);
    }

// isLTE

    public static void argIsLTE( byte a, byte b, String argName ) {
        if ( a > b ) {
            throwIllegalArgumentException("%s (%s) must be <= %s", argName, a, b);
        }
    }

    public static void argIsLTE( short a, short b, String argName ) {
        if ( a > b ) {
            throwIllegalArgumentException("%s (%s) must be <= %s", argName, a, b);
        }
    }

    public static void argIsLTE( int a, int b, String argName ) {
        if ( a > b ) {
            throwIllegalArgumentException("%s (%s) must be <= %s", argName, a, b);
        }
    }

    public static void argIsLTE( long a, long b, String argName ) {
        if ( a > b ) {
            throwIllegalArgumentException("%s (%s) must be <= %s", argName, a, b);
        }
    }

    public static void argIsLTE( long a, long b, String argName1, String argName2 ) {
        if ( a > b ) {
            throwIllegalArgumentException("%s (%s) must be <= %s (%s)", argName1, a, argName2, b);
        }
    }

    public static void argIsLTE( float a, float b, String argName ) {
        argIsLTE( a, b, argName, DEFAULT_FLOAT_TOLERANCE );
    }

    public static void argIsLTE( float a, float b, String argName, float tolerance ) {
        if ( a > b-tolerance ) {
            throwIllegalArgumentException("%s (%s) must be <= %s", argName, a, b);
        }
    }

    public static void argIsLTE( double a, double b, String argName ) {
        argIsLTE( a, b, argName, DEFAULT_DOUBLE_TOLERANCE );
    }

    public static void argIsLTE( double a, double b, String argName, double tolerance ) {
        if ( a-tolerance > b ) {
            throwIllegalArgumentException("%s (%s) must be <= %s", argName, a, b);
        }
    }

    public static <T extends Comparable<T>> void argIsLTE( T a, T b, String argName ) {
        if ( a.compareTo(b) > 0 ) {
            throwIllegalArgumentException("%s (%s) must be <= %s", argName, a, b);
        }
    }

    public static <T extends Comparable<T>> void argIsLTE( T a, T b, String argNameA, String argNameB ) {
        if ( a.compareTo(b) > 0 ) {
            throwIllegalArgumentException("%s (%s) must be <= %s (%s)", argNameA, a, argNameB, b);
        }
    }

    public static void isLTE( byte a, byte b, String message, Object...args ) {
        isTrueState(a <= b, IllegalStateException.class, message, args);
    }

    public static void isLTE( short a, short b, String message, Object...args ) {
        isTrueState(a <= b, IllegalStateException.class, message, args);
    }

    public static void isLTE( int a, int b, String message, Object...args ) {
        isTrueState(a <= b, IllegalStateException.class, message, args);
    }

    public static void isLTE( long a, long b, String message, Object...args ) {
        isTrueState(a <= b, IllegalStateException.class, message, args);
    }

    public static void isLTE( float a, float b, String message, Object...args ) {
        isLTE( a, b, DEFAULT_FLOAT_TOLERANCE, message, args );
    }

    public static void isLTE( float a, float b, float tolerance, String message, Object...args ) {
        isTrueState(a - tolerance <= b, IllegalStateException.class, message, args);
    }

    public static void isLTE( double a, double b, String message, Object...args ) {
        isLTE( a, b, DEFAULT_DOUBLE_TOLERANCE, message, args );
    }

    public static void isLTE( double a, double b, double tolerance, String message, Object...args ) {
        isTrueState(a - tolerance <= b, IllegalStateException.class, message, args);
    }

    public static <T extends Comparable<T>> void isLTEObjects( T a, T b, String message, Object...args ) {
        isTrueState(a.compareTo(b) <= 0, IllegalStateException.class, message, args);
    }

// isLTZero

    public static void argIsLTZero( byte a, String argName ) {
        if ( a >= 0 ) {
            throwIllegalArgumentException("%s (%s) must be < 0", argName, a);
        }
    }

    public static void argIsLTZero( short a, String argName ) {
        if ( a >= 0 ) {
            throwIllegalArgumentException("%s (%s) must be < 0", argName, a);
        }
    }

    public static void argIsLTZero( int a, String argName ) {
        if ( a >= 0 ) {
            throwIllegalArgumentException("%s (%s) must be < 0", argName, a);
        }
    }

    public static void argIsLTZero( long a, String argName ) {
        if ( a >= 0 ) {
            throwIllegalArgumentException("%s (%s) must be < 0", argName, a);
        }
    }

    public static void argIsLTZero( float a, String argName ) {
        argIsLTZero( a, argName, DEFAULT_FLOAT_TOLERANCE );
    }

    public static void argIsLTZero( float a, String argName, float tolerance ) {
        if ( a >= 0 ) {
            throwIllegalArgumentException("%s (%s) must be < 0", argName, a);
        }
    }

    public static void argIsLTZero( double a, String argName ) {
        argIsLTZero( a, argName, DEFAULT_DOUBLE_TOLERANCE );
    }

    public static void argIsLTZero( double a, String argName, double tolerance ) {
        if ( a >= 0 ) {
            throwIllegalArgumentException("%s (%s) must be < 0", argName, a);
        }
    }

    public static void isLTZero( byte a, String message, Object...args ) {
        isTrueState(a < 0, IllegalStateException.class, message, args);
    }

    public static void isLTZero( short a, String message, Object...args ) {
        isTrueState(a < 0, IllegalStateException.class, message, args);
    }

    public static void isLTZero( int a, String message, Object...args ) {
        isTrueState(a < 0, IllegalStateException.class, message, args);
    }

    public static void isLTZero( long a, String message, Object...args ) {
        isTrueState(a < 0, IllegalStateException.class, message, args);
    }

    public static void isLTZero( float a, String message, Object...args ) {
        isLTZero( a, DEFAULT_FLOAT_TOLERANCE, message, args );
    }

    public static void isLTZero( float a, float tolerance, String message, Object...args ) {
        isTrueState(a < 0f, IllegalStateException.class, message, args, tolerance);
    }

    public static void isLTZero( double a, String message, Object...args ) {
        isLTZero( a, DEFAULT_DOUBLE_TOLERANCE, message, args );
    }

    public static void isLTZero( double a, double tolerance, String message, Object...args ) {
        isTrueState(a < 0.0, IllegalStateException.class, message, args, tolerance);
    }


// isLTEZero

    public static void argIsLTEZero( byte a, String argName ) {
        if ( a > 0 ) {
            throwIllegalArgumentException("%s (%s) must be <= 0", argName, a);
        }
    }

    public static void argIsLTEZero( short a, String argName ) {
        if ( a > 0 ) {
            throwIllegalArgumentException("%s (%s) must be <= 0", argName, a);
        }
    }

    public static void argIsLTEZero( int a, String argName ) {
        if ( a > 0 ) {
            throwIllegalArgumentException("%s (%s) must be <= 0", argName, a);
        }
    }

    public static void argIsLTEZero( long a, String argName ) {
        if ( a > 0 ) {
            throwIllegalArgumentException("%s (%s) must be <= 0", argName, a);
        }
    }

    public static void argIsLTEZero( float a, String argName ) {
        argIsLTEZero( a, argName, DEFAULT_FLOAT_TOLERANCE );
    }

    public static void argIsLTEZero( float a, String argName, float tolerance ) {
        if ( a > 0 ) {
            throwIllegalArgumentException("%s (%s) must be <= 0", argName, a);
        }
    }

    public static void argIsLTEZero( double a, String argName ) {
        argIsLTEZero( a, argName, DEFAULT_DOUBLE_TOLERANCE );
    }

    public static void argIsLTEZero( double a, String argName, double tolerance ) {
        if ( a > 0 ) {
            throwIllegalArgumentException("%s (%s) must be <= 0", argName, a);
        }
    }

    public static void isLTEZero( byte a, String message, Object...args ) {
        isTrueState(a <= 0, IllegalStateException.class, message, args);
    }

    public static void isLTEZero( short a, String message, Object...args ) {
        isTrueState(a <= 0, IllegalStateException.class, message, args);
    }

    public static void isLTEZero( int a, String message, Object...args ) {
        isTrueState(a <= 0, IllegalStateException.class, message, args);
    }

    public static void isLTEZero( long a, String message, Object...args ) {
        isTrueState(a <= 0, IllegalStateException.class, message, args);
    }

    public static void isLTEZero( float a, String message, Object...args ) {
        isLTEZero( a, DEFAULT_FLOAT_TOLERANCE, message, args );
    }

    public static void isLTEZero( float a, float tolerance, String message, Object...args ) {
        isTrueState(a - tolerance <= 0, IllegalStateException.class, message, args);
    }

    public static void isLTEZero( double a, String message, Object...args ) {
        isLTEZero( a, DEFAULT_DOUBLE_TOLERANCE, message, args );
    }

    public static void isLTEZero( double a, double tolerance, String message, Object...args ) {
        isTrueState(a - tolerance <= 0, IllegalStateException.class, message, args);
    }


// OPTIONAL

    public static <T> void argIsPresent( Optional<T> v, String argName ) {
        if ( !v.isPresent() ) {
            throwIllegalArgumentException( "%s is empty", argName );
        }
    }

    public static <T> void argIsEmpty( Optional<T> v, String argName ) {
        if ( v.isPresent() ) {
            throwIllegalArgumentException( "%s must be empty (however it actually contains '%s')", argName, v.get() );
        }
    }



/////////

    public static void isMultipleOf2( int v, String argName ) {
        if ( !MathUtils.isPowerOf2(v) ) {
            throwIllegalArgumentException("%s (%s) must be a power of 2", argName, v);
        }
    }

    public static void isMultipleOf2( long v, String argName ) {
        if ( !MathUtils.isPowerOf2(v) ) {
            throwIllegalArgumentException("%s (%s) must be a power of 2", argName, v);
        }
    }

    public static void argNotBlank( String chars, String argName ) {
        if ( isBlank(chars) ) {
            throwIllegalArgumentException("%s must not be blank (was %s)", argName, chars);
        }
    }

    public static void notBlank( String chars, String message, Object...args) {
        if ( isBlank(chars) ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void argNotEmpty( CharSequence chars, String argName ) {
        if ( chars == null || chars.length() == 0 ) {
            throwIllegalArgumentException("%s must not be empty (was %s)", argName, chars);
        }
    }

    public static void notEmpty( CharSequence chars, String message, Object...args ) {
        if ( chars == null || chars.length() == 0 ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void argIsInterface( Class c, String argName ) {
        if ( c == null ) {
            throwIllegalArgumentException("%s must not be null", argName);
        } else if ( !c.isInterface() ) {
            throwIllegalArgumentException("%s (%s) must be an interface", argName, c);
        }
    }

    public static void argNotNull( Object o, String argName ) {
        if ( o == null ) {
            throwIllegalArgumentException("%s must not be null", argName);
        }
    }

    public static void notNull( Object o, String message, Object...args ) {
        if ( o == null ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void argIsNull( Object o, String argName ) {
        if ( o != null ) {
            throwIllegalArgumentException("%s must be null but was %s", argName, o);
        }
    }

    public static void isNull( Object o, String message, Object...args ) {
        if ( o != null ) {
            throwIllegalArgumentException(IllegalStateException.class, message, args);
        }
    }

    public static void argIsTrue( boolean v, String argName ) {
        if ( !v ) {
            throwIllegalArgumentException("%s (%s) must be true", argName, v);
        }
    }

    public static void argIsFalse( boolean v, String argName ) {
        if ( v ) {
            throwIllegalArgumentException("%s (%s) must be false", argName, v);
        }
    }

    public static void isTrueState( boolean condition, String msg, Object... values ) {
        isTrueState(condition, IllegalStateException.class, msg, values);
    }

    public static void isTrueArg( boolean condition, String msg, Object... values ) {
        isTrueState(condition, IllegalArgumentException.class, msg, values);
    }

    public static <T extends Throwable> void isTrueState( boolean condition, Class<T> exceptionType, String msg, Object... values ) {
        if ( !condition ) {
            throwIllegalArgumentException(exceptionType, msg, values);
        }
    }

    public static void isFalseState( boolean condition, String msg, Object... values ) {
        if ( condition ) {
            throwIllegalArgumentException(IllegalStateException.class, msg, values);
        }
    }

    public static void isFalseArg( boolean condition, String msg, Object... values ) {
        if ( condition ) {
            throwIllegalArgumentException(IllegalArgumentException.class, msg, values);
        }
    }

    public static void argInclusiveBetween( int minInc, int v, int maxInc, String argName ) {
        if ( v < minInc || v > maxInc ) {
            throwIllegalArgumentException("%s is not within the specified bounds of %s <= %s <= %s", argName, minInc, v, maxInc);
        }
    }

    @SuppressWarnings("ConstantConditions")
    public static void argHasNoNullElements( Object[] array, String argName ) {
        if ( array == null ) {
            throwIllegalArgumentException("%s is not allowed to be null", argName);
        }

        for ( int i=0; i<array.length; i++ ) {
            Object e = array[i];

            if ( e == null ) {
                throwIllegalArgumentException("%s must not be null", argName + "[" + i + "]");
            }
        }
    }

    public static void fail( String msg, Object...values ) {
        throwIllegalArgumentException(msg, values);
    }

    public static void throwIllegalArgumentException( String msg, Object... values ) {
        throwIllegalArgumentException(IllegalArgumentException.class, msg, values);
    }

    @SuppressWarnings("unchecked")
    private static <T extends Throwable> void throwIllegalArgumentException( Class<T> exceptionType, String msg, Object... values ) {
        String formattedMessage = String.format(msg, formatValues(values));

        try {
            throw exceptionType.getConstructor(String.class).newInstance( formattedMessage );
        } catch ( Throwable ex ) {
            Backdoor.throwException( ex );
        }
    }

    private static void throwIllegalStateException( String msg, Object...values ) {
        String formattedMessage = String.format( msg, values );

        throw new IllegalStateException( formattedMessage );
    }

    private static void throwIndexOutOfBoundsException( String msg, Object...values ) {
        String formattedMessage = String.format( msg, formatValues(values) );

        throw new IndexOutOfBoundsException( formattedMessage );
    }

    private static Object[] formatValues( Object[] values ) {
        int      numValues       = values.length;
        Object[] formattedValues = new Object[numValues];

        for ( int i=0; i<numValues; i++ ) {
            formattedValues[i] = formatValue(values[i]);
        }

        return formattedValues;
    }

    private static Object formatValue( Object value ) {
        if ( value instanceof String ) {
            return "'"+value+"'";
        } else if ( value instanceof Class ) {
            return ((Class) value).getName();
        }

        return value;
    }


// isEqualTo

    public static void argIsEqualTo( byte a, byte b, String argName ) {
        if ( a != b ) {
            throwIllegalArgumentException("%s (%s) must be == %s", argName, a, b);
        }
    }

    public static void argIsEqualTo( short a, short b, String argName ) {
        if ( a != b ) {
            throwIllegalArgumentException("%s (%s) must be == %s", argName, a, b);
        }
    }

    public static void argIsEqualTo( int a, int b, String argName ) {
        if ( a != b ) {
            throwIllegalArgumentException("%s (%s) must be == %s", argName, a, b);
        }
    }

    public static void argIsEqualTo( long a, long b, String argName ) {
        if ( a != b ) {
            throwIllegalArgumentException("%s (%s) must be == %s", argName, a, b);
        }
    }

    public static void argIsEqualTo( long a, long b, String argName1, String argName2 ) {
        if ( a != b ) {
            throwIllegalArgumentException("%s (%s) must be == %s (%s)", argName1, a, argName2, b);
        }
    }

    public static void argIsEqualTo( float a, float b, String argName ) {
        argIsEqualTo( a, b, argName, DEFAULT_FLOAT_TOLERANCE );
    }

    public static void argIsEqualTo( float a, float b, String argName, float tolerance ) {
        if ( Math.abs(a-b) <= tolerance ) {
            throwIllegalArgumentException("%s (%s) must be == %s", argName, a, b);
        }
    }

    public static void argIsEqualTo( double a, double b, String argName ) {
        argIsEqualTo( a, b, argName, DEFAULT_DOUBLE_TOLERANCE );
    }

    public static void argIsEqualTo( double a, double b, String argName, double tolerance ) {
        if ( Math.abs(a - b) <= tolerance ) {
            throwIllegalArgumentException("%s (%s) must be == %s", argName, a, b);
        }
    }

    public static <T> void argIsEqualTo( T a, T b, String argName ) {
        if ( !a.equals(b) ) {
            throwIllegalArgumentException("%s (%s) must be == %s", argName, a, b);
        }
    }

    public static void isEqualTo( byte a, byte b, String msg ) {
        if ( a != b ) {
            throwIllegalStateException(msg, a, b);
        }
    }

    public static void isEqualTo( short a, short b, String msg ) {
        if ( a != b ) {
            throwIllegalStateException(  msg, a, b );
        }
    }

    public static void isEqualTo( int a, int b, String msg ) {
        if ( a != b ) {
            throwIllegalStateException(  msg, a, b );
        }
    }

    public static void isEqualTo( long a, long b, String msg ) {
        if ( a != b ) {
            throwIllegalStateException(msg, a, b);
        }
    }

    public static void isEqualTo( long a, long b, String nameA, String nameB ) {
        if ( a != b ) {
            throwIllegalArgumentException("%s (%s) != %s (%s)", nameA, a, nameB, b);
        }
    }

    public static void isEqualTo( float a, float b, String msg ) {
        isEqualTo( a, b, msg, DEFAULT_FLOAT_TOLERANCE );
    }

    public static void isEqualTo( float a, float b, String msg, float tolerance ) {
        if ( Math.abs(a-b) <= tolerance ) {
            throwIllegalStateException(msg, a, b);
        }
    }

    public static void isEqualTo( double a, double b, String msg ) {
        isEqualTo( a, b, msg, DEFAULT_DOUBLE_TOLERANCE );
    }

    public static void isEqualTo( double a, double b, String msg, double tolerance ) {
        if ( Math.abs(a - b) <= tolerance ) {
            throwIllegalStateException(  msg, a, b );
        }
    }

    public static <T> void isEqualTo( T a, T b, String msg ) {
        if ( !a.equals(b) ) {
            throwIllegalStateException(msg, a, b);
        }
    }

    public static <T> void argIsNotEqualTo( T a, T b, String argName ) {
        if ( Objects.equals(a, b) ) {
            throwIllegalArgumentException("%s (%s) must be != %s", argName, a, b);
        }
    }

    public static void argIsNotEqualTo( long a, long b, String argName1, String argName2 ) {
        if ( a == b ) {
            throwIllegalArgumentException("%s (%s) must be != %s (%s)", argName1, a, argName2, b);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt; maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetween( byte minInc, byte n, byte maxExc, String argName ) {
        if ( !(minInc <= n && n < maxExc) ) {
            throwIllegalArgumentException("%s (%s) must be >= %s and < %s", argName, n, minInc, maxExc);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt; maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetween( short minInc, short n, short maxExc, String argName ) {
        if ( !(minInc <= n && n < maxExc) ) {
            throwIllegalArgumentException("%s (%s) must be >= %s and < %s", argName, n, minInc, maxExc);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt; maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetween( char minInc, char n, char maxExc, String argName ) {
        if ( !(minInc <= n && n < maxExc) ) {
            throwIllegalArgumentException("%s (%s) must be >= %s and < %s", argName, n, minInc, maxExc);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt; maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetween( int minInc, int n, int maxExc, String argName ) {
        if ( !(minInc <= n && n < maxExc) ) {
            throwIllegalArgumentException("%s (%s) must be >= %s and < %s", argName, n, minInc, maxExc);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt; maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetween( long minInc, long n, long maxExc, String argName ) {
        if ( !(minInc <= n && n < maxExc) ) {
            throwIllegalArgumentException("%s (%s) must be >= %s and < %s", argName, n, minInc, maxExc);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt;= maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetweenInc( byte minInc, byte n, byte maxInc, String argName ) {
        if ( !(minInc <= n && n <= maxInc) ) {
            throwIllegalArgumentException("%s (%s) must be >= %s and <= %s", argName, n, minInc, maxInc);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt;= maxInc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetweenInc( short minInc, short n, short maxInc, String argName ) {
        if ( !(minInc <= n && n <= maxInc) ) {
            throwIllegalArgumentException("%s (%s) must be >= %s and <= %s", argName, n, minInc, maxInc);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt;= maxInc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetweenInc( char minInc, char n, char maxInc, String argName ) {
        if ( !(minInc <= n && n <= maxInc) ) {
            throwIllegalArgumentException("%s (%s) must be >= %s and <= %s", argName, n, minInc, maxInc);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt;= maxInc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetweenInc( int minInc, int n, int maxInc, String argName ) {
        if ( !(minInc <= n && n <= maxInc) ) {
            throwIllegalArgumentException("%s (%s) must be >= %s and <= %s", argName, n, minInc, maxInc);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt;= maxInc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetweenInc( long minInc, long n, long maxInc, String argName ) {
        if ( !(minInc <= n && n <= maxInc) ) {
            throwIllegalArgumentException("%s (%s) must be >= %s and <= %s", argName, n, minInc, maxInc);
        }
    }

    /**
     * The specified range must equal or be within specified range.
     */
    public static void argIsWithinRange( int minInc, int minValue, int maxValue, int maxExc, String minValueName, String maxValueName ) {
        argIsBetween( minInc, minValue, maxExc, minValueName );
        argIsBetween( minInc, maxValue, maxExc + 1, maxValueName );

        if ( !(minValue < maxValue) ) {
            throwIllegalArgumentException("%s (%s) < %s (%s)", minValueName, minValue, maxValueName, maxValue);
        }
    }

    /**
     * The specified range must equal or be within specified range.
     */
    public static void argIsWithinRange( long minInc, long v1, long v2, long maxExc, String minValueName, String maxValueName ) {
        argIsBetweenInc( minInc, v1, maxExc, minValueName );
        argIsBetweenInc( minInc, v2, maxExc, maxValueName );

        if ( !(v1 <= v2) ) {
            throwIllegalArgumentException("%s (%s) <= %s (%s)", minValueName, v1, maxValueName, v2);
        }
    }

    public static void argIsUnsignedByte( int v, String name ) {
        if ( v < 0 || v > 255 ) {
            throwIllegalArgumentException("%s (%s) is outside the bounds of an unsigned byte (0-255)", name, v);
        }
    }

    public static void isInt( long v, String argName ) {
        if ( (v & 0x7FFFFFFF) != v ) {
            throwIllegalArgumentException("%s (%s) is out of bounds of an int", argName, v);
        }
    }

    public static void isUnsignedInt( long v, String argName ) {
        if ( (v & 0xFFFFFFFF) != v ) {
            throwIllegalArgumentException("%s (%s) is out of bounds of an unsigned int", argName, v);
        }
    }

    public static void isUnsignedShort( int v, String argName ) {
        if ( (v & 0xFFFF) != v ) {
            throwIllegalArgumentException("%s (%s) is out of bounds of an unsigned short", argName, v);
        }
    }

    /**
     * Fails if the supplied function returns a String.  The string is a failure message.
     * Because function0 is invoked 'lazily', use this method when the arguments
     * to a normal call to QA cannot be hard coded.  Thus when checks are disabled, there
     * will be no runtime cost.
     */
    public static void assertCondition( Supplier<String> function0 ) {
        String failureMessage = function0.get();

        if ( failureMessage != null ) {
            throwIllegalArgumentException(failureMessage);
        }
    }


    private static final boolean isBlank( String s ) {
        return s == null || s.trim().length() == 0;
    }

    public static void throwIfNotDirectory( File f, String name ) {
        if ( f.isFile() ) {
            throwIllegalArgumentException( "'%s' (%s) is not a directory", name, f.getAbsoluteFile() );
        }
    }

    public static void throwIfNotWritable( File f, String name ) {
        if ( !f.canWrite() ) {
            throwIllegalArgumentException( "'%s' (%s) is not writable", name, f.getAbsoluteFile() );
        }
    }

    public static void argMatches( RegExp regExp, String text, String name ) {
        if ( !regExp.matches(text) ) {
            throwIllegalArgumentException( "%s (%s) does not match %s", name, text, regExp.toString() );
        }
    }

    /**
     * Error if obj is found within the collection.
     */
    public static <T> void argNotIn( Collection<T> collection, T obj, String msg ) {
        if ( collection.contains(obj) ) {
            throwIllegalArgumentException( msg );
        }
    }

    public static <T, A extends Annotation> void hasAnnotation( Class<T> type, Class<A> annotationType ) {
        if ( type.getAnnotation(annotationType) == null ) {
            fail( "%s is not annotated with @%s", type, annotationType );
        }
    }

    public static void isException( Class<?> expectedEx, String expectedMsg, Tryable<?> result ) {
        Throwable actualEx = result.getFailure().toException();

        isEqualTo( expectedEx, actualEx.getClass(), "%s does not match %s" );
        isEqualTo( expectedMsg, expectedMsg, "%s does not match %s" );
    }

}
