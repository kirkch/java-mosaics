package mosaics.lang.time;

import lombok.Value;
import lombok.With;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.lang.ComparableMixin;
import mosaics.lang.QA;
import mosaics.utils.ComparatorUtils;

import java.util.Comparator;


/**
 * Specifies a range on the DTM line.  Covers a DTM from one value to another.
 */
@With
@Value
public class DTMRange implements ComparableMixin<DTMRange> {
    public static final Comparator<DTMRange> DTM_RANGE_COMPARATOR = ComparatorUtils
        .comparing( DTMRange::getLhsInc )
        .thenComparing( DTMRange::getRhsExc );

    /**
     * Create an infinite series of DTMRanges increasing 'from' in units of 'size'.
     */
    public static FPIterable<DTMRange> repeatDTMRangeAsc( DTM from, DTMInterval size ) {
        return FPIterable.generate(
            new DTMRange(from, from.addInterval(size)),
            prev -> FP.option(prev.shiftRight(size))
        );
    }

    /**
     * Create an infinite series of DTMRanges increasing 'from' in units of 'size'.
     */
    public static FPIterable<DTMRange> repeatDTMRangeAsc( DTM from, DTMInterval size, DTM max ) {
        DTM      upperBound = from.addInterval( size );
        DTMRange initial    = new DTMRange(from, upperBound);
        if ( from.isGTE(max) || upperBound.isGT(max) ) {
            return FPIterable.empty();
        }

        return FPIterable.generate(
            initial,
            prev -> {
                DTMRange next = prev.shiftRight( size );

                if ( next.contains(max) ) {
                    return FP.emptyOption();
                } else {
                    return FP.option( next );
                }
            }
        );
    }

    /**
     * Create an infinite series of DTMRanges increasing 'from' in units of 'size'.
     *
     * @param from the date that the first frame ends at (exc)
     */
    public static FPIterable<DTMRange> repeatDTMRangeDesc( DTM from, DTMInterval size, DTM min ) {
        DTM lowerBound = from.subtractInterval( size );
        if ( min.isGT(lowerBound) ) {
            return FPIterable.empty();
        }

        return FPIterable.generate(
            new DTMRange( lowerBound, from),
            prev -> {
                DTMRange next = prev.shiftLeft( size );

                if ( next.getLhsInc().isLT(min) || next.getRhsExc().isLTE(min) ) {
                    return FP.emptyOption();
                } else {
                    return FP.option( next );
                }
            }
        );
    }

    /**
     * Create an infinite series of DTMRanges decreasing 'from' in units of 'size'.
     */
    public static FPIterable<DTMRange> repeatDTMRangeDesc( DTM from, DTMInterval size ) {
        return FPIterable.generate(
            new DTMRange(from, from.addInterval(size)),
            prev -> FP.option(prev.shiftLeft(size))
        );
    }

    private final DTM lhsInc;
    private final DTM rhsExc;

    public DTMRange( DTM fromInc, DTM rhsExc ) {
        QA.argIsLTE( fromInc, rhsExc, "lhsInc", "rhsExc" );

        this.lhsInc = fromInc;
        this.rhsExc = rhsExc;
    }

    public DTMRange increaseLHS( DTMInterval delta ) {
        return this.withLhsInc( lhsInc.addInterval(delta) );
    }

    public DTMRange increaseRHS( DTMInterval delta ) {
        return this.withRhsExc( rhsExc.addInterval(delta) );
    }

    /**
     * Add delta to both the from and to edges of this range.  Essentially shifting the entire
     * range to the right on the dtm line.
     */
    public DTMRange shiftRight( DTMInterval delta ) {
        return new DTMRange( lhsInc.addInterval(delta), rhsExc.addInterval(delta) );
    }

    /**
     * Subtract delta from both the from and to edges of this range.  Essentially shifting the entire
     * range to the left on the dtm line.
     */
    public DTMRange shiftLeft( DTMInterval delta ) {
        return new DTMRange( lhsInc.subtractInterval(delta), rhsExc.subtractInterval(delta) );
    }

    public DTMRange decreaseLHS( DTMInterval delta ) {
        return this.withLhsInc( lhsInc.subtractInterval(delta) );
    }

    public DTMRange decreaseRHS( DTMInterval delta ) {
        return this.withRhsExc( rhsExc.subtractInterval(delta) );
    }

    public DTMInterval toInterval() {
        long millis = getRhsExc().getMillisSinceEpoch() - getLhsInc().getMillisSinceEpoch();

        return DTMInterval.milliseconds( millis );
    }

    public int compareTo( DTMRange other ) {
        return DTM_RANGE_COMPARATOR.compare( this, other );
    }

    public boolean overlaps( DTMRange other ) {
        //           |---------------|
        //     |---|
        //       |--------|
        //       |-------------------------|
        //                     |-----------|
        //                                 |------|
        return this.contains(other.lhsInc) || this.contains(other.rhsExc.subtractMillis());
    }

    public boolean contains( DTM v ) {
        return v.isGTE(lhsInc) && v.isLT(rhsExc);
    }

    /**
     * Merges the two ranges.

     * @return a new range that covers both ranges
     */
    public DTMRange merge( DTMRange other ) {
        DTMRange merge = new DTMRange( this.getLhsInc().min( other.getLhsInc() ), this.getRhsExc().max( other.getRhsExc() ) );
        if ( merge.equals(this) ) {
            return this;
        } else if ( merge.equals(other) ) {
            return other;
        }

        return merge;
    }
}
