package mosaics.lang.time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class DayUtils {
    public static final DateTimeFormatter DATE_ISO8601_FORMATTER = DateTimeFormatter.ISO_DATE;


    public static long parse( String str, DateTimeFormatter formatter ) {
        return LocalDate.parse(str,formatter).toEpochDay();
    }

    public static long toEpochDays( int year, int month, int day ) {
        return LocalDate.of( year, month, day ).toEpochDay();
    }

    public static boolean isWeekend( DayOfWeek dayOfWeek ) {
        return switch (dayOfWeek) {
            case SATURDAY, SUNDAY -> true;
            default               -> false;
        };
    }
}
