package mosaics.lang.time;

import lombok.Value;

import java.io.Serializable;
import java.util.TimeZone;


@Value
public class TZ implements Serializable {

    private static final long serialVersionUID = 1289461083540L;

    public static final TZ UTC    = getTimeZone("UTC");
    public static final TZ LONDON = getTimeZone("Europe/London");
    public static final TZ EST    = getTimeZone("EST");
    public static final TZ CET    = getTimeZone("CET");
    public static final TZ ET    = getTimeZone("ET");
    public static final TZ NEW_YORK    = getTimeZone("America/New_York");

    public static TZ getLocalMachineTimeZone() {
        return new TZ( TimeZone.getDefault());
    }

    public static TZ getTimeZone( String zoneId ) {
        return new TZ(zoneId);
    }


    private java.util.TimeZone jdkTimeZone;


    public TZ(String zoneId) {
        this( java.util.TimeZone.getTimeZone(zoneId) );
    }

    public TZ(java.util.TimeZone timeZone) {
        this.jdkTimeZone = timeZone;
    }

    java.util.TimeZone toJDKTimeZone() {
        return jdkTimeZone;
    }

    public String toString() {
        return jdkTimeZone.getID();
    }

}
