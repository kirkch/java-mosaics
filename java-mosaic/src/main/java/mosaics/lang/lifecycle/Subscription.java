package mosaics.lang.lifecycle;

import mosaics.lang.TryUtils;
import mosaics.lang.functions.BooleanFunction0;
import mosaics.lang.functions.VoidFunction0;


/**
 * Represents a registration of some kind.  The subscription can be cancelled at any time, and
 * queried to see if the subscription is still valid.
 */
public class Subscription implements AutoCloseable {

    private static final Subscription NO_OP = new Subscription( () -> {} );

    public static Subscription noOp() {
        return NO_OP;
    }

    // NB: We have two implementations of Subscription.  One is Request aware, the other is not.
    //     This duplication was done purely as a time saver (techdebt) at a time that I was working to
    //     a time scale.  Refactoring Request and unpicking the dependencies will take some time.
    //     TODO move Request so that we can have one request aware subscription.

    private BooleanFunction0 isActiveFetcher;
    private VoidFunction0 cancelSubscriptionFunc;


    public Subscription() {
        this( VoidFunction0.noOp() );
    }

    public Subscription( VoidFunction0 cancelSubscriptionFunc ) {
        this( cancelSubscriptionFunc, () -> true );
    }

    public Subscription( VoidFunction0 cancelSubscriptionFunc, BooleanFunction0 isActiveFetcher ) {
        this.cancelSubscriptionFunc = cancelSubscriptionFunc;
        this.isActiveFetcher        = isActiveFetcher;
    }


    /**
     * Returns true if this subscription is currently still subscribed.  That is, cancel() has
     * not yet been called.
     */
    public synchronized boolean isActive() {
        return isActiveFetcher.invoke();
    }


    public void close() {
        cancel();
    }

    /**
     * Cancels this subscription.  This operation may not be undone.
     */
    public synchronized void cancel() {
        if ( isActive() ) {
            try {
                cancelSubscriptionFunc.invoke();

                cancelSubscriptionFunc = null;
            } finally {
                isActiveFetcher = () -> false;
            }
        }
    }


    /**
     * Chains two subscriptions together.  isActive returns true when all of the chained
     * subscriptions return true and cancelling this subscription will cancel each of the child
     * subscriptions.
     */
    public Subscription and( Subscription b ) {
        if ( b == null ) {
            return this;
        }

        Subscription a = this;
        return new Subscription() {
            public boolean isActive() {
                return a.isActive() || b.isActive();
            }

            public void cancel() {
                TryUtils.invokeAndIgnoreException( () -> a.cancel() );
                TryUtils.invokeAndIgnoreException( () -> b.cancel() );
            }
        };
    }

}
