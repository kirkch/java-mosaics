package mosaics.lang.clock;


import lombok.SneakyThrows;
import mosaics.lang.functions.VoidFunction0WithThrows;
import mosaics.lang.time.DTM;
import mosaics.lang.time.Day;

import static mosaics.lang.Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND;


/**
 * Retrieve the current time.  Different versions support different test modes, and this is where
 * this interface differs from the Clock introduced in JDK8.  We experimented removing this interface
 * when JDK8 came out, however JDK8 does not support the different test modes which is useful.
 */
public interface Clock {
    public abstract long currentTimeMillis();
    public abstract long currentTimeNanos();

    public boolean isModifiable();

    public abstract long incMillis( long millis );
    public abstract void setMillis( long millisSinceEpoch );


    public default DTM currentDTM() {
        return new DTM(currentTimeMillis());
    }

    public default Day currentDay() {
        return new Day(currentTimeMillis());
    }

    /**
     * Invokes the supplied function, and returns how long it took to execute in milliseconds.
     */
    @SneakyThrows
    public default double timeMillis( VoidFunction0WithThrows f ) {
        long t0 = currentTimeNanos();

        f.invoke();

        return (currentTimeNanos() - t0) / (double) NUMBER_OF_NANOSECONDS_PER_MILLISECOND;
    }

    public default long currentDurationNanos( long startNanos ) {
        return (currentTimeNanos() - startNanos);
    }

    public default double currentDurationNanosAsMillis( long startNanos ) {
        return (currentTimeNanos() - startNanos) / (double) NUMBER_OF_NANOSECONDS_PER_MILLISECOND;
    }

    public default long currentDurationMillis( long startMillis ) {
        return currentTimeMillis() - startMillis;
    }
}
