package mosaics.lang.clock;


/**
 * Sets the time of the clock and then carries on ticking at the same rate from there.
 */
public class OffsetClock implements Clock {
    private long startMillis;
    private long baseMillis;

    public OffsetClock( long baseMillis ) {
        setMillis( baseMillis );
    }

    public long currentTimeMillis() {
        long nowMillis = System.currentTimeMillis();
        long delta     = nowMillis - startMillis;

        return baseMillis + delta;
    }

    public long currentTimeNanos() {
        return System.nanoTime();
    }

    public boolean isModifiable() {
        return true;
    }

    public long incMillis( long duration ) {
        this.baseMillis += duration;

        return currentTimeMillis();
    }

    public void setMillis( long millisSinceEpoch ) {
        this.startMillis = System.currentTimeMillis();
        this.baseMillis  = millisSinceEpoch;
    }

}
