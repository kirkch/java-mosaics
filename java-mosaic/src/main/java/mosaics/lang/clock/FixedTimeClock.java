package mosaics.lang.clock;

import mosaics.lang.Backdoor;
import mosaics.lang.QA;


public class FixedTimeClock implements Clock {
    private long epochMillis;

    public FixedTimeClock() {
        this( System.currentTimeMillis() );
    }

    public FixedTimeClock( long epochMillis ) {
        QA.notNull( epochMillis, "epoch");

        this.epochMillis = epochMillis;
    }

    public synchronized long currentTimeMillis() {
        return epochMillis;
    }

    public long currentTimeNanos() {
        return epochMillis * Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND;
    }

    public boolean isModifiable() {
        return true;
    }

    public synchronized long incMillis( long millis ) {
        this.epochMillis = epochMillis + millis;

        return epochMillis;
    }

    public synchronized void setMillis( long millisSinceEpoch ) {
        this.epochMillis = millisSinceEpoch;
    }

}
