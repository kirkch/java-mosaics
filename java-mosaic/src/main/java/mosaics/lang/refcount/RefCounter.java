package mosaics.lang.refcount;

import mosaics.fp.collections.ConsList;
import mosaics.lang.QA;
import mosaics.lang.annotations.ThreadSafe;
import mosaics.lang.functions.Function2;
import mosaics.lang.functions.VoidFunction0;
import mosaics.lang.functions.VoidFunction1;
import mosaics.lang.functions.VoidFunctionLO;
import mosaics.lang.lockable.CloneMixin;
import mosaics.lang.lifecycle.Subscription;

import java.util.concurrent.atomic.AtomicReference;


/**
 * Helper for implementing RefCounted2.  It offers two advantages, first it pulls together everything
 * needed to implement RefCounted2 without having to use inheritance to pull in RefCountedMixin2 AND
 * it lets us swap out the implementation at runtime.  The ability to swap implementations is very
 * useful during development as it lets us use a 'slow' and 'over sized' version that performs more
 * sanity checks, captures more data and logs more information that can help to track down defects
 * quicker without slowing down the production version of the system.
 *
 * A thread safe/lockless implementation of RefCountManager2 that is optimised for production use.
 */
@ThreadSafe
public class RefCounter<T> {
    private final T                              refCountedResource;
    private final VoidFunction1<T>               releaseResourceF;
    private final AtomicReference<RefCountState> state              = new AtomicReference<>(new RefCountState());

    private final boolean                        isDebugEnabled;


    public RefCounter( T refCountedResource, VoidFunction1<T> releaseResourceF ) {
        this( refCountedResource, releaseResourceF, false );
    }

    public RefCounter( T refCountedResource, VoidFunction1<T> releaseResourceF, boolean isDebugEnabled ) {
        QA.notNull( refCountedResource, "refCountedResource" );
        QA.notNull( releaseResourceF, "releaseResourceF" );

        this.refCountedResource = refCountedResource;
        this.releaseResourceF   = releaseResourceF;
        this.isDebugEnabled     = isDebugEnabled;
    }

    public String toString() {
        return "RefCounter2("+getRefCount()+")";
    }


    public long getRefCount() {
        return state.get().refCounter;
    }

    public void incRefCount() {
        RefCountState prev, next;
        do {
            prev = state.get();
            next = prev.incRefCount();
        } while (!state.compareAndSet(prev, next));

        if ( prev.refCounter < next.refCounter ) {
            invokeCallbacks( prev.refIncCallbacks, next.refCounter );
        }
    }

    public void decRefCount() {
        RefCountState prev, next;
        do {
            prev = state.get();
            next = prev.decRefCount();
        } while (!state.compareAndSet(prev, next));

        if ( prev.refCounter > next.refCounter ) {
            if ( prev.refCounter > 0 && next.refCounter == 0 ) {
                invokeCallbacks( prev.justBeforeReleaseCallbacks );

                releaseResourceF.invoke( refCountedResource );

                invokeCallbacks0( prev.justAfterReleaseCallbacks );
            }

            invokeCallbacks( prev.refDecCallbacks, next.refCounter );
        }
    }

    public Subscription onRefIncInvoke( VoidFunctionLO<T> callback ) {
        return registerCallback( callback, RefCountState::addOnRefIncInvoke, RefCountState::removeOnRefIncInvoke );
    }

    public Subscription onRefDecInvoke( VoidFunctionLO<T> callback ) {
        return registerCallback( callback, RefCountState::addOnRefDecInvoke, RefCountState::removeOnRefDecInvoke );
    }

    public Subscription justBeforeReleaseInvoke( VoidFunction1<T> callback ) {
        return registerCallback( callback, RefCountState::addJustBeforeReleaseInvoke, RefCountState::removeJustBeforeReleaseInvoke );
    }

    public Subscription justAfterReleaseInvoke( VoidFunction0 callback ) {
        return registerCallback( callback, RefCountState::addJustAfterReleaseInvoke, RefCountState::removeJustAfterReleaseInvoke );
    }


    private <C> Subscription registerCallback( C callback, Function2<RefCountState,C,RefCountState> append, Function2<RefCountState,C,RefCountState> remove ) {
        state.updateAndGet( s -> append.invoke(s,callback) );

        return new Subscription( () -> this.state.updateAndGet(currentState -> remove.invoke(currentState,callback)) );
    }

    private void invokeCallbacks0( ConsList<VoidFunction0> callbacks ) {
        callbacks.forEach( VoidFunction0::invoke );
    }

    private void invokeCallbacks( ConsList<VoidFunction1<T>> callbacks ) {
        callbacks.forEach( callback -> callback.invoke(refCountedResource) );
    }

    private void invokeCallbacks( ConsList<VoidFunctionLO<T>> callbacks, long newRefCount ) {
        callbacks.forEach( callback -> callback.invoke(newRefCount,refCountedResource) );
    }

    public boolean isActive() {
        return getRefCount() > 0;
    }


    private class RefCountState extends CloneMixin<RefCountState> {
        private long                        refCounter                 = 1;

        private ConsList<VoidFunctionLO<T>> refIncCallbacks            = ConsList.nil();
        private ConsList<VoidFunctionLO<T>> refDecCallbacks            = ConsList.nil();
        private ConsList<VoidFunction1<T>>  justBeforeReleaseCallbacks = ConsList.nil();
        private ConsList<VoidFunction0>     justAfterReleaseCallbacks  = ConsList.nil();


        public RefCountState incRefCount() {
            return update( newState -> {
                if ( isDebugEnabled && newState.refCounter == 0 ) {
                    throw new IllegalStateException( "this resource has already been released, do not call incRefCount after the refCount has reached zero" );
                }

                newState.refCounter += 1;
            } );
        }

        public RefCountState decRefCount() {
            return update( newState -> {
                if ( isDebugEnabled && newState.refCounter <= 0 ) {
                    throw new IllegalStateException( "calls to inc/dec ref count must be ordered and balanced; ref count just went negative" );
                }

                newState.refCounter -= 1;
            } );
        }

        public RefCountState addOnRefIncInvoke( VoidFunctionLO<T> callback ) {
            return updateCallback( newState -> newState.refIncCallbacks = newState.refIncCallbacks.append(callback) );
        }

        public RefCountState addOnRefDecInvoke( VoidFunctionLO<T> callback ) {
            return updateCallback( newState -> newState.refDecCallbacks = newState.refDecCallbacks.append(callback) );
        }

        public RefCountState addJustBeforeReleaseInvoke( VoidFunction1<T> callback ) {
            return updateCallback( newState -> newState.justBeforeReleaseCallbacks = newState.justBeforeReleaseCallbacks.append(callback) );
        }

        public RefCountState addJustAfterReleaseInvoke( VoidFunction0 callback ) {
            return updateCallback( newState -> newState.justAfterReleaseCallbacks = newState.justAfterReleaseCallbacks.append(callback) );
        }

        public RefCountState removeOnRefIncInvoke( VoidFunctionLO<T> callback ) {
            return update( newState -> newState.refIncCallbacks = newState.refIncCallbacks.remove(callback) );
        }

        public RefCountState removeOnRefDecInvoke( VoidFunctionLO<T> callback ) {
            return update( newState -> newState.refDecCallbacks = newState.refDecCallbacks.remove(callback) );
        }

        public RefCountState removeJustBeforeReleaseInvoke( VoidFunction1<T> callback ) {
            return update( newState -> newState.justBeforeReleaseCallbacks = newState.justBeforeReleaseCallbacks.remove(callback) );
        }

        public RefCountState removeJustAfterReleaseInvoke( VoidFunction0 callback ) {
            return update( newState -> newState.justAfterReleaseCallbacks = newState.justAfterReleaseCallbacks.remove(callback) );
        }

        private RefCountState updateCallback( VoidFunction1<RefCountState> updater ) {
            return update( newState -> {
                if ( newState.refCounter <= 0 ) {
                    throw new IllegalStateException( "the counter has reached zero, why subscribe callbacks now?" );
                }

                updater.invoke( newState );
            } );
        }

        private RefCountState update( VoidFunction1<RefCountState> updater ) {
            RefCountState updatedState = this.clone();

            updater.invoke( updatedState );

            return updatedState;
        }
    }
}
