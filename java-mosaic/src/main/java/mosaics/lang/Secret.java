package mosaics.lang;

import lombok.Value;
import lombok.With;


/**
 * Strings stored within the class are marked as confidential.  Take all security pro-cautions to
 * prevent its contents from being leaked.
 */
@With
@Value
public class Secret {
    private String value;

    public String toString() {
        return "********";
    }
}
