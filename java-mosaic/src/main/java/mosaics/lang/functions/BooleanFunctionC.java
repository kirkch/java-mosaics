package mosaics.lang.functions;


import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import lombok.Value;

import java.util.Objects;


public interface BooleanFunctionC {
    public boolean invoke( char c );


    public default FPOption<String> getDescription() {
        return FP.emptyOption();
    }

    public default BooleanFunctionC withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return this;
        }

        return new BooleanFunctionCWithDescription( FP.option(desc), this );
    }

    public default BooleanFunctionC invert() {
        return new InvertedBooleanFunctionC( this );
    }
}


@Value
class InvertedBooleanFunctionC implements BooleanFunctionC {
    private BooleanFunctionC f;

    public boolean invoke( char c ) {
        return !f.invoke(c);
    }

    public BooleanFunctionC invert() {
        return f;
    }
}


@Value
class BooleanFunctionCWithDescription implements BooleanFunctionC {
    private FPOption<String> description;
    private BooleanFunctionC f;

    public boolean invoke( char c ) {
        return f.invoke(c);
    }

    public BooleanFunctionC withDescription( String desc ) {
        if ( Objects.equals(this.description.get(), desc) ) {
            return this;
        } else if ( desc == null || desc.trim().length() == 0 ) {
            return f;
        }

        return new BooleanFunctionCWithDescription( FP.option(desc), f );
    }
}
