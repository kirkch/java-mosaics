package mosaics.lang.functions;

import mosaics.lang.Backdoor;

import java.util.Objects;


public interface VoidFunction2<A,B> {
    public static final VoidFunction2<?,?> NO_OP = (a,b) -> {};

    public static <A,B> VoidFunction2<A,B> noOp() {
        return Backdoor.cast(NO_OP);
    }

    public void invoke(A a, B b);


    public default VoidFunction2<A,B> withDescription( String desc ) {
        return new VoidFunction2WithDescription<>( this, desc );
    }
}

class VoidFunction2WithDescription<A,B> implements VoidFunction2<A,B> {
    private String             description;
    private VoidFunction2<A,B> f;

    public VoidFunction2WithDescription( VoidFunction2<A,B> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public void invoke( A a, B b ) {
        f.invoke(a,b);
    }

    public VoidFunction2<A,B> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new VoidFunction2WithDescription<>( this, desc );
    }
}
