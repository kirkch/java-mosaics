package mosaics.lang.functions;

import java.util.Objects;


public interface VoidFunction3WithThrows<A,B,C> {
    public static final VoidFunction3WithThrows<?,?,?> NO_OP = (a,b,c) -> {};

    public void invoke(A a, B b, C c) throws Throwable;


    public default VoidFunction3WithThrows<A,B,C> withDescription( String desc ) {
        return new VoidFunction3WithThrowsWithDescription<>( this, desc );
    }
}

class VoidFunction3WithThrowsWithDescription<A,B,C> implements VoidFunction3WithThrows<A,B,C> {
    private String             description;
    private VoidFunction3WithThrows<A,B,C> f;

    public VoidFunction3WithThrowsWithDescription( VoidFunction3WithThrows<A,B,C> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public void invoke( A a, B b, C c ) throws Throwable{
        f.invoke(a,b,c);
    }

    public VoidFunction3WithThrows<A,B,C> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new VoidFunction3WithThrowsWithDescription<>( this, desc );
    }
}
