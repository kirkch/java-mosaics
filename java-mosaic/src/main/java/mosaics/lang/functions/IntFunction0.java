package mosaics.lang.functions;

import java.util.Objects;


public interface IntFunction0 {
    public int invoke();

    public default IntFunction0 withDescription( String desc ) {
        return new IntFunction0WithDescription( this, desc );
    }
}

class IntFunction0WithDescription implements IntFunction0 {
    private String       description;
    private IntFunction0 f;

    public IntFunction0WithDescription( IntFunction0 f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public int invoke() {
        return f.invoke();
    }

    public IntFunction0 withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new IntFunction0WithDescription( this, desc );
    }
}
