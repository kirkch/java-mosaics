package mosaics.lang.functions;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import lombok.Value;

import java.util.Objects;


public interface BooleanFunction2<A,B> {
    public boolean invoke(A a, B b);

    public default FPOption<String> getDescription() {
        return FP.emptyOption();
    }

    public default BooleanFunction2<A,B> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return this;
        }

        return new BooleanFunction2WithDescription<>( FP.option(desc), this );
    }

    public default BooleanFunction2<A,B> invert() {
        return new InvertedBooleanFunction2<>(this);
    }
}


@Value
class InvertedBooleanFunction2<A,B> implements BooleanFunction2<A,B> {
    private BooleanFunction2<A,B> f;

    public boolean invoke( A a, B b ) {
        return !f.invoke(a, b);
    }

    public BooleanFunction2<A,B> invert() {
        return f;
    }
}


@Value
class BooleanFunction2WithDescription<A,B> implements BooleanFunction2<A,B> {
    private FPOption<String>      description;
    private BooleanFunction2<A,B> f;

    public boolean invoke( A a, B b ) {
        return f.invoke(a, b);
    }

    public BooleanFunction2<A,B> withDescription( String desc ) {
        if ( Objects.equals(this.description.get(), desc) ) {
            return this;
        } else if ( desc == null || desc.trim().length() == 0 ) {
            return f;
        }

        return new BooleanFunction2WithDescription<>( FP.option(desc), f );
    }
}
