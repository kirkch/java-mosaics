package mosaics.lang.functions;

import java.util.Objects;


public interface DoubleFunction1<A1> {
    public double invoke(A1 arg1);

    public default DoubleFunction1<A1> withDescription( String desc ) {
        return new DoubleFunction1WithDescription<>( this, desc );
    }
}

class DoubleFunction1WithDescription<A> implements DoubleFunction1<A> {
    private String              description;
    private DoubleFunction1<A> f;

    public DoubleFunction1WithDescription( DoubleFunction1<A> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public double invoke( A arg ) {
        return f.invoke(arg);
    }

    public DoubleFunction1<A> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new DoubleFunction1WithDescription<>( this, desc );
    }
}
