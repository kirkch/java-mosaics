package mosaics.lang.functions;

import java.util.Objects;


public interface VoidFunction2WithThrows<A,B> {
    public static final VoidFunction2<?,?> NO_OP = (a,b) -> {};

    public void invoke(A a, B b) throws Throwable;


    public default VoidFunction2WithThrows<A,B> withDescription( String desc ) {
        return new VoidFunction2WithThrowsWithDescription<>( this, desc );
    }
}

class VoidFunction2WithThrowsWithDescription<A,B> implements VoidFunction2WithThrows<A,B> {
    private String             description;
    private VoidFunction2WithThrows<A,B> f;

    public VoidFunction2WithThrowsWithDescription( VoidFunction2WithThrows<A,B> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public void invoke( A a, B b ) throws Throwable{
        f.invoke(a,b);
    }

    public VoidFunction2WithThrows<A,B> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new VoidFunction2WithThrowsWithDescription<>( this, desc );
    }
}
