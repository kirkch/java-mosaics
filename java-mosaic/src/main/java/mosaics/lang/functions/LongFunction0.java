package mosaics.lang.functions;

import java.util.Objects;


public interface LongFunction0 {
    public long invoke();

    public default LongFunction0 withDescription( String desc ) {
        return new LongFunction0WithDescription( this, desc );
    }
}

class LongFunction0WithDescription implements LongFunction0 {
    private String       description;
    private LongFunction0 f;

    public LongFunction0WithDescription( LongFunction0 f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public long invoke() {
        return f.invoke();
    }

    public LongFunction0 withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new LongFunction0WithDescription( this, desc );
    }
}
