package mosaics.lang.functions;

import java.util.Objects;


public interface IntFunction3<A,B,C> {
    public int invoke( A a, B b, C c );

    public default IntFunction3<A,B,C> withDescription( String desc ) {
        return new IntFunction3WithDescription<>( this, desc );
    }
}

class IntFunction3WithDescription<A,B,C> implements IntFunction3<A,B,C> {
    private String              description;
    private IntFunction3<A,B,C> f;

    public IntFunction3WithDescription( IntFunction3<A,B,C> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public int invoke( A a, B b, C c ) {
        return f.invoke(a, b, c);
    }

    public IntFunction3<A,B,C> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new IntFunction3WithDescription<>( this, desc );
    }
}
