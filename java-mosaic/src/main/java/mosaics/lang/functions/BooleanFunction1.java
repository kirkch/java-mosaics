package mosaics.lang.functions;


import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import lombok.Value;

import java.util.Objects;


public interface BooleanFunction1<A> {
    public boolean invoke(A arg);

    public default FPOption<String> getDescription() {
        return FP.emptyOption();
    }

    public default BooleanFunction1<A> invert() {
        return new InvertedBooleanFunction1<>(this);
    }

    public default BooleanFunction1<A> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return this;
        }

        return new BooleanFunction1WithDescription<>( this, FP.option(desc) );
    }
}


@Value
class InvertedBooleanFunction1<T> implements BooleanFunction1<T> {
    private BooleanFunction1<T> f;

    public boolean invoke( T v ) {
        return !f.invoke(v);
    }

    public BooleanFunction1<T> invert() {
        return f;
    }
}


@Value
class BooleanFunction1WithDescription<A> implements BooleanFunction1<A> {
    private FPOption<String>    description;
    private BooleanFunction1<A> f;

    public BooleanFunction1WithDescription( BooleanFunction1<A> f, FPOption<String> description ) {
        this.description = description;
        this.f           = f;
    }

    public boolean invoke( A arg ) {
        return f.invoke(arg);
    }

    public BooleanFunction1<A> withDescription( String desc ) {
        if ( Objects.equals(this.description.get(), desc) ) {
            return this;
        } else if ( desc == null || desc.trim().length() == 0 ) {
            return f;
        }

        return new BooleanFunction1WithDescription<>( f, FP.option(desc) );
    }
}
