package mosaics.lang.functions;

public interface VoidFunctionIOO<B,C> {
    public void invoke( int a, B b, C c);
}
