package mosaics.lang.functions;

public interface IntFunctionLLB {
    public int invoke( long a, long b, byte c );
}
