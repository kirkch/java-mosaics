package mosaics.lang.functions;


import java.util.Objects;


public interface VoidFunction0 {
    public static final VoidFunction0 NO_OP = () -> {};

    public static VoidFunction0 noOp() {
        return NO_OP;
    }

    public void invoke();


    public default VoidFunction0 and( VoidFunction0 b ) {
        if ( b == null ) {
            return this;
        }

        VoidFunction0 a = this;

        return () -> {
            a.invoke();
            b.invoke();
        };
    }

    public default VoidFunction0 withDescription( String desc ) {
        return new VoidFunction0WithDescription( this, desc );
    }
}

class VoidFunction0WithDescription implements VoidFunction0 {
    private String       description;
    private VoidFunction0 f;

    public VoidFunction0WithDescription( VoidFunction0 f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public void invoke() {
        f.invoke();
    }

    public VoidFunction0 withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new VoidFunction0WithDescription( this, desc );
    }
}
