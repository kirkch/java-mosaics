package mosaics.lang.functions;

public interface FunctionI<R> {
    public R invoke( int v );
}
