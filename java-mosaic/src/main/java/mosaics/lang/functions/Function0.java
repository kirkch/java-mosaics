package mosaics.lang.functions;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import lombok.Value;

import java.util.Objects;


public interface Function0<R> {
    public R invoke();

    public default FPOption<String> getDescription() {
        return FP.emptyOption();
    }


    public default Function0<R> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return this;
        }

        return new Function0WithDescription<>( FP.option(desc), this );
    }
}


@Value
class Function0WithDescription<R> implements Function0<R> {
    private FPOption<String> description;
    private Function0<R>     f;


    public R invoke() {
        return f.invoke();
    }

    public Function0<R> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return f;
        } else if ( Objects.equals(this.description.get(), desc) ) {
            return this;
        }

        return new Function0WithDescription<>( FP.option(desc), this );
    }
}
