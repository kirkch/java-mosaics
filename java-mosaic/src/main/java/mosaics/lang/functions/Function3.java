package mosaics.lang.functions;

import java.util.Objects;


public interface Function3<A,B,C,R> {
    public R invoke( A a, B b, C c );

    public default boolean coversArg(A a, B b, C c) {
        return true;
    }

    public default Function3<A,B,C,R> withDescription( String desc ) {
        return new Function3WithDescription<>( this, desc );
    }
}

class Function3WithDescription<A,B,C,R> implements Function3<A,B,C,R> {
    private String          description;
    private Function3<A,B,C,R> f;

    public Function3WithDescription( Function3<A,B,C,R> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public R invoke( A a, B b, C c ) {
        return f.invoke(a,b,c);
    }

    public Function3<A,B,C,R> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new Function3WithDescription<>( this, desc );
    }
}
