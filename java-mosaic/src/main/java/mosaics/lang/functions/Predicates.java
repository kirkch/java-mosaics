package mosaics.lang.functions;

import java.util.Objects;


public class Predicates {

    public static <T> BooleanFunction1<T> eq( T v ) {
        return a -> Objects.equals(a, v);
    }

}
