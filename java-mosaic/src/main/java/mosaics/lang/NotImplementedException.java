package mosaics.lang;

public class NotImplementedException extends RuntimeException {
    public NotImplementedException( String what ) {
        super( "'" + what + "' has not been implemented yet." );
    }
}
