package mosaics.lang;

public enum ComparisonResult {
    LT() {
        public  boolean isGT() {
            return false;
        }

        public boolean isGTE() {
            return false;
        }

        public boolean isEQ() {
            return false;
        }

        public boolean isLTE() {
            return true;
        }

        public boolean isLT() {
            return true;
        }

        public int toInt() {
            return -1;
        }
    }, EQ() {
        public  boolean isGT() {
            return false;
        }

        public boolean isGTE() {
            return true;
        }

        public boolean isEQ() {
            return true;
        }

        public boolean isLTE() {
            return true;
        }

        public boolean isLT() {
            return false;
        }

        public int toInt() {
            return 0;
        }
    }, GT() {
        public  boolean isGT() {
            return true;
        }

        public boolean isGTE() {
            return true;
        }

        public boolean isEQ() {
            return false;
        }

        public boolean isLTE() {
            return false;
        }

        public boolean isLT() {
            return false;
        }

        public int toInt() {
            return 1;
        }
    };

    static ComparisonResult fromInt( int v ) {
        if ( v < 0 ) {
            return LT;
        } else if ( v == 0 ) {
            return EQ;
        } else {
            return GT;
        }
    }

    public abstract boolean isGT();
    public abstract boolean isGTE();
    public abstract boolean isEQ();
    public abstract boolean isLTE();
    public abstract boolean isLT();

    /**
     * For compatibility with java.util.Comparator.
     */
    public abstract int toInt();
}
