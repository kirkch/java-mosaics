package mosaics.lang.ref;

import mosaics.lang.functions.Function3;
import mosaics.lang.functions.VoidFunction1;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.HashSet;
import java.util.Set;

import static mosaics.lang.Backdoor.cast;


/**
 * Manages releasing of objects retained by a java.lang.ref.Reference.<p/>
 *
 * The deallocator takes a ref constructor and a released callback as constructor arguments.  Objects that
 * are registered with this deallocator use the ref constructor to create a new reference and uses
 * the ref destructor when the ref goes out of GC scope.<p/>
 *
 * Currently the ReferenceQueue is only checked when #poll() is called.
 */
public class ReferenceDeallocator<M, V, R extends Reference<V> & HasMetaData<M>> {
    // IDEA add a helper method that spins up a polling thread that sparks periodic clearup of the reference queue

    private final ReferenceQueue<V>                    referenceQueue        = new ReferenceQueue<>();
    private final Set<R>                               allActiveReferences   = new HashSet<>();

    private final Function3<M, V, ReferenceQueue<V>,R> referenceFactory;
    private final VoidFunction1<R>                     releaseObjectFunction;


    /**
     *
     * @param referenceFactory       given (meta, value, queue) return a reference that uses all three values (see constructors to soft, weak and phantom references in mosaics.lang.ref)
     * @param releaseObjectFunction  given (meta, FPOption(value)) perform any clean up required.  NB value will be empty for all references apart from Phantom.
     */
    public ReferenceDeallocator( Function3<M,V,ReferenceQueue<V>,R> referenceFactory, VoidFunction1<R> releaseObjectFunction ) {
        this.referenceFactory      = referenceFactory;
        this.releaseObjectFunction = releaseObjectFunction;
    }


    /**
     * Register object with this class.  When the object goes out of GC scope, #poll() will notice
     * and will call the destructor that was registered with ReferenceDeallocators constructor.
     */
    public R register( M metaData, V object ) {
        R ref = referenceFactory.invoke( metaData, object, referenceQueue );

        allActiveReferences.add( ref );

        return ref;
    }

    /**
     * Check for and invoke the destructor for objects that have gone out of GC scope.
     */
    public int poll() {
        return poll( 10 );
    }

    /**
     * Check for and invoke the destructor for objects that have gone out of GC scope.  When under
     * load, there might be a lot of references up for GC;  to prevent this call from blocking for
     * a long time the maxReferencesToClear parameter specifies the maximum number of references
     * to process in this invocation.
     */
    public int poll( int maxReferencesToClear ) {
        int count = 0;
        R   ref   = cast(referenceQueue.poll());

        while ( ref != null ) {
            releaseObjectFunction.invoke( ref );
            allActiveReferences.remove( ref );

            count += 1;

            if ( count >= maxReferencesToClear ) {
                break;
            }

            ref = cast(referenceQueue.poll());
        }

        return count;
    }
}
