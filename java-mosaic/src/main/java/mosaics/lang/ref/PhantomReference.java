package mosaics.lang.ref;

import lombok.Getter;

import java.lang.ref.ReferenceQueue;


public class PhantomReference<M,V> extends java.lang.ref.PhantomReference<V> implements HasMetaData<M> {
    @Getter
    private M metaData;

    public PhantomReference( M metaData, V referent, ReferenceQueue<? super V> q ) {
        super( referent, q );

        this.metaData = metaData;
    }
}
