package mosaics.lang.ref;

import lombok.Getter;

import java.lang.ref.ReferenceQueue;


public class WeakReference<M,V> extends java.lang.ref.WeakReference<V> implements HasMetaData<M> {
    @Getter private M metaData;

    public WeakReference( M metaData, V referent, ReferenceQueue<? super V> q ) {
        super( referent, q );
        this.metaData = metaData;
    }
}
