package mosaics.utils;

import lombok.Value;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;

import static mosaics.utils.ComparatorUtils.EQ;
import static mosaics.utils.ComparatorUtils.GT;
import static mosaics.utils.ComparatorUtils.LT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;


@SuppressWarnings("ComparatorMethodParameterNotUsed")
public class ComparatorUtilsTest {


    @Nested
    public class AndTestCases {
        private int[][] data = new int[][] {
            new int[] {LT,LT, LT},
            new int[] {LT,EQ, LT},
            new int[] {LT,GT, LT},

            new int[] {EQ,LT, LT},
            new int[] {EQ,EQ, EQ},
            new int[] {EQ,GT, GT},

            new int[] {GT,LT, GT},
            new int[] {GT,EQ, GT},
            new int[] {GT,GT, GT}
        };

        @Test
        public void testAnd() {
            for ( int[] d : data ) {
                Comparator<String> a = (lhs,rhs) -> d[0];
                Comparator<String> b = (lhs,rhs) -> d[1];

                Comparator<String> comparator = ComparatorUtils.and( a, b );

                assertEquals( d[2], comparator.compare("a", "b") );
            }
        }
    }

    @Nested
    public class IdentityComparatorTestCase {
        @Test
        @SuppressWarnings("UnnecessaryBoxing")
        public void testIdentityOfTwoStringsContainingTheSameData() {
            Object a = new String("42");
            Object b = new String("42");

            assertIdentityComparisonOf( a, b );
        }

        @Test
        public void testIdentityOfTwoDataEquivalentDTOs() {
            DTO a = new DTO("foo");
            DTO b = new DTO("foo");

            assertIdentityComparisonOf( a, b );
        }


        private <T> void assertIdentityComparisonOf( T a, T b ) {
            assertEquals( a, b );
            assertEquals( a.hashCode(), b.hashCode() );
            assertNotSame( a, b );

            Comparator<Object> comparator = ComparatorUtils.identityComparator();

            assertEquals( EQ, comparator.compare(a,a) );
            assertEquals( EQ, comparator.compare(b,b) );

            if ( System.identityHashCode(a) < System.identityHashCode(b) ) {
                assertEquals( LT, comparator.compare(a,b) );
                assertEquals( GT, comparator.compare(b,a) );
            } else {
                assertEquals( GT, comparator.compare(a,b) );
                assertEquals( LT, comparator.compare(b,a) );
            }
        }
    }

    @Nested
    public class CompareLongTestCases {
        @Test
        public void compareUsingAnExtractor() {
            LongDTO v1 = new LongDTO(1);
            LongDTO v2 = new LongDTO(2);

            Comparator<LongDTO> comparator = ComparatorUtils.compareAscLong(LongDTO::getValue);
            assertEquals( EQ, comparator.compare(v1,v1) );
            assertEquals( LT, comparator.compare(v1,v2) );
            assertEquals( GT, comparator.compare(v2,v1) );
        }

        @Test
        public void compareAscLongDirect() {
            Long[] array = new Long[] {4L,5L,3L,1L,2L};

            Arrays.sort(array, ComparatorUtils::compareAsc );

            Assertions.assertArrayEquals(new Long[] {1L,2L,3L,4L,5L}, array);
        }


        @Test
        public void compareDescLong() {
            Long[] array = new Long[] {4L,5L,3L,1L,2L};

            Arrays.sort(array, ComparatorUtils::compareDesc );

            Assertions.assertArrayEquals(new Long[] {5L,4L,3L,2L,1L}, array);
        }
    }

    @Nested
    public class CompareBytes {
        @Test
        public void compareByteAscDirect() {
            Byte[] array = new Byte[] {(byte) 4, (byte) 5, (byte) 3, (byte) 1, (byte) 2};

            Arrays.sort(array, ComparatorUtils::compareAsc );

            Assertions.assertArrayEquals(new Byte[] {(byte) 1, (byte) 2, (byte) 3, (byte) 4, (byte) 5}, array);
        }

        @Test
        public void compareByteDescDirect() {
            Byte[] array = new Byte[] {(byte) 4, (byte) 5, (byte) 3, (byte) 1, (byte) 2};

            Arrays.sort(array, ComparatorUtils::compareDesc );

            Assertions.assertArrayEquals(new Byte[] {(byte) 5, (byte) 4, (byte) 3, (byte) 2, (byte) 1}, array);
        }
    }

    @Nested
    public class CompareString {
        private String v1 = "1";
        private String v2 = "2";
        private String v3 = "3";
        private String v4 = "4";
        private String v5 = "5";
        
        @Test
        public void compareByteAscDirect() {
            String[] array = new String[] {v4, v5, v3, v1, v2};

            Arrays.sort(array, ComparatorUtils::compareAsc );

            Assertions.assertArrayEquals(new String[] {v1, v2, v3, v4, v5}, array);
        }

        @Test
        public void compareByteDescDirect() {
            String[] array = new String[] {v4, v5, v3, v1, v2};

            Arrays.sort(array, ComparatorUtils::compareDesc );

            Assertions.assertArrayEquals(new String[] {v5, v4, v3, v2, v1}, array);
        }
    }

    @Nested
    public class CompareFloat {
        private Float v1 = 1f;
        private Float v2 = 2f;
        private Float v3 = 3f;
        private Float v4 = 4f;
        private Float v5 = 5f;
        
        @Test
        public void compareByteAscDirect() {
            Float[] array = new Float[] {v4, v5, v3, v1, v2};

            Arrays.sort(array, ComparatorUtils::compareAsc );

            Assertions.assertArrayEquals(new Float[] {v1, v2, v3, v4, v5}, array);
        }

        @Test
        public void compareByteDescDirect() {
            Float[] array = new Float[] {v4, v5, v3, v1, v2};

            Arrays.sort(array, ComparatorUtils::compareDesc );

            Assertions.assertArrayEquals(new Float[] {v5, v4, v3, v2, v1}, array);
        }
    }

    @Nested
    public class CompareDouble {
        private Double v1 = 1.0;
        private Double v2 = 2.0;
        private Double v3 = 3.0;
        private Double v4 = 4.0;
        private Double v5 = 5.0;
        
        @Test
        public void compareByteAscDirect() {
            Double[] array = new Double[] {v4, v5, v3, v1, v2};

            Arrays.sort(array, ComparatorUtils::compareAsc );

            Assertions.assertArrayEquals(new Double[] {v1, v2, v3, v4, v5}, array);
        }

        @Test
        public void compareByteDescDirect() {
            Double[] array = new Double[] {v4, v5, v3, v1, v2};

            Arrays.sort(array, ComparatorUtils::compareDesc );

            Assertions.assertArrayEquals(new Double[] {v5, v4, v3, v2, v1}, array);
        }
    }



    @Value
    public static class DTO {
        private String name;
    }

    @Value
    public static class LongDTO {
        private long value;
    }

}
