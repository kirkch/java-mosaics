package mosaics.utils;

import mosaics.fp.Try;
import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;


public class SetUtilsTest {

    @Test
    public void testAsSet() {
        assertEquals( 6, sumSet(SetUtils.asSet(1,2,3)) );
        assertEquals( 1, sumSet(SetUtils.asSet(1)) );
        assertEquals( 0, sumSet(SetUtils.asSet(0)) );
        assertEquals( 88, sumSet(SetUtils.asSet(88)) );
        assertEquals( 90, sumSet(SetUtils.asSet(88,2)) );
        assertEquals( -3, sumSet(SetUtils.asSet(-1,-2)) );

        assertEquals(
            "[1, 2, 3]",
            SetUtils.asSet(1, 2, 3).stream().sorted().collect(Collectors.toList()).toString()
        );

        assertTrue( isMutable(SetUtils.asSet(1,2,23), 4) );
    }

    @Test
    public void testAsImmutableSet() {
        assertEquals( 6, sumSet(SetUtils.asImmutableSet(1,2,3)) );
        assertEquals( 1, sumSet(SetUtils.asImmutableSet(1)) );
        assertEquals( 0, sumSet(SetUtils.asImmutableSet(0)) );
        assertEquals( 88, sumSet(SetUtils.asImmutableSet(88)) );
        assertEquals( 90, sumSet(SetUtils.asImmutableSet(88,2)) );
        assertEquals( -3, sumSet(SetUtils.asImmutableSet(-1,-2)) );

        assertEquals(
            "[1, 2, 3]",
            SetUtils.asImmutableSet(1, 2, 3).stream().sorted().collect(Collectors.toList()).toString()
        );

        assertFalse( isMutable(SetUtils.asImmutableSet(1,2,3), 4) );
    }

    private int sumSet( Set<Integer> set ) {
        return set.stream().mapToInt( v -> v ).sum();
    }


    private <T> boolean isMutable( Set<T> set, T newValue ) {
        return Try.invoke( () -> set.add(newValue) ).hasResult();
    }
}
