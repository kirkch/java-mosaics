package mosaics.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class ComparableUtilsTest {
    @Test
    public void testMin() {
        assertEquals( 1, ComparableUtils.min(1,2) );
        assertEquals( 1, ComparableUtils.min(2,1) );
        assertEquals( 2, ComparableUtils.min(2,2) );
        assertEquals( 2, ComparableUtils.min(2,3) );
        assertEquals( 2, ComparableUtils.min(3,2) );
        assertEquals( -3, ComparableUtils.min(-3,-2) );
        assertEquals( -3, ComparableUtils.min(-2,-3) );
        assertEquals( -3, ComparableUtils.min(-3,-3) );


        assertEquals( "a", ComparableUtils.min("a","ab", String::length) );
        assertEquals( "a", ComparableUtils.min("ab","a", String::length) );
        assertEquals( "ab", ComparableUtils.min("ab","ab", String::length) );
    }

    @Test
    public void testMax() {
        assertEquals( 2, ComparableUtils.max(1,2) );
        assertEquals( 2, ComparableUtils.max(2,1) );
        assertEquals( 2, ComparableUtils.max(2,2) );
        assertEquals( 3, ComparableUtils.max(2,3) );
        assertEquals( 3, ComparableUtils.max(3,2) );
        assertEquals( -2, ComparableUtils.max(-3,-2) );
        assertEquals( -2, ComparableUtils.max(-2,-3) );
        assertEquals( -3, ComparableUtils.max(-3,-3) );

        assertEquals( "ab", ComparableUtils.max("a","ab", String::length) );
        assertEquals( "ab", ComparableUtils.max("ab","a", String::length) );
        assertEquals( "a", ComparableUtils.max("a","a", String::length) );
    }
}
