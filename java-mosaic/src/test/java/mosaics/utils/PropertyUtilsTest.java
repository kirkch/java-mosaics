package mosaics.utils;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class PropertyUtilsTest {

    @Nested
    public class ProcessPropertiesTestCases {
        @Test
        public void givenEmptyProperties_expectAnEmptyMap() {
            Properties p = new Properties();

            Map<String,String> map = PropertyUtils.processProperties( p );

            assertEquals( 0, map.size() );
        }

        @Test
        public void givenSingleKVInPropertiesFile_expectSingleKVInMap() {
            Properties p = new Properties();
            p.setProperty( "key", "a b c" );

            Map<String,String> map = PropertyUtils.processProperties( p );

            assertEquals( 1, map.size() );
            assertEquals( "a b c", map.get("key") );
        }

        @Test
        public void givenBlankValue_expectKVToNotBeInTheMap() {
            Properties p = new Properties();
            p.setProperty( "key", "   " );

            Map<String,String> map = PropertyUtils.processProperties( p );

            assertEquals( 0, map.size() );
        }

        @Test
        public void givenCommentOnlyValue_expectKVToNotBeInTheMap() {
            Properties p = new Properties();
            p.setProperty( "key", " # comment 123" );

            Map<String,String> map = PropertyUtils.processProperties( p );

            assertEquals( 0, map.size() );
        }

        @Test
        public void givenValueWithCommentInProperties_expectCommentToBeRemovedInMap() {
            Properties p = new Properties();
            p.setProperty( "key", "a b c   # this is a comment" );

            Map<String,String> map = PropertyUtils.processProperties( p );

            assertEquals( 1, map.size() );
            assertEquals( "a b c", map.get("key") );
        }
    }

}
