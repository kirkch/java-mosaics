package mosaics.utils;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;


public class PeekableIteratorTest {

    @Test
    public void givenEmptyIterator_callPeek_expectException() {
        PeekableIterator<String> it = new PeekableIterator<>( Collections.<String>emptyList().iterator() );

        assertFalse( it.hasNext() );

        try {
            it.peek();
            fail( "expect exception" );
        } catch ( NoSuchElementException ex ) {
            assertEquals( "No value present", ex.getMessage() );
        }
    }

    @Test
    public void givenOneValueInIterator_callPeek_expectToSeeTheValue() {
        PeekableIterator<String> it = new PeekableIterator<>( Arrays.asList("v1").iterator() );

        assertTrue( it.hasNext() );
        assertEquals( "v1", it.peek() );
    }

    @Test
    public void givenOneValueInIterator_callPeekTwice_expectToSeeTheSameValue() {
        PeekableIterator<String> it = new PeekableIterator<>( Arrays.asList("v1").iterator() );

        assertTrue( it.hasNext() );
        assertEquals( "v1", it.peek() );
        assertEquals( "v1", it.peek() );
    }

    @Test
    public void givenOneValueInIterator_callPeekThenNext_expectToSeeTheStateOfTheIteratorScrollOn() {
        PeekableIterator<String> it = new PeekableIterator<>( Collections.singletonList( "v1" ).iterator() );

        assertTrue( it.hasNext() );
        assertEquals( "v1", it.peek() );

        assertTrue( it.hasNext() );
        assertEquals( "v1", it.next() );

        assertFalse( it.hasNext() );
    }

    @Test
    public void givenTwoValuesInIterator_expectPeekWillNotScrollStateButBothValuesAreVisible() {
        PeekableIterator<String> it = new PeekableIterator<>( Arrays.asList("v1", "v2").iterator() );

        assertTrue( it.hasNext() );
        assertEquals( "v1", it.peek() );
        assertEquals( "v1", it.next() );

        assertTrue( it.hasNext() );
        assertEquals( "v2", it.peek() );
        assertEquals( "v2", it.next() );

        assertFalse( it.hasNext() );
    }

}
