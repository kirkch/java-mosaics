package mosaics.collections.immutable.graphs;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class GraphBuilderTest {

    @Test
    public void createEmptyGraph_expectAnEmptyGraph() {
        GraphBuilder<String,String> b     = new GraphBuilder<>();
        Graph<String,String>        graph = b.build();

        assertEquals( 0, graph.getNodeCount() );
        assertEquals( 0, graph.getEdgeCount() );
    }

    @Test
    public void createASingleNodeGraph() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNode( "A" )
            .build();

        assertEquals( "A", graph.getNode(0).get().getNodeValue() );

        assertEquals( 1, graph.getNodeCount() );
        assertEquals( 0, graph.getEdgeCount() );
    }

    @Test
    public void createAThreeNodeGraph() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNodes( "A", "B", "C" )
            .build();

        assertEquals( "A", graph.getNode(0).get().getNodeValue() );
        assertEquals( "B", graph.getNode(1).get().getNodeValue() );
        assertEquals( "C", graph.getNode(2).get().getNodeValue() );

        assertEquals( 3, graph.getNodeCount() );
        assertEquals( 0, graph.getEdgeCount() );
    }

    @Test
    public void createATwoNodesAndLinkInOneLine() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withEdge( "A", "B", "e1" )
            .build();

        assertEquals( "A", graph.getNode(0).get().getNodeValue() );
        assertEquals( "B", graph.getNode(1).get().getNodeValue() );
        assertEquals( new Edge<>(0,"e1", null, 0, 1), graph.getEdge(0).get() );

        assertEquals( 2, graph.getNodeCount() );
        assertEquals( 1, graph.getEdgeCount() );
    }

    @Test
    public void createATwoNodeGraphWithOneEdgeBetweenThem() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .withEdge( "A", "B", "e1" )
            .build();

        assertEquals( "A", graph.getNode(0).get().getNodeValue() );
        assertEquals( "B", graph.getNode(1).get().getNodeValue() );
        assertEquals( new Edge<>(0,"e1", null, 0, 1), graph.getEdge(0).get() );

        assertEquals( 2, graph.getNodeCount() );
        assertEquals( 1, graph.getEdgeCount() );
    }

    @Test
    public void createAOneNodeGraphWithOneEdgeLinkingBackToTheSameNode() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNode( "A" )
            .withEdge( "A", "A", "e1" )
            .build();

        assertEquals( "A", graph.getNode(0).get().getNodeValue() );
        assertEquals( new Edge<>(0,"e1", null, 0, 0), graph.getEdge(0).get() );

        assertEquals( 1, graph.getNodeCount() );
        assertEquals( 1, graph.getEdgeCount() );
    }

    @Test
    public void createThreeNodesWithAWebOfEdges() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withEdge( "A", "B", "e1" )
            .withEdge( "A", "C", "e2" )
            .withEdge( "B", "C", "e3" )
            .build();

        assertEquals( "A", graph.getNode(0).get().getNodeValue() );
        assertEquals( "B", graph.getNode(1).get().getNodeValue() );
        assertEquals( "C", graph.getNode(2).get().getNodeValue() );
        assertEquals( new Edge<>(0,"e1", null, 0, 1), graph.getEdge(0).get() );
        assertEquals( new Edge<>(1,"e2", null, 0, 2), graph.getEdge(1).get() );
        assertEquals( new Edge<>(2,"e3", null, 1, 2), graph.getEdge(2).get() );

        assertEquals( 3, graph.getNodeCount() );
        assertEquals( 3, graph.getEdgeCount() );
    }

}
