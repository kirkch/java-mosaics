package mosaics.collections.immutable.graphs;


import mosaics.fp.FP;
import mosaics.lang.NotFoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;


public class RRBGraphTest {


// ADDNODE and GETNODE

    @Test
    public void givenEmptyGraph_callGetNodeCount_expectZero() {
        Graph<String,String> graph = Graph.emptyGraph();
        
        assertEquals( 0, graph.getNodeCount() );
    }

    @Test
    public void givenEmptyGraph_callGetEdgeCount_expectZero() {
        Graph<String,String> graph = Graph.emptyGraph();
        
        assertEquals( 0, graph.getEdgeCount() );
    }
    
    @Test
    public void givenEmptyGraph_getNodeZero_expectNoMatch() {
        Graph<String,String> graph = Graph.emptyGraph();

        assertEquals( FP.emptyOption(), graph.getNode(0) );
    }

    @Test
    public void givenEmptyGraph_addOneNode_expectToBeAbleToRetrieveIt() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> updatedGraph = tuple.getUpdatedGraph();
        long nodeKey = tuple.getNewKey();

        assertEquals( nodeKey, updatedGraph.getNode(nodeKey).get().getNodeKey() );
        assertEquals( "n1", updatedGraph.getNode(nodeKey).get().getNodeValue() );
    }

    @Test
    public void givenEmptyGraph_addNode_expectGetNodeCountToReturn1() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> updatedGraph = tuple.getUpdatedGraph();

        assertEquals( 1, updatedGraph.getNodeCount() );
    }

    @Test
    public void givenEmptyGraph_addNode_expectGetEdgeCountToReturn0() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> updatedGraph = tuple.getUpdatedGraph();

        assertEquals( 0, updatedGraph.getEdgeCount() );
    }

    @Test
    public void givenEmptyGraph_addOneNode_expectTheOriginalGraphToNotHaveBeenUpdated() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        long nodeKey = tuple.getNewKey();

        try {
            graph.getNode(nodeKey);
        } catch ( NotFoundException ex ) {
            assertEquals( "Node 0 does not exist", ex.getMessage() );
        }
    }

    @Test
    public void givenGraphWithOneNode_getNodeWithInvalidKey_expectException() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> updatedGraph = tuple.getUpdatedGraph();
        long nodeKey = tuple.getNewKey();

        try {
            updatedGraph.getNode(nodeKey+1);
        } catch ( NotFoundException ex ) {
            assertEquals( "Node "+(nodeKey+1)+" does not exist", ex.getMessage() );
        }
    }


// UPDATENODE

    @Test
    public void givenOneNodeGraph_updateNode_expectNodeToBeUpdated() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> graphPostAddition = tuple.getUpdatedGraph();
        long nodeKey = tuple.getNewKey();

        Graph<String, String> graphPostNodeUpdate = graphPostAddition.updateNode( nodeKey, "u1" );

        assertEquals( nodeKey, graphPostNodeUpdate.getNode(nodeKey).get().getNodeKey() );
        assertEquals( "u1", graphPostNodeUpdate.getNode(nodeKey).get().getNodeValue() );
    }

    @Test
    public void givenOneNodeGraph_updateNode_expectGetNodeCountToRemainUnchanged() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> graphPostAddition = tuple.getUpdatedGraph();
        long nodeKey = tuple.getNewKey();

        Graph<String, String> graphPostNodeUpdate = graphPostAddition.updateNode( nodeKey, "u1" );

        assertEquals( 1, graphPostNodeUpdate.getNodeCount() );
    }

    @Test
    public void givenOneNodeGraph_updateNode_expectGetEdgeCountToRemainUnchanged() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> graphPostAddition = tuple.getUpdatedGraph();
        long nodeKey = tuple.getNewKey();

        Graph<String, String> graphPostNodeUpdate = graphPostAddition.updateNode( nodeKey, "u1" );

        assertEquals( 0, graphPostNodeUpdate.getEdgeCount() );
    }

    @Test
    public void givenOneNodeGraph_usingInvalidKeyUpdateNode_expectException() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> graphPostAddition = tuple.getUpdatedGraph();
        long nodeKey = tuple.getNewKey();


        try {
            graphPostAddition.updateNode( nodeKey+1, "u1" );
        } catch ( NotFoundException ex ) {
            assertEquals( "Node "+(nodeKey+1)+" does not exist", ex.getMessage() );
        }
    }

    @Test
    public void givenOneNodeGraph_tryUpdateNodeUsingOriginalGraph_expectException() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        long nodeKey = tuple.getNewKey();


        try {
            graph.updateNode( nodeKey, "u1" );
        } catch ( NotFoundException ex ) {
            assertEquals( "Node "+nodeKey+" does not exist", ex.getMessage() );
        }
    }

    @Test
    public void givenOneNodeGraph_tryGraphPreUpdate_expectOriginalValue() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> graphPostAddition = tuple.getUpdatedGraph();
        long nodeKey = tuple.getNewKey();

        Graph<String, String> graphPostNodeUpdate = graphPostAddition.updateNode( nodeKey, "u1" );

        assertEquals( nodeKey, graphPostNodeUpdate.getNode(nodeKey).get().getNodeKey() );
        assertEquals( "n1", graphPostAddition.getNode(nodeKey).get().getNodeValue() );
    }


// REMOVE NODE

    @Test
    public void givenOneNodeGraph_removeNode_expectSuccessfulRemoval() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> graphPostAddition = tuple.getUpdatedGraph();
        long nodeKey = tuple.getNewKey();

        Graph<String, String> graphPostRemoval = graphPostAddition.removeNode( nodeKey );
        assertTrue( graphPostRemoval.getNode(nodeKey).isEmpty() );
    }

    @Test
    public void givenOneNodeGraph_removeNode_expectNodeCountToBeReduced() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> graphPostAddition = tuple.getUpdatedGraph();
        long nodeKey = tuple.getNewKey();

        Graph<String, String> graphPostRemoval = graphPostAddition.removeNode( nodeKey );

        assertEquals( 0, graphPostRemoval.getNodeCount() );
    }

    @Test
    public void givenOneNodeGraph_removeNode_expectEdgeCountToBeUnchanged() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> graphPostAddition = tuple.getUpdatedGraph();
        long nodeKey = tuple.getNewKey();

        Graph<String, String> graphPostRemoval = graphPostAddition.removeNode( nodeKey );

        assertEquals( 0, graphPostRemoval.getEdgeCount() );
    }

    @Test
    public void givenOneNodeGraph_removeNonExistentNode_expectException() {
        Graph<String,String> graph = Graph.emptyGraph();

        try {
            graph.removeNode( 0 );

            fail( "expected NotFoundException" );
        } catch ( NotFoundException ex ) {
            assertEquals( "Node 0 does not exist", ex.getMessage() );
        }
    }

    @Test
    public void givenOneNodeGraph_removeNode_expectToStillBeAbleToSeeTheNodeInThePreRemovalCopyOfTheGraph() {
        Graph<String,String> graph = Graph.emptyGraph();

        GraphAddition<String,String> tuple = graph.addNode( "n1" );
        Graph<String, String> graphPostAddition = tuple.getUpdatedGraph();
        long nodeKey = tuple.getNewKey();

        graphPostAddition.removeNode( nodeKey );
        assertTrue( graphPostAddition.getNode(nodeKey).hasValue() );
    }

    @Test
    public void givenOneEdgeGraph_removeSourceNode_expectEdgetoAlsoBeRemoved() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        Graph<String,String> postEdgeBeingAddedGraph    = initialGraph.addEdge( 0, 1, "e1", "v1" ).getUpdatedGraph();
        Graph<String,String> postSourceNodeBeingRemoved = postEdgeBeingAddedGraph.removeNode( 0 );

        assertEquals( FP.emptyOption(), postSourceNodeBeingRemoved.getEdge(0) );
        assertEquals( FP.nil(), postSourceNodeBeingRemoved.getNodeMandatory(1).getInboundEdges() );
        assertEquals( FP.nil(), postSourceNodeBeingRemoved.getNodeMandatory(1).getOutboundEdges() );
    }

    @Test
    public void givenOneEdgeGraph_removeDestinationNode_expectEdgetoAlsoBeRemoved() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        Graph<String,String> postEdgeBeingAddedGraph         = initialGraph.addEdge( 0, 1, "e1", "v1" ).getUpdatedGraph();
        Graph<String,String> postDestinationNodeBeingRemoved = postEdgeBeingAddedGraph.removeNode( 1 );

        assertEquals( FP.emptyOption(), postDestinationNodeBeingRemoved.getEdge(0) );
        assertEquals( FP.nil(), postDestinationNodeBeingRemoved.getNodeMandatory(0).getInboundEdges() );
        assertEquals( FP.nil(), postDestinationNodeBeingRemoved.getNodeMandatory(0).getOutboundEdges() );
    }


// ADDEDGE and GETEDGE

    @Test
    public void givenEmptyGraph_getEdgeZero_expectNoMatch() {
        Graph<String,String> graph = Graph.emptyGraph();

        assertEquals( FP.emptyOption(), graph.getEdge(0) );
    }

    @Test
    public void givenTwoNodeAndZeroEdgesGraph_getEdgeZero_expectNoEdge() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        assertEquals( FP.emptyOption(), graph.getEdge(0) );
    }

    @Test
    public void addOneEdge_expectToBeAbleToRetrieveItDirectlyById() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        GraphAddition<String,String> result                  = graph.addEdge( 0, 1, "e1", "v1" );
        Graph<String,String>                postEdgeBeingAddedGraph = result.getUpdatedGraph();
        long                                edgeId                  = result.getNewKey();

        assertEquals( edgeId, postEdgeBeingAddedGraph.getEdge(edgeId).get().getEdgeKey() );
        assertEquals( "e1", postEdgeBeingAddedGraph.getEdge(edgeId).get().getEdgeLabel() );
        assertEquals( "v1", postEdgeBeingAddedGraph.getEdge(edgeId).get().getEdgeValue() );
    }

    @Test
    public void addOneEdge_expectNodeCountToBe2() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        GraphAddition<String,String> result                  = graph.addEdge( 0, 1, "e1", "v1" );
        Graph<String,String>                postEdgeBeingAddedGraph = result.getUpdatedGraph();

        assertEquals( 2, postEdgeBeingAddedGraph.getNodeCount() );
    }

    @Test
    public void addOneEdge_expectEdgeCountToBe1() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        GraphAddition<String,String> result                  = graph.addEdge( 0, 1, "e1", "v1" );
        Graph<String,String>                postEdgeBeingAddedGraph = result.getUpdatedGraph();

        assertEquals( 1, postEdgeBeingAddedGraph.getEdgeCount() );
    }

    @Test
    public void addOneEdge_expectToBeAbleToRetrieveItAsTheOutboundEdgeFromTheFirstNode() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        GraphAddition<String,String> result                  = graph.addEdge( 0, 1, "e1", "v1" );
        Graph<String,String>                postEdgeBeingAddedGraph = result.getUpdatedGraph();

        assertEquals( 1, postEdgeBeingAddedGraph.getNode( 0).get().getOutboundEdges().count() );
        assertEquals( 0, postEdgeBeingAddedGraph.getNode( 0).get().getInboundEdges().count() );
        assertEquals( new Edge<>(0,"e1", "v1",0,1), postEdgeBeingAddedGraph.getNode( 0).get().getOutboundEdges().getHead() );
    }

    @Test
    public void addOneEdge_expectToBeAbleToRetrieveItAsTheInboundEdgeFromTheSecondNode() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        GraphAddition<String,String> result                  = graph.addEdge( 0, 1, "e1", "v1" );
        Graph<String,String>                postEdgeBeingAddedGraph = result.getUpdatedGraph();

        assertEquals( 1, postEdgeBeingAddedGraph.getNode( 1).get().getInboundEdges().count() );
        assertEquals( 0, postEdgeBeingAddedGraph.getNode( 1).get().getOutboundEdges().count() );
        assertEquals( new Edge<>(0,"e1", "v1",0,1), postEdgeBeingAddedGraph.getNode( 1).get().getInboundEdges().getHead() );
    }

    @Test
    public void tryAddingEdgeToNonExistentSourceNode_expectException() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        try {
            graph.addEdge( 2, 1, "e1", "v1" );

            fail( "expected NotFoundException" );
        } catch ( NotFoundException e ) {
            assertEquals( "Node 2 does not exist", e.getMessage() );
        }
    }

    @Test
    public void tryAddingEdgeToNonExistentDestinationNode_expectException() {
        Graph<String,String> graph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        try {
            graph.addEdge( 1, 2, "e1", "v1" );

            fail( "expected NotFoundException" );
        } catch ( NotFoundException e ) {
            assertEquals( "Node 2 does not exist", e.getMessage() );
        }
    }

    @Test
    public void givenGraphWithOneEdge_getEdgeWithInvalidKey_expectNoMatch() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        GraphAddition<String,String> result                  = initialGraph.addEdge( 0, 1, "e1", "v1" );
        Graph<String,String>                postEdgeBeingAddedGraph = result.getUpdatedGraph();

        assertEquals( FP.emptyOption(), postEdgeBeingAddedGraph.getEdge(1) );
    }


// UPDATEEDGE

    @Test
    public void givenOneEdgeGraph_updateEdge_expectEdgeToBeUpdated() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        GraphAddition<String,String> result                    = initialGraph.addEdge( 0, 1, "e1", "v1" );
        Graph<String,String>                postEdgeBeingAddedGraph   = result.getUpdatedGraph();
        Graph<String,String>                postEdgeBeingUpdatedGraph = postEdgeBeingAddedGraph.updateEdge( 0, "ue1", "uv1" );

        assertEquals( 0, postEdgeBeingUpdatedGraph.getEdge(0).get().getEdgeKey() );
        assertEquals( "ue1", postEdgeBeingUpdatedGraph.getEdge(0).get().getEdgeLabel() );
        assertEquals( "uv1", postEdgeBeingUpdatedGraph.getEdge(0).get().getEdgeValue() );
    }

    @Test
    public void givenOneEdgeGraph_updateEdge_expectNodeCountToBeUnchanged() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        GraphAddition<String,String> result                    = initialGraph.addEdge( 0, 1, "e1", "v1" );
        Graph<String,String>                postEdgeBeingAddedGraph   = result.getUpdatedGraph();
        Graph<String,String>                postEdgeBeingUpdatedGraph = postEdgeBeingAddedGraph.updateEdge( 0, "ue1", "uv1" );

        assertEquals( 2, postEdgeBeingUpdatedGraph.getNodeCount() );
    }

    @Test
    public void givenOneEdgeGraph_updateEdge_expectEdgeCountToBeUnchanged() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        GraphAddition<String,String> result                    = initialGraph.addEdge( 0, 1, "e1", "v1" );
        Graph<String,String>                postEdgeBeingAddedGraph   = result.getUpdatedGraph();
        Graph<String,String>                postEdgeBeingUpdatedGraph = postEdgeBeingAddedGraph.updateEdge( 0, "ue1", "uv1" );

        assertEquals( 1, postEdgeBeingUpdatedGraph.getEdgeCount() );
    }

    @Test
    public void givenOneEdgeGraph_usingInvalidKeyUpdateEdge_expectException() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        GraphAddition<String,String> result                    = initialGraph.addEdge( 0, 1, "e1", "v1" );
        Graph<String,String>                postEdgeBeingAddedGraph   = result.getUpdatedGraph();

        try {
            postEdgeBeingAddedGraph.updateEdge( 1, "ue1", "uv1" );
            fail("expected exception");
        } catch ( NotFoundException ex ) {
            assertEquals( "Edge 1 does not exist", ex.getMessage() );
        }
    }

    @Test
    public void givenOneEdgeGraph_tryUpdateEdgeUsingOriginalGraph_expectException() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        initialGraph.addEdge( 0, 1, "e1", "v1" );

        try {
            initialGraph.updateEdge( 0, "ue1", "uv1" );
            fail("expected exception");
        } catch ( NotFoundException ex ) {
            assertEquals( "Edge 0 does not exist", ex.getMessage() );
        }
    }

    @Test
    public void givenOneEdgeGraph_tryGraphPreUpdate_expectOriginalValue() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        Graph<String,String> postEdgeBeingAddedGraph = initialGraph.addEdge( 0, 1, "e1", "v1" ).getUpdatedGraph();

        postEdgeBeingAddedGraph.updateEdge( 0, "ue1", "uv1" );

        assertEquals( 0, postEdgeBeingAddedGraph.getEdge(0).get().getEdgeKey() );
        assertEquals( "e1", postEdgeBeingAddedGraph.getEdge(0).get().getEdgeLabel() );
        assertEquals( "v1", postEdgeBeingAddedGraph.getEdge(0).get().getEdgeValue() );
    }

    @Test
    public void givenOneEdgeGraph_updateEdge_expectSourceAndDestinationNodesToBeUpdatedToo() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        Graph<String,String> postEdgeBeingAddedGraph   = initialGraph.addEdge( 0, 1, "e1", "v1" ).getUpdatedGraph();
        Graph<String,String> postEdgeBeingUpdatedGraph = postEdgeBeingAddedGraph.updateEdge( 0, "ue1", "uv1" );

        assertEquals( "ue1", postEdgeBeingUpdatedGraph.getNodeMandatory(0).getOutboundEdges().getHead().getEdgeLabel() );
        assertEquals( "uv1", postEdgeBeingUpdatedGraph.getNodeMandatory(0).getOutboundEdges().getHead().getEdgeValue() );
        assertEquals( "ue1", postEdgeBeingUpdatedGraph.getNodeMandatory(1).getInboundEdges().getHead().getEdgeLabel() );
        assertEquals( "uv1", postEdgeBeingUpdatedGraph.getNodeMandatory(1).getInboundEdges().getHead().getEdgeValue() );
    }


// REMOVE EDGE

    @Test
    public void givenOneEdgeGraph_removeNonExistentEdge_expectException() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        try {
            initialGraph.removeEdge( 1 );
            fail("expected exception");
        } catch ( NotFoundException ex ) {
            assertEquals( "Edge 1 does not exist", ex.getMessage() );
        }
    }

    @Test
    public void givenOneEdgeGraph_removeEdge_expectSuccessfulRemoval() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        Graph<String,String> postEdgeBeingAddedGraph   = initialGraph.addEdge( 0, 1, "e1", "v1" ).getUpdatedGraph();
        Graph<String,String> postEdgeBeingRemovedGraph = postEdgeBeingAddedGraph.removeEdge( 0 );

        assertEquals( FP.emptyOption(), postEdgeBeingRemovedGraph.getEdge(0) );
        assertEquals( FP.nil(), postEdgeBeingRemovedGraph.getNodeMandatory(0).getOutboundEdges() );
        assertEquals( FP.nil(), postEdgeBeingRemovedGraph.getNodeMandatory(0).getInboundEdges() );
        assertEquals( FP.nil(), postEdgeBeingRemovedGraph.getNodeMandatory(1).getOutboundEdges() );
        assertEquals( FP.nil(), postEdgeBeingRemovedGraph.getNodeMandatory(1).getInboundEdges() );
    }

    @Test
    public void givenOneEdgeGraph_removeEdge_expectNodeCountToBeUnchanged() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        Graph<String,String> postEdgeBeingAddedGraph   = initialGraph.addEdge( 0, 1, "e1", "v1" ).getUpdatedGraph();
        Graph<String,String> postEdgeBeingRemovedGraph = postEdgeBeingAddedGraph.removeEdge( 0 );

        assertEquals( 2, postEdgeBeingRemovedGraph.getNodeCount() );
    }

    @Test
    public void givenOneEdgeGraph_removeEdge_expectEdgeCountToHaveReducedBy1() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        Graph<String,String> postEdgeBeingAddedGraph   = initialGraph.addEdge( 0, 1, "e1", "v1" ).getUpdatedGraph();
        Graph<String,String> postEdgeBeingRemovedGraph = postEdgeBeingAddedGraph.removeEdge( 0 );

        assertEquals( 0, postEdgeBeingRemovedGraph.getEdgeCount() );
    }

    @Test
    public void givenOneEdgeGraph_removeEdge_expectToStillBeAbleToSeeTheEdgeInThePreRemovalCopyOfTheGraph() {
        Graph<String,String> initialGraph = new GraphBuilder<String,String>()
            .withNodes( "A", "B" )
            .build();

        Graph<String,String> preEdgeBeingRemovedGraph  = initialGraph.addEdge( 0, 1, "e1", "v1" ).getUpdatedGraph();
        preEdgeBeingRemovedGraph.removeEdge( 0 );

        assertNotNull( preEdgeBeingRemovedGraph.getEdge(0) );
        assertEquals( 1, preEdgeBeingRemovedGraph.getNodeMandatory(0).getOutboundEdges().count() );
        assertEquals( 0, preEdgeBeingRemovedGraph.getNodeMandatory(0).getInboundEdges().count() );
        assertEquals( 0, preEdgeBeingRemovedGraph.getNodeMandatory(1).getOutboundEdges().count() );
        assertEquals( 1, preEdgeBeingRemovedGraph.getNodeMandatory(1).getInboundEdges().count() );
    }

}
