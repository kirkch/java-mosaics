package mosaics.collections.mutable;

import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class AverageAccumulatorTest {
    @Test
    public void testCollector() {
        assertEquals(0.0, Stream.<Double>of().collect( AverageAccumulator.newCollector()), 0.000000001 );
        assertEquals(1.5, Stream.of(1.0,2.0).collect( AverageAccumulator.newCollector()), 0.000000001 );
        assertEquals(2.2, Stream.of(1.1,2.2,3.3).collect( AverageAccumulator.newCollector()), 0.000000001 );
    }

    @Test
    public void testAccumulator() {
        AverageAccumulator acc = new AverageAccumulator();

        assertEquals(0, acc.getAverage());

        acc.append( 3.0 );
        assertEquals(3.0, acc.getAverage());

        acc.append( 2.0 );
        assertEquals(2.5, acc.getAverage());

        acc.append( 1.0 );
        assertEquals(2.0, acc.getAverage());
    }
}
