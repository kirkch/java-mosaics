package mosaics.collections.mutable.ring;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPIterator;
import mosaics.junit.JMAssertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;


public class RingBufferTest {
    @Nested
    public class GetByIndexTestCases {
        RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

        @Test
        public void givenAnEmptyBuffer_getIndexZero_expectException() {
            try {
                buf.get( 0 );
                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 0 is out of bounds, as the buffer is currently empty", ex.getMessage() );
            }
        }

        @Test
        public void givenAnEmptyBuffer_getIndexOne_expectException() {
            try {
                buf.get( 1 );
                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 1 is out of bounds, as the buffer is currently empty", ex.getMessage() );
            }
        }

        @Test
        public void givenAnEmptyBuffer_pushAndCallGetZero_expectToReadTheValueBack() {
            buf.append( "hello" );

            assertEquals( "hello", buf.get(0) );
        }

        @Test
        public void givenAnEmptyBuffer_pushLHSAndCallGetOne_expectException() {
            buf.append( "hello" );

            try {
                buf.get( 1 );
                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 1 is out of bounds, currently supported indexes are 0 to 0 (inc)", ex.getMessage() );
            }
        }

        @Test
        public void givenAFullBuffer_showGetByIndexWillRetrieveEveryValueCorrectly() {
            buf.appendAll( "10", "11", "12", "13" );

            assertEquals( "10", buf.get( 0 ) );
            assertEquals( "11", buf.get( 1 ) );
            assertEquals( "12", buf.get( 2 ) );
            assertEquals( "13", buf.get( 3 ) );
        }

        @Test
        public void givenAFullBuffer_showThatTryingToRetrieveBeyondTheRHSErrors() {
            buf.appendAll( "10", "11", "12", "13" );

            try {
                buf.get( 4 );
                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 4 is out of bounds, currently supported indexes are 0 to 3 (inc)", ex.getMessage() );
            }
        }

        @Test
        public void givenAFullBuffer_pushAnExtraValue_showThatTheLHSValueIsLostButTheNewValueIsKept() {
            buf.appendAll( "10", "11", "12", "13" );
            buf.append( "14" );

            assertEquals( "11", buf.get( 1 ) );
            assertEquals( "12", buf.get( 2 ) );
            assertEquals( "13", buf.get( 3 ) );
            assertEquals( "14", buf.get( 4 ) );
        }

        @Test
        public void givenAFullBuffer_pushAnExtraValue_showThatTryingToRetrieveTheDroppedLHSErrors() {
            buf.appendAll( "10", "11", "12", "13" );
            buf.append( "14" );

            try {
                buf.get( 0 );
                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 0 is out of bounds, currently supported indexes are 1 to 4 (inc)", ex.getMessage() );
            }
        }
    }


    @Nested
    public class PopLHSTestCases {
        private RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

        @Test
        public void givenEmptyRingBuffer_callPopLHS_expectNone() {
            assertEquals( FP.emptyOption(), buf.popLHS() );
            assertEquals( 4, buf.remaining() );
        }

        @Test
        public void givenOneValueRingBuffer_callPopLHS_expectValue() {
            buf.append( "a1" );

            assertEquals( 3, buf.remaining() );
            assertEquals( FP.option("a1"), buf.popLHS() );
        }

        @Test
        public void givenOneValueRingBuffer_callPopLHSTwice_expectValueForFirstCallOnly() {
            buf.append( "a1" );
            assertFalse( buf.isEmpty() );
            assertFalse(buf.isFull());

            assertEquals( FP.option("a1"), buf.popLHS() );
            assertEquals( FP.emptyOption(), buf.popLHS() );
            assertTrue( buf.isEmpty() );
        }

        @Test
        public void givenTwoValueRingBuffer_callPopLHSThreeTimes_expectValueForFirstTwoCallsOnly() {
            buf.append( "a1" );
            buf.append( "a2" );
            assertFalse( buf.isEmpty() );
            assertFalse(buf.isFull());

            assertEquals( 2, buf.remaining() );
            assertEquals( FP.option("a1"), buf.popLHS() );
            assertEquals( FP.option("a2"), buf.popLHS() );
            assertEquals( FP.emptyOption(), buf.popLHS() );
            assertTrue( buf.isEmpty() );
        }

        @Test
        public void givenFourValueRingBuffer_callPopLHSFiveTimes_expectValueForFirstFourCallsOnly() {
            buf.append( "a1" );
            buf.append( "a2" );
            buf.append( "a3" );
            buf.append( "a4" );
            assertFalse(buf.isEmpty());
            assertTrue(buf.isFull());

            assertEquals( 0, buf.remaining() );
            assertEquals( FP.option("a1"), buf.popLHS() );
            assertEquals( FP.option("a2"), buf.popLHS() );
            assertEquals( FP.option("a3"), buf.popLHS() );
            assertEquals( FP.option("a4"), buf.popLHS() );
            assertEquals( FP.emptyOption(), buf.popLHS() );
            assertTrue( buf.isEmpty() );
            assertFalse(buf.isFull());
        }

        @Test
        @SuppressWarnings({"RedundantStringConstructorCall", "unchecked", "UnusedAssignment"})
        public void givenFiveValueRingBuffer_callPopLHSFiveTimes_expectFirstValueToHaveBeenDropped() {
            String    a1  = new String("a1");
            Reference ref = new WeakReference( a1 );

            buf.append( a1 );
            buf.append( "a2" );
            buf.append( "a3" );
            buf.append( "a4" );
            buf.append( "a5" );
            assertFalse(buf.isEmpty());
            assertTrue(buf.isFull());

            a1 = null;
            JMAssertions.spinUntilReleased( ref );

            assertEquals( 0, buf.remaining() );
            assertEquals( FP.option("a2"), buf.popLHS() );
            assertEquals( FP.option("a3"), buf.popLHS() );
            assertEquals( FP.option("a4"), buf.popLHS() );
            assertEquals( FP.option("a5"), buf.popLHS() );
            assertEquals( FP.emptyOption(), buf.popLHS() );
            assertTrue( buf.isEmpty() );
            assertFalse(buf.isFull());
        }

        @Test
        @SuppressWarnings({"unchecked", "UnusedAssignment"})
        public void givenTwoValueRingBuffer_callPopLHS_expectThePoppedValueToGetGCd() {
            Object v = new Object();
            Reference ref = new WeakReference( v );

            RingBuffer<Object> buf = new RingBuffer<>(Object.class, 10);

            assertEquals( 10, buf.remaining() );

            buf.append( v );
            v = null;

            buf.popLHS();

            assertEquals( 10, buf.remaining() );
            assertFalse(buf.isFull());

            JMAssertions.spinUntilReleased( ref );
        }
    }

    @Nested
    public class SizeTestCases {
        @Test
        public void givenAnEmptyBuffer_showThatNoMatterHowManyValuesArePushed_theSizeDoesNotChange() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            assertEquals( 4, buf.size() );
            for ( long v = 0; v < buf.size() * 10; v++ ) {
                buf.append( Long.toString(v) );

                assertEquals( 4, buf.size() );
            }
        }
    }


    @Nested
    public class HasOverflowedTestCases {
        @Test
        public void givenEmptySeq_expectHasOverflowedToReturnFalse() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 3 );

            assertFalse( buf.hasOverflowed() );
        }

        @Test
        public void givenOneSeq_expectHasOverflowedToReturnFalse() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 3 );

            buf.append("7");

            assertFalse( buf.hasOverflowed() );
        }

        @Test
        public void givenFullSeq_expectHasOverflowedToReturnFalse() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 3 );

            buf.append("7");
            buf.append("7");
            buf.append("7");

            assertFalse( buf.hasOverflowed() );
        }

        @Test
        public void givenFullSeq_appendOneMoreValue_expectHasOverflowedToReturnTrue() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 3 );

            buf.append("7");
            buf.append("7");
            buf.append("7");
            buf.append("7");

            assertTrue( buf.hasOverflowed() );
        }
    }

    @Nested
    public class RemainingTestCases {
        @Test
        public void showThatRemainingDropsByOneEverytimeAValueIsPushedUpToSize_andThenStaysAtZeroFromThenOn() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            assertEquals( 4, buf.remaining() );

            buf.append( "42" );
            assertEquals( 3, buf.remaining() );

            buf.append( "42" );
            assertEquals( 2, buf.remaining() );

            buf.append( "42" );
            assertEquals( 1, buf.remaining() );

            buf.append( "42" );
            assertEquals( 0, buf.remaining() );

            for ( long v = 0; v < buf.size() * 2; v++ ) {
                buf.append( Long.toString(v) );

                assertEquals( 0, buf.remaining() );
            }
        }
    }


    @Nested
    public class FilledTestCases {
        @Test
        public void showThatFilledStartsAtZeroAndThenClimbsUpToSizeAsEachValueIsPushedAndThenNeverIncreasesPastSize() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            assertEquals( 0, buf.contentsCount() );

            buf.append( "42" );
            assertEquals( 1, buf.contentsCount() );

            buf.append( "42" );
            assertEquals( 2, buf.contentsCount() );

            buf.append( "42" );
            assertEquals( 1, buf.remaining() );

            buf.append( "42" );
            assertEquals( 0, buf.remaining() );

            for ( long v = 0; v < buf.size() * 2; v++ ) {
                buf.append( Long.toString(v) );

                assertEquals( 0, buf.remaining() );
            }
        }
    }


    @Nested
    public class SetTestCases {
        @Test
        public void givenAFullBuffer_showThatEveryValueCanBeSet() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            buf.appendAll( "1", "2", "3", "4" );

            buf.set( 2, "33" );
            assertEquals( "[1, 2, 33, 4]", buf.mkString() );

            buf.set( 0, "11" );
            assertEquals( "[11, 2, 33, 4]", buf.mkString() );

            buf.set( 3, "44" );
            assertEquals( "[11, 2, 33, 44]", buf.mkString() );

            buf.set( 1, "22" );
            assertEquals( "[11, 22, 33, 44]", buf.mkString() );
        }

        @Test
        public void givenBufferThatHasWrapped_showThatEveryValueCanBeSet() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            buf.appendAll( "1", "2", "3", "4", "5", "6", "7", "8" );

            buf.set( 6, "33" );
            assertEquals( "[5, 6, 33, 8]", buf.mkString() );

            buf.set( 4, "11" );
            assertEquals( "[11, 6, 33, 8]", buf.mkString() );

            buf.set( 7, "44" );
            assertEquals( "[11, 6, 33, 44]", buf.mkString() );

            buf.set( 5, "22" );
            assertEquals( "[11, 22, 33, 44]", buf.mkString() );
        }

        @Test
        public void givenBufferThatHasWrapped_tryAccessingOneToTheLeftOfTheLHSIndex_expectException() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            buf.appendAll( "1", "2", "3", "4", "5", "6", "7", "8" );

            try {
                buf.set( 3, "33" );
                fail( "Expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 3 is out of bounds, currently supported indexes are 4 to 7 (inc)", ex.getMessage() );
            }
        }

        @Test
        public void givenBufferThatHasWrapped_tryAccessingOneToTheRightOfTheRHSIndexInc_expectException() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            buf.appendAll( "1", "2", "3", "4", "5", "6", "7", "8" );

            try {
                buf.set( 8, "33" );
                fail( "Expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 8 is out of bounds, currently supported indexes are 4 to 7 (inc)", ex.getMessage() );
            }
        }
    }


    @Nested
    public class IsEmptyTestCases {
        @Test
        public void showThaIsEmptyStartsOfTrueAndThenBecomesFalseAfterTheFirstValueIsPushedAndThenRemainsThatWay() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            assertTrue( buf.isEmpty() );

            buf.append( "42" );
            assertFalse( buf.isEmpty() );

            for ( long v = 0; v < buf.size() * 2; v++ ) {
                buf.append( Long.toString(v) );

                assertFalse( buf.isEmpty() );
            }
        }
    }


    @Nested
    public class IsFullTestCases {
        @Test
        public void showThaIsFullStartsOfFalseAndThenRemainsFalseUntilSizeNumberOfValuesHaveBeenPushedAndThenFromThenOnRemainsTrue() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            assertFalse( buf.isFull() );

            buf.append( "42" );
            assertFalse( buf.isFull() );

            buf.append( "42" );
            assertFalse( buf.isFull() );

            buf.append( "42" );
            assertFalse( buf.isFull() );

            buf.append( "42" );
            assertTrue( buf.isFull() );

            for ( long v = 0; v < buf.size() * 2; v++ ) {
                buf.append( Long.toString(v) );

                assertTrue( buf.isFull() );
            }
        }
    }


    @Nested
    public class LHSIndexTestCases {
        @Test
        public void showThatTheLHSIndexRemainsAtZeroUntilTheBufferIsFull() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            assertEquals( 0, buf.lhsIndex() );

            buf.append( "42" );
            assertEquals( 0, buf.lhsIndex() );

            buf.append( "42" );
            assertEquals( 0, buf.lhsIndex() );

            buf.append( "42" );
            assertEquals( 0, buf.lhsIndex() );

            buf.append( "42" );
            assertEquals( 0, buf.lhsIndex() );

            int expectedLHS = 0;
            for ( long v = 0; v < buf.size() * 2; v++ ) {
                buf.append( Long.toString(v) );

                expectedLHS += 1;
                assertEquals( expectedLHS, buf.lhsIndex() );
            }
        }
    }


    @Nested
    public class RHSIndexIncTestCases {
        @Test
        public void showThatRHSIndexIncStartsAtZeroAndIncrementsByOneEverytimeAValueIsPushed() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            assertEquals( 0, buf.rhsIndexInc() );

            buf.append( "42" );
            assertEquals( 0, buf.rhsIndexInc() );


            int expectedRHSInc = 0;
            for ( long v = 0; v < buf.size() * 2; v++ ) {
                buf.append( Long.toString(v) );

                expectedRHSInc += 1;
                assertEquals( expectedRHSInc, buf.rhsIndexInc() );
            }
        }
    }


    @Nested
    public class RHSIndexExcTestCases {
        @Test
        public void showThatRHSIndexExcStartsAtZeroAndIncrementsByOneEverytimeAValueIsPushed() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            assertEquals( 0, buf.rhsIndexExc() );

            int expectedRHSInc = 0;
            for ( long v = 0; v < buf.size() * 2; v++ ) {
                buf.append( Long.toString(v) );

                expectedRHSInc += 1;
                assertEquals( expectedRHSInc, buf.rhsIndexExc() );
            }
        }
    }


    @Nested
    public class IteratorTestCases {
        @Test
        public void testTheIteratorByInvokingMkStringOnIterator() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            assertEquals( "[]", buf.iterator().mkString() );

            buf.append( "1" );
            assertEquals( "[1]", buf.iterator().mkString() );

            buf.append( "2" );
            assertEquals( "[1, 2]", buf.iterator().mkString() );

            buf.append( "3" );
            assertEquals( "[1, 2, 3]", buf.iterator().mkString() );

            buf.append( "4" );
            assertEquals( "[1, 2, 3, 4]", buf.iterator().mkString() );

            buf.append( "5" );
            assertEquals( "[2, 3, 4, 5]", buf.iterator().mkString() );

            buf.append( "6" );
            assertEquals( "[3, 4, 5, 6]", buf.iterator().mkString() );

            buf.append( "7" );
            assertEquals( "[4, 5, 6, 7]", buf.iterator().mkString() );
        }
    }


    @Nested
    public class GetRHSByOffsetTestCases {
        @Test
        public void givenAnEmptyBuffer_insertARandomSampleOfValues() {
            // todo inject random samples
            String[] samples = new String[] {"10L", "Long.MAX_VALUE", "Long.MIN_VALUE", "0", "-1", "100", "2001", "42", "-100"};
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            Iterator<String[]> it = FPIterable.wrapArray( samples ).iterator().slice( String.class,4 );
            while ( it.hasNext() ) {
                String[] values = it.next();
                if ( values.length != 4 ) {
                    break;
                }

                buf.appendAll( values );

                assertEquals( values[3], buf.getRHS() );
                assertEquals( values[3], buf.getRHSByOffset( 0 ) );
                assertEquals( values[2], buf.getRHSByOffset( 1 ) );
                assertEquals( values[1], buf.getRHSByOffset( 2 ) );
                assertEquals( values[0], buf.getRHSByOffset( 3 ) );
            }
        }
    }


    @Nested
    public class UpdateRHSTestCases {
        @Test
        public void updateAnEmptyBuffer_expectException() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            try {
                buf.updateRHS( "3" );

                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 0 is out of bounds, currently supported indexes are 0 to 0 (inc)", ex.getMessage() );
            }
        }

        @Test
        public void givenASingleValueBuffer_expectUpdates() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            buf.append( "10" );

            buf.updateRHS( "3" );
            buf.updateRHS( "2" );
            buf.updateRHS( "1" );

            assertEquals( 4, buf.size() );
            assertEquals( 3, buf.remaining() );
            assertEquals( "1", buf.get(0) );
        }

        @Test
        public void givenATwoValueBuffer_expectUpdates() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            buf.append( "10" );
            buf.append( "9" );

            buf.updateRHS( "3" );
            buf.updateRHS( "2" );
            buf.updateRHS( "1" );

            assertEquals( 4, buf.size() );
            assertEquals( 2, buf.remaining() );
            assertEquals( "10", buf.get(0) );
            assertEquals( "1", buf.get(1) );
        }

        @Test
        public void givenFullValueBuffer_expectUpdates() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            buf.append( "10" );
            buf.append( "9" );
            buf.append( "8" );
            buf.append( "7" );

            buf.updateRHS( "3" );
            buf.updateRHS( "2" );
            buf.updateRHS( "1" );

            assertEquals( 4, buf.size() );
            assertEquals( 0, buf.remaining() );
            assertEquals( "10", buf.get(0) );
            assertEquals( "9", buf.get(1) );
            assertEquals( "8", buf.get(2) );
            assertEquals( "1", buf.get(3) );
        }

        @Test
        public void givenOverflowedBuffer_expectUpdates() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            buf.append( "10" );
            buf.append( "9" );
            buf.append( "8" );
            buf.append( "7" );
            buf.append( "6" );
            buf.append( "5" );

            buf.updateRHS( "3" );
            buf.updateRHS( "2" );
            buf.updateRHS( "1" );

            assertEquals( 4, buf.size() );
            assertEquals( 0, buf.remaining() );
            assertEquals( "8", buf.get(2) );
            assertEquals( "7", buf.get(3) );
            assertEquals( "6", buf.get(4) );
            assertEquals( "1", buf.get(5) );
        }
    }


    @Nested
    public class MapTestCases {
        @Test
        public void testMap() {
            RingBuffer<String> origBuf   = new RingBuffer<>( String.class, 4 );
            FPIterable<String> mappedBuf = origBuf.map( i -> i + "0" );


            assertEquals( "[]", origBuf.iterator().mkString() );
            assertEquals( "[]", mappedBuf.iterator().mkString() );

            origBuf.append( "1" );
            assertEquals( "[1]", origBuf.iterator().mkString() );
            assertEquals( "[10]", mappedBuf.iterator().mkString() );

            origBuf.append( "2" );
            assertEquals( "[1, 2]", origBuf.iterator().mkString() );
            assertEquals( "[10, 20]", mappedBuf.iterator().mkString() );

            origBuf.append( "3" );
            assertEquals( "[1, 2, 3]", origBuf.iterator().mkString() );
            assertEquals( "[10, 20, 30]", mappedBuf.iterator().mkString() );


            origBuf.append( "4" );
            assertEquals( "[1, 2, 3, 4]", origBuf.iterator().mkString() );
            assertEquals( "[10, 20, 30, 40]", mappedBuf.iterator().mkString() );

            origBuf.append( "5" );
            assertEquals( "[2, 3, 4, 5]", origBuf.iterator().mkString() );
            assertEquals( "[20, 30, 40, 50]", mappedBuf.iterator().mkString() );

            origBuf.append( "6" );
            assertEquals( "[3, 4, 5, 6]", origBuf.iterator().mkString() );
            assertEquals( "[30, 40, 50, 60]", mappedBuf.iterator().mkString() );

            origBuf.append( "7" );
            assertEquals( "[4, 5, 6, 7]", origBuf.iterator().mkString() );
            assertEquals( "[40, 50, 60, 70]", mappedBuf.iterator().mkString() );
        }

        @Test
        public void testClonedMap() {
            RingBuffer<String> origBuf   = new RingBuffer<>( String.class, 4 );
            FPIterable<String> mappedBuf = origBuf.map( i -> i + "0" );

            FPIterable<String> copiedBuf0 = origBuf.clone().map( i -> i + "0" );
            assertEquals( "[]", origBuf.iterator().mkString() );
            assertEquals( "[]", mappedBuf.iterator().mkString() );
            assertEquals( "[]", copiedBuf0.iterator().mkString() );

            origBuf.append( "1" );
            FPIterable<String> copiedBuf1 = origBuf.clone().map( i -> i + "0" );

            assertEquals( "[1]", origBuf.iterator().mkString() );
            assertEquals( "[10]", mappedBuf.iterator().mkString() );
            assertEquals( "[]", copiedBuf0.iterator().mkString() );
            assertEquals( "[10]", copiedBuf1.iterator().mkString() );

            origBuf.append( "2" );
            FPIterable<String> copiedBuf2 = origBuf.clone().map( i -> i + "0" );

            assertEquals( "[1, 2]", origBuf.iterator().mkString() );
            assertEquals( "[10, 20]", mappedBuf.iterator().mkString() );
            assertEquals( "[]", copiedBuf0.iterator().mkString() );
            assertEquals( "[10]", copiedBuf1.iterator().mkString() );
            assertEquals( "[10, 20]", copiedBuf2.iterator().mkString() );

            origBuf.append( "3" );
            FPIterable<String> copiedBuf3 = origBuf.clone().map( i -> i + "0" );

            assertEquals( "[1, 2, 3]", origBuf.iterator().mkString() );
            assertEquals( "[10, 20, 30]", mappedBuf.iterator().mkString() );
            assertEquals( "[]", copiedBuf0.iterator().mkString() );
            assertEquals( "[10]", copiedBuf1.iterator().mkString() );
            assertEquals( "[10, 20]", copiedBuf2.iterator().mkString() );
            assertEquals( "[10, 20, 30]", copiedBuf3.iterator().mkString() );


            origBuf.append( "4" );
            FPIterable<String> copiedBuf4 = origBuf.clone().map( i -> i + "0" );

            assertEquals( "[1, 2, 3, 4]", origBuf.iterator().mkString() );
            assertEquals( "[10, 20, 30, 40]", mappedBuf.iterator().mkString() );
            assertEquals( "[]", copiedBuf0.iterator().mkString() );
            assertEquals( "[10]", copiedBuf1.iterator().mkString() );
            assertEquals( "[10, 20]", copiedBuf2.iterator().mkString() );
            assertEquals( "[10, 20, 30]", copiedBuf3.iterator().mkString() );
            assertEquals( "[10, 20, 30, 40]", copiedBuf4.iterator().mkString() );

            origBuf.append( "5" );
            FPIterable<String> copiedBuf5 = origBuf.clone().map( i -> i + "0" );

            assertEquals( "[2, 3, 4, 5]", origBuf.iterator().mkString() );
            assertEquals( "[20, 30, 40, 50]", mappedBuf.iterator().mkString() );
            assertEquals( "[]", copiedBuf0.iterator().mkString() );
            assertEquals( "[10]", copiedBuf1.iterator().mkString() );
            assertEquals( "[10, 20]", copiedBuf2.iterator().mkString() );
            assertEquals( "[10, 20, 30]", copiedBuf3.iterator().mkString() );
            assertEquals( "[10, 20, 30, 40]", copiedBuf4.iterator().mkString() );
            assertEquals( "[20, 30, 40, 50]", copiedBuf5.iterator().mkString() );

            origBuf.append( "6" );
            FPIterable<String> copiedBuf6 = origBuf.clone().map( i -> i + "0" );
            assertEquals( "[3, 4, 5, 6]", origBuf.iterator().mkString() );
            assertEquals( "[30, 40, 50, 60]", mappedBuf.iterator().mkString() );
            assertEquals( "[]", copiedBuf0.iterator().mkString() );
            assertEquals( "[10]", copiedBuf1.iterator().mkString() );
            assertEquals( "[10, 20]", copiedBuf2.iterator().mkString() );
            assertEquals( "[10, 20, 30]", copiedBuf3.iterator().mkString() );
            assertEquals( "[10, 20, 30, 40]", copiedBuf4.iterator().mkString() );
            assertEquals( "[20, 30, 40, 50]", copiedBuf5.iterator().mkString() );
            assertEquals( "[30, 40, 50, 60]", copiedBuf6.iterator().mkString() );

            origBuf.append( "7" );
            FPIterable<String> copiedBuf7 = origBuf.clone().map( i -> i + "0" );

            assertEquals( "[4, 5, 6, 7]", origBuf.iterator().mkString() );
            assertEquals( "[40, 50, 60, 70]", mappedBuf.iterator().mkString() );
            assertEquals( "[]", copiedBuf0.iterator().mkString() );
            assertEquals( "[10]", copiedBuf1.iterator().mkString() );
            assertEquals( "[10, 20]", copiedBuf2.iterator().mkString() );
            assertEquals( "[10, 20, 30]", copiedBuf3.iterator().mkString() );
            assertEquals( "[10, 20, 30, 40]", copiedBuf4.iterator().mkString() );
            assertEquals( "[20, 30, 40, 50]", copiedBuf5.iterator().mkString() );
            assertEquals( "[30, 40, 50, 60]", copiedBuf6.iterator().mkString() );
            assertEquals( "[40, 50, 60, 70]", copiedBuf7.iterator().mkString() );
        }
    }


    @Nested
    public class GeneralTestCases {
        @Test
        public void testLongArray() {
            RingBuffer<String> buf = new RingBuffer<>( String.class, 4 );

            // VALIDATE NEW CREATED/EMPTY RING BUFFER
            assertTrue( buf.isEmpty() );
            assertFalse( buf.isFull() );
            assertEquals( 0, buf.lhsIndex() );
            assertEquals( 0, buf.rhsIndexExc() );
            assertEquals( 4, buf.remaining() );
            assertEquals( 0, buf.contentsCount() );

            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 0 ), "index 0 is out of bounds, as the buffer is currently empty" );
            assertFalse( buf.iterator().hasNext() );


            // ADD ONE VALUE TO THE BUFFER
            buf.append( "3" );

            assertFalse( buf.isEmpty() );
            assertFalse( buf.isFull() );
            assertEquals( 0, buf.lhsIndex() );
            assertEquals( 0, buf.rhsIndexInc() );
            assertEquals( 3, buf.remaining() );
            assertEquals( 1, buf.contentsCount() );

            assertEquals( "3", buf.get( 0 ) );
            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 1 ), "index 1 is out of bounds, currently supported indexes are 0 to 0 (inc)" );

            assertIterator( buf, "3" ); // ensure that iterator() can be called twice, giving the same results
            assertIterator( buf, "3" );


            // ADD ANOTHER VALUE TO THE BUFFER  (BUFFER WILL NOT WRAP YET)
            buf.append( "2" );

            assertFalse( buf.isEmpty() );
            assertFalse( buf.isFull() );
            assertEquals( 0, buf.lhsIndex() );
            assertEquals( 1, buf.rhsIndexInc() );
            assertEquals( 2, buf.remaining() );
            assertEquals( 2, buf.contentsCount() );

            assertEquals( "3", buf.get( 0 ) );
            assertEquals( "2", buf.get( 1 ) );
            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 2 ), "index 2 is out of bounds, currently supported indexes are 0 to 1 (inc)" );

            assertIterator( buf, "3", "2" ); // ensure that iterator() can be called twice, giving the same results
            assertIterator( buf, "3", "2" );


            // ADD ANOTHER VALUE TO THE BUFFER  (BUFFER WILL NOT WRAP YET)
            buf.append( "1" );

            assertFalse( buf.isEmpty() );
            assertFalse( buf.isFull() );
            assertEquals( 0, buf.lhsIndex() );
            assertEquals( 2, buf.rhsIndexInc() );
            assertEquals( 1, buf.remaining() );
            assertEquals( 3, buf.contentsCount() );

            assertEquals( "3", buf.get( 0 ) );
            assertEquals( "2", buf.get( 1 ) );
            assertEquals( "1", buf.get( 2 ) );
            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 3 ), "index 3 is out of bounds, currently supported indexes are 0 to 2 (inc)" );

            assertIterator( buf, "3", "2", "1" ); // ensure that iterator() can be called twice, giving the same results
            assertIterator( buf, "3", "2", "1" );


            // ADD ANOTHER VALUE TO THE BUFFER  (BUFFER WILL NOT WRAP YET)
            buf.append( "2" );

            assertFalse( buf.isEmpty() );
            assertTrue( buf.isFull() );
            assertEquals( 0, buf.lhsIndex() );
            assertEquals( 3, buf.rhsIndexInc() );
            assertEquals( 0, buf.remaining() );
            assertEquals( 4, buf.contentsCount() );

            assertEquals( "3", buf.get( 0 ) );
            assertEquals( "2", buf.get( 1 ) );
            assertEquals( "1", buf.get( 2 ) );
            assertEquals( "2", buf.get( 3 ) );
            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 4 ), "index 4 is out of bounds, currently supported indexes are 0 to 3 (inc)" );

            assertIterator( buf, "3", "2", "1", "2" ); // ensure that iterator() can be called twice, giving the same results
            assertIterator( buf, "3", "2", "1", "2" );


            // ADD ANOTHER VALUE TO THE BUFFER  (BUFFER WILL WRAP, DROPPING THE FIRST VALUE)
            buf.append( "3" );

            assertFalse( buf.isEmpty() );
            assertTrue( buf.isFull() );
            assertEquals( 1, buf.lhsIndex() );
            assertEquals( 4, buf.rhsIndexInc() );
            assertEquals( 0, buf.remaining() );
            assertEquals( 4, buf.contentsCount() );

            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 0 ), "index 0 is out of bounds, currently supported indexes are 1 to 4 (inc)" );
            assertEquals( "2", buf.get( 1 ) );
            assertEquals( "1", buf.get( 2 ) );
            assertEquals( "2", buf.get( 3 ) );
            assertEquals( "3", buf.get( 4 ) );
            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 5 ), "index 5 is out of bounds, currently supported indexes are 1 to 4 (inc)" );

            assertIterator( buf, "2", "1", "2", "3" ); // ensure that iterator() can be called twice, giving the same results
            assertIterator( buf, "2", "1", "2", "3" );
        }

        private void assertIterator( RingBuffer<String> buf, String... expectedValues ) {
            FPIterator<String> it = buf.iterator();

            for ( int i = 0; i < expectedValues.length; i++ ) {
                assertTrue( it.hasNext() );
                assertEquals( expectedValues[i], it.next() );
            }

            assertFalse( it.hasNext() );
        }
    }
}
