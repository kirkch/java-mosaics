package mosaics.xml.ast;

import lombok.Value;
import mosaics.io.xml.TagWriter;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.VoidFunction2;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


public class XmlIfASTTest {

    private POJO                            pojo                 = new POJO( "abc" );
    private TagWriter                       tagWriterMock        = Mockito.mock(TagWriter.class);
    private VoidFunction2<Object,TagWriter> nestedSerializerMock = Backdoor.cast(Mockito.mock(VoidFunction2.class));


    @Test
    public void givenFalsePredicate_expectElseBlockOutput() {
        XmlIfAST<POJO> ast = new XmlIfAST<POJO>( pojo -> false )
            .appendIfTrueBodyAST( new XmlStringBodyAST<>(v -> "on true body text") )
            .appendElseBodyAST( new XmlStringBodyAST<>(v -> "on false body text") );

        ast.writeTo( pojo, nestedSerializerMock, tagWriterMock );

        Mockito.verify( tagWriterMock ).bodyText( "on false body text" );
        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenTruePredicate_expectTrueBlockOutput() {
        XmlIfAST<POJO> ast = new XmlIfAST<POJO>( pojo -> true )
            .appendIfTrueBodyAST( new XmlStringBodyAST<>(v -> "on true body text") )
            .appendElseBodyAST( new XmlStringBodyAST<>(v -> "on false body text") );

        ast.writeTo( pojo, nestedSerializerMock, tagWriterMock );

        Mockito.verify( tagWriterMock ).bodyText( "on true body text" );
        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }


    @Value
    public static class POJO {
        private String v;
    }

}
