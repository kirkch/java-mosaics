package mosaics.xml;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;
import mosaics.fp.FP;
import mosaics.fp.collections.RRBVector;
import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.VoidFunction2;
import mosaics.strings.parser.ParseException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static mosaics.lang.Backdoor.cast;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;


public class XmlTemplateFactoryTest {

    private XmlTemplateFactory              factory              = new XmlTemplateFactory();
    private TagWriter                       tagWriterMock        = Mockito.mock( TagWriter.class );
    private VoidFunction2<Object,TagWriter> nestedSerializerMock = cast(Mockito.mock(VoidFunction2.class));

    @Test
    public void givenNullInput_expectError() {
        try {
            factory.parse( "t1", (String) null, POJO.class );

            fail("expected IllegalStateException");
        } catch ( IllegalStateException ex ) {
            assertEquals( "in", ex.getMessage() );
        }
    }

    @Test
    public void givenBlankInput_expectError() {
        try {
            factory.parse( "t1", "  ", POJO.class );

            fail("expected IllegalStateException");
        } catch ( IllegalStateException ex ) {
            assertEquals( "in", ex.getMessage() );
        }
    }


// BASIC TAG SUPPORT

    @Test
    public void givenEmptyTag_expectEmptyTagOutput() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<name/>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "name" );
        Mockito.verify(tagWriterMock).closeTag( "name" );
        Mockito.verifyNoMoreInteractions( tagWriterMock, nestedSerializerMock );
    }

    @Test
    public void givenTagWithFixedAttribute() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<book name=\"foo\"/>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "name", "foo" );
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenTagWithFixedStringBody() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<book>foo</book>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "foo" );
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenTagWithFixedStringBodyWithWhitespace() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<book>  foo  </book>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "foo" );
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenTagWithTwoFixedAttributesAndFixedStringBody() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<book a1=\" v1 \" a2=\" v2 \">foo</book>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "a1", " v1 ");
        Mockito.verify(tagWriterMock).attr( "a2", " v2 ");
        Mockito.verify(tagWriterMock).bodyText( "foo" );
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenAttributeNotOnPOJO_expectError() {
        try {
            factory.parse( "t1", "<book a1=$missingParam/>", POJO.class );
            fail( "expected ParseException" );
        } catch ( ParseException ex ) {
            assertEquals( "t1 [1,10,9]: Invalid property path 'missingParam' for mosaics.xml.XmlTemplateFactoryTest$POJO", ex.getMessage() );
        }
    }

    @Test
    public void givenAttributeThatFollowsASinglePropertyPathOnPOJO() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<book a1=$attr2/>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "a1", "b");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenAttributeThatFollowsATwoElementPropertyPathOnPOJO() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<book a1=${attr2.length}/>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "a1", "1");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenBodyNotOnPOJO_expectError() {
        try {
            factory.parse( "t1", "<book>$missingBody</book>", POJO.class );
            fail( "expected ParseException" );
        } catch ( ParseException ex ) {
            assertEquals( "t1 [1,7,6]: Invalid property path 'missingBody' for mosaics.xml.XmlTemplateFactoryTest$POJO", ex.getMessage() );
        }
    }

    @Test
    public void givenBodyOnPOJO() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<book> $attr2 </book>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "b");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenBodyThatFollowsAPropertyPathOnPOJO() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<book> ${attr2.length} </book>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "1");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }


// NESTED TAG SUPPORT

    @Test
    public void givenNestedChildTagThatIsEmpty() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<books> <book/> </books>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenNestedChildTagThatHasFixedStringBody() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<books> <book>foo</book> </books>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "foo");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenNestedChildTagThatHasOneAttributeProperty() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<books> <book name=\"foo\"></book> </books>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "name", "foo");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenNestedChildTagThatHasBodyProperty() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<books> <book>$attr1</book> </books>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "a");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenTwoChildTags() {
        XmlTemplate<POJO> template = factory.parse( "t1", "<books> <book>$attr1</book>\n<book>$attr2</book> </books>", POJO.class );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock, times(2)).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "a");
        Mockito.verify(tagWriterMock).bodyText( "b");
        Mockito.verify(tagWriterMock, times(2)).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }


// IF SUPPORT WITHIN TAG BODY

    @Test
    public void givenIfBlockWithinTagBody_supplyTrueValue_expectIfBlockToBeIncludedInOutput() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<books> #if $includeMe <book>$attr1</book> #endif </books>",
            POJO.class
        );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "a");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfBlockWithinTagBody_supplyFalseValue_expectIfBlockToNotBeIncludedInOutput() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<books flag=\"true\"> #if $includeMe <book>$attr1</book> #endif </books>",
            POJO.class
        );

        template.invoke( new POJO("a","b").withIncludeMe(false), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).attr( "flag", "true" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfBlockContainingTwoTagsWithinTagBody_supplyTrueValue_expectIfBlockToBeIncludedInOutput() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<books> #if $includeMe <book>$attr1</book> <book>$attr2</book> #endif </books>",
            POJO.class
        );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock, times(2)).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "a");
        Mockito.verify(tagWriterMock).bodyText( "b");
        Mockito.verify(tagWriterMock, times(2)).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseBlockWithinTagBody_supplyTrueValue_expectTrueCase() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<books> #if $includeMe <book>$attr1</book> #else <book>$attr2</book> #endif </books>",
            POJO.class
        );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "a");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseBlockWithinTagBody_supplyFalseValue_expectElseCase() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<books> #if $includeMe <book>$attr1</book> #else <book>$attr2</book> #endif </books>",
            POJO.class
        );

        template.invoke( new POJO("a","b").withIncludeMe(false), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "b");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfBlockWithinTagBody_supplyFirstPredicate_expectFirstIfToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<books> #if $isAttr1A <book>Harry Potter 1</book> #elseif $isAttr1B <book>Harry Potter 2</book> #endif </books>",
            POJO.class
        );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "Harry Potter 1");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfBlockWithinTagBody_supplySecondPredicate_expectSecondIfToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<books> #if $isAttr1A <book>Harry Potter 1</book> #elseif $isAttr1B <book>Harry Potter 2</book> #endif </books>",
            POJO.class
        );

        template.invoke( new POJO("b","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "Harry Potter 2");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfBlockWithinTagBody_supplyNoPredicate_expectIfToNotExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<books> #if $isAttr1A <book>Harry Potter 1</book> #elseif $isAttr1B <book>Harry Potter 2</book> #endif </books>",
            POJO.class
        );

        template.invoke( new POJO("c","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verifyNoInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfElseBlockWithinTagBody_supplyFirstPredicate_expectFirstIfToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<books> #if $isAttr1A <book>Harry Potter 1</book> #elseif $isAttr1B <book>Harry Potter 2</book> #else <book>Harry Potter 3</book> #endif </books>",
            POJO.class
        );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "Harry Potter 1");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfElseBlockWithinTagBody_supplySecondPredicate_expectSecondIfToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<books> #if $isAttr1A <book>Harry Potter 1</book> #elseif $isAttr1B <book>Harry Potter 2</book> #else <book>Harry Potter 3</book> #endif </books>",
            POJO.class
        );

        template.invoke( new POJO("b","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "Harry Potter 2");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfElseBlockWithinTagBody_supplyNoPredicate_expectElseToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<books> #if $isAttr1A <book>Harry Potter 1</book> #elseif $isAttr1B <book>Harry Potter 2</book> #else <book>Harry Potter 3</book> #endif </books>",
            POJO.class
        );

        template.invoke( new POJO("c","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books");
        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).bodyText( "Harry Potter 3");
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }


// IF SUPPORT WITHIN TAG ATTRIBUTE SECTION

    @Test
    public void givenIfBlockWithinTagAttributeSection_supplyFalsePredicate_expectIfBlockToNotExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<book #if $includeMe name=$attr1 #endif/>",
            POJO.class
        );

        template.invoke( new POJO("a","b").withIncludeMe(false), nestedSerializerMock, tagWriterMock );

        Mockito.verifyNoInteractions( tagWriterMock );
    }

    @Test
    public void givenIfBlockWithinTagAttributeSection_supplyTruePredicate_expectIfBlockToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<book #if $includeMe name=$attr1 #endif/>",
            POJO.class
        );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "name", "a");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseBlockWithinTagAttributeSection_supplyFalsePredicate_expectElseBlockToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<book #if $includeMe name=$attr1 #else name=$attr2 #endif/>",
            POJO.class
        );

        template.invoke( new POJO("a","b").withIncludeMe(false), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "name", "b");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseBlockWithinTagAttributeSection_supplyTruePredicate_expectIfBlockToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<book #if $includeMe name=$attr1 #else name=$attr2 #endif/>",
            POJO.class
        );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "name", "a");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfBlockWithinTagAttributeSection_supplyPred1True_expectFirstIfBlockToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<book #if $isAttr1A name=\"book1\" #elseif $isAttr1B name=\"book2\" #endif/>",
            POJO.class
        );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "name", "book1");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfBlockWithinTagAttributeSection_supplyPred1FalsePred2True_expectSecondIfBlockToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<book #if $isAttr1A name=\"book1\" #elseif $isAttr1B name=\"book2\" #endif/>",
            POJO.class
        );

        template.invoke( new POJO("b","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "name", "book2");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfBlockWithinTagAttributeSection_supplyPred1FalsePred2False_expectNoIfBlockToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<book #if $isAttr1A name=\"book1\" #elseif $isAttr1B name=\"book2\" #endif/>",
            POJO.class
        );

        template.invoke( new POJO("c","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verifyNoInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfElseBlockWithinTagAttributeSection_supplyPred1True_expectFirstIfBlockToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<book #if $isAttr1A name=\"book1\" #elseif $isAttr1B name=\"book2\" #else name=\"book3\" #endif/>",
            POJO.class
        );

        template.invoke( new POJO("a","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "name", "book1");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfElseBlockWithinTagAttributeSection_supplyPred1FalsePred2True_expectSecondIfBlockToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<book #if $isAttr1A name=\"book1\" #elseif $isAttr1B name=\"book2\" #else name=\"book3\" #endif/>",
            POJO.class
        );

        template.invoke( new POJO("b","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "name", "book2");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenIfElseIfElseBlockWithinTagAttributeSection_supplyPred1FalsePred2False_expectElseIfBlockToExecute() {
        XmlTemplate<POJO> template = factory.parse(
            "t1",
            "<book #if $isAttr1A name=\"book1\" #elseif $isAttr1B name=\"book2\" #else name=\"book3\" #endif/>",
            POJO.class
        );

        template.invoke( new POJO("c","b"), nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "book");
        Mockito.verify(tagWriterMock).attr( "name", "book3");
        Mockito.verify(tagWriterMock).closeTag( "book" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }


// FOR LOOP SUPPORT

    @Test
    public void givenForLoopWithinTagBody_emptyIterable_expectNoOutput() {
        XmlTemplate<BookCollection> template = cast(
            factory.parse(
                "t1",
                "<books>#foreach $books <book name=$name/> #endforeach</books>",
                BookCollection.class
            )
        );

        BookCollection books = new BookCollection(FP.emptyVector());
        template.invoke( books, nestedSerializerMock, tagWriterMock );

        Mockito.verifyNoInteractions( tagWriterMock );
    }

    @Test
    public void givenForLoopWithinTagBody_singleBookIterable_expectOneBookOutput() {
        XmlTemplate<BookCollection> template = cast(
            factory.parse(
                "t1",
                "<books>#foreach $books <book name=$name/> #endforeach</books>",
                BookCollection.class
            )
        );

        BookCollection books = new BookCollection(FP.toVector(new Book("LOTR")));
        template.invoke( books, nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books" );
        Mockito.verify(tagWriterMock).tag( "book" );
        Mockito.verify(tagWriterMock).attr( "name", "LOTR" );
        Mockito.verify(tagWriterMock).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );
    }

    @Test
    public void givenForLoopWithinTagBody_twoBookIterable_expectTwoBooksOutput() {
        XmlTemplate<BookCollection> template = cast(
            factory.parse(
                "t1",
                "<books>#foreach $books <book name=$name/> #endforeach</books>",
                BookCollection.class
            )
        );

        BookCollection books = new BookCollection(FP.toVector(new Book("LOTR"), new Book("SW")));
        template.invoke( books, nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books" );
        Mockito.verify(tagWriterMock, times(2)).tag( "book" );
        Mockito.verify(tagWriterMock).attr( "name", "LOTR" );
        Mockito.verify(tagWriterMock).attr( "name", "SW" );
        Mockito.verify(tagWriterMock, times(2)).closeTag( "book" );
        Mockito.verify(tagWriterMock).closeTag( "books" );
    }

    @Test
    public void givenForLoopWithinTagBody_nonIterable_expectException() {
        try {
            factory.parse(
                "t1",
                "<books>#foreach $toString <book name=$name/> #endforeach</books>",
                BookCollection.class
            );

            fail( "expected exception" );
        } catch ( ParseException ex ) {
            assertEquals( "t1 [1,16,15]: this.string is of type 'java.lang.String';  expected something that implements Iterable.", ex.getMessage() );
        }
    }

    @Test
    public void givenForEachOverBooksUsingThis() {
        XmlTemplate<BookCollection> template = cast(
            factory.parse(
                "t1",
                "<books>#foreach $books $this #endforeach</books>",
                BookCollection.class
            )
        );

        Book            book1          = new Book( "a" );
        Book            book2          = new Book( "b" );
        RRBVector<Book> books          = FP.toVector( book1, book2 );
        BookCollection  bookCollection = new BookCollection( books );

        template.invoke( bookCollection, nestedSerializerMock, tagWriterMock );

        Mockito.verify( tagWriterMock ).tag("books");
        Mockito.verify( nestedSerializerMock ).invoke(book1, tagWriterMock);
        Mockito.verify( nestedSerializerMock ).invoke(book2, tagWriterMock);
        Mockito.verify( tagWriterMock ).closeTag("books");

        Mockito.verifyNoMoreInteractions( tagWriterMock, nestedSerializerMock );
    }

    @Test
    public void givenForEachOverBooksUsingThis_includeExtraTextWithinForEachLoop() {
        XmlTemplate<BookCollection> template = cast(
            factory.parse(
                "t1",
                "<books>#foreach $books $this <br/> #endforeach</books>",
                BookCollection.class
            )
        );

        Book            book1          = new Book( "a" );
        Book            book2          = new Book( "b" );
        RRBVector<Book> books          = FP.toVector( book1, book2 );
        BookCollection  bookCollection = new BookCollection( books );

        template.invoke( bookCollection, nestedSerializerMock, tagWriterMock );

        Mockito.verify( tagWriterMock ).tag("books");
        Mockito.verify( nestedSerializerMock ).invoke(book1, tagWriterMock);
        Mockito.verify( nestedSerializerMock ).invoke(book2, tagWriterMock);
        Mockito.verify( tagWriterMock, times(2) ).tag("br");
        Mockito.verify( tagWriterMock, times(2) ).closeTag("br");
        Mockito.verify( tagWriterMock ).closeTag("books");

        Mockito.verifyNoMoreInteractions( tagWriterMock, nestedSerializerMock );
    }


// NESTED TAGS

    @Test
    public void givenBodyParameterThatHasNoStringCodec_callWithTwoElementVector_expectDoubleDispatchToHandle() {
        XmlTemplate<BookCollection> template = cast(
            factory.parse(
                "t1",
                "<books>$books</books>",
                BookCollection.class
            )
        );

        RRBVector<Book> books          = FP.toVector( new Book( "LOTR" ), new Book( "SW" ) );
        BookCollection  bookCollection = new BookCollection( books );

        template.invoke( bookCollection, nestedSerializerMock, tagWriterMock );

        Mockito.verify(tagWriterMock).tag( "books" );
        Mockito.verify(nestedSerializerMock).invoke( books, tagWriterMock );
        Mockito.verify(tagWriterMock).closeTag( "books" );

        Mockito.verifyNoMoreInteractions( tagWriterMock, nestedSerializerMock );
    }

    @Test
    public void givenBodyParameterThatHasNoStringCodec_callWithEmptyVector_expectNoTagOutput() {
        XmlTemplate<BookCollection> template = cast(
            factory.parse(
                "t1",
                "<books>$books</books>",
                BookCollection.class
            )
        );

        RRBVector<Book> books          = FP.emptyVector();
        BookCollection  bookCollection = new BookCollection( books );

        template.invoke( bookCollection, nestedSerializerMock, tagWriterMock );

        Mockito.verifyNoInteractions( tagWriterMock, nestedSerializerMock );
    }



    @Value
    public static final class BookCollection {
        private RRBVector<Book> books;
    }

    @Value
    public static class Book {
        private String name;
    }

    @Value
    @With
    @AllArgsConstructor
    public static final class POJO {
        private String  attr1;
        private String  attr2;
        private boolean includeMe;

        public POJO( String attr1, String attr2 ) {
            this( attr1, attr2, true );
        }

        public boolean isAttr1A() {
            return "a".equals( attr1 );
        }

        public boolean isAttr1B() {
            return "b".equals( attr1 );
        }
    }
}
