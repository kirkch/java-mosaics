package mosaics.lang.refcount;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class RefCounterDebugModeTest extends BaseRefCounterTestCases {

    protected RefCounter<Resource> createRefCount() {
        return new RefCounter<>( resource, Resource::close, true );
    }


    @Nested
    public class ShowThatDebugAssertionsAreTurnedOnTestCases {
        @Test
        public void givenNewManager_callDecTwice_expectException() {
            refCount.decRefCount();

            try {
                refCount.decRefCount();
                fail( "expected exception" );
            } catch ( IllegalStateException ex ) {
                assertEquals( "calls to inc/dec ref count must be ordered and balanced; ref count just went negative", ex.getMessage() );
            }
        }

        @Test
        public void givenReleasedResource_callInc_expectException() {
            refCount.decRefCount();

            try {
                refCount.incRefCount();
                fail( "expected exception" );
            } catch ( IllegalStateException ex ) {
                assertEquals( "this resource has already been released, do not call incRefCount after the refCount has reached zero", ex.getMessage() );
            }
        }
    }
}
