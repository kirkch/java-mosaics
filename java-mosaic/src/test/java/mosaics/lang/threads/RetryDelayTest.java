package mosaics.lang.threads;

import org.junit.jupiter.api.Test;

import java.util.function.LongSupplier;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class RetryDelayTest {

    @Test
    public void defaultRetryGenerator() {
        // // seq: 25+rnd(50), 50+rnd(100), 100+rnd(200), ..., capped at 30_000
        expectBetween( 12, 12+25, () -> RetryDelay.exponentialBackoffSeries(0) );
        expectBetween( 25,    25+50, () -> RetryDelay.exponentialBackoffSeries(1) );
        expectBetween( 50,    50+100, () -> RetryDelay.exponentialBackoffSeries(2) );
        expectBetween( 100,   100+200, () -> RetryDelay.exponentialBackoffSeries(3) );
        expectBetween( 200,   200+400, () -> RetryDelay.exponentialBackoffSeries(4) );
        expectBetween( 400,   400+800, () -> RetryDelay.exponentialBackoffSeries(5) );
        expectBetween( 800,   800+1600, () -> RetryDelay.exponentialBackoffSeries(6) );
        expectBetween( 1600,  1600+3200, () -> RetryDelay.exponentialBackoffSeries(7) );
        expectBetween( 3200,  3200+6400, () -> RetryDelay.exponentialBackoffSeries(8) );
        expectBetween( 6400,  6400+12800, () -> RetryDelay.exponentialBackoffSeries(9) );
        expectBetween( 12800, 30000, () -> RetryDelay.exponentialBackoffSeries(10) );
        expectBetween( 25600, 30000, () -> RetryDelay.exponentialBackoffSeries(11) );
        expectBetween( 30000, 30000, () -> RetryDelay.exponentialBackoffSeries(12) );
        expectBetween( 30000, 30000, () -> RetryDelay.exponentialBackoffSeries(13) );

        expectBetween( 30000, 30000, () -> RetryDelay.exponentialBackoffSeries(1_000_000) );
    }


    public static void expectBetween( long minInc, long maxInc, LongSupplier task ) {
        for ( int i=0; i<100; i++ ) {
            long v = task.getAsLong();

            assertTrue( v >= minInc, v+" >= " + minInc );
            assertTrue( v <= maxInc, v + " <= " + maxInc );
        }
    }

}
