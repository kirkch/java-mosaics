package mosaics.lang.threads;

import mosaics.fp.FP;
import mosaics.fp.Failure;
import mosaics.fp.collections.FPOption;
import mosaics.junit.JMAssertions;
import mosaics.lang.Backdoor;
import mosaics.lang.RandomUtils;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.VoidFunctionL;
import mosaics.lang.lifecycle.Subscription;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.IntToLongFunction;

import static mosaics.junit.JMAssertions.awaitLatch;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;


@SuppressWarnings("unchecked")
public class PollingThreadTest {

    private Function0<FPOption<String>> pollFMock            = Mockito.mock(Function0.class);
    private PollingThreadListener       callbacksMock        = Mockito.mock(PollingThreadListener.class);
    private CapturingListener           callbacks            = new CapturingListener();
    private IntToLongFunction           calcSleepLengthFMock = Mockito.mock(IntToLongFunction.class);
    private VoidFunctionL               sleepFMock           = Mockito.mock(VoidFunctionL.class);


    @BeforeEach
    public void setup() {
        Mockito.when( pollFMock.invoke() ).thenReturn( FP.emptyOption() );
        Mockito.when( calcSleepLengthFMock.applyAsLong( anyInt()) ).thenReturn( 13L );
    }


    @Test
    public void givenNewPollingThread_expectThreadWithCorrectNameToBeCreated() {
        String threadName = generatePollingThreadName();

        try ( Subscription sub = PollingThread.createPollingDaemonThread(threadName, pollFMock, callbacks) ) {
            assertNotNull( sub );

            JMAssertions.spinUntilTrue( () -> ThreadUtils.fetchThreadByName(threadName).hasValue() );
        }
    }

    @Test
    public void givenNewPollingThread_expectThreadStartedCallback() {
        String threadName = generatePollingThreadName();

        try ( Subscription sub = PollingThread.createPollingDaemonThread(threadName, pollFMock, callbacks) ) {
            assertNotNull( sub );

            JMAssertions.spinUntilTrue( () -> callbacks.events.contains("STARTED") );
        }
    }

    @Test
    public void givenShutdownPollingThread_expectThreadFinishedCallback() {
        String threadName = generatePollingThreadName();

        try ( Subscription sub = PollingThread.createPollingDaemonThread(threadName, pollFMock, callbacks) ) {
            assertNotNull( sub );
        }

        JMAssertions.spinUntilTrue( () -> callbacks.events.contains("FINISHED") );
    }

    @Test
    public void givenNewPollingThread_callCancel_expectNewThreadToExit() {
        String threadName = generatePollingThreadName();

        try ( Subscription sub = PollingThread.createPollingDaemonThread(threadName, pollFMock, callbacks) ) {
            assertNotNull( sub );

            JMAssertions.spinUntilTrue( () -> ThreadUtils.fetchThreadByName(threadName).hasValue() );
        }

        JMAssertions.spinUntilTrue( () -> ThreadUtils.fetchThreadByName(threadName).isEmpty() );
    }

    @Test
    public void givenPollerThatReturnsNone_sleepFToBeInvokedWithSpecifiedDelay() throws InterruptedException {
        String         threadName     = generatePollingThreadName();
        CountDownLatch runLatch       = new CountDownLatch( 10 );
        long           specifiedDelay = RandomUtils.rnd( 1, 1000 );

        VoidFunctionL sleepF = delayMillis -> {
            assertEquals(specifiedDelay, delayMillis);

            runLatch.countDown();
        };


        try ( Subscription sub = PollingThread.createPollingDaemonThread(
            threadName, pollFMock, callbacks, calcSleepLengthFMock, sleepF, specifiedDelay
        ) ) {
            assertNotNull( sub );

            Backdoor.sleep( 100 );

            assertTrue(sub.isActive());
        }

        awaitLatch(runLatch);

        verify(callbacksMock, never()).objectReceived( any() );
        verify(callbacksMock, never()).pollFailure( any() );
        verify(callbacksMock, never()).dispatchFailure( any(), any() );
    }

    @Test
    public void givenPollerThatReturnsSome_expectDispatcherToBeInvokedAndThenPollerAgainWithNoDelay() throws InterruptedException {
        String         threadName     = generatePollingThreadName();
        long           specifiedDelay = RandomUtils.rnd( 1, 1000 );
        String         value          = RandomUtils.randomString( 100 );

        Mockito.when( pollFMock.invoke() ).thenReturn( FP.option(value) );


        try ( Subscription sub = PollingThread.createPollingDaemonThread(
            threadName, pollFMock, callbacks, calcSleepLengthFMock, sleepFMock, specifiedDelay
        ) ) {
            assertNotNull( sub );

            Backdoor.sleep( 100 );

            assertTrue(sub.isActive());
        }

        awaitLatch(callbacks.threadFinishedLatch);

        callbacks.assertAllValuesReceivedEquals( value );

        verify(callbacksMock, never()).pollFailure( any() );
        verify(callbacksMock, never()).dispatchFailure( any(), any() );
    }

    @Test
    public void givenPollFThatErrors_expectErrorFToBeInvokedThenSleepBeforeCallingPollerAgain() throws InterruptedException {
        String threadName     = generatePollingThreadName();
        long   specifiedDelay = RandomUtils.rnd( 1, 1000 );

        RuntimeException            expectedException = new RuntimeException( "expected exception" );
        Function0<FPOption<String>> pollF             = () -> {throw expectedException;};


        try ( Subscription sub = PollingThread.createPollingDaemonThread(
            threadName, pollF, callbacks, calcSleepLengthFMock, sleepFMock, specifiedDelay
        ) ) {
            assertNotNull( sub );

            Backdoor.sleep( 100 );

            assertTrue(sub.isActive());
        }

        awaitLatch(callbacks.pollFailureLatch);
        awaitLatch(callbacks.threadFinishedLatch);

        callbacks.assertAllPollFailuresWereEqualTo( expectedException );
        callbacks.assertNoObjectsWereReceived();
    }

//    @Test  todo flakey test
    public void givenPollExceptions_expectEachCallToCalcSleepLengthToHaveAnIncrementingAttemptCount() throws InterruptedException {
        String         threadName     = generatePollingThreadName();
        CountDownLatch runLatch       = new CountDownLatch( 10 );
        long           specifiedDelay = RandomUtils.rnd( 1, 1000 );

        RuntimeException expectedException = new RuntimeException( "expected exception" );
        Mockito.when( pollFMock.invoke() ).thenThrow( expectedException );

        AtomicLong lastAttemptCount = new AtomicLong( 0 );
        AtomicLong errorCount       = new AtomicLong( 0 );

        IntToLongFunction calcSleepLengthF = attemptCount -> {
            if ( attemptCount != lastAttemptCount.get() + 1 ) {
                errorCount.incrementAndGet();
            }

            lastAttemptCount.set( attemptCount);
            runLatch.countDown();

            return specifiedDelay;
        };

        try ( Subscription sub = PollingThread.createPollingDaemonThread(
            threadName, pollFMock, callbacksMock, calcSleepLengthF, sleepFMock, specifiedDelay
        ) ) {
            assertNotNull( sub );

            Backdoor.sleep( 100 );

            assertTrue(sub.isActive());
        }

        awaitLatch(runLatch);

        assertEquals( 0, errorCount.get() );
        verify( callbacksMock, never() ).objectReceived( any());
    }

    @Test
    public void givenPollThrowsExceptionOnceAndThenRecoversOverAndOverAgain_expectTheAttemptCountToAlwaysResetAfterTheSuccessfulValue() throws InterruptedException {
        String         threadName     = generatePollingThreadName();
        CountDownLatch runLatch       = new CountDownLatch( 10 );
        long           specifiedDelay = RandomUtils.rnd( 1, 1000 );

        AtomicLong errorToggle = new AtomicLong();

        Function0<FPOption<String>> alternatingPollF = () -> {
            long v = errorToggle.incrementAndGet();

            if ( v % 3 == 0 ) {
                throw new RuntimeException( "expected exception" );
            } else if ( v % 2 == 0 ) {
                return FP.emptyOption();
            } else {
                return FP.option(RandomUtils.randomString( 100 ));
            }
        };

        AtomicLong errorCount = new AtomicLong( 0 );

        IntToLongFunction calcSleepLengthF = attemptCount -> {
            if ( attemptCount != 1 ) {
                errorCount.incrementAndGet();
            }

            runLatch.countDown();

            return specifiedDelay;
        };

        try ( Subscription sub = PollingThread.createPollingDaemonThread(
            threadName, alternatingPollF, callbacksMock, calcSleepLengthF, sleepFMock, specifiedDelay
        ) ) {
            assertNotNull( sub );

            Backdoor.sleep( 100 );

            assertTrue(sub.isActive());
        }

        awaitLatch(runLatch);

        assertEquals( 0, errorCount.get() );
    }

    
    @Test
    public void givenDispatchFThatErrors_expectErrorFToBeInvokedThenSleepBeforeCallingPollerAgain() throws InterruptedException {
        String         threadName     = generatePollingThreadName();
        CountDownLatch runLatch       = new CountDownLatch( 10 );
        long           specifiedDelay = RandomUtils.rnd( 1, 1000 );
        String         randomValue    = "A"+RandomUtils.randomString( 100 );
        RuntimeException expectedException = new RuntimeException( "expected exception" );

        Mockito.when(pollFMock.invoke()).thenReturn( FP.option(randomValue) );
        Mockito.doThrow(expectedException).when(callbacksMock).objectReceived(Mockito.any());

        AtomicLong unexpectedExceptionCount = new AtomicLong();

        doAnswer( invocation -> {
            String  v = invocation.getArgument( 0 );
            Failure f = invocation.getArgument( 1 );

            if ( expectedException != f.toException() ) {
                unexpectedExceptionCount.incrementAndGet();
            }

            assertEquals( randomValue, v );

            runLatch.countDown();

            return null;
        } ).when(callbacksMock).dispatchFailure( any(), any() );


        try ( Subscription sub = PollingThread.createPollingDaemonThread(
            threadName, pollFMock, callbacksMock, calcSleepLengthFMock, sleepFMock, specifiedDelay
        ) ) {
            assertNotNull( sub );

            Backdoor.sleep( 100 );

            assertTrue(sub.isActive());
        }

        awaitLatch(runLatch);

        assertEquals( 0, unexpectedExceptionCount.get() );
    }

    @Test
    public void givenDispatcherExceptions_expectEachCallToCalcSleepLengthToHaveAnIncrementingAttemptCount() throws InterruptedException {
        String           threadName        = generatePollingThreadName();
        CountDownLatch   runLatch          = new CountDownLatch( 10 );
        long             specifiedDelay    = RandomUtils.rnd( 1, 1000 );
        String           randomValue       = "A"+RandomUtils.randomString( 100 );
        RuntimeException expectedException = new RuntimeException( "expected exception" );

        Mockito.when(pollFMock.invoke()).thenReturn( FP.option(randomValue) );
        Mockito.doThrow(expectedException).when(callbacksMock).objectReceived(Mockito.any());

        AtomicLong lastAttemptCount = new AtomicLong( 0 );
        AtomicLong errorCount       = new AtomicLong( 0 );

        doAnswer( invocation -> {
            int attemptCount = invocation.getArgument( 0 );
            if ( attemptCount != lastAttemptCount.get() + 1 ) {
                errorCount.incrementAndGet();
            }

            lastAttemptCount.set( attemptCount);
            runLatch.countDown();

            return specifiedDelay;
        }).when( calcSleepLengthFMock ).applyAsLong( anyInt() );

        try ( Subscription sub = PollingThread.createPollingDaemonThread(
            threadName, pollFMock, callbacksMock, calcSleepLengthFMock, sleepFMock, specifiedDelay
        ) ) {
            assertNotNull( sub );

            Backdoor.sleep( 100 );

            assertTrue(sub.isActive());
        }

        awaitLatch(runLatch);

        assertEquals( 0, errorCount.get() );
    }

    @Test
    public void givenDispatcherThrowsExceptionOnceAndThenRecoversOverAndOverAgain_expectTheAttemptCountToAlwaysResetAfterTheSuccessfulValue() throws InterruptedException {
        String         threadName     = generatePollingThreadName();
        CountDownLatch runLatch       = new CountDownLatch( 10 );
        long           specifiedDelay = RandomUtils.rnd( 1, 1000 );
        String         randomValue    = "A"+RandomUtils.randomString( 100 );

        Mockito.when(pollFMock.invoke()).thenReturn( FP.option(randomValue) );

        AtomicLong errorToggle = new AtomicLong();

        doAnswer( invocation -> {
           String str = invocation.getArgument( 0 );

            long v = errorToggle.incrementAndGet();

            if ( v % 2 == 0 ) {
                throw new RuntimeException( "expected exception" );
            }

            return null;
        }).when( callbacksMock ).objectReceived( any() );


        AtomicLong errorCount = new AtomicLong( 0 );

        IntToLongFunction calcSleepLengthF = attemptCount -> {
            if ( attemptCount != 1 ) {
                errorCount.incrementAndGet();
            }

            runLatch.countDown();

            return specifiedDelay;
        };

        try ( Subscription sub = PollingThread.createPollingDaemonThread(
            threadName, pollFMock, callbacksMock, calcSleepLengthF, sleepFMock, specifiedDelay
        ) ) {
            assertNotNull( sub );

            Backdoor.sleep( 100 );

            assertTrue(sub.isActive());
        }

        awaitLatch(runLatch);

        assertEquals( 0, errorCount.get() );
    }

    @Test
    public void givenPollExceptionCallbackErrors_expectItsErrorToBeIgnoredAndThePollingThreadToContinue() throws InterruptedException {
        String         threadName     = generatePollingThreadName();
        CountDownLatch runLatch       = new CountDownLatch( 10 );
        long           specifiedDelay = RandomUtils.rnd( 1, 1000 );

        Mockito.when( pollFMock.invoke() ).thenThrow( new RuntimeException("expected exception") );
        Mockito.doThrow(new RuntimeException("expected exception")).when(callbacksMock).pollFailure(Mockito.any());


        AtomicLong lastAttemptCount = new AtomicLong( 0 );
        AtomicLong errorCount       = new AtomicLong( 0 );

        IntToLongFunction calcSleepLengthF = attemptCount -> {
            if ( attemptCount != lastAttemptCount.get() + 1 ) {
                errorCount.incrementAndGet();
            }

            lastAttemptCount.set( attemptCount);
            runLatch.countDown();

            return specifiedDelay;
        };

        try ( Subscription sub = PollingThread.createPollingDaemonThread(
            threadName, pollFMock, callbacksMock, calcSleepLengthF, sleepFMock, specifiedDelay
        ) ) {
            assertNotNull( sub );

            Backdoor.sleep( 100 );

            assertTrue(sub.isActive());
        }

        awaitLatch(runLatch);

        assertEquals( 0, errorCount.get() );
        verify(callbacksMock, never()).objectReceived( any() );
    }

    @Test
    public void givenDispatchFailureCallbackErrors_expectItsErrorToBeIgnoredAndThePollingThreadToContinue() throws InterruptedException {
        String         threadName     = generatePollingThreadName();
        CountDownLatch runLatch       = new CountDownLatch( 10 );
        long           specifiedDelay = RandomUtils.rnd( 1, 1000 );

        Mockito.when( pollFMock.invoke() ).thenReturn( FP.option("value") );
        Mockito.doThrow(new RuntimeException("expected exception")).when(callbacksMock).objectReceived(Mockito.any());
        Mockito.doThrow(new RuntimeException("expected exception")).when(callbacksMock).dispatchFailure(any(), any());


        AtomicLong lastAttemptCount = new AtomicLong( 0 );
        AtomicLong errorCount       = new AtomicLong( 0 );

        IntToLongFunction calcSleepLengthF = attemptCount -> {
            if ( attemptCount != lastAttemptCount.get() + 1 ) {
                errorCount.incrementAndGet();
            }

            lastAttemptCount.set( attemptCount);
            runLatch.countDown();

            return specifiedDelay;
        };

        try ( Subscription sub = PollingThread.createPollingDaemonThread(
            threadName, pollFMock, callbacksMock, calcSleepLengthF, sleepFMock, specifiedDelay
        ) ) {
            assertNotNull( sub );

            Backdoor.sleep( 100 );

            assertTrue(sub.isActive());
        }

        awaitLatch(runLatch);

        assertEquals( 0, errorCount.get() );
    }


    private static class CapturingListener implements PollingThreadListener<String> {
        private List<String> events = new Vector<>();

        public CountDownLatch startedLatch         = new CountDownLatch( 1 );
        public CountDownLatch objectReceivedLatch  = new CountDownLatch( 10 );
        public CountDownLatch pollFailureLatch     = new CountDownLatch( 10 );
        public CountDownLatch dispatchFailureLatch = new CountDownLatch( 10 );
        public CountDownLatch threadFinishedLatch  = new CountDownLatch( 1 );


        public void threadStarted() {
            events.add( "STARTED" );

            startedLatch.countDown();
        }

        public void objectReceived( String o ) {
            events.add( "RECEIVED " + o );

            objectReceivedLatch.countDown();
        }

        public void pollFailure( Failure f ) {
            events.add( "POLL FAILURE( " + f.toException().getClass().getSimpleName() + " " + f.toException().getMessage() + " )" );

            pollFailureLatch.countDown();
        }

        public void dispatchFailure( String o, Failure f ) {
            events.add( "DISPATCH FAILURE( " + o + ", " + f.toException().getClass().getSimpleName() + " " + f.toException().getMessage() + " )" );

            dispatchFailureLatch.countDown();
        }

        public void threadFinished() {
            events.add( "FINISHED" );

            threadFinishedLatch.countDown();
        }

        public void assertAllValuesReceivedEquals( String value ) {
            int count = 0;

            for ( String actual : events ) {
                if ( actual.startsWith("RECEIVED") ) {
                    assertEquals( "RECEIVED " + value, actual );
                    count += 1;
                }
            }

            assertTrue( count > 0 );
        }

        public void assertAllPollFailuresWereEqualTo( RuntimeException ex ) {
            int count = 0;

            for ( String actual : events ) {
                if ( actual.startsWith("POLL FAILURE") ) {
                    assertEquals( "POLL FAILURE( " + ex.getClass().getSimpleName() + " " + ex.getMessage() + " )", actual );
                    count += 1;
                }
            }

            assertTrue( count > 0 );
        }

        public void assertNoObjectsWereReceived() {
            for ( String actual : events ) {
                if ( actual.startsWith("RECEIVED") ) {
                    fail( "expected no called to receiveObject" );
                }
            }
        }
    }

    private String generatePollingThreadName() {
        return getClass().getSimpleName()+RandomUtils.rnd(Integer.MAX_VALUE);
    }

}
