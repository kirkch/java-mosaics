package mosaics.lang.time;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.junit.JMAssertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


public class DTMRangeTest extends JMAssertions {
    private static final DTM T0 = new DTM(2020,3,20, 10,30);

    @Nested
    public class GenerateInfiniteStreamOfDTMRangesTestCases {
        @Test
        public void testRepeatDTMRangeInc() {
            FPIterable<DTMRange> actual = DTMRange.repeatDTMRangeAsc(T0, DTMInterval.day());

            FPIterable<DTMRange> expected = FP.wrapAll(
                new DTMRange(T0,T0.addDay()),
                new DTMRange(T0.addDay(),T0.addDays(2)),
                new DTMRange(T0.addDays(2),T0.addDays(3)),
                new DTMRange(T0.addDays(3),T0.addDays(4))
            );

            assertEquals( expected, actual.limit(4) );
        }

        @Test
        public void testRepeatDTMRangeIncWithMax() {
            FPIterable<DTMRange> actual = DTMRange.repeatDTMRangeAsc(T0, DTMInterval.day(), T0.addDays(2).addHours(1));

            FPIterable<DTMRange> expected = FPIterable.wrapAll(
                new DTMRange(T0,T0.addDay()),
                new DTMRange(T0.addDay(),T0.addDays(2))
            );

            assertEquals( expected, actual );
        }

        @Test
        public void givenFirstValuePassesMax_callRepeatDTMRangeAsc_expectRangeToNotGetClippedToMax() {
            DTM                  max      = T0.addDays( 1 ).addHours( 1 );
            FPIterable<DTMRange> actual   = DTMRange.repeatDTMRangeAsc(T0, DTMInterval.days(2), max );
            FPIterable<DTMRange> expected = FPIterable.empty();

            assertEquals( expected, actual );
        }

        @Test
        public void givenIncrementPassesMax_callRepeatDTMRangeAsc_expectRangeToNotGetClippedToMax() {
            DTM                  max    = T0.addDays( 3 ).addHours( 1 );
            FPIterable<DTMRange> actual = DTMRange.repeatDTMRangeAsc(T0, DTMInterval.days(2), max );

            FPIterable<DTMRange> expected = FPIterable.wrapAll(
                new DTMRange(T0,T0.addDays(2))
            );

            assertEquals( expected, actual );
        }

        @Test
        public void givenNextRangeExceedsMax_callRepeatDTMRangeAscWithMax_expectEmptyStream() {
            FPIterable<DTMRange> actual = DTMRange.repeatDTMRangeAsc(T0, DTMInterval.day(), T0.addHour());

            assertEquals( FPIterable.empty(), actual );
        }

        @Test
        public void givenMaxIsLessThanFrom_callRepeatDTMRangeAscWithMax_expectEmptyStream() {
            FPIterable<DTMRange> actual = DTMRange.repeatDTMRangeAsc(T0, DTMInterval.day(), T0.subtractHour());

            assertEquals( FPIterable.empty(), actual );
        }

        @Test
        public void testRepeatDTMRangeDesc() {
            FPIterable<DTMRange> actual = DTMRange.repeatDTMRangeDesc(T0, DTMInterval.day());
            FPIterable<DTMRange> expected = FP.wrapAll(
                new DTMRange(T0,T0.addDay()),
                new DTMRange(T0.subtractDay(),T0),
                new DTMRange(T0.subtractDays(2),T0.subtractDay()),
                new DTMRange(T0.subtractDays(3),T0.subtractDays(2)),
                new DTMRange(T0.subtractDays(4),T0.subtractDays(3))
            );

            assertEquals( expected, actual.limit(5) );
        }

        @Test
        public void testRepeatDTMRangeDescWithMin() {
            FPIterable<DTMRange> actual   = DTMRange.repeatDTMRangeDesc(T0, DTMInterval.day(), T0.subtractDays(3));
            FPIterable<DTMRange> expected = FP.wrapAll(
                new DTMRange(T0.subtractDay(),T0),
                new DTMRange(T0.subtractDays(2),T0.subtractDay()),
                new DTMRange(T0.subtractDays(3),T0.subtractDays(2))
            );

            assertEquals( expected, actual );
        }

        @Test
        public void givenNextRangeExceedsMin_callRepeatDTMRangeDescWithMin_expectEmptyStream() {
            FPIterable<DTMRange> actual = DTMRange.repeatDTMRangeDesc(T0, DTMInterval.day(), T0.subtractHour());

            assertEquals( FPIterable.empty(), actual );
        }

        @Test
        public void givenFirstValuePassesMin_callRepeatDTMRangeDesc_expectRangeToNotGetClippedToMin() {
            DTM                  min      = T0.subtractDays( 1 ).subtractHours( 1 );
            FPIterable<DTMRange> actual   = DTMRange.repeatDTMRangeDesc(T0, DTMInterval.days(2), min );
            FPIterable<DTMRange> expected = FP.wrapAll();

            assertEquals( expected, actual );
        }

        @Test
        public void givenFirstValueIsLessThanMin_callRepeatDTMRangeDesc_expectRangeToNotGetClippedToMin() {
            DTM                  min      = T0.subtractDays( 1 ).subtractHours( 1 );
            FPIterable<DTMRange> actual   = DTMRange.repeatDTMRangeDesc(T0.subtractHours(2), DTMInterval.days(2), min );
            FPIterable<DTMRange> expected = FP.wrapAll();

            assertEquals( expected, actual );
        }

        @Test
        public void givenIncrementPassesMin_callRepeatDTMRangeADesc_expectRangeToNotGetClippedToMin() {
            DTM                  min    = T0.subtractDays( 3 ).subtractHours( 1 );
            FPIterable<DTMRange> actual = DTMRange.repeatDTMRangeDesc(T0, DTMInterval.days(2), min );

            FPIterable<DTMRange> expected = FP.wrapAll(
                new DTMRange( T0.subtractDays( 2 ), T0 )
            );

            assertEquals( expected, actual );
        }
    }


    /**
     * Visualising the test points:
     *
     *     |---------------|
     * x   x   x          xx  x
     * F   T   T          TF  F
     */
    @Test
    public void testContains() {
        DTMRange range = new DTMRange( T0, T0.addDays( 2 ) );

        assertFalse( range.contains(T0.subtractDays(2)) );
        assertFalse( range.contains(T0.subtractDays(1)) );
        assertFalse( range.contains(T0.subtractMillis()) );

        assertTrue( range.contains(T0) );
        assertTrue( range.contains(T0.addMillis()) );
        assertTrue( range.contains(range.getRhsExc().subtractMillis()) );

        assertFalse( range.contains(range.getRhsExc()) );
        assertFalse( range.contains(range.getRhsExc().addMillis()) );
    }

    /**
     *           |---------------|
     *  F   |---|
     *  T     |--------|
     *  T     |-------------------------|
     *  T                   |-----------|
     *  F                               |------|
     */
    @Test
    public void testOverlaps() {
        DTMRange range = new DTMRange( T0, T0.addDays(2) );

        assertFalse( range.overlaps(new DTMRange(T0.subtractDays(4), T0.subtractDays(2))) );
        assertFalse( range.overlaps(new DTMRange(T0.subtractDays(4), T0)) );
        assertTrue( range.overlaps(new DTMRange(T0, T0.addMillis())) );
        assertTrue( range.overlaps(new DTMRange(T0.addDay(), T0.addDay().addMillis())) );
        assertTrue( range.overlaps(new DTMRange(T0.addDay(), range.getRhsExc())) );
        assertTrue( range.overlaps(new DTMRange(T0.addDay(), range.getRhsExc().addMillis())) );
        assertTrue( range.overlaps(new DTMRange(range.getRhsExc().subtractMillis(), range.getRhsExc().addMillis())) );
        assertFalse( range.overlaps(new DTMRange(range.getRhsExc(), range.getRhsExc().addMillis())) );
    }

    @Test
    public void testToInterval() {
        assertEquals(
            new DTMInterval(0,0,0,1,0,0,0,0),
            new DTMRange(new DTM(2020,1,1), new DTM(2020,1,2)).toInterval()
        );
        assertEquals(
            new DTMInterval(0,0,0,1,10,30,11, 101),
            new DTMRange(new DTM(2020,1,1), new DTM(2020,1,2, 10,30,11, 101)).toInterval()
        );
        assertEquals(
            new DTMInterval(0,0,0,1,9,29,10, 100),
            new DTMRange(new DTM(2020,1,1,1,1,1,1), new DTM(2020,1,2, 10,30,11, 101)).toInterval()
        );
    }

    @Test
    public void testMerge() {
        DTM T0 = new DTM(2020,1,1);
        DTM T1 = new DTM(2020,1,2);
        DTM T2 = new DTM(2020,1,3);
        DTM T3 = new DTM(2020,1,4);
        DTM T4 = new DTM(2020,1,5);

        DTMRange t0t4 = new DTMRange( T0, T4 );
        DTMRange t0t1 = new DTMRange( T0, T1 );
        DTMRange t0t2 = new DTMRange( T0, T2 );
        DTMRange t0t3 = new DTMRange( T0, T3 );
        DTMRange t1t2 = new DTMRange( T1, T2 );
        DTMRange t1t3 = new DTMRange( T1, T3 );
        DTMRange t3t4 = new DTMRange( T3, T4 );


        assertEquals( t0t4, t0t1.merge(t3t4) );
        assertEquals( t0t4, t3t4.merge(t0t1) );

        assertEquals( t0t3, t0t2.merge(t1t3) );
        assertEquals( t0t2, t1t2.merge(t0t1) );

        assertSame( t0t2, t0t2.merge(t0t1) );
        assertSame( t0t2, t0t1.merge(t0t2) );
        assertSame( t0t3, t0t3.merge(t1t2) );
        assertSame( t0t3, t1t2.merge(t0t3) );
    }

    // testCompareTo
}
