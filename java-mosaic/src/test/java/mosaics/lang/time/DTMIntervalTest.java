package mosaics.lang.time;

import mosaics.fp.FP;
import mosaics.junit.JMAssertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


public class DTMIntervalTest extends JMAssertions {
    private static final DTM T0 = new DTM(2020,3,20, 11, 32);

    @Test
    public void testMultiplyBy() {
        DTMInterval ONE_DAY  = DTMInterval.builder().numDays(1).build();
        DTMInterval TWO_DAYS = DTMInterval.builder().numDays(2).build();

        assertSame(   ONE_DAY,  ONE_DAY.multiplyBy(1) );
        assertEquals( TWO_DAYS, ONE_DAY.multiplyBy(2) );
        assertEquals( new DTMInterval(61,0,12,5,1,1,1,0), new DTMInterval(30,6,4,16,12,30,30,500).multiplyBy(2) );
    }

    @Test
    public void testToMillis() {
        assertEquals(1L, DTMInterval.millisecond().toMillis());
        assertEquals(11L, DTMInterval.milliseconds(11).toMillis());
        assertEquals(2000L, DTMInterval.seconds(2).toMillis());
        assertEquals(2000L*60, DTMInterval.minutes(2).toMillis());
        assertEquals(2000L*60*60, DTMInterval.hours(2).toMillis());
        assertEquals(2000L*60*60*24, DTMInterval.days(2).toMillis());
        assertEquals(2000L*60*60*24*7, DTMInterval.weeks(2).toMillis());
        assertEquals(2000L*60*60*24*30, DTMInterval.months(2).toMillis());
        assertEquals(2000L*60*60*24*365, DTMInterval.years(2).toMillis());

        assertEquals(2000L*60*60 + 3000L*60*60*24*365, DTMInterval.builder().numHours(2).numYears(3).build().toMillis());
    }

    @Test
    public void testRemainder() {
        assertEquals( 0L, DTMInterval.seconds(2).remainder(DTMInterval.seconds(2)).toMillis() );
        assertEquals( 0L, DTMInterval.seconds(4).remainder(DTMInterval.seconds(2)).toMillis() );
        assertEquals( 0L, DTMInterval.seconds(8).remainder(DTMInterval.seconds(2)).toMillis() );
        assertEquals( 2000L, DTMInterval.seconds(2).remainder(DTMInterval.seconds(3)).toMillis() );
        assertEquals( 1000L, DTMInterval.seconds(1).remainder(DTMInterval.seconds(3)).toMillis() );
        assertEquals( 1000L, DTMInterval.seconds(4).remainder(DTMInterval.seconds(3)).toMillis() );
        assertEquals( 2000L, DTMInterval.seconds(5).remainder(DTMInterval.seconds(3)).toMillis() );
    }

    @Test
    public void isZero() {
        assertTrue( DTMInterval.seconds(0).isZero() );
        assertFalse( DTMInterval.seconds(1).isZero() );
    }


    @Test
    public void repeatDTMRangeAsc() {
        assertEquals(
            FP.wrapAll(
                new DTMRange(new DTM(2020,1,1), new DTM(2020,1,2)),
                new DTMRange(new DTM(2020,1,2), new DTM(2020,1,3))
            ),
            DTMRange.repeatDTMRangeAsc(new DTM(2020,1,1), DTMInterval.ONE_DAY, new DTM(2020,1,3))
        );
    }

    @Test
    public void repeatDTMRangeDesc() {
        assertEquals(
            FP.wrapAll(
                new DTMRange(new DTM(2019,12,31), new DTM(2020,1,1)),
                new DTMRange(new DTM(2019,12,30), new DTM(2019,12,31)),
                new DTMRange(new DTM(2019,12,29), new DTM(2019,12,30))
            ),
            DTMRange.repeatDTMRangeDesc(new DTM(2020,1,1), DTMInterval.ONE_DAY, new DTM(2019,12,29))
        );
    }

    @Nested
    public class YearTestCases {
        @Test
        public void addDayTests() {
            assertSame(T0, T0.addInterval(DTMInterval.years(0)) );
            assertEquals(T0.addYear(), T0.addInterval(DTMInterval.year()) );
            assertEquals(T0.addYears(2), T0.addInterval(DTMInterval.years(2)) );
            assertEquals(T0.addYears(3), T0.addInterval(DTMInterval.years(3)) );
            assertEquals(T0.addYears(-1), T0.addInterval(DTMInterval.years(-1)) );
        }
        
        @Test
        public void subtractYearTests() {
            assertSame(T0, T0.subtractInterval(DTMInterval.years(0)) );
            assertEquals(T0.subtractYear(), T0.subtractInterval(DTMInterval.year()) );
            assertEquals(T0.subtractYears(2), T0.subtractInterval(DTMInterval.years(2)) );
            assertEquals(T0.subtractYears(3), T0.subtractInterval(DTMInterval.years(3)) );
            assertEquals(T0.subtractYears(-1), T0.subtractInterval(DTMInterval.years(-1)) );
        }

        @Test
        public void toStringTests() {
            assertEquals("0", DTMInterval.years(0).toString() );
            assertEquals( "1Y", DTMInterval.year().toString() );
            assertEquals( "2Y", DTMInterval.years(2).toString() );
            assertEquals( "3Y", DTMInterval.years(3).toString() );
            assertEquals( "3Y1M", new DTMInterval(3, 1, 0, 0, 0, 0, 0).toString() );
            assertEquals( "3Y10M", new DTMInterval(3, 10, 0, 0, 0, 0, 0).toString() );
            assertEquals( "4Y2M", new DTMInterval(3, 14, 0, 0, 0, 0, 0).toString() );
            assertEquals( "3Y-10M", new DTMInterval(3, -10, 0, 0, 0, 0, 0).toString() );
            assertEquals( "-1Y", DTMInterval.years(-1).toString() );
        }

        @Test
        public void testCompareTo() {
            assertEquals( 0, DTMInterval.year().compareTo(DTMInterval.year()) );
            assertEquals( 0, DTMInterval.year().compareTo(DTMInterval.year()) );
        }
    }

    @Nested
    public class MonthTestCases {
        @Test
        public void showMonthsWrap() {
            assertEquals(
                new DTMInterval(0,1,1,1,1,1,1),
                new DTMInterval(0,1,1,1,1,1,1)
            );
            assertEquals(
                new DTMInterval(1,1,1,1,1,1,1),
                new DTMInterval(0,12+1,1,1,1,1,1)
            );
            assertEquals(
                new DTMInterval(101,1,1,1,1,1,1),
                new DTMInterval(0,100*12+12+1,1,1,1,1,1)
            );
        }

        @Test
        public void addDayTests() {
            assertSame(T0, T0.addInterval(DTMInterval.months(0)) );
            assertEquals(T0.addMonth(), T0.addInterval(DTMInterval.month()) );
            assertEquals(T0.addMonths(2), T0.addInterval(DTMInterval.months(2)) );
            assertEquals(T0.addMonths(3), T0.addInterval(DTMInterval.months(3)) );
            assertEquals(T0.addMonths(-1), T0.addInterval(DTMInterval.months(-1)) );
        }
        
        @Test
        public void subtractMonthTests() {
            assertSame(T0, T0.subtractInterval(DTMInterval.months(0)) );
            assertEquals(T0.subtractMonth(), T0.subtractInterval(DTMInterval.month()) );
            assertEquals(T0.subtractMonths(2), T0.subtractInterval(DTMInterval.months(2)) );
            assertEquals(T0.subtractMonths(3), T0.subtractInterval(DTMInterval.months(3)) );
            assertEquals(T0.subtractMonths(-1), T0.subtractInterval(DTMInterval.months(-1)) );
        }

        @Test
        public void toStringTests() {
            assertEquals("0", DTMInterval.months(0).toString() );
            assertEquals( "1M", DTMInterval.month().toString() );
            assertEquals( "2M", DTMInterval.months(2).toString() );
            assertEquals( "3M", DTMInterval.months(3).toString() );
            assertEquals( "3M1w", new DTMInterval(0, 3, 1, 0, 0, 0, 0).toString() );
            assertEquals( "3M10w", new DTMInterval(0, 3, 10, 0, 0, 0, 0).toString() );
            assertEquals( "3M14w", new DTMInterval(0, 3, 14, 0, 0, 0, 0).toString() );
            assertEquals( "3M-10w", new DTMInterval(0, 3, -10, 0, 0, 0, 0).toString() );
            assertEquals( "-1M", DTMInterval.months(-1).toString() );
        }
    }

    @Nested
    public class DayTestCases {
        @Test
        public void showDaysWrap() {
            assertEquals(
                new DTMInterval(0,0,0,1,1,1,1),
                new DTMInterval(0,0,0,1,1,1,1)
            );
            assertEquals(
                new DTMInterval(0,0,1,1,1,1,1),
                new DTMInterval(0,0,0,8,1,1,1)
            );
            assertEquals(
                new DTMInterval(0,0,10,1,1,1,1),
                new DTMInterval(0,0,0,7*10+1,1,1,1)
            );
        }

        @Test
        public void addDayTests() {
            assertSame(T0, T0.addInterval(DTMInterval.days(0)) );
            assertEquals(T0.addDay(), T0.addInterval(DTMInterval.day()) );
            assertEquals(T0.addDays(2), T0.addInterval(DTMInterval.days(2)) );
            assertEquals(T0.addDays(3), T0.addInterval(DTMInterval.days(3)) );
            assertEquals(T0.addDays(-1), T0.addInterval(DTMInterval.days(-1)) );
        }

        @Test
        public void subtractDayTests() {
            assertSame(T0, T0.subtractInterval(DTMInterval.days(0)) );
            assertEquals(T0.subtractDay(), T0.subtractInterval(DTMInterval.day()) );
            assertEquals(T0.subtractDays(2), T0.subtractInterval(DTMInterval.days(2)) );
            assertEquals(T0.subtractDays(3), T0.subtractInterval(DTMInterval.days(3)) );
            assertEquals(T0.subtractDays(-1), T0.subtractInterval(DTMInterval.days(-1)) );
        }

        @Test
        public void toStringTests() {
            assertEquals("0", DTMInterval.days(0).toString() );
            assertEquals( "1d", DTMInterval.day().toString() );
            assertEquals( "2d", DTMInterval.days(2).toString() );
            assertEquals( "3d", DTMInterval.days(3).toString() );
            assertEquals( "-1d", DTMInterval.days(-1).toString() );
        }
    }


    @Nested
    public class HourTestCases {
        @Test
        public void showHoursWrap() {
            assertEquals(
                new DTMInterval(0,0,0,0,1,1,1),
                new DTMInterval(0,0,0,0,1,1,1)
            );
            assertEquals(
                new DTMInterval(0,0,0,1,2,1,1),
                new DTMInterval(0,0,0,0,24+2,1,1)
            );
            assertEquals(
                new DTMInterval(0,0,1,1,2,1,1),
                new DTMInterval(0,0,0,0,24*7+24+2,1,1)
            );
            assertEquals(
                new DTMInterval(0,0,10,1,2,1,1),
                new DTMInterval(0,0,0,0,24*7*10+24+2,1,1)
            );
        }

        @Test
        public void addHourTests() {
            assertSame(T0, T0.addInterval(DTMInterval.hours(0)) );
            assertEquals(T0.addHour(), T0.addInterval(DTMInterval.hour()) );
            assertEquals(T0.addHours(2), T0.addInterval(DTMInterval.hours(2)) );
            assertEquals(T0.addHours(3), T0.addInterval(DTMInterval.hours(3)) );
            assertEquals(T0.addHours(-1), T0.addInterval(DTMInterval.hours(-1)) );
        }

        @Test
        public void subtractHourTests() {
            assertSame(T0, T0.subtractInterval(DTMInterval.hours(0)) );
            assertEquals(T0.subtractHour(), T0.subtractInterval(DTMInterval.hour()) );
            assertEquals(T0.subtractHours(2), T0.subtractInterval(DTMInterval.hours(2)) );
            assertEquals(T0.subtractHours(3), T0.subtractInterval(DTMInterval.hours(3)) );
            assertEquals(T0.subtractHours(-1), T0.subtractInterval(DTMInterval.hours(-1)) );
        }

        @Test
        public void toStringTests() {
            assertEquals("0", DTMInterval.hours(0).toString() );
            assertEquals( "1h", DTMInterval.hour().toString() );
            assertEquals( "2h", DTMInterval.hours(2).toString() );
            assertEquals( "3h", DTMInterval.hours(3).toString() );
            assertEquals( "1d6h", DTMInterval.hours(30).toString() );
            assertEquals( "-1h", DTMInterval.hours(-1).toString() );
        }
    }


    @Nested
    public class MinuteTestCases {
        @Test
        public void showMinutesWrap() {
            assertEquals(
                new DTMInterval(0,0,0,0,1,0,1),
                new DTMInterval(0,0,0,0,0,60,1)
            );
            assertEquals(
                new DTMInterval(0,0,0,0,1,31,1),
                new DTMInterval(0,0,0,0,0,91,1)
            );
            assertEquals(
                new DTMInterval(0,0,0,0,2,0,0),
                new DTMInterval(0,0,0,0,0,60*2,0)
            );
            assertEquals(
                new DTMInterval(0,0,0,1,1,1,2),
                new DTMInterval(0,0,0,0,0,60*24+61,2)
            );
            assertEquals(
                new DTMInterval(0,0,1,1,1,1,2),
                new DTMInterval(0,0,0,0,0,60*24*7+60*24+61,2)
            );
            assertEquals(
                new DTMInterval(0,0,11,1,1,1,2),
                new DTMInterval(0,0,0,0,0,60*24*7*10+60*24*7+60*24+61,2)
            );
        }

        @Test
        public void addMinuteTests() {
            assertSame(T0, T0.addInterval(DTMInterval.minutes(0)) );
            assertEquals(T0.addMinute(), T0.addInterval(DTMInterval.minute()) );
            assertEquals(T0.addMinutes(2), T0.addInterval(DTMInterval.minutes(2)) );
            assertEquals(T0.addMinutes(3), T0.addInterval(DTMInterval.minutes(3)) );
            assertEquals(T0.addMinutes(-1), T0.addInterval(DTMInterval.minutes(-1)) );
        }

        @Test
        public void subtractMinuteTests() {
            assertSame(T0, T0.subtractInterval(DTMInterval.minutes(0)) );
            assertEquals(T0.subtractMinute(), T0.subtractInterval(DTMInterval.minute()) );
            assertEquals(T0.subtractMinutes(2), T0.subtractInterval(DTMInterval.minutes(2)) );
            assertEquals(T0.subtractMinutes(3), T0.subtractInterval(DTMInterval.minutes(3)) );
            assertEquals(T0.subtractMinutes(-1), T0.subtractInterval(DTMInterval.minutes(-1)) );
        }

        @Test
        public void toStringTests() {
            assertEquals("0", DTMInterval.minutes(0).toString() );
            assertEquals( "1m", DTMInterval.minute().toString() );
            assertEquals( "2m", DTMInterval.minutes(2).toString() );
            assertEquals( "3m", DTMInterval.minutes(3).toString() );
            assertEquals( "30m", DTMInterval.minutes(30).toString() );
            assertEquals( "1h30m", DTMInterval.minutes(90).toString() );
            assertEquals( "-1m", DTMInterval.minutes(-1).toString() );
        }
    }



    @Nested
    public class SecondTestCases {
        @Test
        public void showSecondsWrap() {
            assertEquals(
                new DTMInterval(0,0,0,0,0,1,0),
                new DTMInterval(0,0,0,0,0,0,60)
            );
            assertEquals(
                new DTMInterval(0,0,0,0,0,1,31),
                new DTMInterval(0,0,0,0,0,0,91)
            );
            assertEquals(
                new DTMInterval(0,0,0,0,0,2,0),
                new DTMInterval(0,0,0,0,0,0,60*2)
            );
            assertEquals(
                new DTMInterval(0,0,0,0,1,2,3),
                new DTMInterval(0,0,0,0,0,0,60*60+60*2+3)
            );
            assertEquals(
                new DTMInterval(0,0,1,1,1,2,3),
                new DTMInterval(0,0,0,0,0,0,60*60*24*7+60*60*24+60*60+60*2+3)
            );
            assertEquals(
                new DTMInterval(0,0,11,1,1,2,3),
                new DTMInterval(0,0,0,0,0,0,60*60*24*7*10+60*60*24*7+60*60*24+60*60+60*2+3)
            );
        }

        @Test
        public void addSecondTests() {
            assertSame(T0, T0.addInterval(DTMInterval.seconds(0)) );
            assertEquals(T0.addSecond(), T0.addInterval(DTMInterval.second()) );
            assertEquals(T0.addSeconds(2), T0.addInterval(DTMInterval.seconds(2)) );
            assertEquals(T0.addSeconds(3), T0.addInterval(DTMInterval.seconds(3)) );
            assertEquals(T0.addSeconds(-1), T0.addInterval(DTMInterval.seconds(-1)) );
        }

        @Test
        public void subtractSecondTests() {
            assertSame(T0, T0.subtractInterval(DTMInterval.seconds(0)) );
            assertEquals(T0.subtractSecond(), T0.subtractInterval(DTMInterval.second()) );
            assertEquals(T0.subtractSeconds(2), T0.subtractInterval(DTMInterval.seconds(2)) );
            assertEquals(T0.subtractSeconds(3), T0.subtractInterval(DTMInterval.seconds(3)) );
            assertEquals(T0.subtractSeconds(-1), T0.subtractInterval(DTMInterval.seconds(-1)) );
        }

        @Test
        public void toStringTests() {
            assertEquals("0", DTMInterval.seconds(0).toString() );
            assertEquals( "1s", DTMInterval.second().toString() );
            assertEquals( "2s", DTMInterval.seconds(2).toString() );
            assertEquals( "3s", DTMInterval.seconds(3).toString() );
            assertEquals( "30s", DTMInterval.seconds(30).toString() );
            assertEquals( "1m30s", DTMInterval.seconds(90).toString() );
            assertEquals( "-1s", DTMInterval.seconds(-1).toString() );
        }
    }

    @Nested
    public class CompositeTestCases {
        @Test
        public void addDays() {
            assertEquals( DTMInterval.ZERO, DTMInterval.days(0).addInterval(DTMInterval.days(0)) );
            assertEquals( DTMInterval.days(1), DTMInterval.days(1).addInterval(DTMInterval.days(0)) );

            assertEquals( T0.addDay(), T0.addInterval(DTMInterval.days(0).addInterval(DTMInterval.days(1))) );
            assertEquals( T0.addDays(2), T0.addInterval(DTMInterval.days(1).addInterval(DTMInterval.days(1))) );
            assertEquals( T0.addDays(3), T0.addInterval(DTMInterval.days(1).addInterval(DTMInterval.days(2))) );
            assertEquals( T0.addDays(3), T0.addInterval(DTMInterval.days(2).addInterval(DTMInterval.days(1))) );

            assertEquals( "0", DTMInterval.days(0).addInterval(DTMInterval.days(0)).toString() );
            assertEquals( "2d", DTMInterval.days(1).addInterval(DTMInterval.days(1)).toString() );
        }
    }
}
