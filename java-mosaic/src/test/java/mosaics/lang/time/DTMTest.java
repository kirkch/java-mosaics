package mosaics.lang.time;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class DTMTest {
    @Test
    public void testSetMillis() {
        DTM dtmWithMillis = new DTM(1020);

        assertEquals( 20L, dtmWithMillis.getMillis() );
        assertEquals( 1000L, dtmWithMillis.setMillis(0).getMillisSinceEpoch() );
        assertEquals( 0L, dtmWithMillis.setMillis(0).getMillis() );
        assertEquals( 1050L, dtmWithMillis.setMillis(50).getMillisSinceEpoch() );
        assertEquals( 50L, dtmWithMillis.setMillis(50).getMillis() );
    }

    @Test
    public void setDayOfWeek() {
        DTM monday    = new DTM(2020,12,28, 13,0,0);
        DTM tuesday   = new DTM(2020,12,29, 13,0,0);
        DTM wednesday = new DTM(2020,12,30, 13,0,0);
        DTM thursday  = new DTM(2020,12,31, 13,0,0);
        DTM friday    = new DTM(2021, 1, 1, 13,0,0);
        DTM saturday  = new DTM(2021, 1, 2, 13,0,0);
        DTM sunday    = new DTM(2021, 1, 3, 13,0,0);

        assertSame( monday, monday.setDayOfWeek(DayOfWeek.MONDAY) );
        assertSame( tuesday, tuesday.setDayOfWeek(DayOfWeek.TUESDAY) );

        assertEquals( tuesday, monday.setDayOfWeek(DayOfWeek.TUESDAY) );
        assertEquals( wednesday, monday.setDayOfWeek(DayOfWeek.WEDNESDAY) );
        assertEquals( thursday, monday.setDayOfWeek(DayOfWeek.THURSDAY) );

        assertEquals( monday, tuesday.setDayOfWeek(DayOfWeek.MONDAY) );
        assertEquals( tuesday, friday.setDayOfWeek(DayOfWeek.TUESDAY) );
        assertEquals( sunday, friday.setDayOfWeek(DayOfWeek.SUNDAY) );
        assertEquals( monday, sunday.setDayOfWeek(DayOfWeek.MONDAY) );
    }

    @Test
    public void testToInterval() {
        DTM t0 = new DTM(2020,12,28, 13,0,0);

        assertEquals( DTMInterval.milliseconds(0), t0.toInterval(t0) );
        assertEquals( DTMInterval.milliseconds(100), t0.toInterval(t0.addMillis(100)) );
        assertEquals( DTMInterval.seconds(2), t0.addSeconds(2).toInterval(t0) );
    }

    @Test
    public void testSubtractDTM() {
        DTM t0 = new DTM(2020,12,28, 13,0,0);
        DTM t1 = t0.addHour();

        assertEquals( DTMInterval.hour(), t1.subtractDTM(t0) );
        assertEquals( DTMInterval.hours(-1), t0.subtractDTM(t1) );

        assertNotEquals( DTMInterval.hour(), t1.subtractDTM(t0.addMillis()) );
    }
}
