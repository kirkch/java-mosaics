package mosaics.lang;

import mosaics.lang.functions.Function0;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ArrayUtilsTests {


// fill()

    @Test
    public void fill_createZeroLengthStringArray() {
        String[] a = ArrayUtils.fill( String.class, 0, new MyStringFactory() );

        assertEquals( 0, a.length );
    }

    @Test
    public void fill_createSingleElementLengthStringArray() {
        String[] a = ArrayUtils.fill( String.class, 1, new MyStringFactory() );

        assertEquals( 1, a.length );
        assertEquals( "s0", a[0] );
    }

    @Test
    public void fill_createDoubleElementLengthStringArray() {
        String[] a = ArrayUtils.fill( String.class, 2, new MyStringFactory() );

        assertEquals( 2, a.length );
        assertEquals( "s0", a[0] );
        assertEquals( "s1", a[1] );
    }

// ifEvery

    @Test
    public void ifEvery() {
        assertTrue( ArrayUtils.ifEvery( null, v -> false ) );
        assertTrue( ArrayUtils.ifEvery( new String[] {}, v -> false ) );
        assertFalse( ArrayUtils.ifEvery( new String[] {"abc"}, v -> v.length() == 2 ) );
        assertTrue( ArrayUtils.ifEvery( new String[] {"ab"}, v -> v.length() == 2 ) );
        assertTrue( ArrayUtils.ifEvery( new String[] {"ab", "cd", "ef"}, v -> v.length() == 2 ) );
        assertFalse( ArrayUtils.ifEvery( new String[] {"abc", "cd", "ef"}, v -> v.length() == 2 ) );
        assertFalse( ArrayUtils.ifEvery( new String[] {"ab", "cde", "ef"}, v -> v.length() == 2 ) );
        assertFalse( ArrayUtils.ifEvery( new String[] {"ab", "cd", "eff"}, v -> v.length() == 2 ) );
    }



    private static class MyStringFactory implements Function0<String> {
        private int i = 0;

        public String invoke() {
            return "s"+(i++);
        }
    }

}
