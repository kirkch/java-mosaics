package mosaics.lang.functions;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unchecked")
public class BooleanFunction1Test {

    @Nested
    public class InvokeTestCases {
        @Test
        public void givenFunctionReturnsTrue_expectValue() {
            BooleanFunction1 f = v -> true;

            assertTrue( f.invoke(1) );
        }
        @Test
        public void givenFunctionReturnsFalse_expectValue() {
            BooleanFunction1 f = v -> false;

            assertFalse( f.invoke(1) );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() {
            BooleanFunction1 f = v -> {throw new RuntimeException("expected");};

            try {
                f.invoke(1);
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }


    @Nested
    public class InvertTestCases {
        @Test
        public void expectInvertedMethodsToPassTheInvokeArgsToTheOriginalFunction() {
            BooleanFunction1 f         = v -> v.equals("hello");
            BooleanFunction1 invertedF = f.invert();

            assertFalse( invertedF.invoke("hello") );
            assertTrue( invertedF.invoke("hell") );
            assertTrue( invertedF.invoke("Hello") );
            assertFalse( invertedF.invoke("hello") );
        }

        @Test
        public void givenUnderlyingFunctionReturnsFalse_expectInvertedFunctionToReturnTrue() {
            BooleanFunction1 f         = v -> false;
            BooleanFunction1 invertedF = f.invert();

            assertTrue( invertedF.invoke("v") );
        }

        @Test
        public void givenUnderlyingFunctionReturnsTrue_expectInvertedFunctionToReturnFalse() {
            BooleanFunction1 f         = v -> true;
            BooleanFunction1 invertedF = f.invert();

            assertFalse( invertedF.invoke("v") );
        }

        @Test
        public void invertAnInvertedFunction_expectTheOriginalFunctionBack() {
            BooleanFunction1 f                 = v -> false;
            BooleanFunction1 invertedF         = f.invert();
            BooleanFunction1 invertedInvertedF = invertedF.invert();

            assertSame( invertedInvertedF, f );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotEqualTheOriginalFunction() {
            BooleanFunction1 f         = v -> false;
            BooleanFunction1 invertedF = f.invert();

            assertNotEquals( invertedF, f );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotHaveTheSameHashCodeAsTheOriginalFunction() {
            BooleanFunction1 f         = v -> false;
            BooleanFunction1 invertedF = f.invert();

            assertNotEquals( f.hashCode(), invertedF.hashCode() );
        }
    }


    @Nested
    public class WithDescriptionTestCases {
        @Test
        public void expectNoDefaultDescription() {
            BooleanFunction1 f = v -> true;

            assertEquals( FP.emptyOption(), f.getDescription() );
        }

        @Test
        public void givenNullDescription_expectNoDescription() {
            BooleanFunction1 f                = v -> true;
            BooleanFunction1 fWithDescription = f.withDescription( null );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenBlankDescription_expectNoDescription() {
            BooleanFunction1 f                = v -> true;
            BooleanFunction1 fWithDescription = f.withDescription( "    " );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenDescription_callGetDescription_expectDescription() {
            BooleanFunction1 f                = v -> true;
            BooleanFunction1 fWithDescription = f.withDescription( "hello world" );

            assertEquals( FP.option("hello world"), fWithDescription.getDescription() );
        }

        @Test
        public void givenSameDescription_callGetDescription_expectNoChange() {
            BooleanFunction1 f                = v -> true;
            BooleanFunction1 fWithDescription = f.withDescription( "hello world" );

            assertSame( fWithDescription, fWithDescription.withDescription("hello world") );
        }

        @Test
        public void givenNewDescription_callGetDescription_expectChange() {
            BooleanFunction1 f                 = v -> true;
            BooleanFunction1 fWithDescription  = f.withDescription( "hello world" );
            BooleanFunction1 fWithDescription2 = fWithDescription.withDescription( "abc" );

            assertEquals( FP.option("abc"), fWithDescription2.getDescription() );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithNull_expectOriginalFunctionBack() {
            BooleanFunction1 f                = v -> true;
            BooleanFunction1 fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription(null) );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithBlankString_expectOriginalFunctionBack() {
            BooleanFunction1 f                = v -> true;
            BooleanFunction1 fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription("  ") );
        }

        @Test
        public void testToString() {
            BooleanFunction1 f                = v -> true;
            BooleanFunction1 fWithDescription = f.withDescription( "hello world" );

            assertEquals( "BooleanFunction1WithDescription(description=Some(hello world), f="+f+")", fWithDescription.toString() );
        }

        @Test
        public void givenFReturnsTrue_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunction1 f = v -> true;

            assertEquals( f.invoke("v"), f.withDescription( "hello world" ).invoke("v") );
        }

        @Test
        public void givenFReturnsFalse_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunction1 f = v -> false;

            assertEquals( f.invoke("v"), f.withDescription( "hello world" ).invoke("v") );
        }
    }
}
