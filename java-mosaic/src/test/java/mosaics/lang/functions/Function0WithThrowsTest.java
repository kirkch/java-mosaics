package mosaics.lang.functions;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class Function0WithThrowsTest {

    @Nested
    public class InvokeTestCases {
        @Test
        public void givenFunctionReturnsValue_expectValue() throws Throwable {
            Function0WithThrows<String> f = () -> "hello";

            assertEquals( "hello", f.invoke() );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() throws Throwable {
            Function0WithThrows f = () -> {throw new RuntimeException("expected");};

            try {
                f.invoke();
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }

    @Nested
    public class WithDescriptionTestCases {
        @Test
        public void expectNoDefaultDescription() {
            Function0WithThrows f = () -> null;

            assertEquals( FP.emptyOption(), f.getDescription() );
        }

        @Test
        public void givenNullDescription_expectNoDescription() {
            Function0WithThrows f                = () -> null;
            Function0WithThrows fWithDescription = f.withDescription( null );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenBlankDescription_expectNoDescription() {
            Function0WithThrows f                = () -> null;
            Function0WithThrows fWithDescription = f.withDescription( "    " );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenDescription_callGetDescription_expectDescription() {
            Function0WithThrows f                = () -> null;
            Function0WithThrows fWithDescription = f.withDescription( "hello world" );

            assertEquals( FP.option("hello world"), fWithDescription.getDescription() );
        }

        @Test
        public void givenSameDescription_callGetDescription_expectNoChange() {
            Function0WithThrows f                = () -> null;
            Function0WithThrows fWithDescription = f.withDescription( "hello world" );

            assertSame( fWithDescription, fWithDescription.withDescription("hello world") );
        }

        @Test
        public void givenNewDescription_callGetDescription_expectChange() {
            Function0WithThrows f                 = () -> null;
            Function0WithThrows fWithDescription  = f.withDescription( "hello world" );
            Function0WithThrows fWithDescription2 = fWithDescription.withDescription( "abc" );

            assertEquals( FP.option("abc"), fWithDescription2.getDescription() );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithNull_expectOriginalFunctionBack() {
            Function0WithThrows f                = () -> null;
            Function0WithThrows fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription(null) );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithBlankString_expectOriginalFunctionBack() {
            Function0WithThrows f                = () -> null;
            Function0WithThrows fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription("  ") );
        }

        @Test
        public void testToString() {
            Function0WithThrows f                = () -> null;
            Function0WithThrows fWithDescription = f.withDescription( "hello world" );

            assertEquals( "Function0WithThrowsWithDescription(description=Some(hello world), f="+f+")", fWithDescription.toString() );
        }

        @Test
        public void givenFReturnsNull_showThatDescriptionDoesNotAffectResultOfCallingInvoke() throws Throwable {
            Function0WithThrows f = () -> null;

            assertEquals( f.invoke(), f.withDescription( "hello world" ).invoke() );
        }

        @Test
        public void givenFReturnsNonNull_showThatDescriptionDoesNotAffectResultOfCallingInvoke() throws Throwable {
            Function0WithThrows f = () -> "foo";

            assertEquals( f.invoke(), f.withDescription( "hello world" ).invoke() );
        }
    }


    @Nested
    public class InvokeSilentlyTestCases {
        @Test
        public void givenFunctionThatThrowsAnException_invokeSilently_expectException() {
            Function0WithThrows f = () -> {throw new RuntimeException("expected");};

            try {
                f.invokeSilently();
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }
}
