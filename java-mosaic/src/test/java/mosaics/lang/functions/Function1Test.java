package mosaics.lang.functions;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings( "unchecked" )
public class Function1Test {

    @Nested
    public class InvokeTestCases {
        @Test
        public void givenFunctionReturnsTrue_expectValue() {
            BooleanFunction1 f = v -> true;

            assertTrue( f.invoke("v") );
        }
        @Test
        public void givenFunctionReturnsFalse_expectValue() {
            BooleanFunction1 f = v -> false;

            assertFalse( f.invoke("v") );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() {
            BooleanFunction1 f = v -> {throw new RuntimeException("expected");};

            try {
                f.invoke("v");
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }


    @Nested
    public class ToFunctionTestCases {
        @Test
        public void givenFunctionReturnsStringA_expectValue() {
            Function1<String,String> f = v -> "a";

            assertEquals( "a", f.toFunction().apply("v") );
        }

        @Test
        public void givenFunctionReturnsStringB_expectValue() {
            Function1<String,String> f = v -> "b";

            assertEquals( "b", f.toFunction().apply("v") );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() {
            Function1 f = v -> {throw new RuntimeException("expected");};

            try {
                f.invoke("v");
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }


    @Nested
    public class WithDescriptionTestCases {
        @Test
        public void expectNoDefaultDescription() {
            Function1 f = v -> null;

            assertEquals( FP.emptyOption(), f.getDescription() );
        }

        @Test
        public void givenNullDescription_expectNoDescription() {
            Function1 f                = v -> null;
            Function1 fWithDescription = f.withDescription( null );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenBlankDescription_expectNoDescription() {
            Function1 f                = v -> null;
            Function1 fWithDescription = f.withDescription( "    " );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenDescription_callGetDescription_expectDescription() {
            Function1 f                = v -> null;
            Function1 fWithDescription = f.withDescription( "hello world" );

            assertEquals( FP.option("hello world"), fWithDescription.getDescription() );
        }

        @Test
        public void givenSameDescription_callGetDescription_expectNoChange() {
            Function1 f                = v -> null;
            Function1 fWithDescription = f.withDescription( "hello world" );

            assertSame( fWithDescription, fWithDescription.withDescription("hello world") );
        }

        @Test
        public void givenNewDescription_callGetDescription_expectChange() {
            Function1 f                 = v -> null;
            Function1 fWithDescription  = f.withDescription( "hello world" );
            Function1 fWithDescription2 = fWithDescription.withDescription( "abc" );

            assertEquals( FP.option("abc"), fWithDescription2.getDescription() );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithNull_expectOriginalFunctionBack() {
            Function1 f                = v -> null;
            Function1 fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription(null) );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithBlankString_expectOriginalFunctionBack() {
            Function1 f                = v -> null;
            Function1 fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription("  ") );
        }

        @Test
        public void testToString() {
            Function1 f                = v -> null;
            Function1 fWithDescription = f.withDescription( "hello world" );

            assertEquals( "Function1WithDescription(description=Some(hello world), f="+f+")", fWithDescription.toString() );
        }

        @Test
        public void givenFReturnsNull_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            Function1 f = v -> null;

            assertEquals( f.invoke("v"), f.withDescription( "hello world" ).invoke("v") );
        }

        @Test
        public void givenFReturnsNonNull_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            Function1 f = v -> "foo";

            assertEquals( f.invoke("v"), f.withDescription( "hello world" ).invoke("v") );
        }
    }
}
