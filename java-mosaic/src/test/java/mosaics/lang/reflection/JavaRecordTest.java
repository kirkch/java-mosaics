package mosaics.lang.reflection;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class JavaRecordTest {
    private static record Book( String name, FPOption<String> author ) {
    }

    @Nested
    public class CreateRecordTestCases {
        @Test
        public void givenAllElements_invokeConstructor_expectSuccess() {
            Map<String, Object> args       = Map.of( "name", "LOTR", "author", FP.option( "JRR Tolkien" ) );
            JavaRecord          javaRecord = JavaRecord.of( Book.class );
            Book                actual     = javaRecord.createInstance( args );
            Book                expected   = new Book( "LOTR", FP.option( "JRR Tolkien" ) );

            assertEquals( expected, actual );
        }

        @Test
        public void givenMissingFPOption_invokeConstructor_expectEmptyFPOption() {
            Map<String, Object> args       = Map.of( "name", "LOTR" );
            JavaRecord          javaRecord = JavaRecord.of( Book.class );
            Book                actual     = javaRecord.createInstance( args );
            Book                expected   = new Book( "LOTR", FP.emptyOption() );

            assertEquals( expected, actual );
        }

        @Test
        public void givenMissingString_invokeConstructor_expectNull() {
            Map<String, Object> args       = Map.of( "author", FP.option( "JRR Tolkien" ) );
            JavaRecord          javaRecord = JavaRecord.of( Book.class );
            Book                actual     = javaRecord.createInstance( args );
            Book                expected   = new Book( null, FP.option( "JRR Tolkien" ) );

            assertEquals( expected, actual );
        }
    }
}