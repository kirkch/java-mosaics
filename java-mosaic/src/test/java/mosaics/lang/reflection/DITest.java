package mosaics.lang.reflection;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.lifecycle.StartStoppable;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static mosaics.lang.QA.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DITest {
    private DI di = new DI();


    @Nested
    public class CopyDIContextTestCases {
        @Test
        public void copyContext_showThatChangingTheOriginalDoesNotAffectTheCopy() {
            DI original = di;
            DI copy = new DI( original );

            original.putByType( OneStringArgDTO.class, new OneStringArgDTO( "AA" ) );

            assertEquals( FP.option( new OneStringArgDTO( "AA" ) ), original.fetchByType( OneStringArgDTO.class ) );
            assertEquals( FP.emptyOption(), copy.fetchByType( OneStringArgDTO.class ) );
        }

        @Test
        public void copyContext_showThatChangingTheCopyDoesNotAffectTheOriginal() {
            DI original = di;
            DI copy = new DI( original );

            copy.putByType( OneStringArgDTO.class, new OneStringArgDTO( "AA" ) );

            assertEquals( FP.emptyOption(), original.fetchByType( OneStringArgDTO.class ) );
            assertEquals( FP.option( new OneStringArgDTO( "AA" ) ), copy.fetchByType( OneStringArgDTO.class ) );
        }

        @Test
        public void copyContextWith_showThatChangingTheCopyDoesNotAffectTheOriginal() {
            DI original = di;
            DI copy = new DI( original );

            copy.with( OneStringArgDTO.class, new OneStringArgDTO( "AA" ) );

            assertEquals( FP.emptyOption(), original.fetchByType( OneStringArgDTO.class ) );
            assertEquals( FP.option( new OneStringArgDTO( "AA" ) ), copy.fetchByType( OneStringArgDTO.class ) );
        }
    }

    @Nested
    public class NewInstanceTestCases {
        @Test
        public void givenZeroArgConstructor_newInstance_expectNewInstance() {
            assertEquals( new NoArgDTO(), di.newInstance( NoArgDTO.class ) );
        }

        @Test
        public void givenOneArgConstructorWithValueRegisteredInDI_newInstance_expectNewInstance() {
            NoArgDTO noArg = new NoArgDTO();

            di.with( NoArgDTO.class, noArg );

            OneObjectArgDTO actual = di.newInstance( OneObjectArgDTO.class, NoArgDTO.class );

            assertEquals( new OneObjectArgDTO( noArg ), actual );
            assertSame( noArg, actual.obj );
        }

        @Test
        public void givenOneArgConstructorWithValueRegisteredInDIByName_newInstance_expectNewInstance() {
            di.with( "name", "JJ" );

            assertEquals( new OneStringArgDTO( "JJ" ), di.newInstance( OneStringArgDTO.class, String.class ) );
        }
    }

    @Nested
    public class FetchTestCases {
        @Test
        public void tryFetchingAnOptionalThatHasNoGenericType_expectNone() {
            assertEquals( FP.emptyOption(), di.fetchByType(Optional.class) );
        }

        @Test
        public void tryFetchingAPrimitiveInteger_expectNone() {
            assertEquals( FP.emptyOption(), di.fetch(int.class, "value") );
        }

        @Test
        public void fetchCanFetchByNameOrType_tryNameFirst_expectHit() {
            di.with( "foo", new OneStringArgDTO( "abc" ) );

            assertEquals( FP.option( new OneStringArgDTO( "abc" ) ), di.fetch( OneStringArgDTO.class, "foo" ) );
        }

        @Test
        public void fetchCanFetchByNameOrType_tryNameFirst_expectMiss() {
            assertEquals( FP.emptyOption(), di.fetch( OneStringArgDTO.class, "foo" ) );
        }

        @Test
        public void givenStoredByTypeAndName_fetchCanFetchByNameOrType_expectNameHit() {
            di.with( "foo", new OneStringArgDTO( "byName" ) );
            di.with( OneStringArgDTO.class, new OneStringArgDTO( "byType" ) );

            assertEquals( FP.option( new OneStringArgDTO( "byName" ) ), di.fetch( OneStringArgDTO.class, "foo" ) );
        }

        @Test
        public void givenStoredByTypeOnly_fetchCanFetchByNameOrType_expectTypeHit() {
            di.with( OneStringArgDTO.class, new OneStringArgDTO( "byType" ) );

            assertEquals( FP.option( new OneStringArgDTO( "byType" ) ), di.fetch( OneStringArgDTO.class, "foo" ) );
        }

        @Test
        public void givenNonValue_fetchAndUpdateByType_expectNoChangeToTheDI() {
            boolean wasSuccessful = di.fetchAndUpdateByType( OneStringArgDTO.class, v -> new OneStringArgDTO( v.name + "A" ) );

            assertFalse( wasSuccessful );
            assertEquals( FP.emptyOption(), di.fetchByType( OneStringArgDTO.class ) );
        }

        @Test
        public void givenExistingValue_fetchAndUpdateByType_showThatTheValueGetsUpdated() {
            DI original = di;
            DI copy = new DI( original );

            original.putByType( OneStringArgDTO.class, new OneStringArgDTO( "AA" ) );

            boolean wasSuccessful = di.fetchAndUpdateByType( OneStringArgDTO.class, v -> new OneStringArgDTO( v.name + "A" ) );

            assertTrue( wasSuccessful );
            assertEquals( FP.option( new OneStringArgDTO( "AAA" ) ), original.fetchByType( OneStringArgDTO.class ) );
            assertEquals( FP.emptyOption(), copy.fetchByType( OneStringArgDTO.class ) );
        }
    }

    @Nested
    public class NoArgConstructorTestCases {
        @Test
        public void givenEmptyDIRepo_fetchUnregisteredObjectThatHasNoArgConstructor_expectInstance() {
            di = new DI();

            NoArgDTO obj = di.fetchMandatory( NoArgDTO.class );

            assertNotNull( obj );
        }

        @Test
        public void givenEmptyDIRepo_fetchUnregisteredObjectThatHasNoArgConstructorTwice_expectSameInstance() {
            NoArgDTO obj1 = di.fetchMandatory( NoArgDTO.class );
            NoArgDTO obj2 = di.fetchMandatory( NoArgDTO.class );

            assertSame( obj1, obj2 );
        }

        @Test
        public void noArgConstructor_registerAType_expectFetchToReturnThatSameRegisteredInstance() {
            NoArgDTO obj1 = new NoArgDTO();

            di.putByType( NoArgDTO.class, obj1 );

            assertSame( obj1, di.fetchMandatory( NoArgDTO.class ) );
        }
    }


    @Nested
    public class OneArgConstructorTestCases {
        @Test
        public void givenEmptyDIRepo_fetchUnregisteredObjectWithOneStringArgConstructor_expectException() {
            try {
                di.fetchMandatory( OneStringArgDTO.class );
                fail( "Expected exception" );
            } catch ( ReflectionException ex ) {
                assertEquals( "Unable to populate arguments for any of " + OneStringArgDTO.class.getName() + "' constructors", ex.getMessage() );
            }
        }

        @Test
        public void oneArgConstructor_argHasNoArgConstructor_expectChainedConstructorCalls() {
            OneObjectArgDTO obj1 = di.fetchMandatory( OneObjectArgDTO.class );

            assertNotNull( obj1 );
            assertNotNull( obj1.obj );
        }

        @Test
        public void oneArgConstructor_argHasNoArgConstructor_expectTheArgToBeTheSameInstanceAsWhenFetchedDirectlyByType() {
            OneObjectArgDTO obj1 = di.fetchMandatory( OneObjectArgDTO.class );
            NoArgDTO obj2 = di.fetchMandatory( NoArgDTO.class );

            assertSame( obj1.obj, obj2 );
        }

        @Test
        public void oneArgConstructor_registerAType_expectFetchToReturnThatSameRegisteredInstanceAsTheParameter() {
            NoArgDTO paramVal = new NoArgDTO();
            di.putByType( NoArgDTO.class, paramVal );

            OneObjectArgDTO obj1 = di.fetchMandatory( OneObjectArgDTO.class );

            assertSame( obj1.obj, paramVal );
        }
    }


    @Nested
    public class TwoArgConstructorTestCases {
        @Test
        public void twoArgConstructor_argHasNoArgConstructor_expectChainedConstructorCalls() {
            TwoObjectArgDTO obj1 = di.fetchMandatory( TwoObjectArgDTO.class );

            assertNotNull( obj1 );
            assertNotNull( obj1.obj1 );
            assertNotNull( obj1.obj2 );
        }

        @Test
        public void fetchByName_doesNotExist() {
            try {
                di.fetchMandatory( OneStringArgDTO.class );
                fail( "expected exception" );
            } catch ( ReflectionException ex ) {
                assertEquals( "Unable to populate arguments for any of mosaics.lang.reflection.DITest$OneStringArgDTO' constructors", ex.getMessage() );
            }
        }

        @Test
        public void fetchByName_fetchConstructorParamByName() {
            di.putByName( "name", "John Bean" );

            assertEquals( new OneStringArgDTO( "John Bean" ), di.fetchMandatory( OneStringArgDTO.class ) );
        }

        @Test
        public void fetchByNameWith_fetchConstructorParamByName() {
            di.with( "name", "John Bean" );

            assertEquals( new OneStringArgDTO( "John Bean" ), di.fetchMandatory( OneStringArgDTO.class ) );
        }
    }

    @Nested
    public class CurryTestCases {
        @Test
        public void givenMethodThatHasNoArgs_curryIt_expectNoChangeToTheMethod() {
            JavaMethod m = JavaClass.of( Service.class ).getMethod( "noArgMethod" ).get();
            
            JavaMethod curriedMethod = di.curry( m );
            
            assertSame( m, curriedMethod );
        }
        
        @Test
        public void givenMethodThatHasArgsNotFoundInTheDIContext_curryIt_expectNoChangeToTheMethod() {
            JavaMethod m             = JavaClass.of( Service.class ).getMethod( "methodWithNoArgsFoundInDIContext", Integer.TYPE, String.class ).get();
            JavaMethod curriedMethod = di.curry( m );
            
            assertSame( m, curriedMethod );
        }
        
        @Test
        public void givenMethodThatHasArgsFoundByTypeInTheDIContext_curryIt_expectMethodThatFixesThatType() {
            Service s = new Service();
            JavaMethod m = JavaClass.of( Service.class ).getMethod( "methodThatHasArgsFoundByTypeInTheDIContext", OneStringArgDTO.class ).get();
            OneStringArgDTO arg1 = new OneStringArgDTO( "Rocking!" );
            
            di.putByType( OneStringArgDTO.class, arg1 );
            
            JavaMethod curriedMethod = di.curry( m );
            
            assertTrue( curriedMethod.getParameters().count() == 0 );
            assertEquals( "DITest.OneStringArgDTO(name=Rocking!)", curriedMethod.invokeAgainst( s ) );
        }
        
        @Test
        public void givenMethodThatHasArgsFoundByNameInTheDIContext_curryTheFirstArg_expectMethodThatFixesThatType() {
            Service s = new Service();
            JavaMethod m = JavaClass.of( Service.class ).getMethod( "methodWithArgCurriedByName", String.class, int.class ).get();
            
            di.putByName( "age", 42 );
            
            JavaMethod curriedMethod = di.curry( m );
            
            assertEquals( 1, curriedMethod.getParameters().count() );
            assertEquals( "Katherine:42", curriedMethod.invokeAgainst( s, "Katherine" ) );
        }
        
        @Test
        public void givenMethodThatHasArgsFoundByNameInTheDIContext_curryTheLastArg_expectMethodThatFixesThatType() {
            Service s = new Service();
            JavaMethod m = JavaClass.of( Service.class ).getMethod( "methodWithArgCurriedByName", String.class, int.class ).get();
            
            di.putByName( "name", "Jason" );
            
            JavaMethod curriedMethod = di.curry( m );
            
            assertEquals( 1, curriedMethod.getParameters().count() );
            assertEquals( "Jason:10", curriedMethod.invokeAgainst( s, 10 ) );
        }
        
        @Test
        public void givenMethodThatHasArgsFoundByNameInTheDIContext_curryTheFirstArg_expectUpdatedMethodParameterDocs() {
            JavaMethod m = JavaClass.of( Service.class ).getMethod( "methodWithArgCurriedByNameWithDocs", String.class, int.class ).get();
            
            di.putByName( "age", 42 );
            
            JavaMethod curriedMethod = di.curry( m );
            
            List<JavaParameter> expected = Collections.singletonList(
                new JavaParameter( m, 0, JavaClass.STRING, "name", List.of() )
            );
            
            assertEquals( expected, curriedMethod.getParameters().toList() );
        }
        
        @Test
        public void givenMethodThatHasArgsFoundByNameInTheDIContext_curryTheLastArg_expectUpdatedMethodParameterDocs() {
            JavaMethod m = JavaClass.of( Service.class ).getMethod( "methodWithArgCurriedByNameWithDocs", String.class, int.class ).get();
            
            di.putByName( "name", "Jason" );
            
            JavaMethod curriedMethod = di.curry( m );
            
            List<JavaParameter> expected = Collections.singletonList(
                new JavaParameter( m, 1, JavaClass.INTEGER_PRIMITIVE, "age", List.of() )
            );

            JavaDoc expectedDoc = new JavaDoc("age docs");
            assertEquals( FPIterable.wrap(expected), curriedMethod.getParameters() );
            assertEquals( expectedDoc, curriedMethod.getParameters().firstOrNull().getJavaDoc().get() );
        }
        
        @Test
        public void givenMethodThatHasArgsFoundByNameInTheDIContext_curryTheLastArg_expectUpdatedMethodDocs() {
            JavaMethod m = JavaClass.of( Service.class ).getMethod( "methodWithArgCurriedByNameWithDocs", String.class, int.class ).get();
            
            di.putByName( "name", "Jason" );
            
            JavaMethod curriedMethod = di.curry( m );
            
            FPOption<JavaDoc> expected = FP.option(
                new JavaDoc( "Method docs" )
                .withTags( List.of(new JavaDocTag("return", FP.emptyOption(), List.of("return docs"))) )
            );
            
            assertEquals( expected, curriedMethod.getJavaDoc() );
        }
    }


    @Nested
    public class PopulateFieldsTestCases {
        @Test
        public void givenNoMatch_populateNullValuedFieldByName_expectFieldRemainsNull() {
            OneStringArgDTO dto = new OneStringArgDTO( null );
            
            di.populateFields( dto );
            
            assertNull( dto.name );
        }
        
        @Test
        public void givenNoMatch_populateNullOptionalValuedFieldByName_expectFieldGetsSetToEmpty() {
            OneOptionalArgDTO dto = new OneOptionalArgDTO( null );
            
            di.populateFields( dto );
            
            assertEquals( Optional.empty(), dto.name );
        }
        
        @Test
        public void givenNoMatch_populateFPOptionValuedFieldByName_expectFieldGetsSetToNone() {
            OneFPOptionArgDTO dto = new OneFPOptionArgDTO( null );
            
            di.populateFields( dto );
            
            assertEquals( FP.emptyOption(), dto.name );
        }
        
        @Test
        public void givenMatch_populateNullValuedFieldByName_expectFieldGetsSetToValue() {
            OneStringArgDTO dto = new OneStringArgDTO( null );
            
            di.putByName( "name", "JJ" );
            di.populateFields( dto );
            
            assertEquals( "JJ", dto.name );
        }
        
        @Test
        public void givenMatch_populateNullOptionalValuedFieldByName_expectFieldGetsSetToSome() {
            OneOptionalArgDTO dto = new OneOptionalArgDTO( null );
            
            di.putByName( "name", "JJ" );
            di.populateFields( dto );
            
            assertEquals( Optional.of( "JJ" ), dto.name );
        }
        
        @Test
        public void givenMatch_populateNullFPOptionValuedFieldByName_expectFieldGetsSetToSome() {
            OneFPOptionArgDTO dto = new OneFPOptionArgDTO( null );
            
            di.putByName( "name", "JJ" );
            di.populateFields( dto );
            
            assertEquals( FP.option( "JJ" ), dto.name );
        }
        
        @Test
        public void givenMatch_populateNoneValuedFieldByName_expectFieldIsOverwritten() {
            OneOptionalArgDTO dto = new OneOptionalArgDTO( Optional.empty() );
            
            di.putByName( "name", "JJ" );
            di.populateFields( dto );
            
            assertEquals( Optional.of( "JJ" ), dto.name );
        }
        
        @Test
        public void givenMatch_populateSomeOptionalValuedFieldByName_expectIsOverwritten() {
            OneOptionalArgDTO dto = new OneOptionalArgDTO( Optional.of( "KK" ) );
            
            di.putByName( "name", "JJ" );
            di.populateFields( dto );
            
            assertEquals( Optional.of( "JJ" ), dto.name );
        }
        
        @Test
        public void givenMatch_populateSomeFPOptionValuedFieldByName_expectIsOverwritten() {
            OneFPOptionArgDTO dto = new OneFPOptionArgDTO( FP.option( "KK" ) );
            
            di.putByName( "name", "JJ" );
            di.populateFields( dto );
            
            assertEquals( FP.option( "JJ" ), dto.name );
        }
        
        @Test
        public void givenMatch_populatePrimitiveValueFieldByName_expectItToBeOverwritten() {
            OnePrimitiveArgDTO dto = new OnePrimitiveArgDTO( 6 );
            
            di.putByName( "age", 42 );
            di.populateFields( dto );
            
            assertEquals( 42, dto.age );
        }
        
        @Test
        public void givenMatch_populateNullValuedFieldByType_expectFieldGetsSetToValue() {
            OneNestedObjectArgDTO dto = new OneNestedObjectArgDTO( null );
            
            di.putByType( OneStringArgDTO.class, new OneStringArgDTO( "JJ" ) );
            di.populateFields( dto );
            
            assertEquals( new OneStringArgDTO( "JJ" ), dto.arg );
        }
        
        @Test
        public void givenMatch_populateNullOptionalValuedFieldByType_expectFieldGetsSetToSome() {
            OneOptionalArgDTO dto = new OneOptionalArgDTO( null );
            
            di.putByType( String.class, "42" );
            di.populateFields( dto );
            
            assertEquals( Optional.of( "42" ), dto.name );
        }
        
        @Test
        public void givenMatch_populateNullFPOptionValuedFieldByType_expectFieldGetsSetToSome() {
            OneFPOptionArgDTO dto = new OneFPOptionArgDTO( null );
            
            di.putByType( String.class, "42" );
            di.populateFields( dto );
            
            assertEquals( FP.option( "42" ), dto.name );
        }
        
        @Test
        public void givenMatch_populateNoneNullValuedFieldByType_expectFieldIsNotChanged() {
            OneNestedObjectArgDTO dto = new OneNestedObjectArgDTO( new OneStringArgDTO( "KK" ) );
            
            di.putByType( OneStringArgDTO.class, new OneStringArgDTO( "JJ" ) );
            di.populateFields( dto );
            
            assertEquals( new OneStringArgDTO( "KK" ), dto.arg );
        }
        
        @Test
        public void givenMatch_populateSomeOptionalValuedFieldByType_expectFieldIsNotChanged() {
            OneOptionalArgDTO dto = new OneOptionalArgDTO( Optional.of( "KK" ) );
            
            di.putByType( String.class, "42" );
            di.populateFields( dto );
            
            assertEquals( Optional.of( "KK" ), dto.name );
        }
        
        @Test
        public void givenMatch_populateSomeFPOptionValuedFieldByType_expectFieldIsNotChanged() {
            OneFPOptionArgDTO dto = new OneFPOptionArgDTO( FP.option( "KK" ) );
            
            di.putByType( String.class, "42" );
            di.populateFields( dto );
            
            assertEquals( FP.option( "KK" ), dto.name );
        }
    }


    @Nested
    public class StartStoppableTestCases {
        @Test
        public void giveStartStoppables_populateField_expectNeitherToBeRunning() {
            StartStoppableRootDTO root = new StartStoppableRootDTO();
            StartStoppableLeafDTO leaf = new StartStoppableLeafDTO();

            di.putByType( StartStoppableLeafDTO.class, leaf );
            di.populateFields( root );

            assertSame( leaf, root.dto );
            assertFalse( root.isRunning() );
            assertFalse( leaf.isRunning() );
        }

        @Test
        public void giveStartStoppables_startTheField_expectOnlyTheFieldToBeRunning() {
            StartStoppableRootDTO root = new StartStoppableRootDTO();
            StartStoppableLeafDTO leaf = new StartStoppableLeafDTO();

            di.putByType( StartStoppableLeafDTO.class, leaf );
            di.populateFields( root );

            leaf.start();

            assertFalse( root.isRunning() );
            assertTrue( leaf.isRunning() );
        }

        @Test
        public void giveStartStoppables_startTheDTO_expectBothObjectsToBeRunning() {
            StartStoppableRootDTO root = new StartStoppableRootDTO();
            StartStoppableLeafDTO leaf = new StartStoppableLeafDTO();

            di.putByType( StartStoppableLeafDTO.class, leaf );
            di.populateFields( root );

            root.start();

            assertTrue( root.isRunning() );
            assertTrue( leaf.isRunning() );
        }

        @Test
        public void giveOptionalStartStoppables_startTheDTO_expectBothObjectsToBeRunning() {
            StartStoppableOptionalRootDTO root = new StartStoppableOptionalRootDTO();
            StartStoppableLeafDTO leaf = new StartStoppableLeafDTO();

            di.putByType( StartStoppableLeafDTO.class, leaf );
            di.populateFields( root );

            root.start();

            assertTrue( root.isRunning() );
            assertTrue( leaf.isRunning() );
            assertSame( leaf, root.dto.get() );
        }

        @Test
        public void giveFPOptionStartStoppables_startTheDTO_expectBothObjectsToBeRunning() {
            StartStoppableFPOptionRootDTO root = new StartStoppableFPOptionRootDTO();
            StartStoppableLeafDTO leaf = new StartStoppableLeafDTO();

            di.putByType( StartStoppableLeafDTO.class, leaf );
            di.populateFields( root );

            root.start();

            assertTrue( root.isRunning() );
            assertTrue( leaf.isRunning() );
            assertSame( leaf, root.dto.get() );
        }
    }

    @Value
    public static class NoArgDTO {

    }

    @Value
    public static class OneObjectArgDTO {
        private NoArgDTO obj;
    }

    @ToString
    @EqualsAndHashCode
    public static class OneStringArgDTO {
        private String name;

        public OneStringArgDTO( String name ) {
            this.name = name;
        }
    }

    @ToString
    @EqualsAndHashCode
    public static class OneNestedObjectArgDTO {
        private OneStringArgDTO arg;

        public OneNestedObjectArgDTO( OneStringArgDTO arg ) {
            this.arg = arg;
        }
    }

    @ToString
    @EqualsAndHashCode
    public static class OneOptionalArgDTO {
        private Optional<String> name;

        public OneOptionalArgDTO( Optional<String> name ) {
            this.name = name;
        }
    }

    @ToString
    @EqualsAndHashCode
    public static class OneFPOptionArgDTO {
        private FPOption<String> name;

        public OneFPOptionArgDTO( FPOption<String> name ) {
            this.name = name;
        }
    }

    @ToString
    @EqualsAndHashCode
    public static class OnePrimitiveArgDTO {
        private int age = 7;

        public OnePrimitiveArgDTO( int age ) {
            this.age = age;
        }
    }

    @Value
    public static class TwoObjectArgDTO {
        private NoArgDTO obj1;
        private OneObjectArgDTO obj2;
    }

    @ToString
    @EqualsAndHashCode(callSuper = false)
    public static class StartStoppableLeafDTO extends StartStoppable<StartStoppableLeafDTO> {
        public String name;

        public StartStoppableLeafDTO() {
            super( "leaf" );
        }
    }

    @ToString
    @EqualsAndHashCode(callSuper = false)
    public static class StartStoppableRootDTO extends StartStoppable<StartStoppableRootDTO> {
        public StartStoppableLeafDTO dto;

        public StartStoppableRootDTO() {
            super( "root" );
        }
    }

    @ToString
    @EqualsAndHashCode(callSuper = false)
    public static class StartStoppableOptionalRootDTO extends StartStoppable<StartStoppableOptionalRootDTO> {
        public Optional<StartStoppableLeafDTO> dto;

        public StartStoppableOptionalRootDTO() {
            super( "root" );
        }
    }

    @ToString
    @EqualsAndHashCode(callSuper = false)
    public static class StartStoppableFPOptionRootDTO extends StartStoppable<StartStoppableFPOptionRootDTO> {
        public FPOption<StartStoppableLeafDTO> dto;

        public StartStoppableFPOptionRootDTO() {
            super( "root" );
        }
    }


    @SuppressWarnings("unused")
    public static class Service {
        public void noArgMethod() {
        }

        public String methodWithNoArgsFoundInDIContext( int age, String name ) {
            return "";
        }

        public String methodThatHasArgsFoundByTypeInTheDIContext( OneStringArgDTO arg1 ) {
            return arg1.toString();
        }

        public String methodWithArgCurriedByName( String name, int age ) {
            return name + ":" + age;
        }

        /**
         * Method docs
         *
         * @param name name docs
         * @param age  age docs
         * @return return docs
         */
        public String methodWithArgCurriedByNameWithDocs( String name, int age ) {
            return name + ":" + age;
        }
    }
}

