package mosaics.lang.reflection;

import lombok.Value;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static mosaics.lang.Backdoor.cast;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SuppressWarnings("unused")
public class ChainedPropertyTest {

    @Test
    public void givenTwoChainedPropertiesWithValues_getValue_expectTheFinalValueToBeReturned() {
        Object o = new Object() {
            public JavaPropertyTest.Film film = new JavaPropertyTest.Film("Lord of the Rings");
        };

        JavaClass jc = JavaClass.of(o.getClass());

        JavaProperty<Object,Object> property1 = jc.getProperty( "film" ).get();
        JavaProperty<Object,String> property2 = property1.getProperty( String.class, "name" ).get();

        assertEquals( FPOption.of("Lord of the Rings"), property2.getValueFrom(o) );
    }

    @Test
    public void givenThreeChainedPropertiesWithValues_getValue_expectTheFinalValueToBeReturned() {
        Object o = new Object() {
            public JavaPropertyTest.Film film = new JavaPropertyTest.Film("Lord of the Rings", new JavaPropertyTest.Director("Jackson"));
        };

        JavaClass jc = cast(JavaClass.of(o.getClass()));

        JavaProperty<Object,Object>   property1 = jc.getProperty("film").get();
        JavaProperty<Object,JavaPropertyTest.Director> property2 = property1.getProperty(JavaPropertyTest.Director.class, "director").get();
        JavaProperty<Object,String>   property3 = property2.getProperty(String.class, "name").get();

        assertEquals( FPOption.of("Jackson"), property3.getValueFrom(o) );
    }

    @Test
    public void givenThreeChainedPropertiesFirstValueMissing_getValue_expectNone() {
        Object o = new Object() {
            public JavaPropertyTest.Film film;
        };

        JavaClass jc = cast(JavaClass.of(o.getClass()));

        JavaProperty<Object,Object> property1 = jc.getProperty("film").get();
        JavaProperty<Object,JavaPropertyTest.Director> property2 = property1.getProperty(JavaPropertyTest.Director.class, "director").get();
        JavaProperty<Object,String> property3 = property2.getProperty(String.class, "name").get();

        assertEquals( FPOption.none(), property3.getValueFrom(o) );
    }

    @Test
    public void givenThreeChainedPropertiesFirstValueMissing_init_expectBlankString() {
        Object o = new Object() {
            public JavaPropertyTest.Film film;
        };

        JavaClass jc = cast(JavaClass.of(o.getClass()));

        JavaProperty<Object,Object> property1 = jc.getProperty("film").get();
        JavaProperty<Object,JavaPropertyTest.Director> property2 = property1.getProperty(JavaPropertyTest.Director.class, "director").get();
        JavaProperty<Object,String> property3 = property2.getProperty(String.class, "name").get();

        property3.init(o);

        assertEquals( FPOption.of(""), property3.getValueFrom(o) );
    }

    @Test
    public void givenMandatoryChain_setValueWhenRootFilmIsMissing_expectTheFilmObjectToBeCreatedAutomatically() {
        Object o = new Object() {
            public JavaPropertyTest.Film film;
        };

        JavaClass jc = cast(JavaClass.of(o.getClass()));

        JavaProperty<Object,JavaPropertyTest.Film>   property1 = jc.getProperty(JavaPropertyTest.Film.class, "film").get();
        JavaProperty<Object,String> property2 = property1.getProperty(String.class, "name").get();

        property2.setValueOn(o, "Mowana");

        assertEquals( FPOption.of("Mowana"), property2.getValueFrom(o) );
    }

    @Test
    public void givenOptionChain_setValueWhenRootIsMissing_expectTheFilmObjectToBeCreatedAutomatically() {
        Object o = new Object() {
            public FPOption<JavaPropertyTest.Film> film;
        };

        JavaClass jc = cast(JavaClass.of(o.getClass()));

        JavaProperty<Object,JavaPropertyTest.Film>   property1 = jc.getProperty(JavaPropertyTest.Film.class, "film").get();
        JavaProperty<Object,String> property2 = property1.getProperty(String.class, "name").get();

        property2.setValueOn(o, "Mowana");

        assertEquals( FPOption.of("Mowana"), property2.getValueFrom(o) );
    }

    @Test
    public void givenOptionalChain_setValueWhenRootIsMissing_expectTheFilmObjectToBeCreatedAutomatically() {
        Object o = new Object() {
            public Optional<JavaPropertyTest.Film> film;
        };

        JavaClass jc = cast(JavaClass.of(o.getClass()));

        JavaProperty<Object,JavaPropertyTest.Film>   property1 = jc.getProperty(JavaPropertyTest.Film.class, "film").get();
        JavaProperty<Object,String> property2 = property1.getProperty(String.class, "name").get();

        property2.setValueOn(o, "Mowana");

        assertEquals( FPOption.of("Mowana"), property2.getValueFrom(o) );
    }

    @Test
    public void givenChainedDTO_checkDocumentationComesFromFieldOnDTO() {
        Object o = new Object() {
            public Optional<Book> book;
        };

        JavaClass jc = cast(JavaClass.of(o.getClass()));

        JavaProperty<Object,Book>   bookProperty        = jc.getProperty(Book.class, "book").get();
        JavaProperty<Object,String> nameProperty        = bookProperty.getProperty(String.class, "name").get();
        JavaProperty<Object,String> descriptionProperty = bookProperty.getProperty(String.class, "description").get();

        assertEquals( "Film name documentation.", nameProperty.getJavaDoc().get().getText().get(0) );
        assertEquals( "Film description documentation.", descriptionProperty.getJavaDoc().get().getText().get(0) );
    }

    @Test
    public void givenChainedDTOWithOverriddenFieldDocumentation_checkDocumentationComesFromOverrides() {
        JavaClass jc = JavaClass.of( Course.class );

        JavaProperty<Course,Book>   bookProperty        = cast(jc.getProperty(Book.class, "mandatoryReading").get());
        JavaProperty<Course,String> nameProperty        = bookProperty.getProperty(String.class, "name").get();
        JavaProperty<Course,String> descriptionProperty = bookProperty.getProperty(String.class, "description").get();

        assertEquals( "The name of the mandatory reading book.", nameProperty.getJavaDoc().get().getText().get(0) );
        assertEquals( "A short summary of why this book is on the mandatory reading list.", descriptionProperty.getJavaDoc().get().getText().get(0) );
    }

    @Test
    public void givenChainedDTOWithNestedOverriddenFieldDocumentation_checkDocumentationComesFromOverrides() {
        JavaClass jc = JavaClass.of( UberCourse.class );

        JavaProperty<UberCourse,String> nameProperty        = cast(jc.getProperty(String.class, "subcourse.mandatoryReading.name").get());
        JavaProperty<UberCourse,String> descriptionProperty = cast(jc.getProperty(String.class, "subcourse.mandatoryReading.description").get());

        assertEquals( "The title of the subcourses mandatory reading book.", nameProperty.getJavaDoc().get().getText().get(0) );
        assertEquals( "A short description of the subcourses mandatory reading book.", descriptionProperty.getJavaDoc().get().getText().get(0) );
    }


    @Value
    public static class UberCourse {
        /**
         * @param mandatoryReading.name The title of the subcourses mandatory reading book.
         * @param mandatoryReading.description A short description of the subcourses mandatory reading book.
         */
        private Course subcourse;
    }

    @Value
    public static class Course {
        /**
         * @param name The name of the mandatory reading book.
         * @param description A short summary of why this book is on the mandatory reading list.
         */
        private Book mandatoryReading;
    }

    @Value
    public static class Book {
        /**
         * Film name documentation.
         */
        public String name;

        /**
         * Film description documentation.
         */
        public String description;
    }
}
