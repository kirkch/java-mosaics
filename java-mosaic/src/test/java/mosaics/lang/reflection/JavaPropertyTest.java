package mosaics.lang.reflection;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Secret;
import mosaics.lang.annotations.Mutable;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SuppressWarnings({"OptionalUsedAsFieldOrParameterType", "unused"})
public class JavaPropertyTest {

    @Nested
    public class SingleFieldTestCases {
        @Test
        public void givenMissingField_expectNoPropertyFound() {
            Object o = new Object() {};
            JavaClass jc = JavaClass.of( o.getClass() );

            assertEquals( FPOption.none(), jc.getProperty( "name" ) );
        }

        @Test
        public void givenPublicField_getNullValue_expectNoneBack() {
            Object o = new Object() {
                public String name;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            assertEquals( FPOption.none(), property.getValueFrom( o ) );
        }

        @Test
        public void givenPrivateField_getValue_expectNoProperty() {
            Object o = new Object() {
                private String name = "Bob";
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            assertEquals( FPOption.none(), jc.getPublicProperty( String.class, "name" ) );
        }

        @Test
        public void givenPublicField_getValue_expectValueBack() {
            Object o = new Object() {
                public String name = "Bob";
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            assertEquals( FPOption.of( "Bob" ), property.getValueFrom( o ) );
        }

        @Test
        public void givenPublicField_setValue_expectValueToBeUpdated() {
            Object o = new Object() {
                public String name = "Bob";
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            property.setValueOn( o, "Sarah" );
            assertEquals( FPOption.of( "Sarah" ), property.getValueFrom( o ) );
        }


        @Test
        public void givenJavaDoc_callGetDocs_expectDocs() {
            JavaClass jc = JavaClass.of( StringFieldWithDocs.class );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            FPOption<JavaDoc> expected = FPOption.of(
                new JavaDoc("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
            );

            assertEquals( expected, property.getJavaDoc() );
        }
    }

    @Nested
    public class SingleFieldLombokValueTestCases {
        @Test
        public void givenPublicField_getNullValue_expectNoneBack() {
            Object o = new Object() {
                public Secret password;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "password" ).get();

            assertEquals( FPOption.none(), property.getValueFrom( o ) );
        }

        @Test
        public void givenPrivateField_getValue_expectNoProperty() {
            Object o = new Object() {
                private Secret password = new Secret("pass");
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            assertEquals( FPOption.none(), jc.getPublicProperty( String.class, "password" ) );
        }

        @Test
        public void givenPublicField_getValue_expectValueBack() {
            Object o = new Object() {
                public Secret password = new Secret("pass");
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Secret> property = jc.getPublicProperty( Secret.class, "password" ).get();

            assertEquals( FPOption.of(new Secret("pass")), property.getValueFrom( o ) );
        }

        @Test
        public void givenPublicField_setValue_expectValueToBeUpdated() {
            Object o = new Object() {
                public Secret password = new Secret("pass");
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Secret> property = jc.getProperty( Secret.class, "password" ).get();

            property.setValueOn( o, new Secret("Sarah") );
            assertEquals( FPOption.of(new Secret("Sarah")), property.getValueFrom(o) );
        }

        @Test
        public void givenLombokValueWithAnnotationOnField_expectAnnotationToBeVisibleViaProperty() {
            JavaClass jc = JavaClass.of( LombokValueDTO.class );

            JavaProperty<Object,Object> prop = jc.getProperty("prop1").get();
            assertTrue(prop.getAnnotation(Mutable.class).hasValue());
        }
    }

    /**
     * Nam eget iaculis odio.
     */
    @Value
    public static class LombokValueDTO {
        /**
         * Aliquam varius malesuada erat, eu rhoncus magna.
         */
        @Mutable
        private String prop1;
    }


    @Nested
    public class SingleOptionFieldTestCases {
        @Test
        public void givenPublicOptionFieldSetToNull_getValue_expectNone() {
            Object o = new Object() {
                public FPOption<String> name;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            assertEquals( FPOption.class, property.getValueType().getJdkClass() );
            assertEquals( String.class, property.getValueType().getClassGenerics().first().get().getJdkClass() );
            assertTrue( property.isOptional() );
            assertEquals( FPOption.none(), property.getValueFrom( o ) );
        }

        @Test
        public void givenPublicOptionFieldSetToNone_getValue_expectNone() {
            Object o = new Object() {
                public FPOption<String> name = FPOption.none();
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            assertEquals( FPOption.none(), property.getValueFrom( o ) );
        }

        @Test
        public void givenPublicOptionFieldSetToValue_getValue_expectSome() {
            Object o = new Object() {
                public FPOption<String> name = FPOption.of( "Mandrake" );
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            assertEquals( FPOption.of( "Mandrake" ), property.getValueFrom( o ) );
        }

        @Test
        public void givenPublicOptionField_setValue_expectSome() {
            Object o = new Object() {
                public FPOption<String> name = FPOption.of( "Mandrake" );
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            property.setValueOn( o, "Cannon" );
            assertEquals( FPOption.of( "Cannon" ), property.getValueFrom( o ) );
        }

        @Test
        public void givenPublicOptionField_unsetValue_expectNone() {
            Object o = new Object() {
                public FPOption<String> name = FPOption.of( "Mandrake" );
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            property.unsetOn( o );
            assertEquals( FPOption.none(), property.getValueFrom( o ) );
        }
    }


    @Nested
    public class SingleOptionalFieldTestCases {
        @Test
        public void givenPublicOptionalFieldSetToNone_getValue_expectNone() {
            Object o = new Object() {
                public Optional<String> name = Optional.empty();
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            assertEquals( FPOption.none(), property.getValueFrom( o ) );
        }

        @Test
        public void givenPublicOptionalFieldSetToValue_getValue_expectSome() {
            Object o = new Object() {
                public Optional<String> name = Optional.of( "Mandrake" );
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            assertEquals( FPOption.of( "Mandrake" ), property.getValueFrom( o ) );
        }

        @Test
        public void givenPublicOptionalField_setValue_expectSome() {
            Object o = new Object() {
                public Optional<String> name = Optional.of( "Mandrake" );
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            property.setValueOn( o, "Cannon" );
            assertEquals( FPOption.of( "Cannon" ), property.getValueFrom( o ) );
        }

        @Test
        public void givenPublicOptionalField_unsetValue_expectSome() {
            Object o = new Object() {
                public Optional<String> name = Optional.of( "Mandrake" );
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            property.unsetOn( o );
            assertEquals( FPOption.none(), property.getValueFrom( o ) );
        }
    }


    @Nested
    public class SingleIntFieldTestCases {
        @Test
        public void givenPublicIntField_getValue_expectSomeValue() {
            Object o = new Object() {
                public int age = 10;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Integer> property = jc.getProperty( Integer.class, "age" ).get();

            assertEquals( FPOption.of( 10 ), property.getValueFrom( o ) );
        }

        @Test
        public void givenPublicIntField_unsetValue_expectSomeZero() {
            Object o = new Object() {
                public int age = 10;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Integer> property = jc.getProperty( Integer.class, "age" ).get();

            property.unsetOn( o );
            assertEquals( FPOption.of( 0 ), property.getValueFrom( o ) );
        }
    }


    @Nested
    public class SingleGetterMethodTestCases {
        @Test
        public void givenGetter_getValue_expectSome() {
            Object o = new Object() {
                private String name = "Superman";

                public String getName() {
                    return name;
                }
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, String> property = jc.getProperty( String.class, "name" ).get();

            assertEquals( FPOption.of( "Superman" ), property.getValueFrom( o ) );
        }
    }


    @Nested
    public class SingleIsMethodTestCases {
        @Test
        public void givenIsGetter_getValue_expectSome() {
            Object o = new Object() {
                private boolean dryFlag = true;

                public boolean isDry() {
                    return dryFlag;
                }
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Boolean> property = jc.getProperty( Boolean.class, "dry" ).get();

            assertEquals( FPOption.of( true ), property.getValueFrom( o ) );
        }
    }


    @Nested
    public class SingleHasMethodTestCases {

        @Test
        public void givenhasGetter_getValue_expectSome() {
            Object o = new Object() {
                private boolean fleas = true;

                public boolean hasFleas() {
                    return fleas;
                }
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Boolean> property = jc.getProperty( Boolean.class, "fleas" ).get();

            assertEquals( FPOption.of( true ), property.getValueFrom( o ) );
        }
    }


    @Nested
    public class SingleGetterSetterPairTestCases {

        @Test
        public void givenGetterMethodOnly_hasSetter_expectFalse() {
            Object o = new Object() {
                private boolean fleas = true;

                public boolean hasFleas() {
                    return fleas;
                }
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Boolean> property = jc.getProperty( Boolean.class, "fleas" ).get();

            assertEquals( false, property.hasSetter() );
        }

        @Test
        public void givenGetterAndSetterMethod_hasSetter_expectTrue() {
            Object o = new Object() {
                private boolean fleas = true;

                public boolean hasFleas() {
                    return fleas;
                }

                public void hasFleas( boolean flag ) {
                    this.fleas = flag;
                }
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Boolean> property = jc.getProperty( Boolean.class, "fleas" ).get();

            assertEquals( true, property.hasSetter() );
        }

        @Test
        public void givenGetterAndSetterMethod_invokeSetter_expectValueToChange() {
            Object o = new Object() {
                private boolean fleas = true;

                public boolean hasFleas() {
                    return fleas;
                }

                public void hasFleas( boolean flag ) {
                    this.fleas = flag;
                }
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Boolean> property = jc.getProperty( Boolean.class, "fleas" ).get();

            property.setValueOn( o, false );
            assertEquals( FPOption.of( false ), property.getValueFrom( o ) );
        }

        @Test
        public void givenBooleanValue_callUnset_expectValueToChangeToFalse() {
            Object o = new Object() {
                private boolean fleas = true;

                public boolean hasFleas() {
                    return fleas;
                }

                public void hasFleas( boolean flag ) {
                    this.fleas = flag;
                }
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Boolean> property = jc.getProperty( Boolean.class, "fleas" ).get();

            property.unsetOn( o );
            assertEquals( FPOption.of( false ), property.getValueFrom( o ) );
        }

        @Test
        public void givenWithMethod_hasSetter_expectFalse() {
            Object o = new Object() {
                private boolean fleas = true;

                public boolean hasFleas() {
                    return fleas;
                }

                public Object withFleas( boolean flag ) {
                    this.fleas = flag;

                    return this;
                }
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Boolean> property = jc.getProperty( Boolean.class, "fleas" ).get();

            assertEquals( false, property.hasSetter() );
        }
    }


    @Nested
    public class InitTestCases {
        @Test
        public void initFalseBoolean_expectNoChangeAsPrimitivesAlreadyHaveAValue() {
            Object o = new Object() {
                public boolean flag = false;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Object> property = jc.getProperty( "flag" ).get();

            property.init( o );
            assertEquals( FPOption.of( false ), property.getValueFrom( o ) );
        }

        @Test
        public void initTrueBoolean_expectNoChangeAsPrimitivesAlreadyHaveAValue() {
            Object o = new Object() {
                public boolean flag = true;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Object> property = jc.getProperty( "flag" ).get();

            property.init( o );
            assertEquals( FPOption.of( true ), property.getValueFrom( o ) );
        }

        @Test
        public void initIntPrimitive_expectNoChangeAsPrimitivesAlreadyHaveAValue() {
            Object o = new Object() {
                public int value = 42;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Object> property = jc.getProperty( "value" ).get();

            property.init( o );
            assertEquals( FPOption.of( 42 ), property.getValueFrom( o ) );
        }

        @Test
        public void initString_expectEmptyString() {
            Object o = new Object() {
                public String name;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Object> property = jc.getProperty( "name" ).get();

            property.init( o );
            assertEquals( FPOption.of( "" ), property.getValueFrom( o ) );
        }

        @Test
        public void initOptionString_expectEmptyString() {
            Object o = new Object() {
                public FPOption<String> name;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Object> property = jc.getProperty( "name" ).get();

            property.init( o );
            assertEquals( FPOption.of( "" ), property.getValueFrom( o ) );
        }

        @Test
        public void initOptionalString_expectEmptyString() {
            Object o = new Object() {
                public Optional<String> name;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Object> property = jc.getProperty( "name" ).get();

            property.init( o );
            assertEquals( FPOption.of( "" ), property.getValueFrom( o ) );
        }

        @Test
        public void initOptionalString_withExistingValue_expectNoChange() {
            Object o = new Object() {
                public Optional<String> name = Optional.of( "Foo" );
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Object> property = jc.getProperty( "name" ).get();

            property.init( o );
            assertEquals( FPOption.of( "Foo" ), property.getValueFrom( o ) );
        }
    }


    @Nested
    public class ToStringTestCases {
        @Test
        public void givenAFieldProperty_expectToStringToBeTheFieldName() {
            Object o = new Object() {
                public Film film;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Object> property = jc.getProperty( "film" ).get();

            assertEquals( "film", property.toString() );
        }

        @Test
        public void givenAMethodProperty_expectToStringToBeThePropertyName() {
            Object o = new Object() {
                private Film film;

                public Film getFilm() {
                    return film;
                }
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Object> property = jc.getProperty( "film" ).get();

            assertEquals( "film", property.toString() );
        }

        @Test
        public void givenThreeChainedProperties_toString_expectDotSeparatedChain() {
            Object o = new Object() {
                public Film film;
            };

            JavaClass jc = JavaClass.of( o.getClass() );

            JavaProperty<Object, Object> property1 = jc.getProperty( "film" ).get();
            JavaProperty<Object, Director> property2 = property1.getProperty( Director.class, "director" ).get();
            JavaProperty<Object, String> property3 = property2.getProperty( String.class, "name" ).get();

            assertEquals( "film.director.name", property3.toString() );
        }
    }


    @Nested
    public class ChainedPropertiesTestCases {
        @Test
        public void givenJavaDoc_callGetDocs_expectDocs() {
            JavaClass jc = JavaClass.of( Film.class );

            JavaProperty<Object, String> property = jc.getProperty( Director.class, "director" ).get()
                .getProperty( String.class, "name" ).get();

            FPOption<JavaDoc> expected = FPOption.of(
                new JavaDoc("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
            );

            assertEquals( expected, property.getJavaDoc() );
        }

        @Test
        public void givenDeepChainWithJavaDocsAtEachStepOfTheWay_ensureTheLeafJavaDocIsReturned() {
            JavaClass jc = JavaClass.of( ClassWithPublicFields.class );

            JavaProperty<Object, Boolean> property = jc
                .getProperty( PublicFieldDto.class, "dtoField" ).get()
                .getProperty( PublicFieldDto2.class, "dtoField2" ).get()
                .getProperty( Boolean.class, "booleanObjectField" ).get();

            FPOption<JavaDoc> expected = FPOption.of(
                new JavaDoc("PublicFieldDto2.booleanObjectField")
            );

            assertEquals( expected, property.getJavaDoc() );
        }
    }


    @Nested
    public class WrapTypeOfMetaTestCases {
        @Test
        public void WrapTypeofMeta_getProperty_expectValue() {
            JavaProperty<Object, Object> property = JavaProperty.wrap( JavaClass.of(Film.class), "film" );

            assertEquals( "Ponyo", property.getProperty( "name" ).get().getValueFrom( new Film( "Ponyo" ) ).get() );
        }


        @Test
        public void WrapTypeofMeta_getProperties_expectValue() {
            List<String> path = Arrays.asList( "director", "name" );
            JavaProperty<Object, Object> root = JavaProperty.wrap( JavaClass.of(Film.class), "film" );
            JavaProperty<Object, Object> property = root.getProperties( path );

            assertEquals( "G", property.getValueFrom( new Film( "Ponyo", new Director( "G" ) ) ).get() );
        }
    }

    @ToString
    @EqualsAndHashCode
    public static class Film {
        public String             name;
        public FPOption<Director> director = FP.emptyOption();

        public Film() {}

        public Film( String name ) {
            this.name = name;
        }

        public Film( String name, Director director ) {
            this.name     = name;
            this.director = FP.option( director );
        }
    }

    @ToString
    @EqualsAndHashCode
    public static class Director {
        /**
         * Lorem ipsum dolor sit amet, consectetur adipiscing elit.
         */
        public String name;

        public Director() {}

        public Director( String name ) {
            this.name = name;
        }
    }

    public static class StringFieldWithDocs {
        /**
         * Lorem ipsum dolor sit amet, consectetur adipiscing elit.
         */
        public String name = "Bob";
    }

    public static class ClassWithPublicFields {
        /**
         * stringField
         */
        public String         stringField;

        /**
         * booleanField
         */
        public boolean        booleanField;

        public PublicFieldDto dtoField;
    }

    /**
     * PublicFieldDto
     */
    public static class PublicFieldDto {
        /**
         * PublicFieldDto.stringField
         */
        public String stringField;

        /**
         * PublicFieldDto2
         */
        public PublicFieldDto2 dtoField2;
    }

    /**
     * PublicFieldDto2
     */
    public static class PublicFieldDto2 {
        /**
         * PublicFieldDto2.booleanObjectField
         */
        public Boolean booleanObjectField;
    }
}
