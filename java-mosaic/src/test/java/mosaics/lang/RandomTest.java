package mosaics.lang;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


public class RandomTest {

    @Test
    public void nextInt() {
        int rnd = new Random().nextInt( 10 );

        assertTrue( rnd >= 0 );
        assertTrue( rnd < 10 );
    }

    @Nested
    public class UUIDTestCases {
        @Test
        public void givenASeed_expectUUIDsToBeReproducable() {
            Random rnd1 = new Random(100L);
            Random rnd2 = new Random(100L);

            assertEquals( rnd1.nextUUID(), rnd2.nextUUID() );
            assertEquals( rnd1.nextUUID(), rnd2.nextUUID() );
            assertEquals( rnd1.nextUUID(), rnd2.nextUUID() );
            assertEquals( rnd1.nextUUID(), rnd2.nextUUID() );
        }

        @Test
        public void givenASeed_expectUUIDsToBeRepeatedBetweenRuns() {
            Random rnd = new Random(100L);

            assertEquals( "d69fd5b8-b4db-42bc-85f3-e93169e33eb7", rnd.nextUUID() );
            assertEquals( "67f8caaa-7457-4c07-98e6-47c751fa954d", rnd.nextUUID() );
        }

        @Test
        public void guidsGeneratedAreUnique() {
            Random      rnd = new Random();
            Set<String> all = new HashSet<>();

            for ( int i=0; i<100; i++ ) {
                String guid = rnd.nextUUID();

                boolean wasFirstAdd = all.add( guid );
                assertTrue( wasFirstAdd, guid );
            }
        }
    }

}
