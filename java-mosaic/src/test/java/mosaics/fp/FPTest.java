package mosaics.fp;

import mosaics.fp.collections.FPIterable;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class FPTest {

    @Test
    public void testWrapStream() {
        List<String> actual = FP.wrapStream( Stream.of("a","b","c") ).toList();

        assertEquals( List.of("a","b","c"), actual );
    }

    @Test
    public void testWrapArray() {
        List<String> actual = FP.wrapArray("  a  \n  b  ".split("\n"))
            .map( String::trim )
            .toList();

        assertEquals( Arrays.asList("a","b"), actual );
    }

    @Test
    public void testRange() {
        assertEquals( List.of(), FP.range(0,0).toList() );
        assertEquals( List.of(0), FP.range(0,1).toList() );
        assertEquals( List.of(0,1), FP.range(0,2).toList() );
        assertEquals( List.of(0,1,2), FP.range(0,3).toList() );

        FPIterable<Integer> r2 = FP.range(0,2);
        assertEquals( List.of(0,1), r2.toList() );
        assertEquals( List.of(0,1), r2.toList() );
    }
}
