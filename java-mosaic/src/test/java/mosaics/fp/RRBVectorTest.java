package mosaics.fp;

import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.RRBVector;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;


public class RRBVectorTest {
    @Nested
    public class AddAndGetTestCases {
        @Test
        public void givenANewlyCreatedVector_callSize_expect0() {
            RRBVector<String> vector = new RRBVector<>();

            assertEquals( 0, vector.size() );
        }

        @Test
        public void givenANewlyCreatedVector_get0_expectEmptyOption() {
            RRBVector<String> vector = new RRBVector<>();

            assertEquals( FP.emptyOption(), vector.get( 0 ) );
        }

        @Test
        public void givenANewlyCreatedVector_get1_expectEmptyOption() {
            RRBVector<String> vector = new RRBVector<>();

            assertEquals( FP.emptyOption(), vector.get( 1 ) );
        }


        @Test
        public void givenANewlyCreatedVector_addOneElement_callSize_expect1() {
            RRBVector<String> vector = new RRBVector<>();
            RRBVector<String> modifiedVector = vector.add( "a" );

            assertEquals( 1, modifiedVector.size() );
        }

        @Test
        public void givenANewlyCreatedVector_addOneElement_callSizeOnOriginalVector_expect0() {
            RRBVector<String> originalVector = new RRBVector<>();

            originalVector.add( "a" );

            assertEquals( 0, originalVector.size() );
        }

        @Test
        public void givenANewlyCreatedVector_addOneElement_callGet0_expectValueBack() {
            String a = "a";

            RRBVector<String> originalVector = new RRBVector<>();
            RRBVector<String> modifiedVector = originalVector.add( a );

            assertSame( a, modifiedVector.get( 0 ).get() );
        }

        @Test
        public void givenANewlyCreatedVector_addOneElement_callGet1_expectEmpty() {
            String a = "a";

            RRBVector<String> originalVector = new RRBVector<>();
            RRBVector<String> modifiedVector = originalVector.add( a );

            assertEquals( FP.emptyOption(), modifiedVector.get( 1 ) );
        }

        @Test
        public void givenAVectorWithTwoElements_callGet0_expectValueBack() {
            String a = "a";
            String b = "b";

            RRBVector<String> originalVector = new RRBVector<>();
            RRBVector<String> modifiedVector = originalVector.add( a );
            modifiedVector = modifiedVector.add( b );

            assertSame( a, modifiedVector.get( 0 ).get() );
        }

        @Test
        public void givenAVectorWithTwoElements_callGet1_expectValueBack() {
            String a = "a";
            String b = "b";

            RRBVector<String> originalVector = new RRBVector<>();
            RRBVector<String> modifiedVector = originalVector.add( a );
            modifiedVector = modifiedVector.add( b );

            assertSame( b, modifiedVector.get( 1 ).get() );
        }

        @Test
        public void givenAVectorWithTwoElements_callGet2_expectError() {
            String a = "a";
            String b = "b";

            RRBVector<String> originalVector = new RRBVector<>();
            RRBVector<String> modifiedVector = originalVector.add( a );
            modifiedVector = modifiedVector.add( b );

            assertEquals( FP.emptyOption(), modifiedVector.get( 2 ) );
        }

        @Test
        public void givenAVectorOfFiveElements_showThatEachElementMayBeRetrievedAndThatGoingBeyondThatIsAnError() {
            assertAddingAndRetrivingNValues( 5 );
        }

        @Test
        public void givenAVectorOfTenElements_showThatEachElementMayBeRetrievedAndThatGoingBeyondThatIsAnError() {
            assertAddingAndRetrivingNValues( 10 );
        }

        @Test
        public void givenAVectorOfWithAsManyElementsAsItsBranchingFactor_showThatEachElementMayBeRetrievedAndThatGoingBeyondThatIsAnError() {
            assertAddingAndRetrivingNValues( 32 );
        }

        @Test
        public void givenAVectorOfWithOneMoreElementThanItsBranchingFactor_showThatEachElementMayBeRetrievedAndThatGoingBeyondThatIsAnError() {
            assertAddingAndRetrivingNValues( 32 + 1 );
        }

        @Test
        public void givenOneHundredThousandValues_storeThemInTheVector_expectToBeAbleToReadThemAllBack() {
            assertAddingAndRetrivingNValues( 100_000 );
        }

        @Test
        public void bulkAddOneThousandEntries_expectToBeAbleToRetrieveAllThousand() {
            int numEntriesToAdd = 1000;

            RRBVector<String> rrbVector = RRBVector.emptyVector();
            for ( int i=0; i<numEntriesToAdd; i++ ) {
                rrbVector = rrbVector.add( Integer.toString(i) );
            }

            for ( int i=0; i<numEntriesToAdd; i++ ) {
                assertEquals( FP.option(Integer.toString(i)), rrbVector.get(i) );
            }
        }
    }


    @Nested
    public class SetTestCases {
        @Test
        public void givenANewlyCreatedVector_set0_expectSuccess() {
            RRBVector<String> vector = new RRBVector<String>()
                .set( 0, "a" );

            assertEquals( "a", vector.get( 0 ).get() );
            assertEquals( 1, vector.size() );
        }

        @Test
        public void givenANewlyCreatedVector_set1_expectSuccess() {
            RRBVector<String> vector = new RRBVector<String>()
                .set( 1, "a" );

            assertEquals( FP.emptyOption(), vector.get( 0 ) );
            assertEquals( "a", vector.get( 1 ).get() );
            assertEquals( 2, vector.size() );
        }

        @Test
        public void givenANewlyCreatedVector_set2_expectSuccess() {
            RRBVector<String> vector = new RRBVector<String>()
                .set( 2, "a" );

            assertEquals( FP.emptyOption(), vector.get( 0 ) );
            assertEquals( FP.emptyOption(), vector.get( 1 ) );
            assertEquals( FP.option( "a" ), vector.get( 2 ) );
            assertEquals( 3, vector.size() );
        }

        @Test
        public void given3ElementVector_set0_expectOnly0ToChange() {
            RRBVector<String> vector = new RRBVector<String>()
                .add( "a" )
                .add( "b" )
                .add( "c" );

            vector = vector.set( 0, "X" );

            assertEquals( "X", vector.get( 0 ).get() );
            assertEquals( "b", vector.get( 1 ).get() );
            assertEquals( "c", vector.get( 2 ).get() );
            assertEquals( 3, vector.size() );
        }

        @Test
        public void given3ElementVector_set1_expectOnly1ToChange() {
            RRBVector<String> vector = new RRBVector<String>()
                .add( "a" )
                .add( "b" )
                .add( "c" );

            vector = vector.set( 1, "X" );

            assertEquals( FP.option( "a" ), vector.get( 0 ) );
            assertEquals( FP.option( "X" ), vector.get( 1 ) );
            assertEquals( FP.option( "c" ), vector.get( 2 ) );
            assertEquals( 3, vector.size() );
        }

        @Test
        public void given3ElementVector_set2_expectOnly2ToChange() {
            RRBVector<String> vector = new RRBVector<String>()
                .add( "a" )
                .add( "b" )
                .add( "c" );

            vector = vector.set( 2, "X" );

            assertEquals( "a", vector.get( 0 ).get() );
            assertEquals( "b", vector.get( 1 ).get() );
            assertEquals( "X", vector.get( 2 ).get() );
            assertEquals( 3, vector.size() );
        }

        @Test
        public void givenEmptyElement_setAValueInTheNextNode() {
            RRBVector<String> vector = new RRBVector<>();

            vector = vector.set( 32, "X" );

            for ( int i = 0; i < 32; i++ ) {
                assertEquals( FP.emptyOption(), vector.get( i ) );
            }

            assertEquals( FP.option( "X" ), vector.get( 32 ) );
            assertEquals( 33, vector.size() );
        }

        @Test
        public void givenEmptyElement_setMaxLongMinusOne_expectSuccess() {
            RRBVector<String> vector = new RRBVector<>();

            vector = vector.set( Long.MAX_VALUE - 1, "X" );

            assertEquals( "X", vector.get( Long.MAX_VALUE - 1 ).get() );
            assertEquals( Long.MAX_VALUE, vector.size() );
        }

        @Test
        public void givenEmptyElement_setMaxLong_expectFailureAsSizeWouldOverflow() {
            RRBVector<String> vector = new RRBVector<>();

            try {
                vector.set( Long.MAX_VALUE, "X" );
                fail( "expected exception" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "9223372036854775807 is out of bounds", ex.getMessage() );
            }
        }

        @Test
        public void bulkAddOneThousandEntries_expectToBeAbleToRetrieveAllThousand() {
            int numEntriesToAdd = 1000;

            RRBVector<String> rrbVector = RRBVector.emptyVector();
            for ( int i=0; i<numEntriesToAdd; i++ ) {
                rrbVector = rrbVector.set( i, Integer.toString(i) );
            }

            for ( int i=0; i<numEntriesToAdd; i++ ) {
                assertEquals( FP.option(Integer.toString(i)), rrbVector.get(i) );
            }
        }
    }


    @Nested
    public class IsEmptyAndHasContentsTestCases {
        @Test
        public void givenEmptyVector_expectIsEmptyToReturnTrue() {
            RRBVector<String> vector = new RRBVector<>();

            assertTrue( vector.isEmpty() );
        }

        @Test
        public void givenEmptyVector_expectHasContentsToReturnFalse() {
            RRBVector<String> vector = new RRBVector<>();

            assertFalse( vector.hasContents() );
        }

        @Test
        public void givenNonEmptyVector_expectIsEmptyToReturnFalse() {
            RRBVector<String> vector = new RRBVector<String>()
                .add( "a" )
                .add( "b" )
                .add( "c" );

            assertFalse( vector.isEmpty() );
        }

        @Test
        public void givenNonEmptyVector_expectHasContentsToReturnTrue() {
            RRBVector<String> vector = new RRBVector<String>()
                .add( "a" )
                .add( "b" )
                .add( "c" );

            assertTrue( vector.hasContents() );
        }
    }


    @Nested
    public class GetHeadTestCases {
        @Test
        public void givenEmptyVector_callGetHead_expectNone() {
            RRBVector<String> vector = RRBVector.emptyVector();

            assertEquals( FPOption.none(), vector.getHead() );
        }

        @Test
        public void givenNonEmptyVector_callGetHead_expectSome() {
            RRBVector<String> vector = RRBVector.toVector( "a", "b", "c" );

            assertEquals( FPOption.of( "a" ), vector.getHead() );
        }

        @Test
        public void givenNonEmptyVectorWhereHeadIsNull_callGetHead_expectNone() {
            RRBVector<String> vector = RRBVector.toVector( null, "b", "c" );

            assertEquals( FPOption.none(), vector.getHead() );
        }
    }


    @Nested
    public class RemoveTestCases {
        @Test
        public void givenEmptyVector_removeMissingValue_expectNoChange() {
            RRBVector<String> originalVector = RRBVector.emptyVector();

            assertSame( originalVector, originalVector.remove( "a" ) );
        }

        @Test
        public void givenNonEmptyVector_removeMissingValue_expectNoChange() {
            RRBVector<String> originalVector = RRBVector.toVector( "a" );

            assertSame( originalVector, originalVector.remove( "b" ) );
        }

        @Test
        public void givenSingleValueVector_removeValue_expectEmptyVector() {
            RRBVector<String> originalVector = RRBVector.toVector( "a" );
            RRBVector<String> updatedVector = originalVector.remove( "a" );

            assertTrue( updatedVector.isEmpty() );
            assertEquals( RRBVector.emptyVector(), updatedVector );
        }

        @Test
        public void givenTwoValueVector_removeFirstValue_expectSingleValueVector() {
            RRBVector<String> originalVector = RRBVector.toVector( "a", "b" );
            RRBVector<String> updatedVector = originalVector.remove( "a" );

            assertTrue( updatedVector.hasContents() );
            assertEquals( RRBVector.toVector( "b" ), updatedVector );
        }

        @Test
        public void givenTwoValueVector_removeSecondValue_expectSingleValueVector() {
            RRBVector<String> originalVector = RRBVector.toVector( "a", "b" );
            RRBVector<String> updatedVector = originalVector.remove( "b" );

            assertTrue( updatedVector.hasContents() );
            assertEquals( RRBVector.toVector( "a" ), updatedVector );
        }

        @Test
        public void givenTwoDuplicateValueVector_removeValue_expectSingleValueVector() {
            RRBVector<String> originalVector = RRBVector.toVector( "a", "a" );
            RRBVector<String> updatedVector = originalVector.remove( "a" );

            assertTrue( updatedVector.hasContents() );
            assertEquals( RRBVector.toVector( "a" ), updatedVector );
        }
    }


    @Nested
    public class AddAllTestCases {
        @Test
        public void givenEmptyVector_callAddAllWithNoElements_expectNoChange() {
            RRBVector<String> originalVector = RRBVector.emptyVector();
            RRBVector<String> updatedVector = originalVector.addAll();

            assertSame( originalVector, updatedVector );
        }

        @Test
        public void givenEmptyVector_callAddAllWithTwoElements_expectTwoElementVector() {
            RRBVector<String> originalVector = RRBVector.emptyVector();
            RRBVector<String> updatedVector = originalVector.addAll( "a", "b" );

            assertEquals( 2, updatedVector.size() );
            assertEquals( FP.option( "a" ), updatedVector.get( 0 ) );
            assertEquals( FP.option( "b" ), updatedVector.get( 1 ) );
            assertEquals( "a, b", updatedVector.toString() );
        }

        @Test
        public void givenTwoElementVector_callAddAllWithNoElements_expectNoChange() {
            RRBVector<String> originalVector = RRBVector.toVector( "a", "b" );
            RRBVector<String> updatedVector = originalVector.addAll();

            assertSame( originalVector, updatedVector );
        }

        @Test
        public void givenTwoElementVector_callAddAllWithThreeElements_expectFiveElementVector() {
            RRBVector<String> originalVector = RRBVector.toVector( "a", "b" );
            RRBVector<String> updatedVector = originalVector.addAll( "c", "d", "e" );

            assertEquals( 5, updatedVector.size() );
            assertEquals( FP.option( "a" ), updatedVector.get( 0 ) );
            assertEquals( FP.option( "b" ), updatedVector.get( 1 ) );
            assertEquals( FP.option( "c" ), updatedVector.get( 2 ) );
            assertEquals( FP.option( "d" ), updatedVector.get( 3 ) );
            assertEquals( FP.option( "e" ), updatedVector.get( 4 ) );
            assertEquals( "a, b, c, d, e", updatedVector.toString() );
        }
    }

    private void assertAddingAndRetrivingNValues( int numValues ) {
        String[] array = generateRandomValues( numValues );

        RRBVector<String> vector = new RRBVector<>();

        for ( String v : array ) {
            vector = vector.add(v);
        }

        for ( int i=0; i<numValues; i++ ) {
            assertSame( array[i], vector.get(i).get() );
        }

        assertEquals( FP.emptyOption(), vector.get(numValues) );
    }

    private String[] generateRandomValues( int numValues ) {
        return Stream.generate(() -> Long.toString( ThreadLocalRandom.current().nextLong()))
            .limit( numValues )
            .toArray(String[]::new );
    }
}
