package mosaics.fp.collections;

import mosaics.lang.functions.BooleanFunction1;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 *
 */
public abstract class ConsListSharedTestsBase {



// skipForwardUntil

    @Test
    public void skipForwardUntil_givenEmptyList_predAlwaysReturnsTrue_expectEmptyList() {
        ConsList<String> list = createList();

        ConsList<String> result = list.skipForwardUntil( v -> true );
        assertTrue( result.isEOL() );
    }

    @Test
    public void skipForwardUntil_givenEmptyList_predAlwaysReturnsFalse_expectEmptyList() {
        ConsList<String> list = createList();

        ConsList<String> result = list.skipForwardUntil( v -> false );
        assertTrue( result.isEOL() );
    }

    @Test
    public void skipForwardUntil_givenNonEmptyList_predReturnsTrueOnFirstElement_expectUnmodifiedListBack() {
        ConsList<String> list = createList("a", "b", "c", "d");

        ConsList<String> result = list.skipForwardUntil( v -> true );
        assertEquals( "a, b, c, d", result.mkString(", ") );
    }

    @Test
    public void skipForwardUntil_givenNonEmptyList_predAlwaysReturnsFalse_expectEmptyListBack() {
        ConsList<String> list = createList("a", "b", "c", "d");

        ConsList<String> result = list.skipForwardUntil( v -> false );

        assertTrue( result.isEOL() );
    }

    @Test
    public void skipForwardUntil_givenNonEmptyList_predReturnsTrueOnSecondElement_expectHeadToHaveBeenDropped() {
        ConsList<String> list = createList("a", "b", "c", "d");

        ConsList<String> result = list.skipForwardUntil( new BooleanFunction1<String>() {
            private int count = 0;
            public boolean invoke( String arg ) {
                count++;

                return count == 2;
            }
        } );

        assertEquals( "b, c, d", result.mkString(", ") );
    }

    @Test
    public void skipForwardUntil_givenNonEmptyList_predReturnsTrueOnThirdElement_expectFirstTwoElementsToHaveBeenDropped() {
        ConsList<String> list = createList("a", "b", "c", "d");

        ConsList<String> result = list.skipForwardUntil( new BooleanFunction1<String>() {
            private int count = 0;
            public boolean invoke( String arg ) {
                count++;

                return count == 3;
            }
        } );

        assertEquals( "c, d", result.mkString(", ") );
    }



    protected abstract ConsList<String> createList( String...args );
}
