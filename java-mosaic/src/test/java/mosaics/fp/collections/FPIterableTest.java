package mosaics.fp.collections;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class FPIterableTest {

    @Nested
    public class ZipTestCases {
        @Test
        public void zipTwoListsTogether() {
            List<String> lhs = asList( "a", "b" );
            List<String> rhs = asList( "1", "2", "3" );

            List<Tuple2<String, String>> expected = asList(
                new Tuple2<>( "a", "1" ),
                new Tuple2<>( "b", "2" ),
                new Tuple2<>( null, "3" )
            );

            assertEquals( expected, FPIterable.wrap( lhs ).zip( rhs ).toList() );
        }

        @Test
        public void min() {
            assertEquals( FP.emptyOption(), FPIterator.wrap().min( Object::hashCode ) );
            assertEquals( FP.option( "a" ), FPIterator.wrap( "a", "bc" ).min( String::length ) );
            assertEquals( FP.option( "a" ), FPIterator.wrap( "bc", "a" ).min( String::length ) );
        }
    }

    @Nested
    public class PairsTestCases {
        @Test
        public void pairs() {
            assertEquals( Collections.emptyList(), FPIterable.wrapAll().pairs().toList() );
            assertEquals( Collections.emptyList(), FPIterable.wrapAll(1).pairs().toList() );
            assertEquals( Collections.singletonList(new Tuple2<>(1,2)), FPIterable.wrapAll(1,2).pairs().toList() );
            assertEquals( Collections.singletonList(new Tuple2<>(1,2)), FPIterable.wrapAll(1,2,3).pairs().toList() );
            assertEquals( Arrays.asList(new Tuple2<>(1,2),new Tuple2<>(3,4)), FPIterable.wrapAll(1,2,3,4).pairs().toList() );
        }
    }


    @Nested
    public class AndTestCases {
        @Test
        public void andTwoListsTogether() {
            List<String> lhs = asList( "a", "b" );
            List<String> rhs = asList( "1", "2", "3" );

            List<String> expected = asList( "a", "b", "1", "2", "3" );

            assertEquals( expected, FPIterable.wrap( lhs ).and( rhs ).toList() );
        }
    }


    @Nested
    public class FilterTestCases {
        @Test
        public void filterAList() {
            List<String> lhs = asList( "a", "b", "hello", "c", "world", "d" );

            List<String> expected = asList( "hello", "world" );

            assertEquals( expected, FPIterable.wrap( lhs ).filter( s -> s.length() > 1 ).toList() );
        }
    }

    @Nested
    public class DropWhileTestCases {
        @Test
        public void dropWhileAList() {
            List<String> lhs = asList( "a", "b", "hello", "c", "world", "d" );

            assertEquals( asList( "hello", "c", "world", "d" ), FPIterable.wrap( lhs ).dropWhile( s -> s.length() == 1 ).toList() );
            assertEquals( asList( "a", "b", "hello", "c", "world", "d" ), FPIterable.wrap( lhs ).dropWhile( s -> s.length() > 1 ).toList() );
            assertEquals( asList(), FPIterable.wrap( lhs ).dropWhile( s -> true ).toList() );
        }
    }

    @Nested
    public class DropTestCases {
        @Test
        public void dropWhileAList() {
            List<String> lhs = asList( "a", "b", "hello", "c", "world", "d" );

            assertEquals( asList( "b", "hello", "c", "world", "d" ), FPIterable.wrap(lhs).drop(1).toList() );
            assertEquals( asList( "hello", "c", "world", "d" ), FPIterable.wrap(lhs).drop(2).toList() );
            assertEquals( asList( "c", "world", "d" ), FPIterable.wrap(lhs).drop(3).toList() );
            assertEquals( asList(), FPIterable.wrap(lhs).drop(20).toList() );
        }
    }

    @Nested
    public class FlatMapTestCases {
        @Test
        public void flatMapAList() {
            List<String> lhs = asList( "", "12", "1", "" );

            List<Character> expected = asList( '1', '2', '1' );

            assertEquals( expected, FPIterable.wrap( lhs ).flatMap( s -> FPList.wrapList( toCharList( s.toCharArray() ) ) ).toList() );
        }
    }

    @Nested
    public class MapFirstTestCases {
        @Test
        public void mapFirst() {
            List<String> orig     = asList( "", "12", "12", "" );
            List<String> expected = asList( "", "24", "12", "" );

            assertEquals( expected, FPIterable.wrap(orig).mapFirst( s -> s.equals("12"), s -> "24" ).toList() );
        }
    }

    @Nested
    public class MapToDoubleTestCases {
        @Test
        public void mapFirst() {
            List<Double> orig = asList( 1.0, 2.0, 3.0 );

            assertEquals( 2.0, FPIterable.wrap(orig).mapToDouble().average() );
        }
    }


    @Nested
    public class MatchesTestCases {
        @Test
        public void matches() {
            List<String> lhs = asList( "", "12", "1", "" );


            assertTrue( FPIterable.wrap( lhs ).matches( "", "12", "1", "" ) );
            assertFalse( FPIterable.wrap( lhs ).matches( "12", "", "1", "" ) );
            assertFalse( FPIterable.wrap( lhs ).matches( "", "12", "1" ) );
            assertFalse( FPIterable.wrap( lhs ).matches( "", "12", "1", "", "" ) );
            assertFalse( FPIterable.wrap( lhs ).matches() );
        }
    }

    @Nested
    public class IfEveryTestCases {
        @Test
        public void ifEvery() {
            assertTrue( FPIterable.wrapAll().isEvery( v -> false ) );
            assertTrue( FPIterable.wrapAll().isEvery( v -> true ) );

            assertFalse( FPIterable.wrapAll( "abc" ).isEvery( v -> v.length() == 2 ) );
            assertTrue( FPIterable.wrapAll( "ab", "cd", "ef" ).isEvery( v -> v.length() == 2 ) );

            assertFalse( FPIterable.wrapAll( "aba", "cd", "ef" ).isEvery( v -> v.length() == 2 ) );
            assertFalse( FPIterable.wrapAll( "ab", "cdb", "ef" ).isEvery( v -> v.length() == 2 ) );
            assertFalse( FPIterable.wrapAll( "ab", "cd", "efc" ).isEvery( v -> v.length() == 2 ) );
        }
    }


    @Nested
    public class MinMaxTestCases {
        @Test
        public void min() {
            assertEquals( FP.emptyOption(), FP.<String>wrapAll().min() );
            assertEquals( FP.option("a"), FP.wrapAll("a","b","c").min() );
            assertEquals( FP.option("a"), FP.wrapAll("a","bb","c").min() );
            assertEquals( FP.option("a"), FP.wrapAll("a","c","bb").min() );
            assertEquals( FP.option("aaa"), FP.wrapAll("aaa","bb","ccc").min() );

            assertEquals( FP.emptyOption(), FP.<String>wrapAll().min(String::length) );
            assertEquals( FP.option("a"), FP.wrapAll("a","b","c").min(String::length) );
            assertEquals( FP.option("a"), FP.wrapAll("a","bb","c").min(String::length) );
            assertEquals( FP.option("bb"), FP.wrapAll("aaa","bb","ccc").min(String::length) );
        }

        @Test
        public void max() {
            assertEquals( FP.emptyOption(), FP.<String>wrapAll().max() );
            assertEquals( FP.option("c"), FP.wrapAll("a","b","c").max() );
            assertEquals( FP.option("c"), FP.wrapAll("a","bb","c").max() );
            assertEquals( FP.option("c"), FP.wrapAll("a","c","bb").max() );

            assertEquals( FP.emptyOption(), FP.<String>wrapAll().max(String::length) );
            assertEquals( FP.option("a"), FP.wrapAll("a","b","c").max(String::length) );
            assertEquals( FP.option("bb"), FP.wrapAll("a","bb","c").max(String::length) );
        }
    }

    @Nested
    public class FlattenTestCases {
        @Test
        public void flatten() {
            assertEquals(
                FPIterable.wrapAll( "A", "B", "C" ),
                FPIterable.wrapAll( FPOption.of( "A" ), FPOption.of( "B" ), FPOption.of( "C" ) ).flatten()
            );

            assertEquals(
                FPIterable.wrapAll( "B", "C" ),
                FPIterable.wrapAll( FPOption.none(), FPOption.of( "B" ), FPOption.of( "C" ) ).flatten()
            );

            assertEquals(
                FPIterable.wrapAll( "A", "C" ),
                FPIterable.wrapAll( FPOption.of( "A" ), FPOption.none(), FPOption.of( "C" ) ).flatten()
            );

            assertEquals(
                FPIterable.wrapAll( "A", "B" ),
                FPIterable.wrapAll( FPOption.of( "A" ), FPOption.of( "B" ), FPOption.none() ).flatten()
            );

            assertEquals(
                FPIterable.wrapAll( "A", "B", "C" ),
                FPIterable.wrapAll( Arrays.asList( "A", "B" ), Collections.emptyList(), Collections.singletonList( "C" ) ).flatten()
            );

            assertEquals(
                FPIterable.wrapAll(),
                FPIterable.wrapAll().flatten()
            );

            assertEquals(
                FPIterable.wrapAll( "A", "C" ),
                FPIterable.wrapAll( Optional.of( "A" ), Optional.empty(), Optional.of( "C" ), null ).flatten()
            );

            assertEquals(
                FPIterable.wrapAll( "A", "C" ),
                FPIterable.wrapAll( new Object[]{"A", "C"} ).flatten()
            );

            assertEquals(
                FPIterable.wrapAll(),
                FPIterable.wrapAll( new Object[]{} ).flatten()
            );

            assertEquals(
                FPIterable.wrapAll(),
                FPIterable.wrapAll( FP.option( new Object[]{} ) ).flatten()
            );
        }
    }


    @Nested
    public class CompactTestCases {
        @Test
        public void givenEmptyIterable_expectEmptyResult() {
            FPIterable<String> orig     = FPIterable.wrapAll();
            FPIterable<Object> expected = FP.wrapAll();
            FPIterable<String> actual   = orig.compact( ( a, b ) -> true, ( a, b ) -> a + b );

            assertEquals( expected, actual );
        }

        @Test
        public void given3ElementsAndPredicateAlwaysReturnsFalse_expectThreeElementsBackUntouched() {
            FPIterable<String> orig     = FPIterable.wrapAll("a","b","c");
            FPIterable<Object> expected = FP.wrapAll("a","b","c");
            FPIterable<String> actual   = orig.compact( ( a, b ) -> false, ( a, b ) -> a + b );

            assertEquals( expected, actual );
        }

        @Test
        public void given3ElementsAndPredicateAlwaysReturnsTrue_expectOneElementBackWhichIsTheMergeOfAllThreeElements() {
            FPIterable<String> orig     = FPIterable.wrapAll("a","b","c");
            FPIterable<Object> expected = FP.wrapAll("abc");
            FPIterable<String> actual   = orig.compact( ( a, b ) -> true, ( a, b ) -> a + b );

            assertEquals( expected, actual );
        }

        @Test
        public void given3ElementsMergeStringLessThanTwoCharsLong() {
            FPIterable<String> orig     = FPIterable.wrapAll("a","b","c");
            FPIterable<Object> expected = FP.wrapAll("ab","c");
            FPIterable<String> actual   = orig.compact( (a,b) -> a.length()+b.length()<=2, (a,b) -> a + b );

            assertEquals( expected, actual );
        }

        @Test
        public void given4ElementsMergeStringLessThanTwoCharsLong() {
            FPIterable<String> orig     = FPIterable.wrapAll("a","b","c","d");
            FPIterable<Object> expected = FP.wrapAll("ab","cd");
            FPIterable<String> actual   = orig.compact( (a,b) -> a.length()+b.length()<=2, (a,b) -> a + b );

            assertEquals( expected, actual );
        }
    }

    @Nested
    public class GenerateStreamsTestCases {
        @Test
        public void simpleSeriesWhereNextValueIsGeneratedDirectlyFromThePreviousValue() {
            FPIterable<Long> stream = FPIterable.generate( 1L, x -> FPOption.of(x*2) );

            assertEquals( FP.wrapAll(1L,2L,4L,8L,16L), stream.limit(5) );
        }

        @Test
        public void generatorHasItsOwnMutableState() {
            AtomicLong       state  = new AtomicLong();
            FPIterable<Long> stream = FPIterable.generate( () -> FPOption.of(state.incrementAndGet()) );

            assertEquals( FP.wrapAll(1L,2L,3L,4L,5L), stream.limit(5) );
        }

        @Test
        public void genFibSequenceExample() {
            FPIterable<Long> stream = FPIterable.generate(
                new Tuple2<>(0L,1L),
                t -> FPOption.of(new Tuple2<>(t.getSecond(), t.getFirst()+t.getSecond())),
                Tuple2::getSecond
            );

            assertEquals( FP.wrapAll(1L,1L,2L,3L,5L), stream.limit(5) );
        }
    }


    @Test
    public void testLimit() {
        assertEquals( FP.wrapAll(), FP.wrapAll(1,2,3,4,5).limit(0) );
        assertEquals( FP.wrapAll(1), FP.wrapAll(1,2,3,4,5).limit(1) );
        assertEquals( FP.wrapAll(1,2), FP.wrapAll(1,2,3,4,5).limit(2) );
        assertEquals( FP.wrapAll(1,2,3), FP.wrapAll(1,2,3,4,5).limit(3) );
        assertEquals( FP.wrapAll(1,2,3,4), FP.wrapAll(1,2,3,4,5).limit(4) );
        assertEquals( FP.wrapAll(1,2,3,4,5), FP.wrapAll(1,2,3,4,5).limit(5) );
        assertEquals( FP.wrapAll(1,2,3,4,5), FP.wrapAll(1,2,3,4,5).limit(6) );
        assertEquals( FP.wrapAll(1,2,3,4,5), FP.wrapAll(1,2,3,4,5).limit(7) );

        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> FP.wrapAll(1,2,3,4,5).limit(-1));
        assertEquals( "'maxNumElements' (-1) must be >= 0", ex.getMessage() );
    }

    private static List<Character> toCharList( char[] chars ) {
        List<Character> list = new ArrayList<>();

        for ( char c : chars ) {
            list.add(c);
        }

        return list;
    }

}
