package mosaics.fp.collections;

import mosaics.utils.SetUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Value;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class FPListTest {

    @Test
    public void testPermutations() {
        Set<String> expectedSet1 = SetUtils.asSet(
            Arrays.asList(1).toString()
        );

        Set<String> expectedSet2 = SetUtils.asSet(
            Arrays.asList(1, 2).toString(),
            Arrays.asList(2, 1).toString()
        );

        Set<String> expectedSet3 = SetUtils.asSet(
            Arrays.asList(1, 2, 3).toString(),
            Arrays.asList(1, 3, 2).toString(),
            Arrays.asList(2, 1, 3).toString(),
            Arrays.asList(2, 3, 1).toString(),
            Arrays.asList(3, 1, 2).toString(),
            Arrays.asList(3, 2, 1).toString()
        );

        assertEquals( expectedSet1, FPList.wrapArray(1).permutations().map( FPList::toString).toSet() );
        assertEquals( expectedSet2, FPList.wrapArray(1,2).permutations().map( FPList::toString).toSet() );
        assertEquals( expectedSet3, FPList.wrapArray(1,2,3).permutations().map( FPList::toString).toSet() );

        assertEquals( 24, FPList.wrapArray(1,2,3,4).permutations().count() );
        assertEquals( 120, FPList.wrapArray(1,2,3,4,5).permutations().count() );
        assertEquals( 720, FPList.wrapArray(1,2,3,4,5,6).permutations().count() );
        assertEquals( 5040, FPList.wrapArray(1,2,3,4,5,6,7).permutations().count() );
    }


    @Nested
    public class LazyTestCases {
        @Test
        public void givenFlatMapCall_expectCallsToNotBeInvokedOnAAsIterableIsLazy() {
            A a1 = new A( "1" );
            A a2 = new A( "2" );

            FPList.wrapArray(a1,a2).flatMap( A::toB );

            assertEquals( 0, a1.count );
            assertEquals( 0, a2.count );
        }

        @Test
        public void givenFlatMapCall_materialiseList_expectCallsToMadeToA() {
            A a1 = new A( "1" );
            A a2 = new A( "2" );

            List<B> actual = FPList.wrapArray(a1,a2).flatMap( A::toB ).toList();

            assertEquals( 1, a1.count );
            assertEquals( 1, a2.count );

            assertEquals( Arrays.asList(new B("1"), new B("2")), actual );
        }

        @Test
        public void givenFlatMapCall_materialiseArray_expectCallsToMadeToA() {
            A a1 = new A( "1" );
            A a2 = new A( "2" );

            Object[] actual = FPList.wrapArray(a1,a2).flatMap( A::toB ).toArray();

            assertEquals( 1, a1.count );
            assertEquals( 1, a2.count );

            assertArrayEquals( new Object[] {new B("1"), new B("2")}, actual );
        }
    }


    @Getter
    @EqualsAndHashCode
    public static class A {
        private String value;
        private int    count;

        public A( String value ) {
            this.value = value;
        }

        public FPIterable<B> toB() {
            count += 1;

            return FPList.wrapArray( new B(value) );
        }
    }

    @Value
    public static class B {
        private String value;
    }
}
