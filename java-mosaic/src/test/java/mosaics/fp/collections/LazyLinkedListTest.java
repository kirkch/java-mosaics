package mosaics.fp.collections;

import mosaics.lang.functions.Function0;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LazyLinkedListTest extends ConsListSharedTestsBase {

    @Test
    public void givenFactoryThatReturnsNoneStraightAway_expectEmptySequence() {
        ConsList<Long> stream = new LazyLinkedList<>( FPOption::none );

        assertTrue( stream.isEOL() );
    }

    @Test
    public void givenFactoryThatOneValue_expectOneElementList() {
        ConsList<Long> stream = new LazyLinkedList<>( new Function0<FPOption<Long>>() {
            private long v;

            public FPOption<Long> invoke() {
                if ( v == 0 ) {
                    return FPOption.of( v++ );
                } else {
                    return FPOption.none();
                }
            }
        } );

        assertFalse( stream.isEOL() );
        assertEquals( 0L, stream.getHead().longValue() );
        assertTrue( stream.getTail().isEOL() );
    }

    @Test
    public void givenInfiniteStream_pullFirstFourValues() {
        ConsList<Long> stream = new LazyLinkedList<>( new Function0<FPOption<Long>>() {
            private long v;

            public FPOption<Long> invoke() {
                return FPOption.of(v++);
            }
        } );

        assertFalse( stream.isEOL() );
        assertEquals( 0L, stream.getHead().longValue() );
        assertEquals( 1L, stream.getTail().getHead().longValue() );
        assertEquals( 0L, stream.getHead().longValue() );
        assertEquals( 2L, stream.getTail().getTail().getHead().longValue() );
        assertEquals( 3L, stream.getTail().getTail().getTail().getHead().longValue() );
        assertEquals( 1L, stream.getTail().getHead().longValue() );
        assertFalse( stream.isEOL() );
    }


    protected ConsList<String> createList( String...args ) {
        return new LazyLinkedList<>( new Function0<FPOption<String>>() {
            private Iterator<String> it = Arrays.asList(args).iterator();

            public FPOption<String> invoke() {
                return it.hasNext() ? FPOption.of(it.next()) : FPOption.none();
            }
        } );
    }

}
