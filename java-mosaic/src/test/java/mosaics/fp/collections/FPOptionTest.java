package mosaics.fp.collections;

import mosaics.fp.FP;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;


public class FPOptionTest {

    @Test
    public void givenSomeMapToSameValue_expectExactSameOptionBack() {
        FPOption<String> option = FP.option("hello world");

        assertSame(option, option.map(v -> v));
        assertNotSame(option, option.map(v -> new String(v)));
    }

    @Test
    public void flatMapSomeToFPIterableContainingTwoElementsAfterConvertingOptionToAnIterable_expectFPIterableContainingTwoElements() {
        FPIterable<String> actual   = FPOption.of("a").toIterable().flatMap( x -> FPIterable.wrapAll(x,x+x) );
        FPIterable<String> expected = FPIterable.wrapAll( "a", "aa" );

        assertEquals( expected, actual );
    }

    @Test
    public void flatMapSomeToFPIterableContainingTwoElements_expectSomeContainingTheFirstElement() {
        FPIterable<String> actual   = FPOption.of("a").flatMap( x -> FPIterable.wrapAll(x,x+x) );
        FPOption<String>   expected = FPOption.of( "a" );

        assertEquals( expected, actual );
    }

}
