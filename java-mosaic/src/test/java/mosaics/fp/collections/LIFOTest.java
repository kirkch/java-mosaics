package mosaics.fp.collections;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LIFOTest {

    @Test
    public void givenEmptyStack_callIsEmpty_expectTrue() {
        LIFO<String> q = new LIFO<>();

        assertTrue( q.isEmpty() );
    }

    @Test
    public void givenEmptyStack_callHasContents_expectFalse() {
        LIFO<String> q = new LIFO<>();

        assertFalse( q.hasContents() );
    }

    @Test
    public void givenStackWithOneElement_callIsEmpty_expectFalse() {
        LIFO<String> q = new LIFO<>();

        q.push("1");

        assertFalse( q.isEmpty() );
    }

    @Test
    public void givenStackWithOneElement_callHasContents_expectTrue() {
        LIFO<String> q = new LIFO<>();

        q.push("1");

        assertTrue( q.hasContents() );
    }

    @Test
    public void givenTwoElements_pushAndPop() {
        LIFO<String> q = new LIFO<>();

        q.push("1");
        q.push("2");

        assertEquals( "2", q.pop() );
        assertEquals( "1", q.pop() );

        assertFalse( q.hasContents() );
    }

}
