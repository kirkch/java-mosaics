package mosaics.io;

import mosaics.fp.FP;
import mosaics.junit.JMFileExtension;
import mosaics.lang.RandomUtils;
import mosaics.utils.OSUtils;
import mosaics.lang.lifecycle.Subscription;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Isolated;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


@Isolated
@ExtendWith(JMFileExtension.class)
public class LockFileTest {

    public  File     dir;
    public  File     expectedUnderlyingFile;

    private String   name = RandomUtils.randomString( "^[a-z][a-z0-9]*$", 10 );
    private LockFile lock;
    private String   currentPID = Integer.toString( OSUtils.getCurrentPid() );




    @BeforeEach
    public void setup() {
        lock                   = new LockFile( dir, name );
        expectedUnderlyingFile = new File( dir, name+".lock" );
    }


    @Nested
    public class InprocessTestCases {
        private Subscription lockSubscription;

        @AfterEach
        public void tearDown() {
            if ( lockSubscription != null ) {
                lockSubscription.cancel();

                lockSubscription = null;
            }
        }

        @Test
        public void givenNoLock_callIsAvailable_expectTrue() {
            assertTrue( lock.isAvailable() );
            assertTrue( lock.isAvailable() );
        }

        @Test
        public void givenNoLock_takeLock_expectNewFileToAppear() throws IOException {
            lockSubscription = lock.lock();

            assertNotNull( lockSubscription );

            assertEquals( 1, requireNonNull( dir.listFiles() ).length );
            assertLockFileContents( currentPID );
        }

        @Test
        public void givenNoLock_takeLockInDirectoryThatDoesNotExist_expectNewFileToAppear() throws IOException {
            dir = new File( dir, "subdir" );
            lock = new LockFile( dir, name );
            expectedUnderlyingFile = new File( dir, name + ".lock" );

            lockSubscription = lock.lock();
            assertNotNull( lockSubscription );

            assertEquals( 1, requireNonNull( dir.listFiles() ).length );
            assertLockFileContents( currentPID );
        }

        @Test
        public void givenValidLock_isAvailable_expectFalse() {
            lockSubscription = lock.lock();

            assertFalse( lock.isAvailable() );
        }

        @Test
        public void givenValidLock_takeLock_expectFailure() {
            lockSubscription = lock.lock();


            assertEquals( FP.emptyOption(), lock.tryLock() );
        }

        @Test
        public void givenLockContainingInvalidPID_takeLock_expectSuccessfulLockAndFilePIDToBeUpdated() throws IOException {
            FileUtils.writeTextTo( expectedUnderlyingFile, Integer.toString( Integer.MAX_VALUE ) );

            lockSubscription = lock.lock();

            assertLockFileContents( currentPID );
        }

        @Test
        public void givenLockContainingInvalidPID_callIsAvailable_expectTrue() {
            FileUtils.writeTextTo( expectedUnderlyingFile, Integer.toString( Integer.MAX_VALUE ) );

            assertTrue( lock.isAvailable() );
        }

        @Test
        public void givenValidLock_cancelLock_expectLockFileToBeRemoved() {
            lockSubscription = lock.lock();

            lockSubscription.cancel();

            assertFalse( expectedUnderlyingFile.exists() );
        }

        @Test
        public void givenReleasedLock_takeLock_expectSuccess() {
            lockSubscription = lock.lock();

            lockSubscription.cancel();

            lockSubscription = lock.lock();

            assertTrue( expectedUnderlyingFile.exists() );
        }
    }


    private void assertLockFileContents( String currentPID ) throws IOException {
        assertTrue( expectedUnderlyingFile.exists() );

        String fileContents = Files.lines(expectedUnderlyingFile.toPath()).collect( Collectors.joining());

        assertEquals( currentPID, fileContents.trim() );
    }

}
