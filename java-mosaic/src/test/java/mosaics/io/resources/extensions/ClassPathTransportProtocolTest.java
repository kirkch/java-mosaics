package mosaics.io.resources.extensions;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SuppressWarnings("WeakerAccess")
public class ClassPathTransportProtocolTest {

    @Test
    public void givenMissingDirectory_fetchChildFiles_expectNoChildren() {
        List<String> actual   = ClassPathTransportProtocol.INSTANCE.fetchChildFiles( "classpath:///mosaics/io/resources/extensions/missing" );
        List<String> expected = Collections.emptyList();

        assertEquals( expected, actual );
    }

    @Test
    public void givenEmptyDirectory_fetchChildFiles_expectNoChildren() {
        List<String> actual = ClassPathTransportProtocol.INSTANCE.fetchChildFiles( "classpath:///mosaics/io/resources/extensions/empty" );

        List<String> expected = Collections.emptyList();

        assertEquals( expected, actual );
    }

    @Test
    public void givenDirectoryContainingOneFile_fetchChildFiles_expectOneChild() {
        List<String> actual = ClassPathTransportProtocol.INSTANCE.fetchChildFiles( "classpath:mosaics/io/resources/extensions/one_file" );

        List<String> expected = Collections.singletonList(
            "classpath:mosaics/io/resources/extensions/one_file/foo.txt"
        );

        assertEquals( expected, actual );
    }

    @Test
    public void givenDirectoryContainingTwoFiles_fetchChildrenUrls_expectTwoChildren() {
        List<String> actual = ClassPathTransportProtocol.INSTANCE.fetchChildFiles( "classpath:///mosaics/io/resources/extensions/two_files" );

        Collections.sort( actual );


        List<String> expected = Arrays.asList(
            "classpath:///mosaics/io/resources/extensions/two_files/foo1.txt",
            "classpath:///mosaics/io/resources/extensions/two_files/foo2.txt"
        );

        assertEquals( expected, actual );
    }

    @Test
    public void givenDirectoryContainingTwoFilesAndADirectory_fetchChildFiles_expectTwoChildren() {
        List<String> actual = ClassPathTransportProtocol.INSTANCE.fetchChildFiles( "classpath:///mosaics/io/resources/extensions/two_files_and_one_dir" );

        Collections.sort( actual );


        List<String> expected = Arrays.asList(
            "classpath:///mosaics/io/resources/extensions/two_files_and_one_dir/foo1.txt",
            "classpath:///mosaics/io/resources/extensions/two_files_and_one_dir/foo2.txt"
        );

        assertEquals( expected, actual );
    }

    @Test
    public void givenDirectoryContainingTwoFilesAndADirectory_fetchChildrenUrls_expectOneChildren() {
        List<String> actual = ClassPathTransportProtocol.INSTANCE.fetchChildDirectories( "classpath:///mosaics/io/resources/extensions/two_files_and_one_dir" );

        Collections.sort( actual );


        List<String> expected = Collections.singletonList(
            "classpath:///mosaics/io/resources/extensions/two_files_and_one_dir/foo_dir"
        );

        assertEquals( expected, actual );
    }

    @Test
    public void givenDirectoryWithinAJarFile_fetchChildrenUrls() {
        List<String> actual = ClassPathTransportProtocol.INSTANCE.fetchChildFiles( "classpath:///java/lang" );

        Collections.sort( actual );


        assertTrue( actual.contains("classpath:///java/lang/Boolean.class"), actual.toString() );
        assertFalse( actual.contains("classpath:///java/lang/reflect/WeakCache.class") );
    }

    @Test
    public void givenDirectoryWithinAJarFile_fetchChildrenUris() {
        List<String> actual = ClassPathTransportProtocol.INSTANCE.fetchChildFiles( "classpath:java/lang" );

        Collections.sort( actual );


        assertTrue( actual.contains("classpath:java/lang/Boolean.class"), actual.toString() );
        assertFalse( actual.contains("classpath:java/lang/reflect/WeakCache.class") );
    }

    @Test
    public void fetchChildFilesRecursively() {
        List<String> actual = ClassPathTransportProtocol.INSTANCE.fetchChildFilesRecursively( "classpath:mosaics/io/resources/extensions/recursive" );

        Collections.sort( actual );


        List<String> expected = Arrays.asList(
            "classpath:mosaics/io/resources/extensions/recursive/a.txt",
            "classpath:mosaics/io/resources/extensions/recursive/d1/b.txt",
            "classpath:mosaics/io/resources/extensions/recursive/d2/d3/c.txt",
            "classpath:mosaics/io/resources/extensions/recursive/d2/d3/d.txt"
        );

        assertEquals( expected, actual );
    }


}
