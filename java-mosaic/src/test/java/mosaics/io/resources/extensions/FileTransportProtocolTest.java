package mosaics.io.resources.extensions;

import mosaics.junit.JMFileExtension;
import mosaics.junit.JMFileExtension.DirectoryContents;
import mosaics.junit.JMFileExtension.FileContents;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Isolated;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@Isolated
@ExtendWith( JMFileExtension.class )
@SuppressWarnings("ResultOfMethodCallIgnored")
public class FileTransportProtocolTest {

    @Test
    public void givenMissingDirectory_fetchChildFiles_expectNoChildren( File tmpDir ) {
        List<String> actual = FileTransportProtocol.INSTANCE.fetchChildFiles( "file://"+tmpDir.getAbsolutePath()+"/missing" );

        List<String> expected = Collections.emptyList();

        assertEquals( expected, actual );
    }

    @Test
    public void givenEmptyDirectory_fetchChildFiles_expectNoChildren( File tmpDir ) {
        List<String> actual = FileTransportProtocol.INSTANCE.fetchChildFiles( "file://"+tmpDir.getAbsolutePath() );

        List<String> expected = Collections.emptyList();

        assertEquals( expected, actual );
    }

    @Test
    public void givenDirectoryContainingOneFile_fetchChildFiles_expectOneChild(
        @DirectoryContents({
            @FileContents(name="foo.txt", lines={"abc"})
        })
            File tmpDir
    ) {
        String parentPath = "file://" + tmpDir.getAbsolutePath();
        List<String> actual = FileTransportProtocol.INSTANCE.fetchChildFiles( parentPath );

        List<String> expected = Collections.singletonList(parentPath+"/foo.txt");

        assertEquals( expected, actual );
    }

    @Test
    public void givenDirectoryContainingTwoFiles_fetchChildFiles_expectTwoChildren(
        @DirectoryContents({
            @FileContents(name="foo1.txt", lines={"abc"}),
            @FileContents(name="foo2.txt", lines={"abc"})
        })
            File tmpDir
    ) {
        String       parentPath = "file://" + tmpDir.getAbsolutePath();
        List<String> actual     = FileTransportProtocol.INSTANCE.fetchChildFiles( parentPath );

        Collections.sort( actual );


        List<String> expected = Arrays.asList(
            parentPath + "/foo1.txt",
            parentPath + "/foo2.txt"
        );

        assertEquals( expected, actual );
    }

    @Test
    public void givenDirectoryContainingTwoFilesAndADirectory_fetchChildFiles_expectTwoFiles(
        @DirectoryContents({
            @FileContents(name="foo1.txt", lines={"abc"}),
            @FileContents(name="foo2.txt", lines={"abc"}),
            @FileContents(name="foo_dir/a.txt", lines={"123"})
        }) File tmpDir
    ) {
        String parentPath = "file://" + tmpDir.getAbsolutePath();
        List<String> actual = FileTransportProtocol.INSTANCE.fetchChildFiles( parentPath );

        Collections.sort( actual );


        List<String> expected = Arrays.asList(
            parentPath + "/foo1.txt",
            parentPath + "/foo2.txt"
        );

        assertEquals( expected, actual );
    }

    @Test
    public void givenDirectoryContainingTwoFilesAndADirectory_fetchChildDirectories_expectOneDirectory(
        @DirectoryContents({
            @FileContents(name="foo1.txt", lines={"abc"}),
            @FileContents(name="foo2.txt", lines={"abc"}),
            @FileContents(name="foo_dir/a.txt", lines={"123"})
        }) File tmpDir
    ) {
        String parentPath = "file://" + tmpDir.getAbsolutePath();
        List<String> actual = FileTransportProtocol.INSTANCE.fetchChildDirectories( parentPath );

        Collections.sort( actual );


        List<String> expected = Collections.singletonList(
            parentPath + "/foo_dir"
        );

        assertEquals( expected, actual );
    }

    @Test
    public void givenDirectoryContainingTwoFilesAndADirectory_fetchChildFilesRecursively_expectThreeFiles(
        @DirectoryContents({
            @FileContents(name="foo1.txt", lines={"abc"}),
            @FileContents(name="foo2.txt", lines={"abc"}),
            @FileContents(name="foo_dir/a.txt", lines={"123"})
        }) File tmpDir
    ) {
        String parentPath = "file://" + tmpDir.getAbsolutePath();
        List<String> actual = FileTransportProtocol.INSTANCE.fetchChildFilesRecursively( parentPath );

        Collections.sort( actual );


        List<String> expected = Arrays.asList(
            parentPath + "/foo1.txt",
            parentPath + "/foo2.txt",
            parentPath + "/foo_dir/a.txt"
        );

        assertEquals( expected, actual );
    }

}
