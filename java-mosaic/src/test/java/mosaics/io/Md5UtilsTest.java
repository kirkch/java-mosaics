package mosaics.io;

import mosaics.io.Md5Utils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class Md5UtilsTest {

    @Test
    public void testCalcMd5For() {
        assertEquals( "0b8f345ae1836b1da53eb1bb2e169605", Md5Utils.calcMd5For("The desolation of Smaug.") );
        assertEquals( "0b8f345ae1836b1da53eb1bb2e169605", Md5Utils.calcMd5For("The desolation of Smaug.") );
        assertEquals( "37d6e40befe70b8568bcb5ea3e805db4", Md5Utils.calcMd5For("Nothing is impossible.  The word itself says 'I'm possible'.") );
        assertEquals( "d6b61ed68998f64a6e12cfde7d9d0a04", Md5Utils.calcMd5For("Whether you think you can, or you think that you cannot.  You are right.") );
        assertEquals( "c7118aec2f4b1adeb0880c4ad8dbfa52", Md5Utils.calcMd5For("Life is 10% what happens to me and 90% of how I react to it") );
        assertEquals( "465480f532d578edae0a8902c84441e6", Md5Utils.calcMd5For("Strive not to be a success, but rather to be of value.") );
    }

}
