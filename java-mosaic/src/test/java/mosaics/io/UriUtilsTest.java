package mosaics.io;

import mosaics.io.UriUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class UriUtilsTest {

    @Test
    public void testExtractPath() {
        assertArrayEquals( new String[] {"a","b","c"}, UriUtils.extractPathArrayFrom("classpath:a/b/c") );
        assertArrayEquals( new String[] {"a","b","c"}, UriUtils.extractPathArrayFrom("classpath:/a/b/c") );
        assertArrayEquals( new String[] {"b","c"}, UriUtils.extractPathArrayFrom("classpath://a/b/c") );
        assertArrayEquals( new String[] {"a","b","c"}, UriUtils.extractPathArrayFrom("classpath:///a/b/c") );
    }

    @Test
    public void testExtractFileNameFrom() {
        assertEquals( "c.txt", UriUtils.extractFileNameFrom("http://foo/a/b/c.txt") );
        assertEquals( "b", UriUtils.extractFileNameFrom("http://foo/a/b") );
        assertEquals( "b", UriUtils.extractFileNameFrom("http://foo/a/b/") );
        assertEquals( "foo", UriUtils.extractFileNameFrom("http:///foo") );
        assertEquals( "foo", UriUtils.extractFileNameFrom("http:///foo/") );
        assertEquals( "foo", UriUtils.extractFileNameFrom("http:foo") );
        assertEquals( "bar", UriUtils.extractFileNameFrom("http:foo/bar") );
        assertEquals( "bar", UriUtils.extractFileNameFrom("http:foo/bar//") );
        assertEquals( "..", UriUtils.extractFileNameFrom("http:foo/bar/../") );

        assertNull( UriUtils.extractFileNameFrom("http://foo") );
        assertNull( UriUtils.extractFileNameFrom("http://foo/") );
    }

}
