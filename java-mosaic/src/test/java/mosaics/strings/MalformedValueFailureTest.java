package mosaics.strings;

import mosaics.fp.FP;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class MalformedValueFailureTest {

    @Test
    public void givenNoValidValue_expectNoValidValue() {
        MalformedValueFailure f = new MalformedValueFailure("v");

        assertEquals( "v", f.getValue() );
        assertEquals( FP.emptyOption(), f.getValidValues() );
    }

    @Test
    public void givenNoValidValue_callToException_expectException() {
        MalformedValueFailure    f  = new MalformedValueFailure("v");
        IllegalArgumentException ex = (IllegalArgumentException) f.toException();

        assertEquals( "'v' is malformed.", ex.getMessage() );
    }

    @Test
    public void givenValidValue_callToException_expectException() {
        MalformedValueFailure    f  = new MalformedValueFailure("v", "s");
        IllegalArgumentException ex = (IllegalArgumentException) f.toException();

        assertEquals( "'v' is malformed. Valid values: s", ex.getMessage() );
    }

}
