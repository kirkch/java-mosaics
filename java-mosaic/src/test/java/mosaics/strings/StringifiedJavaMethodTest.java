package mosaics.strings;

import mosaics.fp.FP;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPOption;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaMethod;
import mosaics.lang.reflection.JavaParameter;
import mosaics.lang.reflection.ReflectionException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.fail;


public class StringifiedJavaMethodTest {
    public static final JavaClass TESTSERVICE_JC = JavaClass.of( TestService.class );
    private StringCodecs codecs = StringCodecs.DEFAULT_CODECS;


    @Nested
    public class NonVarArgTestCases {
        @Test
        public void callGetParameterTypes_expectAllStringParts() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( String.class, "intFloatMethod", int.class, float.class ).get();
            StringifiedJavaMethod m = (StringifiedJavaMethod) codecs.stringify( origMethod );

            assertArrayEquals( new Object[]{JavaClass.STRING, JavaClass.STRING}, m.getParameterTypes().toArray() );
        }

        @Test
        public void callGetDeclaringClass_expectUnsupportedOpException() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( String.class, "intFloatMethod", int.class, float.class ).get();
            StringifiedJavaMethod m = (StringifiedJavaMethod) codecs.stringify( origMethod );


            assertEquals(TESTSERVICE_JC, m.getDeclaringClass());
        }

        @Test
        public void callToJdk_expectNone() {
            JavaMethod            origMethod = TESTSERVICE_JC.getStaticMethod( String.class, "intFloatMethod", int.class, float.class ).get();
            StringifiedJavaMethod m          = (StringifiedJavaMethod) codecs.stringify( origMethod );

            assertEquals( FP.emptyOption(), m.getJdkMethod() );
        }

        @Test
        public void givenNoArgMethod_stringify_expectSameMethodBack() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( Void.TYPE, "noArgMethod" ).get();

            assertSame( origMethod, codecs.stringify( origMethod ) );
        }

        @Test
        public void givenStringArgsMethod_stringify_expectSameMethodBack() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( Void.TYPE, "stringArgsMethod", String.class, String.class ).get();

            assertSame( origMethod, codecs.stringify( origMethod ) );
        }

        @Test
        public void givenFPOptionStringArgsMethod_stringify_invokeMethodAndEnsureArgIsAsExpected() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( Void.TYPE, "fpOptionStringArgsMethod", FPOption.class ).get();
            JavaMethod stringifiedMethod = codecs.stringify( origMethod );

            TestService service = new TestService();
            assertEquals( FP.option( "hello" ), stringifiedMethod.invokeAgainst( service, "hello" ) );
            assertEquals( FP.emptyOption(), stringifiedMethod.invokeAgainst( service, new Object[]{null} ) );
        }

        @Test
        public void givenJdkOptionalStringArgsMethod_stringify_invokeMethodAndEnsureArgIsAsExpected() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( Void.TYPE, "jdkOptionalStringArgsMethod", Optional.class ).get();
            JavaMethod stringifiedMethod = codecs.stringify( origMethod );

            TestService service = new TestService();
            assertEquals( Optional.of( "hello" ), stringifiedMethod.invokeAgainst( service, "hello" ) );
            assertEquals( Optional.empty(), stringifiedMethod.invokeAgainst( service, new Object[]{null} ) );
        }

        @Test
        public void givenIntFloatMethod_stringify_expectMethodThatDecodesStringArgs() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( String.class, "intFloatMethod", int.class, float.class ).get();
            JavaMethod stringifiedMethod = codecs.stringify( origMethod );

            assertNotSame( origMethod, stringifiedMethod );

            assertEquals( "42:1.23", stringifiedMethod.invokeStaticAgainst( "42", "1.23" ) );
        }

        @Test
        public void invokeStringifiedMethodWithUnconvertableFirstArg_expectReflectionException() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( String.class, "intFloatMethod", int.class, float.class ).get();
            JavaMethod stringifiedMethod = codecs.stringify( origMethod );

            TestService app = new TestService();

            try {
                stringifiedMethod.invokeAgainst( app, "aaa", "10" );
                fail( "expected ReflectionException" );
            } catch ( ReflectionException ex ) {
                assertEquals( "Malformed value 'aaa' for arg 'a'", ex.getMessage() );
            }
        }

        @Test
        public void invokeStringifiedMethodWithUnconvertableSecondArg_expectReflectionException() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( String.class, "intFloatMethod", int.class, float.class ).get();
            JavaMethod stringifiedMethod = codecs.stringify( origMethod );

            TestService app = new TestService();

            try {
                stringifiedMethod.invokeAgainst( app, "10", "bbb" );
                fail( "expected ReflectionException" );
            } catch ( ReflectionException ex ) {
                assertEquals( "Malformed value 'bbb' for arg 'b'", ex.getMessage() );
            }
        }

        @Test
        public void givenIntFloatMethod_stringify_expectDocsToBeUpdated() {
            JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( String.class, "intFloatMethod", int.class, float.class ).get();
            JavaMethod stringifiedMethod = codecs.stringify( origMethod );

            List<JavaParameter> expected = Arrays.asList(
                new JavaParameter( stringifiedMethod, 0, JavaClass.STRING, "a", Collections.emptyList() ),
                new JavaParameter( stringifiedMethod, 1, JavaClass.STRING, "b", Collections.emptyList() )
            );

            assertArrayEquals( expected.toArray(), stringifiedMethod.getParameters().toArray() );
        }

        @Test
        public void givenMethodWithNonEncodableArg_stringify_expectException() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( String.class, "hasUnencodableArg", StringCodecsTest.UnencodableDTO.class ).get();

            try {
                codecs.stringify( origMethod );
                fail( "expected exception" );
            } catch ( CodecNotFoundException ex ) {
                assertEquals( "Unable to find StringCodec: 'mosaics.strings.StringCodecsTest$UnencodableDTO'", ex.getMessage() );
            }
        }
    }

    @Nested
    public class StringArrayArgTestCases {
        @Nested
        public class InvalidVarArgTestCases {
            @Test
            public void varArgBeforeBooleanArg_expectException() {
                JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( String.class, "varArgArrayBeforeBoolean", boolean.class, String[].class, boolean.class ).get();

                try {
                    codecs.stringify( origMethod );
                    fail( "expected IllegalArgumentException" );
                } catch ( IllegalArgumentException ex ) {
                    assertEquals( "var arg parameter must appear at the end of the method", ex.getMessage() );
                }
            }
        }

        @Nested
        public class VarArgOnlyTestCases {
            @Test
            public void givenNoArgs_expectEmptyArrayToBePastIntoTheMethod() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( String[].class, "stringArrayArg", String[].class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                String[] expected = new String[]{};

                assertArrayEquals( expected, (String[]) stringifiedMethod.invokeStaticAgainst() );
            }

            @Test
            public void givenOneArg_expectSingleArgArrayToBePastIn() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( String[].class, "stringArrayArg", String[].class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                String[] expected = new String[] {"a"};

                assertArrayEquals( expected, (String[]) stringifiedMethod.invokeStaticAgainst("a") );
            }

            @Test
            public void givenTwoArgs_expectSingleArgArrayToBePastIn() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( String[].class, "stringArrayArg", String[].class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                String[] expected = new String[] {"a", "b"};

                assertArrayEquals( expected, (String[]) stringifiedMethod.invokeStaticAgainst("a", "b") );
            }
        }

        @Nested
        public class OneArgAndAVarArgTestCases {
            @Test
            public void givenTwoBooleansAndVarArgs() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( String.class, "twoBooleansAndStringArrayArg", Boolean.TYPE, Boolean.TYPE, String[].class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                String expected = "true,false,[a, b]";

                assertEquals( expected, stringifiedMethod.invokeStaticAgainst("true", "false", "a", "b") );
            }
        }
    }

    @Nested
    public class ListArgTestCases {
        @Nested
        public class InvalidVarArgTestCases {
            @Test
            public void varArgBeforeBooleanArg_expectException() {
                JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( String.class, "varArgListBeforeBoolean", boolean.class, List.class, boolean.class ).get();

                try {
                    codecs.stringify( origMethod );
                    fail( "expected IllegalArgumentException" );
                } catch ( IllegalArgumentException ex ) {
                    assertEquals( "var arg parameter must appear at the end of the method", ex.getMessage() );
                }
            }
        }

        @Nested
        public class VarArgOnlyTestCases {
            @Test
            public void givenNoArgs_expectEmptyArrayToBePastIntoTheMethod() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( List.class, "intListArg", List.class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                List<Integer> expected = Collections.emptyList();

                assertEquals( expected, stringifiedMethod.invokeStaticAgainst() );
            }

            @Test
            public void givenOneArg_expectSingleArgArrayToBePastIn() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( List.class, "intListArg", List.class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                List<Integer> expected = Collections.singletonList(1);

                assertEquals( expected, stringifiedMethod.invokeStaticAgainst("1") );
            }

            @Test
            public void givenTwoArgs_expectSingleArgArrayToBePastIn() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( List.class, "intListArg", List.class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                List<Integer> expected = Arrays.asList(6,5);

                assertEquals( expected, stringifiedMethod.invokeStaticAgainst("6", "5") );
            }
        }

        @Nested
        public class OneArgAndAVarArgTestCases {
            @Test
            public void givenTwoBooleansAndVarArgs() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( String.class, "twoBooleansAndIntegerListArg", Boolean.TYPE, Boolean.TYPE, List.class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                String expected = "true,false,[1, 2]";

                assertEquals( expected, stringifiedMethod.invokeStaticAgainst("true", "false", "1", "2") );
            }
        }
    }

    @Nested
    public class FPListArgTestCases {
        @Nested
        public class InvalidVarArgTestCases {
            @Test
            public void varArgBeforeBooleanArg_expectException() {
                JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( String.class, "varArgFPListBeforeBoolean", boolean.class, FPList.class, boolean.class ).get();

                try {
                    codecs.stringify( origMethod );
                    fail( "expected IllegalArgumentException" );
                } catch ( IllegalArgumentException ex ) {
                    assertEquals( "var arg parameter must appear at the end of the method", ex.getMessage() );
                }
            }
        }

        @Nested
        public class VarArgOnlyTestCases {
            @Test
            public void givenNoArgs_expectEmptyArrayToBePastIntoTheMethod() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( FPList.class, "stringFPListArg", FPList.class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                FPList<Integer> expected = FPList.newArrayList();

                assertEquals( expected, stringifiedMethod.invokeStaticAgainst() );
            }

            @Test
            public void givenOneArg_expectSingleArgArrayToBePastIn() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( FPList.class, "stringFPListArg", FPList.class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                FPList<String> expected = FPList.wrapArray("1");

                assertEquals( expected, stringifiedMethod.invokeStaticAgainst("1") );
            }

            @Test
            public void givenTwoArgs_expectSingleArgArrayToBePastIn() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( FPList.class, "stringFPListArg", FPList.class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                FPList<String> expected = FPList.wrapArray("6","5");

                assertEquals( expected, stringifiedMethod.invokeStaticAgainst("6", "5") );
            }
        }

        @Nested
        public class OneArgAndAVarArgTestCases {
            @Test
            public void givenTwoBooleansAndVarArgs() {
                JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( String.class, "twoBooleansAndStringFPListArg", Boolean.TYPE, Boolean.TYPE, FPList.class ).get();
                JavaMethod stringifiedMethod = codecs.stringify( origMethod );

                String expected = "true,false,[1, 2]";

                assertEquals( expected, stringifiedMethod.invokeStaticAgainst("true", "false", "1", "2") );
            }
        }
    }


    @SuppressWarnings("unused")
    public static class TestService {
        public static void noArgMethod() {}
        public static void stringArgsMethod( String a, String b ) {}

        public static FPOption<String> fpOptionStringArgsMethod( FPOption<String> a ) {
            return a;
        }

        public static Optional<String> jdkOptionalStringArgsMethod( Optional<String> a ) {
            return a;
        }

        public static String intFloatMethod( int a, float b ) {
            return a + ":" + b;
        }

        public static void hasUnencodableArg( StringCodecsTest.UnencodableDTO a ) {}

        public static String[] stringArrayArg( String[] arg ) {
            return arg;
        }

        public static String twoBooleansAndStringArrayArg( boolean a, boolean b, String[] arg ) {
            return a+","+b+","+Arrays.asList(arg);
        }

        public static String varArgArrayBeforeBoolean( boolean a, String[] arg, boolean b ) {
            return a+","+Arrays.asList(arg)+","+b;
        }

        public static List<Integer> intListArg( List<Integer> arg ) {
            return arg;
        }

        public static String twoBooleansAndIntegerListArg( boolean a, boolean b, List<Integer> arg ) {
            return a+","+b+","+arg;
        }

        public static String varArgListBeforeBoolean( boolean a, List<Integer> arg, boolean b ) {
            return a+","+arg+","+b;
        }

        public static FPList<String> stringFPListArg( FPList<String> arg ) {
            return arg;
        }

        public static String twoBooleansAndStringFPListArg( boolean a, boolean b, FPList<String> arg ) {
            return a+","+b+","+arg;
        }

        public static String varArgFPListBeforeBoolean( boolean a, FPList<String> arg, boolean b ) {
            return a+","+arg+","+b;
        }
    }
}
