package mosaics.strings.templates.ast.nodes;


import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class PropertyASTTest {

    private CharacterPosition pos = new CharacterPosition( "junit" );
    private TemplateContext   ctx = new TemplateContext();


    @Test
    public void walkAST_expectWalkPropertyASTCallback() {
        FPOption<String>  parameterType     = FP.emptyOption();
        List<String>      chainedProperties = Collections.emptyList();
        List<String>      modifiers         = Collections.emptyList();
        FPOption<String>  defaultValue      = FP.emptyOption();
        TemplateASTWalker walkerMock        = Mockito.mock(TemplateASTWalker.class);
        ParameterAST      ast               = new ParameterAST( pos, "prop", parameterType, chainedProperties, modifiers, defaultValue, FP.emptyOption() );

        ast.walkAST( walkerMock );

        Mockito.verify(walkerMock).walkPropertyAST(pos,"prop", parameterType, chainedProperties, modifiers, defaultValue);
    }

    @Test
    public void givenEmptyChainedProperties_optimise_expectNoChange() {
        FPOption<String> parameterType     = FP.emptyOption();
        List<String>     chainedProperties = Collections.emptyList();
        List<String>     modifiers         = Arrays.asList("a","b");
        FPOption<String> defaultValue      = FP.option("v");
        ParameterAST     ast               = new ParameterAST( pos, "prop", parameterType, chainedProperties, modifiers, defaultValue, FP.emptyOption() );

        assertEquals( ast, ast.optimiseSelf(ctx).get() );
    }

    @Test
    public void givenEmptyModifiers_optimise_expectNoChange() {
        FPOption<String> parameterType     = FP.emptyOption();
        List<String>     chainedProperties = Arrays.asList( "c1", "c2" );
        List<String>     modifiers         = Collections.emptyList();
        FPOption<String> defaultValue      = FP.option("v");
        ParameterAST     ast               = new ParameterAST( pos, "prop", parameterType, chainedProperties, modifiers, defaultValue, FP.emptyOption() );

        assertEquals( ast, ast.optimiseSelf(ctx).get() );
    }

    @Test
    public void givenEmptyDefaultValue_optimise_expectNoChange() {
        FPOption<String> parameterType     = FP.emptyOption();
        List<String>     chainedProperties = Arrays.asList( "c1", "c2" );
        List<String>     modifiers         = Arrays.asList("a","b");
        FPOption<String> defaultValue      = FP.emptyOption();
        ParameterAST     ast               = new ParameterAST( pos, "prop", parameterType, chainedProperties, modifiers, defaultValue, FP.emptyOption() );

        assertEquals( ast, ast.optimiseSelf(ctx).get() );
    }

    @Test
    public void givenNoEmptyParts_optimise_expectNoChange() {
        FPOption<String> parameterType     = FP.emptyOption();
        List<String>     chainedProperties = Arrays.asList( "c1", "c2" );
        List<String>     modifiers         = Arrays.asList("a","b");
        FPOption<String> defaultValue      = FP.option("v");
        ParameterAST     ast               = new ParameterAST( pos, "prop", parameterType, chainedProperties, modifiers, defaultValue, FP.emptyOption() );

        assertEquals( ast, ast.optimiseSelf(ctx).get() );
    }

}
