package mosaics.strings;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CodecNotFoundExceptionTest {

    @Test
    public void givenException_verifyMessage() {
        CodecNotFoundException ex = new CodecNotFoundException("c");

        assertEquals( "Unable to find StringCodec: 'c'", ex.getMessage() );
    }

    @Test
    public void givenException_verifyEqualsAndHashCode() {
        EqualsVerifier.forClass( CodecNotFoundException.class)
            .withIgnoredFields( "cause", "stackTrace", "suppressedExceptions" )
            .withNonnullFields( "detailMessage" )
            .suppress( Warning.NONFINAL_FIELDS )
            .verify();
    }

}
