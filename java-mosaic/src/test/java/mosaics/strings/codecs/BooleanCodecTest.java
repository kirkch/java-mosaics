package mosaics.strings.codecs;


import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


@SuppressWarnings("Duplicates")
public class BooleanCodecTest {

    @Test
    public void testHashCode() {
        assertNotEquals( BooleanCodec.BOOLEAN_PRIMITIVE_CODEC, BooleanCodec.BOOLEAN_OBJECT_CODEC );
    }

    @Nested
    public class PrimitiveBooleanTestCases {
        private BooleanCodec codec = BooleanCodec.BOOLEAN_PRIMITIVE_CODEC;


        @Test
        public void encodeBooleanPrimitives() {
            assertEquals( "true", codec.encode(true) );
            assertEquals("false", codec.encode(false) );
        }

        @Test
        public void decodeBoolean() {
            assertEquals(false, codec.decode(null).getResult());

            assertEquals(true, codec.decode("true").getResult());
            assertEquals(true, codec.decode("TruE").getResult());
            assertEquals(true, codec.decode("t").getResult());
            assertEquals(true, codec.decode("y").getResult());
            assertEquals(true, codec.decode("yes").getResult());
            assertEquals(true, codec.decode("1").getResult());

            assertEquals(false, codec.decode("false").getResult());
            assertEquals(false, codec.decode("0").getResult());
            assertEquals(false, codec.decode("2").getResult());
            assertEquals(false, codec.decode("f").getResult());
            assertEquals(false, codec.decode("no").getResult());
            assertEquals(false, codec.decode("foo").getResult());
        }

        @Test
        public void decodeBooleanWithDefault() {
            assertEquals(true, codec.decode(null, true));
            assertEquals(false, codec.decode(null, false));
            assertEquals(true, codec.decode("  ", true));
            assertEquals(false, codec.decode("  ", false));

            assertEquals(true, codec.decode("true", false));
            assertEquals(true, codec.decode("TruE", false));
            assertEquals(true, codec.decode("t", false));
            assertEquals(true, codec.decode("y", false));
            assertEquals(true, codec.decode("yes", false));
            assertEquals(true, codec.decode("1", false));

            assertEquals(false, codec.decode("false", true));
            assertEquals(false, codec.decode("0", true));
            assertEquals(false, codec.decode("2", true));
            assertEquals(false, codec.decode("f", true));
            assertEquals(false, codec.decode("no", true));
            assertEquals(false, codec.decode("foo", true));
        }

        @Test
        public void testToString() {
            assertEquals( "BooleanCodec", codec.toString() );
        }

        @Test
        public void testSupportsNull() {
            assertFalse( codec.supportsNull() );
        }
    }


    @Nested
    public class ObjectTestCases {
        private BooleanCodec codec = BooleanCodec.BOOLEAN_OBJECT_CODEC;

        @Test
        public void encodeBooleanObjects() {
            assertEquals( "true", codec.encode(true) );
            assertEquals("false", codec.encode(false) );
            assertEquals(null, codec.encode(null) );
        }

        @Test
        public void decodeBooleanObjects() {
            assertEquals(null, codec.decode(null).getResult());

            assertEquals(true, codec.decode("true").getResult());
            assertEquals(true, codec.decode("TruE").getResult());
            assertEquals(true, codec.decode("t").getResult());
            assertEquals(true, codec.decode("y").getResult());
            assertEquals(true, codec.decode("yes").getResult());
            assertEquals(true, codec.decode("1").getResult());

            assertEquals(false, codec.decode("false").getResult());
            assertEquals(false, codec.decode("0").getResult());
            assertEquals(false, codec.decode("2").getResult());
            assertEquals(false, codec.decode("f").getResult());
            assertEquals(false, codec.decode("no").getResult());
            assertEquals(false, codec.decode("foo").getResult());
        }

        @Test
        public void decodeBooleanObjectsWithDefault() {
            assertEquals(true, codec.decode(null, true));
            assertEquals(false, codec.decode(null, false));
            assertEquals(true, codec.decode("  ", true));
            assertEquals(false, codec.decode("  ", false));

            assertEquals(true, codec.decode("true", false));
            assertEquals(true, codec.decode("TruE", false));
            assertEquals(true, codec.decode("t", false));
            assertEquals(true, codec.decode("y", false));
            assertEquals(true, codec.decode("yes", false));
            assertEquals(true, codec.decode("1", false));

            assertEquals(false, codec.decode("false", true));
            assertEquals(false, codec.decode("0", true));
            assertEquals(false, codec.decode("2", true));
            assertEquals(false, codec.decode("f", true));
            assertEquals(false, codec.decode("no", true));
            assertEquals(false, codec.decode("foo", true));
        }

        @Test
        public void testToString() {
            assertEquals( "BooleanCodec", codec.toString() );
        }

        @Test
        public void testSupportsNull() {
            assertFalse( codec.supportsNull() );
        }
    }
}
