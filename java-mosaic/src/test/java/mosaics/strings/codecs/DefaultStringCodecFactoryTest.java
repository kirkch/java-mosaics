package mosaics.strings.codecs;

import lombok.Value;
import mosaics.strings.StringCodec;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class DefaultStringCodecFactoryTest {

    @Nested
    public class EnumTestCases {
        @Test
        public void enumWithNoDefault() {
            StringCodec<RGB> codec = DefaultStringCodecFactory.createCodecFor( RGB.class );

            assertEquals( "RED", codec.encode(RGB.RED) );
            assertEquals( RGB.GREEN, codec.decode("GREEN").getResult() );
        }
    }

    @Nested
    public class BeanWithOneStringArg {
        @Test
        public void testSupportsNull() {
            StringCodec<Driver> codec = DefaultStringCodecFactory.createCodecFor( Driver.class );

            assertFalse( codec.supportsNull() );
        }

        @Test
        public void withDefaultValue() {
            StringCodec<Driver> codec = DefaultStringCodecFactory.createCodecFor( Driver.class, new Driver("Bob") );

            assertEquals( new Driver("Bob"), codec.decode( null ).getResult() );
        }
    }


    public enum RGB {
        RED, GREEN, BLUE
    }

    @Value
    public static class Driver {
        private String name;
    }
}
