package mosaics.strings.codecs;


import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class DurationLongCodecTest {

    private StringCodec<Long> codec = DurationLongCodec.INSTANCE;


    @Test
    public void testHashCode() {
        assertEquals( new DurationLongCodec(), new DurationLongCodec() );
    }

    @Test
    public void testType() {
        assertEquals( JavaClass.LONG_PRIMITIVE, codec.getType() );
    }

    @Test
    public void testToString() {
        assertEquals( "DurationPrimitiveLongCodec", codec.toString() );
    }

    @Test
    public void testSupportsNull() {
        assertFalse( codec.supportsNull() );
    }


    @Test
    public void encodeDuration() {
        assertEquals("",   codec.encode(null));
        assertEquals("0s",   codec.encode(0L));
        assertEquals("0.001s",   codec.encode(1L));
        assertEquals("0.002s",   codec.encode(2L));
        assertEquals("0.003s",   codec.encode(3L));
        assertEquals("0.999s",   codec.encode(999L));
        assertEquals("1s",   codec.encode(1000L));
        assertEquals("1.001s",   codec.encode(1001L));

        assertEquals("1m",   codec.encode(60000L));
        assertEquals("1m 3s",   codec.encode(63000L));
        assertEquals("1m 3.050s",   codec.encode(63050L));
        assertEquals("2h 10m",   codec.encode( 2*60*60*1000L + 10*60*1000L ));

        assertEquals("1h",   codec.encode(60*60*1000L));

        assertEquals("-0.001s",   codec.encode(-1L));
        assertEquals("-0.002s",   codec.encode(-2L));
        assertEquals("-0.003s",   codec.encode(-3L));
        assertEquals("-0.999s",   codec.encode(-999L));
        assertEquals("-1s",   codec.encode(-1000L));
        assertEquals("-1.001s",   codec.encode(-1001L));

        assertEquals("-1m",   codec.encode(-60000L));
        assertEquals("-1m 3s",   codec.encode(-63000L));
        assertEquals("-1m 3.050s",   codec.encode(-63050L));

        assertEquals("-1h",   codec.encode(-60*60*1000L));
    }

    @Test
    public void decodeDuration() {
        assertEquals(1L,   DurationLongCodec.INSTANCE.decode("0.001s").getResult().longValue());
        assertEquals(2L,   DurationLongCodec.INSTANCE.decode("0.002s").getResult().longValue());
        assertEquals(3L,   DurationLongCodec.INSTANCE.decode("0.003s").getResult().longValue());
        assertEquals(999L,   DurationLongCodec.INSTANCE.decode("0.999s").getResult().longValue());
        assertEquals(1000L,   DurationLongCodec.INSTANCE.decode("1s").getResult().longValue());
        assertEquals(1001L,   DurationLongCodec.INSTANCE.decode("1.001s").getResult().longValue());

        assertEquals(60000L,   DurationLongCodec.INSTANCE.decode("1m 0s").getResult().longValue());
        assertEquals(63000L,   DurationLongCodec.INSTANCE.decode("1m 3s").getResult().longValue());
        assertEquals(63050L,   DurationLongCodec.INSTANCE.decode("1m 3.050s").getResult().longValue());

        assertEquals(60 * 60 * 1000L,   DurationLongCodec.INSTANCE.decode("1h 0m 0s").getResult().longValue());

        assertEquals(-1L,   DurationLongCodec.INSTANCE.decode("-0.001s").getResult().longValue());
        assertEquals(-2L,   DurationLongCodec.INSTANCE.decode("-0.002s").getResult().longValue());
        assertEquals(-3L,   DurationLongCodec.INSTANCE.decode("-0.003s").getResult().longValue());
        assertEquals(-999L,   DurationLongCodec.INSTANCE.decode("-0.999s").getResult().longValue());
        assertEquals(-1000L,   DurationLongCodec.INSTANCE.decode("-1s").getResult().longValue());
        assertEquals(-1001L,   DurationLongCodec.INSTANCE.decode("-1.001s").getResult().longValue());

        assertEquals(-60000L,   DurationLongCodec.INSTANCE.decode("-1m 0s").getResult().longValue());
        assertEquals(-63000L,   DurationLongCodec.INSTANCE.decode("-1m 3s").getResult().longValue());
        assertEquals(-63050L,   DurationLongCodec.INSTANCE.decode("-1m 3.050s").getResult().longValue());

        assertEquals(-60 * 60 * 1000L,   DurationLongCodec.INSTANCE.decode("-1h 0m 0s").getResult().longValue());


        assertEquals( "java.lang.NumberFormatException: For input string: null", DurationLongCodec.INSTANCE.decode(null).getFailure().toString() );
        assertEquals( "java.lang.NumberFormatException: For input string: \"-1234a5\"", DurationLongCodec.INSTANCE.decode("-1234a5").getFailure().toString() );
        assertEquals( "java.lang.NumberFormatException: For input string: \"1p\"", DurationLongCodec.INSTANCE.decode("1p").getFailure().toString() );
        assertEquals( "java.lang.NumberFormatException: For input string: \"pppp\"", DurationLongCodec.INSTANCE.decode("pppp").getFailure().toString() );
    }

}
