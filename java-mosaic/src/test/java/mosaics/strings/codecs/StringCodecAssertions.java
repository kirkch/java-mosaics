package mosaics.strings.codecs;


import mosaics.strings.StringCodec;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class StringCodecAssertions {

    public static <T> void assertCodecRoundTrip( StringCodec<T> codec, T objValue, String strEncoding ) {
        assertEquals( strEncoding, codec.encode(objValue) );
        assertEquals( objValue, codec.decode(strEncoding).getResult() );
    }

}
