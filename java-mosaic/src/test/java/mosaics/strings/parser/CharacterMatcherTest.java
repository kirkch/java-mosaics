package mosaics.strings.parser;

import lombok.Value;
import org.junit.jupiter.api.Test;

import static mosaics.strings.parser.CharacterIterator.stringIterator;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class CharacterMatcherTest {

// DEFAULT INVERTMATCHER

    @Test
    public void givenEmptyInput_expectInvertedMatcherToMatchZeroBytes() {
        CharacterMatcher origMatcher     = new ConstantMatcherThatUsesDefaultInverter( "abc" );
        CharacterMatcher invertedMatcher = origMatcher.invertMatcher();

        assertEquals( 0, invertedMatcher.matchCount(stringIterator("")) );
    }

    @Test
    public void givenInputThatExactlyMatchesTheOriginalUninvertedMatcher_expectInvertedMatcherToMatchZeroBytes() {
        CharacterMatcher origMatcher     = new ConstantMatcherThatUsesDefaultInverter( "abc" );
        CharacterMatcher invertedMatcher = origMatcher.invertMatcher();

        assertEquals( 0, invertedMatcher.matchCount(stringIterator("abc")) );
    }

    @Test
    public void givenInputThatNeverMatchesTheOriginalUninvertedMatcher_expectInvertedMatcherToMatchAllOfTheBytes() {
        CharacterMatcher origMatcher     = new ConstantMatcherThatUsesDefaultInverter( "abc" );
        CharacterMatcher invertedMatcher = origMatcher.invertMatcher();

        assertEquals( 6, invertedMatcher.matchCount(stringIterator("abdefg")) );
    }

    @Test
    public void givenInputThatEventuallyMatchesTheUninvertedMatcher_expectInvertedMatcherUpToTheTargetBytes() {
        CharacterMatcher origMatcher     = new ConstantMatcherThatUsesDefaultInverter( "abc" );
        CharacterMatcher invertedMatcher = origMatcher.invertMatcher();

        assertEquals( 2, invertedMatcher.matchCount(stringIterator("ababcg")) );
    }


// COMPOSITE MATCHERS

    @Test
    public void compositeMatchers_and() {
        CharacterMatcher a  = CharacterMatchers.constant( "abc" );
        CharacterMatcher b  = CharacterMatchers.constant( "def" );
        CharacterMatcher ab = a.and(b);

        assertEquals( 0, ab.matchCount(stringIterator("")) );
        assertEquals( 0, ab.matchCount(stringIterator("a")) );
        assertEquals( 0, ab.matchCount(stringIterator("ab")) );
        assertEquals( 0, ab.matchCount(stringIterator("abc")) );
        assertEquals( 0, ab.matchCount(stringIterator("abcd")) );
        assertEquals( 0, ab.matchCount(stringIterator("abcde")) );
        assertEquals( 6, ab.matchCount(stringIterator("abcdef")) );
        assertEquals( 6, ab.matchCount(stringIterator("abcdefg")) );
    }

    @Test
    public void compositeMatchers_or() {
        CharacterMatcher a  = CharacterMatchers.constant( "abc" );
        CharacterMatcher b  = CharacterMatchers.constant( "def" );
        CharacterMatcher ab = a.or(b);

        assertEquals( 0, ab.matchCount(stringIterator("")) );
        assertEquals( 0, ab.matchCount(stringIterator("a")) );
        assertEquals( 0, ab.matchCount(stringIterator("ab")) );
        assertEquals( 3, ab.matchCount(stringIterator("abc")) );
        assertEquals( 3, ab.matchCount(stringIterator("abcd")) );
        assertEquals( 3, ab.matchCount(stringIterator("abcde")) );
        assertEquals( 3, ab.matchCount(stringIterator("abcdef")) );
        assertEquals( 3, ab.matchCount(stringIterator("abcdefg")) );
        assertEquals( 0, ab.matchCount(stringIterator("d")) );
        assertEquals( 0, ab.matchCount(stringIterator("de")) );
        assertEquals( 3, ab.matchCount(stringIterator("def")) );
        assertEquals( 3, ab.matchCount(stringIterator("defabc")) );
        assertEquals( 0, ab.matchCount(stringIterator("daefbc")) );
    }

    @Test
    public void compositeMatchers_andOptionally() {
        CharacterMatcher a  = CharacterMatchers.constant( "abc" );
        CharacterMatcher b  = CharacterMatchers.constant( "def" );
        CharacterMatcher ab = a.andOptionally(b);

        assertEquals( 0, ab.matchCount(stringIterator("")) );
        assertEquals( 0, ab.matchCount(stringIterator("a")) );
        assertEquals( 0, ab.matchCount(stringIterator("ab")) );
        assertEquals( 3, ab.matchCount(stringIterator("abc")) );
        assertEquals( 3, ab.matchCount(stringIterator("abcd")) );
        assertEquals( 3, ab.matchCount(stringIterator("abcde")) );
        assertEquals( 6, ab.matchCount(stringIterator("abcdef")) );
        assertEquals( 6, ab.matchCount(stringIterator("abcdefg")) );
        assertEquals( 0, ab.matchCount(stringIterator("d")) );
        assertEquals( 0, ab.matchCount(stringIterator("de")) );
        assertEquals( 0, ab.matchCount(stringIterator("def")) );
        assertEquals( 0, ab.matchCount(stringIterator("defabc")) );
        assertEquals( 0, ab.matchCount(stringIterator("daefbc")) );
    }


// UTILS

    @Test
    public void testTrim() {
        CharacterMatcher a  = CharacterMatchers.constant( "abc" ).trim();

//        assertEquals( "trim(abc)", a.toString() );
//        assertEquals( 3, a.matchCount(stringIterator("abc")) );
        assertEquals( 5, a.matchCount(stringIterator(" abc ")) );
//        assertEquals( 0, a.matchCount(stringIterator(" ab ")) );
    }

    @Value
    private static class ConstantMatcherThatUsesDefaultInverter implements CharacterMatcher {
        private String targetStr;

        public int matchCount( CharacterIterator it ) {
            if ( !it.hasNext(targetStr.length()) ) {
                return 0;
            }

            return it.peek( () -> {
                int targetStrLength = targetStr.length();

                for ( int i=0; i<targetStrLength; i++ ) {
                    if ( it.next() != targetStr.charAt(i) ) {
                        return 0;
                    }
                }

                return targetStrLength;
            });
        }
    }
}
