package mosaics.strings;

import mosaics.fp.FP;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class StringUtilsTest {
    @Test
    public void extractFrom() {
        assertEquals( "bc", StringUtils.extractFrom("abcd", "bc") );
        assertEquals( "ll", StringUtils.extractFrom("hello", "l+") );
        assertEquals( "Foo", StringUtils.extractFrom("public class Foo {", "Foo") );
    }

    @Test
    public void extractUpToFirstMatch() {
        assertEquals( FP.emptyOption(), StringUtils.extractUpToFirstMatch("abcdea",'f') );
        assertEquals( FP.option(""), StringUtils.extractUpToFirstMatch("abcdea",'a') );
        assertEquals( FP.option("a"), StringUtils.extractUpToFirstMatch("abcdea",'b') );
        assertEquals( FP.option("abc"), StringUtils.extractUpToFirstMatch("abcdea",'d') );
        assertEquals( FP.option("abcd"), StringUtils.extractUpToFirstMatch("abcdea",'e') );
    }

    @Test
    public void extractUpToLastMatch() {
        assertEquals( FP.emptyOption(), StringUtils.extractUpToLastMatch("abcdea",'f') );
        assertEquals( FP.option("abcde"), StringUtils.extractUpToLastMatch("abcdea",'a') );
        assertEquals( FP.option("a"), StringUtils.extractUpToLastMatch("abcdea",'b') );
        assertEquals( FP.option("abc"), StringUtils.extractUpToLastMatch("abcdea",'d') );
        assertEquals( FP.option("abcd"), StringUtils.extractUpToLastMatch("abcdea",'e') );
        assertEquals( FP.option(""), StringUtils.extractUpToLastMatch("abcde",'a') );
    }

    @Test
    public void extractAfterFirstMatch() {
        assertEquals( FP.emptyOption(), StringUtils.extractAfterFirstMatch("abcdea",'f') );
        assertEquals( FP.option("bcdea"), StringUtils.extractAfterFirstMatch("abcdea",'a') );
        assertEquals( FP.option("cdea"), StringUtils.extractAfterFirstMatch("abcdea",'b') );
        assertEquals( FP.option("ea"), StringUtils.extractAfterFirstMatch("abcdea",'d') );
        assertEquals( FP.option("a"), StringUtils.extractAfterFirstMatch("abcdea",'e') );
        assertEquals( FP.option(""), StringUtils.extractAfterFirstMatch("bcdea",'a') );
    }

    @Test
    public void extractAfterLastMatch() {
        assertEquals( FP.emptyOption(), StringUtils.extractAfterLastMatch("abcdea",'f') );
        assertEquals( FP.option(""), StringUtils.extractAfterLastMatch("abcdea",'a') );
        assertEquals( FP.option("cdea"), StringUtils.extractAfterLastMatch("abcdea",'b') );
        assertEquals( FP.option("ea"), StringUtils.extractAfterLastMatch("abcdea",'d') );
        assertEquals( FP.option("a"), StringUtils.extractAfterLastMatch("abcdea",'e') );
        assertEquals( FP.option("bcde"), StringUtils.extractAfterLastMatch("abcde",'a') );
    }

    @Test
    public void trimLeft() {
        assertEquals( "", StringUtils.trimLeft("") );
        assertEquals( "", StringUtils.trimLeft("    ") );
        assertEquals( "", StringUtils.trimLeft("   \t \t ") );
        assertEquals( "foo \t \t ", StringUtils.trimLeft("  foo \t \t ") );
        assertEquals( "foo", StringUtils.trimLeft("  foo") );
    }

    @Test
    public void trimRight() {
        assertEquals( "", StringUtils.trimRight("") );
        assertEquals( "", StringUtils.trimRight("    ") );
        assertEquals( "", StringUtils.trimRight("   \t \t ") );
        assertEquals( "  foo", StringUtils.trimRight("  foo \t \t ") );
        assertEquals( "  foo", StringUtils.trimRight("  foo") );
        assertTrue( "  foo" == StringUtils.trimRight("  foo") );
    }

    @Test
    public void removePostFix() {
        assertEquals( "foo", StringUtils.removePostFix("foo", "bar") );
        assertEquals( "foo", StringUtils.removePostFix("foobar", "bar") );
        assertEquals( "", StringUtils.removePostFix("", "bar") );
        assertEquals( null, StringUtils.removePostFix(null, "bar") );
    }

    @Test
    public void upto() {
        assertEquals( "", StringUtils.upto("", '#') );
        assertEquals( "foo", StringUtils.upto("foo", '#') );
        assertEquals( "foo  ", StringUtils.upto("foo  ", '#') );
        assertTrue("foo  " == StringUtils.upto("foo  ", '#'));
        assertEquals( null, StringUtils.upto(null, '#') );
        assertEquals( "foo  ", StringUtils.upto("foo  # foo bar", '#') );
        assertEquals( "foo  ", StringUtils.upto("foo  ## foo bar", '#') );
        assertEquals("foo  ", StringUtils.upto("foo  #", '#'));
    }

    @Test
    public void dropWhile() {
        assertNull(StringUtils.dropWhile(null, Character::isLowerCase));
        assertEquals( "Method", StringUtils.dropWhile("setMethod", Character::isLowerCase) );
        assertEquals( "SetMethod", StringUtils.dropWhile("SetMethod", Character::isLowerCase) );
        assertEquals( "", StringUtils.dropWhile("abc", Character::isLowerCase) );
    }




    @Test
    public void testCharactersLengthOfBytes() {
        assertEquals( 1, StringUtils.countNumberOfCharactersNeededToRepresentByte((byte) 0) );
        assertEquals( 1, StringUtils.countNumberOfCharactersNeededToRepresentByte((byte) 1) );
        assertEquals( 1, StringUtils.countNumberOfCharactersNeededToRepresentByte((byte) 2) );

        for ( byte i=0; i<10; i++ ) {
            assertEquals( 1, StringUtils.countNumberOfCharactersNeededToRepresentByte(i), Integer.toString(i) );
        }
        for ( byte i=10; i<100; i++ ) {
            assertEquals( 2, StringUtils.countNumberOfCharactersNeededToRepresentByte(i) );
        }
        for ( byte i=100; i<Byte.MAX_VALUE; i++ ) {
            assertEquals( 3, StringUtils.countNumberOfCharactersNeededToRepresentByte(i) );
        }

        assertEquals( 2, StringUtils.countNumberOfCharactersNeededToRepresentByte((byte) -1) );

        for ( byte i=-1; i>-10; i-- ) {
            assertEquals( 2, StringUtils.countNumberOfCharactersNeededToRepresentByte(i) );
        }

        for ( byte i=-10; i>-100; i-- ) {
            assertEquals( 3, StringUtils.countNumberOfCharactersNeededToRepresentByte(i) );
        }

        assertEquals( 4, StringUtils.countNumberOfCharactersNeededToRepresentByte(Byte.MIN_VALUE) );
    }

    @Test
    public void testCharactersLengthOfLong() {
        assertEquals(1, StringUtils.countNumberOfCharactersNeededToRepresentLong(0));
        assertEquals( 1, StringUtils.countNumberOfCharactersNeededToRepresentLong(1) );
        assertEquals( 1, StringUtils.countNumberOfCharactersNeededToRepresentLong(2) );

        for ( int i=0; i<10; i++ ) {
            assertEquals( 1, StringUtils.countNumberOfCharactersNeededToRepresentLong(i), Integer.toString(i) );
        }
        for ( int i=10; i<100; i++ ) {
            assertEquals( 2, StringUtils.countNumberOfCharactersNeededToRepresentLong(i) );
        }
        for ( int i=100; i<1000; i++ ) {
            assertEquals( 3, StringUtils.countNumberOfCharactersNeededToRepresentLong(i) );
        }

        assertEquals( 19, StringUtils.countNumberOfCharactersNeededToRepresentLong(Long.MAX_VALUE) );

        assertEquals( 2, StringUtils.countNumberOfCharactersNeededToRepresentLong(-1) );

        for ( int i=-1; i>-10; i-- ) {
            assertEquals( 2, StringUtils.countNumberOfCharactersNeededToRepresentLong(i) );
        }

        for ( int i=-10; i>-100; i-- ) {
            assertEquals( 3, StringUtils.countNumberOfCharactersNeededToRepresentLong(i) );
        }

        assertEquals( 20, StringUtils.countNumberOfCharactersNeededToRepresentLong(Long.MIN_VALUE) );
    }

    @Test
    public void testSplitCamelCase() {
        assertEquals( "abc", StringUtils.splitCamelCase("abc").mkString(" ") );
        assertEquals( "Hello World", StringUtils.splitCamelCase("HelloWorld").mkString(" ") );
        assertEquals( "hello World", StringUtils.splitCamelCase("helloWorld").mkString(" ") );
        assertEquals( "Hello James Kirk", StringUtils.splitCamelCase("HelloJamesKirk").mkString(" ") );
        assertEquals( "ABC Rocks", StringUtils.splitCamelCase("ABCRocks").mkString(" ") );
        assertEquals( "Welcome James T Kirk", StringUtils.splitCamelCase("WelcomeJamesTKirk").mkString(" ") );
        assertEquals( "abc DEF Ghi", StringUtils.splitCamelCase("abcDEFGhi").mkString(" ") );
        assertEquals( "ABC", StringUtils.splitCamelCase("ABC").mkString(" ") );
        assertEquals( "AB", StringUtils.splitCamelCase("AB").mkString(" ") );
        assertEquals( "A", StringUtils.splitCamelCase("A").mkString(" ") );
        assertEquals( "a", StringUtils.splitCamelCase("a").mkString(" ") );
        assertEquals( "", StringUtils.splitCamelCase("").mkString(" ") );
        assertEquals( "hello091", StringUtils.splitCamelCase("hello091").mkString(" ") );
        assertEquals( "hello Alpha091", StringUtils.splitCamelCase("helloAlpha091").mkString(" ") );
        assertEquals( "hello091 Alpha", StringUtils.splitCamelCase("hello091Alpha").mkString(" ") );
    }

}
