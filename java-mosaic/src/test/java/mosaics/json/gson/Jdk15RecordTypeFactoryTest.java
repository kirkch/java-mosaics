package mosaics.json.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Jdk15RecordTypeFactoryTest {

    @Test
    public void recordWithPrimitiveValuesRoundTrip() {
        Jdk15RecordTypeFactory codec = new Jdk15RecordTypeFactory();

        Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory( codec )
            .create();

        Record1 orig1 = new Record1( "alpha", 1 );
        Record1 orig2 = new Record1( "beta", 2 );

        String json1 = gson.toJson( orig1, Record1.class );
        String json2 = gson.toJson( orig2, Record1.class );

        assertEquals(
            "{\"name\":\"alpha\",\"age\":1}",
            json1
        );

        assertEquals(
            "{\"name\":\"beta\",\"age\":2}",
            json2
        );

        Record1 returned1 = gson.fromJson( json1, Record1.class );
        Record1 returned2 = gson.fromJson( json2, Record1.class );

        assertEquals( orig1, returned1 );
        assertEquals( orig2, returned2 );
    }

    @Test
    public void composedRecords() {
        Jdk15RecordTypeFactory codec = new Jdk15RecordTypeFactory();

        Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory( codec )
            .create();

        Record1 orig1   = new Record1( "alpha", 1 );
        Record1 orig2   = new Record1( "beta", 2 );
        Records records = new Records( orig1, orig2 );

        String json = gson.toJson( records, Records.class );

        assertEquals(
            "{\"record1\":{\"name\":\"alpha\",\"age\":1},\"record2\":{\"name\":\"beta\",\"age\":2}}",
            json
        );

        Records actual = gson.fromJson( json, Records.class );

        assertEquals( records, actual );
    }

    @Test
    public void fpOptionWithValue() {
        Jdk15RecordTypeFactory codec = new Jdk15RecordTypeFactory();

        Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory( codec )
            .registerTypeAdapterFactory( new FPTypeFactory() )
            .create();

        FPOptionRecord orig = new FPOptionRecord( FP.option( new Record1( "alpha", 1 ) ) );

        String json = gson.toJson( orig, FPOptionRecord.class );

        assertEquals(
            "{\"record1\":{\"name\":\"alpha\",\"age\":1}}",
            json
        );

        FPOptionRecord actual = gson.fromJson( json, FPOptionRecord.class );

        assertEquals( orig, actual );
    }

    @Test
    public void fpOptionWithNoValue() {
        Jdk15RecordTypeFactory codec = new Jdk15RecordTypeFactory();

        Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory( codec )
            .registerTypeAdapterFactory( new FPTypeFactory() )
            .create();

        FPOptionRecord orig = new FPOptionRecord( FP.emptyOption() );

        String json = gson.toJson( orig, FPOptionRecord.class );

        assertEquals(
            "{}",
            json
        );

        FPOptionRecord actual = gson.fromJson( json, FPOptionRecord.class );

        assertEquals( orig, actual );
    }

    @Test
    public void withNullValueRoundTrip() {
        Jdk15RecordTypeFactory codec = new Jdk15RecordTypeFactory();

        Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory( codec )
            .create();

        Record1 orig1 = new Record1( null, 1 );

        String json1 = gson.toJson( orig1, Record1.class );

        assertEquals(
            "{\"age\":1}",
            json1
        );

        Record1 returned1 = gson.fromJson( json1, Record1.class );

        assertEquals( orig1, returned1 );
    }


    @Test
    public void parseNullValue() {
        Jdk15RecordTypeFactory codec = new Jdk15RecordTypeFactory();

        Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory( codec )
            .create();

        Record1 expected = new Record1( null, 1 );
        Record1 actual   = gson.fromJson( "{\"name\":null, \"age\":1}", Record1.class );

        assertEquals( expected, actual );
    }


    @Test
    public void parseFPOptionInt() {
        Jdk15RecordTypeFactory codec = new Jdk15RecordTypeFactory();

        Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory( codec )
            .registerTypeAdapterFactory( new FPTypeFactory() )
            .create();

        RecordWithIntFPOption expected = new RecordWithIntFPOption( null, FP.option(1) );
        RecordWithIntFPOption actual   = gson.fromJson( "{\"name\":null, \"age\":1}", RecordWithIntFPOption.class );

        assertEquals( expected, actual );
    }


    public static record Record1( String name, int age ) {
    }

    public static record Records( Record1 record1, Record1 record2 ) {
    }

    public static record FPOptionRecord( FPOption<Record1> record1 ) {
    }

    public static record RecordWithIntFPOption( String name, FPOption<Integer> age ) {
    }
}