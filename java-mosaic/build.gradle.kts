plugins {
    id("myproject.java-conventions")
}

project.description = "Java Mosaic provides a collections and language extensions for the Java language"

dependencies {
    api("org.apache.commons:commons-text:1.9")
    api("com.google.code.gson:gson:2.9.0")
    implementation("net.dongliu:gson-java8-datatype:1.1.0")
}
