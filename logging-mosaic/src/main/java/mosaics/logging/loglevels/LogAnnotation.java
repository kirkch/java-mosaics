package mosaics.logging.loglevels;

import lombok.Value;
import lombok.With;
import mosaics.logging.LogSeverity;
import mosaics.logging.TargetAudience;

@Value
@With
public class LogAnnotation{
    private final TargetAudience targetAudience;
    private final LogSeverity severity;
    private final String template;
}
