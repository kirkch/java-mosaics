package mosaics.logging.loglevels;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Mark an @Events method as being targeted as a 'problem' to be picked up by 'developers' asap
 * as a major design constraint has been violated.<p/>
 *
 * Developer warnings, errors and fatal messages all mean that an important design constraint
 * or assumption has been violated and requires attention by a developer to prioritise/risk assess.
 * The severity of the event depends on the previously predicted impact of the violation, such as
 * how much time will the developers have to work on improving the system to prevent the violation
 * from harmfully impacting users.  Warnings imply marginal impact on users, errors suggest high
 * impact on some users and fatal implies that the system will be failing in major ways for most of
 * the users.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DevFatal {
    /**
     * An optional template used to generate descriptions of the event.  The template follows the
     * format rules of TextTemplate;  which uses $parameterName to represent placeholders.
     *
     * For example, an event that has a parameter called 'userName' could have the following
     * template:
     *
     * <pre>
     *     "'$userName' logged in'
     * </pre>
     *
     * @See com.softwaremosaic.strings.templates.TextTemplate
     */
    public String value() default "";
}
