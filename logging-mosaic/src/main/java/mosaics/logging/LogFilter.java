package mosaics.logging;

/**
 * A set of standard log filters used to filter out which LogEvents are to be logged within any
 * single LogHandler.
 */
public enum LogFilter {

    /**
     * All messages are to be logged.
     */
    DEBUG,

    /**
     * All errors and all Ops related messages.
     */
    VERBOSE,

    /**
     * All errors and all User related messages.
     */
    NORMAL,

    /**
     * All errors and all audit related messages.
     */
    AUDIT,

    /**
     * Errors only.
     */
    ERRORS_ONLY,

    /**
     * No messages are to be logged at all.
     */
    SILENT;


    public boolean accept( LogMessage logMessage ) {
        if ( logMessage.isTracerMessage() ) {
            return true;
        }

        return switch (this) {
            case DEBUG       -> true;
            case VERBOSE     -> isErrorOrWorse(logMessage.getSeverity()) || isGTE(logMessage.getTargetAudience(), TargetAudience.OPS);
            case NORMAL      -> isErrorOrWorse(logMessage.getSeverity()) || isGTE(logMessage.getTargetAudience(), TargetAudience.USER);
            case AUDIT       -> isErrorOrWorse(logMessage.getSeverity()) || isGTE(logMessage.getTargetAudience(), TargetAudience.AUDIT);
            case ERRORS_ONLY -> isErrorOrWorse(logMessage.getSeverity());
            case SILENT      -> false;
        };
    }

    private static boolean isErrorOrWorse( LogSeverity currentSeverity ) {
        return currentSeverity.isGTE(LogSeverity.ERROR);
    }

    private static boolean isGTE( TargetAudience currentAudience, TargetAudience minAudienceInc ) {
        return currentAudience.isGTE(minAudienceInc);
    }
}
