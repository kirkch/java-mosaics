package mosaics.logging.writers;

import lombok.SneakyThrows;
import mosaics.io.resources.MimeType;
import mosaics.json.JsonCodec;
import mosaics.logging.LogMessage;


/**
 * Writes log messages as new line delimited json (ndjson).
 */
public class JsonLogWriter implements LogWriter {
    private final JsonCodec  jsonCodec;
    private final Appendable out;


    public JsonLogWriter( Appendable out ) {
        this( JsonCodec.ndJsonCodec, out );
    }

    public JsonLogWriter( JsonCodec jsonCodec, Appendable out ) {
        this.jsonCodec = jsonCodec;
        this.out       = out;
    }

    public MimeType getMimeType() {
        return MimeType.NDJSON;
    }

    @SneakyThrows
    public void write( LogMessage msg ) {
        jsonCodec.toJson( msg, out );
        out.append( System.lineSeparator() );
    }
}
