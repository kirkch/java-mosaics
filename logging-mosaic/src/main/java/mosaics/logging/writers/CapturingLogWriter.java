package mosaics.logging.writers;

import lombok.SneakyThrows;
import mosaics.fp.FP;
import mosaics.io.writers.AppendableWriterAdapter;
import mosaics.io.resources.MimeType;
import mosaics.lang.NotFoundException;
import mosaics.lang.functions.Function1;
import mosaics.logging.LogMessage;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;


public class CapturingLogWriter implements LogWriter {
    private final List<LogMessage> capturedMessages = new LinkedList<>();

    public MimeType getMimeType() {
        return MimeType.TEXT;
    }

    public synchronized void write( LogMessage msg ) {
        capturedMessages.add( msg );
    }

    public synchronized void clear() {
        capturedMessages.clear();
    }

    public synchronized boolean isEmpty() {
        return capturedMessages.isEmpty();
    }

    public synchronized List<LogMessage> getCapturedMessages() {
        return new LinkedList<>(capturedMessages);
    }

    public synchronized List<String> getFormattedLogMessagesByType( String targetType ) {
        return FP.wrap(capturedMessages)
            .filter( m -> targetType.equals(m.getMessageType().orElse("")) )
            .map( TextLogWriter::toString )
            .toList();
    }

    public synchronized List<LogMessage> getLogMessagesByType( String targetType ) {
        return FP.wrap(capturedMessages)
            .filter( m -> targetType.equals(m.getMessageType().orElse("")) )
            .toList();
    }

    public synchronized void assertContainsMessage( String expectedMessage ) {
        for ( LogMessage logMessage : capturedMessages ) {
            String actualMessage = TextLogWriter.toString( logMessage );

            if ( Objects.equals(expectedMessage, actualMessage) ) {
                return;
            }
        }

        throw new NotFoundException( "Missing log line: " + expectedMessage );
    }

    public synchronized void assertContainsMessage( LogMessage expectedMessage ) {
        if ( capturedMessages.stream().noneMatch(candidate -> doLogMessagesMatch(candidate,expectedMessage)) ) {
            throw new NotFoundException( "Missing log line: " + expectedMessage );
        }
    }


    public synchronized void assertLoggedMessages( LogMessage...expectedMessages ) {
        FP.wrapAll( expectedMessages ).zip( capturedMessages ).forEach(
            tuple -> {
                LogMessage a = tuple.getFirst();
                LogMessage b = tuple.getSecond();

                assertEquals( a.getMessageType(), b.getMessageType() );
                assertEquals( a.getFormattedMessage(), b.getFormattedMessage() );
                assertEquals( a.getSeverity(), b.getSeverity() );
                assertEquals( a.getTargetAudience(), b.getTargetAudience() );
                assertEquals( a.getProperties(), b.getProperties() );
            }
        );
    }

    private <T> void assertEquals( T a, T b ) {
        if (!Objects.equals( a, b)) {
            throw new IllegalArgumentException( a + " != " + b );
        }
    }

    private boolean doLogMessagesMatch( LogMessage candidate, LogMessage expectedMessage ) {
        return Objects.equals( candidate.getMessageType(), expectedMessage.getMessageType() )
            && Objects.equals( candidate.getFormattedMessage(), expectedMessage.getFormattedMessage() )
            && Objects.equals( candidate.getSeverity(), expectedMessage.getSeverity() )
            && Objects.equals( candidate.getTargetAudience(), expectedMessage.getTargetAudience() )
            && Objects.equals( candidate.getProperties(), expectedMessage.getProperties() );
    }

    public void dumpAllFormatted() {
        dumpAllFormatted( System.out );
    }

    @SneakyThrows
    public void dumpAllFormatted( Appendable out ) {
        dumpAll( out, TextLogWriter::toString );
    }

    public void dumpAll() {
        dumpAll( System.out );
    }

    public void dumpAll( Appendable out ) {
        dumpAll( out, LogMessage::toString );
    }

    @SneakyThrows
    private synchronized void dumpAll( Appendable out, Function1<LogMessage,String> formatter ) {
        for ( LogMessage logMessage : capturedMessages ) {
            String actualMessage = formatter.invoke( logMessage );

            out.append( actualMessage );
            out.append( System.lineSeparator() );

            logMessage.getProperties().forEach( (k,v) -> {
                if ( v instanceof Throwable ex ) {
                    ex.printStackTrace( new PrintWriter( new AppendableWriterAdapter(out) ) );
                }
            } );
        }
    }
}
