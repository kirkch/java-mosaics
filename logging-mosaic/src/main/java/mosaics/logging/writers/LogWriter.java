package mosaics.logging.writers;

import mosaics.fp.FP;
import mosaics.io.resources.MimeType;
import mosaics.logging.LogMessage;


/**
 * This interface abstracts out the details of how the LogMessage is stored and/or transported.
 */
public interface LogWriter {
    public MimeType getMimeType();
    public void write( LogMessage msg);

    public default void write( LogMessage...messages ) {
        write( FP.wrapArray(messages) );
    }

    public default void write( Iterable<LogMessage> messages ) {
        messages.forEach( this::write );
    }
}
