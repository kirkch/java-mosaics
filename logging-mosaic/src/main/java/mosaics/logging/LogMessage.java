package mosaics.logging;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.With;
import mosaics.fp.FP;
import mosaics.fp.LazyVal;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.maps.FPMap;
import mosaics.lang.time.DTM;
import mosaics.logging.writers.TextLogWriter;


@With
@Value
@AllArgsConstructor
@EqualsAndHashCode(exclude = "formattedMessage")
public class LogMessage {
    /**
     * The date and time of when this message was generated.
     */
    private DTM when;

    /**
     * Who the target audience of the message is.
     */
    private TargetAudience targetAudience;

    /**
     * How serious/important the message is to view and or act upon.
     */
    private LogSeverity severity;

    /**
     * A unique id that identifies the request that triggered this message.
     */
    private FPOption<String> requestId;

    /**
     * Identifies what type of message this is.
     */
    private FPOption<String> messageType;

    /**
     * The human readable log message which supports variable placeholders such as ${propertyLabel}
     * so that the variable data can be stored and queried separately.  This helps us avoid having to
     * parse log messages to understand their message.
     */
    private String messageTemplate;

    /**
     * With whose permissions is this request running with?
     */
    private FPOption<String> userId;

    /**
     * A summary of what the current request is doing.
     */
    private FPOption<String> requestDescription;

    /**
     * Key/Value pairs that are unique to this message.  They may be referenced by the messageTemplate
     * to create the human readable log line.
     */
    private FPMap<String,Object> properties;

    /**
     * Is this message part of a tracer request.  That is, it will be logged not matter what the
     * target audience or log level is.
     */
    private boolean isTracerMessage;

    public LogMessage(
        TargetAudience targetAudience,
        LogSeverity severity,
        String messageTemplate,
        FPMap<String,Object> templatePropertyValues
    ) {
        this(
            null,
            targetAudience,
            severity,
            FP.emptyOption(),
            FP.emptyOption(),
            messageTemplate,
            FP.emptyOption(),
            FP.emptyOption(),
            templatePropertyValues,
            false
        );
    }

    public LogMessage(
        TargetAudience       targetAudience,
        LogSeverity          severity,
        FPOption<String>     messageType,
        String               messageTemplate,
        FPMap<String,Object> templatePropertyValues
    ) {
        this(
            null,
            targetAudience,
            severity,
            FP.emptyOption(),
            messageType,
            messageTemplate,
            FP.emptyOption(),
            FP.emptyOption(),
            templatePropertyValues,
            false
        );
    }

    public LogMessage(
        TargetAudience targetAudience,
        LogSeverity severity,
        String messageTemplate,
        Object...templatePropertyValues
    ) {
        this(
            targetAudience,
            severity,
            messageTemplate,
            FPMap.wrapMap(templatePropertyValues)
        );
    }

    private LazyVal<String> formattedMessage = new LazyVal<>(
        () -> TextLogWriter.toString( this )
    );

    public String getFormattedMessage() {
        return formattedMessage.fetch();
    }
}
