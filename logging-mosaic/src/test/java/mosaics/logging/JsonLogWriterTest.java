package mosaics.logging;

import mosaics.fp.FP;
import mosaics.json.JsonCodec;
import mosaics.lang.time.DTM;
import mosaics.logging.writers.JsonLogWriter;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class JsonLogWriterTest {
    public static record Book(String name, int pageCount) {}

    private StringBuilder stringBuilder = new StringBuilder();
    private JsonLogWriter jsonLogWriter = new JsonLogWriter( stringBuilder );

    @Test
    public void mimeType() {
        assertEquals( "application/x-ndjson", jsonLogWriter.getMimeType().getMimeType() );
    }

    @Test
    public void testTwoLogMessages() {
        LogMessage msg1 = new LogMessage(
            new DTM( 2020, 5, 26, 10, 31, 0 ),
            TargetAudience.DEV,
            LogSeverity.INFO,
            FP.emptyOption(),
            FP.emptyOption(),
            "Hello World",
            FP.emptyOption(),
            FP.emptyOption(),
            FP.emptyMap(),
            false
        );

        LogMessage msg2 = new LogMessage(
            new DTM( 2020, 5, 26, 10, 32, 17 ),
            TargetAudience.OPS,
            LogSeverity.ERROR,
            FP.option("req2"),
            FP.option("msgType"),
            "Hello $name",
            FP.emptyOption(),
            FP.emptyOption(),
            FP.toMap("name", "Chris"),
            true
        );

        String expected = """
            {"when":"2020-05-26T10:31:00.0Z","targetAudience":"DEV","severity":"INFO","messageTemplate":"Hello World","properties":{},"isTracerMessage":false,"formattedMessage":"Hello World"}
            {"when":"2020-05-26T10:32:17.0Z","targetAudience":"OPS","severity":"ERROR","requestId":"req2","messageType":"msgType","messageTemplate":"Hello $name","properties":{"name":"Chris"},"isTracerMessage":true,"formattedMessage":"Hello Chris"}
            """.stripIndent().trim();


        assertEquals( expected, formatMessage(msg1,msg2) );
    }

    @Test
    public void testRoundTrip() {
        JsonCodec codec = JsonCodec.ndJsonCodec;
        LogMessage msg1 = new LogMessage(
            new DTM( 2020, 5, 26, 10, 31, 0 ),
            TargetAudience.DEV,
            LogSeverity.INFO,
            FP.emptyOption(),
            FP.emptyOption(),
            "Hello World",
            FP.emptyOption(),
            FP.emptyOption(),
            FP.emptyMap(),
            false
        );

        LogMessage parsedMessage = codec.fromJson(codec.toJson(msg1), LogMessage.class);


        assertEquals( "Hello World", parsedMessage.getFormattedMessage() );
    }

    @Test
    public void testRoundTripWithMessageContainingAnException() {
        JsonCodec codec = JsonCodec.ndJsonCodec;
        LogMessage msg1 = new LogMessage(
            new DTM( 2020, 5, 26, 10, 31, 0 ),
            TargetAudience.DEV,
            LogSeverity.INFO,
            FP.emptyOption(),
            FP.emptyOption(),
            "Hello $ex",
            FP.emptyOption(),
            FP.emptyOption(),
            FP.toMap("ex", new RuntimeException("exception msg")),
            false
        );

        String     json          = codec.toJson( msg1 );
        LogMessage parsedMessage = codec.fromJson( json, LogMessage.class);


        assertEquals( "Hello java.lang.RuntimeException: exception msg", parsedMessage.getFormattedMessage() );
    }

    private String formatMessage( LogMessage...messages ) {
        return formatMessage( Arrays.asList(messages) );
    }

    private String formatMessage( List<LogMessage> messages ) {
        jsonLogWriter.write( messages );

        return stringBuilder.toString().trim();
    }
}
