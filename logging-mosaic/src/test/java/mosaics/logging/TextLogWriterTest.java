package mosaics.logging;

import mosaics.fp.FP;
import mosaics.junit.JMAssertions;
import mosaics.lang.time.DTM;
import mosaics.logging.writers.TextLogWriter;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;


public class TextLogWriterTest extends JMAssertions {

    @Nested
    public class LogLevelTestCases {
        @Test
        public void basicDevInfoMessage() {
            LogMessage msg = new LogMessage(
                new DTM( 2020, 5, 26, 10, 31, 0 ),
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.emptyOption(),
                FP.emptyOption(),
                "Hello World",
                FP.emptyOption(),
                FP.emptyOption(),
                FP.emptyMap(),
                false
            );

            assertEquals( "[2020-05-26 10:31:00 DEVINFO -]: Hello World", formatMessage( msg ) );
        }

        @ParameterizedTest
        @CsvSource({
            "DEV,  FATAL", "DEV,  ERROR", "DEV,  WARN", "DEV,  INFO",
            "OPS,  FATAL", "OPS,  ERROR", "OPS,  WARN", "OPS,  INFO",
            "USER, FATAL", "USER, ERROR", "USER, WARN", "USER, INFO",
            "AUDIT,FATAL", "AUDIT,ERROR", "AUDIT,WARN", "AUDIT,INFO",
        })
        public void logLevel( TargetAudience audience, LogSeverity logSeverity ) {
            LogMessage msg = new LogMessage(
                new DTM(2020,5,26, 10, 31, 0),
                audience,
                logSeverity,
                FP.emptyOption(),
                FP.emptyOption(),
                "Hello World",
                FP.emptyOption(),
                FP.emptyOption(),
                FP.emptyMap(),
                false
            );

            assertEquals( "[2020-05-26 10:31:00 "+audience+logSeverity+" -]: Hello World", formatMessage( msg ) );
        }
    }

    @Test
    public void requestId() {
        LogMessage msg = new LogMessage(
            new DTM( 2020, 5, 26, 10, 31, 0 ),
            TargetAudience.DEV,
            LogSeverity.INFO,
            FP.option("requestId"),
            FP.emptyOption(),
            "Hello World",
            FP.emptyOption(),
            FP.emptyOption(),
            FP.emptyMap(),
            false
        );

        assertEquals( "[2020-05-26 10:31:00 DEVINFO requestId]: Hello World", formatMessage( msg ) );
    }

    @Test
    public void msgType() {
        LogMessage msg = new LogMessage(
            new DTM( 2020, 5, 26, 10, 31, 0 ),
            TargetAudience.DEV,
            LogSeverity.INFO,
            FP.emptyOption(),
            FP.option("msgType"),
            "Hello World",
            FP.emptyOption(),
            FP.emptyOption(),
            FP.emptyMap(),
            false
        );

        assertEquals( "[2020-05-26 10:31:00 DEVINFO -]: Hello World", formatMessage(msg) );
    }

    @Nested
    public class ExceptionStackTraceTestCases {
        @Test
        public void givenExcludeStackTracesExpectNoStackTrace() {
            LogMessage msg = new LogMessage(
                new DTM( 2020, 5, 26, 10, 31, 0 ),
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.emptyOption(),
                FP.option("msgType"),
                "Hello World",
                FP.emptyOption(),
                FP.emptyOption(),
                FP.toMap(
                    "ex", new RuntimeException("expected exception")
                ),
                false
            );

            assertFalse( formatMessage(msg, false).contains("TextLogWriterTest$ExceptionStackTraceTestCases") );
        }

        @Test
        public void givenIncludeStackTracesExpectStackTrace() {
            LogMessage msg = new LogMessage(
                new DTM( 2020, 5, 26, 10, 31, 0 ),
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.emptyOption(),
                FP.option("msgType"),
                "Hello World",
                FP.emptyOption(),
                FP.emptyOption(),
                FP.toMap(
                    "ex", new RuntimeException("expected exception")
                ),
                false
            );

            assertTrue( formatMessage(msg, true).contains("TextLogWriterTest$ExceptionStackTraceTestCases") );
        }
    }

    @Nested
    public class TemplatePropertySubstitutionTestCases {
        @Test
        public void propertiesGetUsed() {
            LogMessage msg = new LogMessage(
                new DTM( 2020, 5, 26, 10, 31, 0 ),
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.emptyOption(),
                FP.emptyOption(),
                "Hello $name",
                FP.emptyOption(),
                FP.emptyOption(),
                FP.toMap("name", "James"),
                false
            );

            assertEquals( "[2020-05-26 10:31:00 DEVINFO -]: Hello James", formatMessage(msg) );
        }

        @Test
        public void logMessageFieldsAreAvailable() {
            LogMessage msg = new LogMessage(
                new DTM( 2020, 5, 26, 10, 31, 0 ),
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.option("req1"),
                FP.option("type1"),
                "Hello ${when} $messageType $requestId $severity $targetAudience $name",
                FP.emptyOption(),
                FP.emptyOption(),
                FP.toMap("name", "Alice"),
                false
            );

            String expected = "[2020-05-26 10:31:00 DEVINFO req1]: Hello 2020-05-26 10:31:00 type1 req1 INFO DEV Alice";

            assertEquals( expected, formatMessage(msg) );
        }

        @Test
        public void changeLogLinePrefix() {
            LogMessage msg = new LogMessage(
                new DTM( 2020, 5, 26, 10, 31, 0 ),
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.option("req1"),
                FP.option("type1"),
                "Hello ${when} $messageType $requestId $severity $targetAudience $name",
                FP.emptyOption(),
                FP.emptyOption(),
                FP.toMap("name", "Alice"),
                false
            );

            String prefix   = "-- $severity -- ";
            String expected = "-- INFO -- Hello 2020-05-26 10:31:00 type1 req1 INFO DEV Alice";

            assertEquals( expected, formatMessageWithCustomPrefix(prefix, msg) );
        }

        private String formatMessageWithCustomPrefix( String prefix, LogMessage msg ) {
            StringWriter  stringWriter  = new StringWriter();
            TextLogWriter textLogWriter = new TextLogWriter(stringWriter).withMessageTemplatePrefix( prefix );

            textLogWriter.write( msg );

            return stringWriter.toString().trim();
        }
    }

    private record LogSpec(String type, String msg) {}

    @Nested
    public class CacheBehaviourTestCases {
        private DTM currentTime = new DTM( 2020, 5, 26, 10, 31, 0 );

        @Test
        public void multipleLinesWithMessageType() {
            List<LogMessage> messages = logMessages(
                new LogSpec("t1", "msg1"),
                new LogSpec("t2", "msg2"),
                new LogSpec("t3", "msg3")
            );

            String expected = """
               [2020-05-26 10:31:00 DEVINFO -]: msg1
               [2020-05-26 10:31:01 DEVINFO -]: msg2
               [2020-05-26 10:31:02 DEVINFO -]: msg3
                """.stripIndent().trim();

            assertEquals(expected, formatMessage(messages) );
        }

        @Test
        public void multipleLinesWithNoMessageType() {
            List<LogMessage> messages = logMessages(
                new LogSpec(null, "msg1"),
                new LogSpec(null, "msg2"),
                new LogSpec(null, "msg3")
            );

            String expected = """
               [2020-05-26 10:31:00 DEVINFO -]: msg1
               [2020-05-26 10:31:01 DEVINFO -]: msg2
               [2020-05-26 10:31:02 DEVINFO -]: msg3
                """.stripIndent().trim();

            assertEquals(expected, formatMessage(messages) );
        }

        @Test
        public void multipleLinesWithSameMessageTypeAndDifferentMessage_thisIsAnErrorWhereTheCacheWillCauseTheDifferingMessagesToGetLost_alwaysGiveADifferentMessageTypeToAMessage() {
            List<LogMessage> messages = logMessages(
                new LogSpec("t1", "msg1"),
                new LogSpec("t1", "msg2"),
                new LogSpec("t1", "msg3")
            );

            String expected = """
               [2020-05-26 10:31:00 DEVINFO -]: msg1
               [2020-05-26 10:31:01 DEVINFO -]: msg1
               [2020-05-26 10:31:02 DEVINFO -]: msg1
                """.stripIndent().trim();

            assertEquals(expected, formatMessage(messages) );
        }

        @Test
        public void multipleLinesWithSameMessageTypeAndDifferentMessage_thisIsAnErrorWhereTheCacheWillCauseTheDifferingMessagesToGetLost_alwaysGiveADifferentMessageTypeToAMessage2() {
            List<LogMessage> messages = logMessages(
                new LogSpec("t1", "msg1"),
                new LogSpec("t2", "msg2"),
                new LogSpec("t2", "msg3")
            );

            String expected = """
               [2020-05-26 10:31:00 DEVINFO -]: msg1
               [2020-05-26 10:31:01 DEVINFO -]: msg2
               [2020-05-26 10:31:02 DEVINFO -]: msg2
                """.stripIndent().trim();

            assertEquals(expected, formatMessage(messages) );
        }

        private List<LogMessage> logMessages(LogSpec...msgTypes) {
            return FP.wrapArray(msgTypes).map( spec -> new LogMessage(
                getCurrentTimeAndInc(),
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.emptyOption(),
                FP.option( spec.type() ),
                spec.msg(),
                FP.emptyOption(),
                FP.emptyOption(),
                FP.emptyMap(),
                false
            )).toList();
        }

        private DTM getCurrentTimeAndInc() {
            DTM t = currentTime;

            currentTime = t.addSecond();

            return t;
        }
    }

    private static String formatMessage( LogMessage...messages ) {
        return formatMessage( Arrays.asList(messages), false );
    }

    private static String formatMessage( LogMessage messages, boolean incStackTraces ) {
        return formatMessage( Arrays.asList(messages), incStackTraces );
    }

    private static String formatMessage( List<LogMessage> message ) {
        return formatMessage( message, false );
    }

    private static String formatMessage( List<LogMessage> messages, boolean incStackTraces ) {
        StringWriter  stringWriter  = new StringWriter();
        TextLogWriter textLogWriter = new TextLogWriter(stringWriter, incStackTraces);

        textLogWriter.write( messages );

        return stringWriter.toString().trim();
    }
}
