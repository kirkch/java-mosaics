package mosaics.logging.writers;

import mosaics.io.resources.MimeType;
import mosaics.logging.LogFilter;
import mosaics.logging.LogMessage;
import mosaics.logging.LogSeverity;
import mosaics.logging.TargetAudience;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


public class LogFilterWriterTest {
    private LogFilter       logFilterMock        = Mockito.mock(LogFilter.class);
    private LogWriter       wrappedLogWriterMock = Mockito.mock(LogWriter.class);
    private LogFilterWriter logFilterWriter      = new LogFilterWriter( logFilterMock, wrappedLogWriterMock );

    private LogMessage      logMessage           = new LogMessage( TargetAudience.DEV, LogSeverity.INFO, "msg" );

    @Test
    public void callGetMimeType_expectPassThroughToWrappedLogWriter() {
        when(wrappedLogWriterMock.getMimeType()).thenReturn( MimeType.BINARY );

        assertEquals( MimeType.BINARY, logFilterWriter.getMimeType() );

        verify(wrappedLogWriterMock).getMimeType();
        verifyNoMoreInteractions(wrappedLogWriterMock,logFilterMock);
    }

    @Test
    public void givenLogFilterRejectsMessage_callWrite_expectWrappedLogWriterToNOTBeCalled() {
        when(logFilterMock.accept(logMessage)).thenReturn( false );

        logFilterWriter.write(logMessage);

        verifyNoMoreInteractions(wrappedLogWriterMock);
    }

    @Test
    public void givenLogFilterAcceptsMessage_callWrite_expectWrappedLogWriterToBeCalled() {
        when(logFilterMock.accept(logMessage)).thenReturn( true );

        logFilterWriter.write(logMessage);

        verify(wrappedLogWriterMock).write( logMessage );

        verifyNoMoreInteractions(wrappedLogWriterMock);
    }
}
