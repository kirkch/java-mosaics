package mosaics.logging;

import mosaics.fp.FP;
import mosaics.lang.time.DTM;
import org.junit.jupiter.api.Test;

import static mosaics.logging.LogSeverity.ERROR;
import static mosaics.logging.LogSeverity.FATAL;
import static mosaics.logging.LogSeverity.INFO;
import static mosaics.logging.LogSeverity.WARN;
import static mosaics.logging.TargetAudience.AUDIT;
import static mosaics.logging.TargetAudience.DEV;
import static mosaics.logging.TargetAudience.OPS;
import static mosaics.logging.TargetAudience.USER;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;



public class LogFilterTest {
    @Test
    public void testDebug() {
        assertTrue( LogFilter.DEBUG.accept(ev(DEV, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(ev(DEV, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(ev(DEV, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(ev(DEV, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(ev(OPS, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(ev(OPS, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(ev(OPS, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(ev(OPS, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(ev(USER, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(ev(USER, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(ev(USER, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(ev(USER, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(ev(AUDIT, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(ev(AUDIT, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(ev(AUDIT, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(ev(AUDIT, INFO)) );
    }

    @Test
    public void testVerbose() {
        assertTrue( LogFilter.VERBOSE.accept(ev(DEV, FATAL)) );
        assertTrue( LogFilter.VERBOSE.accept(ev(DEV, ERROR)) );
        assertFalse( LogFilter.VERBOSE.accept(ev(DEV, WARN)) );
        assertFalse( LogFilter.VERBOSE.accept(ev(DEV, INFO)) );

        assertTrue( LogFilter.VERBOSE.accept(ev(OPS, FATAL)) );
        assertTrue( LogFilter.VERBOSE.accept(ev(OPS, ERROR)) );
        assertTrue( LogFilter.VERBOSE.accept(ev(OPS, WARN)) );
        assertTrue( LogFilter.VERBOSE.accept(ev(OPS, INFO)) );

        assertTrue( LogFilter.VERBOSE.accept(ev(USER, FATAL)) );
        assertTrue( LogFilter.VERBOSE.accept(ev(USER, ERROR)) );
        assertTrue( LogFilter.VERBOSE.accept(ev(USER, WARN)) );
        assertTrue( LogFilter.VERBOSE.accept(ev(USER, INFO)) );

        assertTrue( LogFilter.VERBOSE.accept(ev(AUDIT, FATAL)) );
        assertTrue( LogFilter.VERBOSE.accept(ev(AUDIT, ERROR)) );
        assertTrue( LogFilter.VERBOSE.accept(ev(AUDIT, WARN)) );
        assertTrue( LogFilter.VERBOSE.accept(ev(AUDIT, INFO)) );
    }

    @Test
    public void testNormal() {
        assertTrue( LogFilter.NORMAL.accept(ev(DEV, FATAL)) );
        assertTrue( LogFilter.NORMAL.accept(ev(DEV, ERROR)) );
        assertFalse( LogFilter.NORMAL.accept(ev(DEV, WARN)) );
        assertFalse( LogFilter.NORMAL.accept(ev(DEV, INFO)) );

        assertTrue( LogFilter.NORMAL.accept(ev(OPS, FATAL)) );
        assertTrue( LogFilter.NORMAL.accept(ev(OPS, ERROR)) );
        assertFalse( LogFilter.NORMAL.accept(ev(OPS, WARN)) );
        assertFalse( LogFilter.NORMAL.accept(ev(OPS, INFO)) );

        assertTrue( LogFilter.NORMAL.accept(ev(USER, FATAL)) );
        assertTrue( LogFilter.NORMAL.accept(ev(USER, ERROR)) );
        assertTrue( LogFilter.NORMAL.accept(ev(USER, WARN)) );
        assertTrue( LogFilter.NORMAL.accept(ev(USER, INFO)) );

        assertTrue( LogFilter.NORMAL.accept(ev(AUDIT, FATAL)) );
        assertTrue( LogFilter.NORMAL.accept(ev(AUDIT, ERROR)) );
        assertTrue( LogFilter.NORMAL.accept(ev(AUDIT, WARN)) );
        assertTrue( LogFilter.NORMAL.accept(ev(AUDIT, INFO)) );
    }

    @Test
    public void testAudit() {
        assertTrue( LogFilter.AUDIT.accept(ev(DEV, FATAL)) );
        assertTrue( LogFilter.AUDIT.accept(ev(DEV, ERROR)) );
        assertFalse( LogFilter.AUDIT.accept(ev(DEV, WARN)) );
        assertFalse( LogFilter.AUDIT.accept(ev(DEV, INFO)) );

        assertTrue( LogFilter.AUDIT.accept(ev(OPS, FATAL)) );
        assertTrue( LogFilter.AUDIT.accept(ev(OPS, ERROR)) );
        assertFalse( LogFilter.AUDIT.accept(ev(OPS, WARN)) );
        assertFalse( LogFilter.AUDIT.accept(ev(OPS, INFO)) );

        assertTrue( LogFilter.AUDIT.accept(ev(USER, FATAL)) );
        assertTrue( LogFilter.AUDIT.accept(ev(USER, ERROR)) );
        assertFalse( LogFilter.AUDIT.accept(ev(USER, WARN)) );
        assertFalse( LogFilter.AUDIT.accept(ev(USER, INFO)) );

        assertTrue( LogFilter.AUDIT.accept(ev(AUDIT, FATAL)) );
        assertTrue( LogFilter.AUDIT.accept(ev(AUDIT, ERROR)) );
        assertTrue( LogFilter.AUDIT.accept(ev(AUDIT, WARN)) );
        assertTrue( LogFilter.AUDIT.accept(ev(AUDIT, INFO)) );
    }

    @Test
    public void testErrorsOnly() {
        assertTrue( LogFilter.ERRORS_ONLY.accept(ev(DEV, FATAL)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(ev(DEV, ERROR)) );
        assertFalse( LogFilter.ERRORS_ONLY.accept(ev(DEV, WARN)) );
        assertFalse( LogFilter.ERRORS_ONLY.accept(ev(DEV, INFO)) );

        assertTrue( LogFilter.ERRORS_ONLY.accept(ev(OPS, FATAL)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(ev(OPS, ERROR)) );
        assertFalse( LogFilter.ERRORS_ONLY.accept(ev(OPS, WARN)) );
        assertFalse( LogFilter.ERRORS_ONLY.accept(ev(OPS, INFO)) );

        assertTrue( LogFilter.ERRORS_ONLY.accept(ev(USER, FATAL)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(ev(USER, ERROR)) );
        assertFalse( LogFilter.ERRORS_ONLY.accept(ev(USER, WARN)) );
        assertFalse( LogFilter.ERRORS_ONLY.accept(ev(USER, INFO)) );

        assertTrue( LogFilter.ERRORS_ONLY.accept(ev(AUDIT, FATAL)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(ev(AUDIT, ERROR)) );
        assertFalse( LogFilter.ERRORS_ONLY.accept(ev(AUDIT, WARN)) );
        assertFalse( LogFilter.ERRORS_ONLY.accept(ev(AUDIT, INFO)) );
    }

    @Test
    public void testSilent() {
        assertFalse( LogFilter.SILENT.accept(ev(DEV, FATAL)) );
        assertFalse( LogFilter.SILENT.accept(ev(DEV, ERROR)) );
        assertFalse( LogFilter.SILENT.accept(ev(DEV, WARN)) );
        assertFalse( LogFilter.SILENT.accept(ev(DEV, INFO)) );

        assertFalse( LogFilter.SILENT.accept(ev(OPS, FATAL)) );
        assertFalse( LogFilter.SILENT.accept(ev(OPS, ERROR)) );
        assertFalse( LogFilter.SILENT.accept(ev(OPS, WARN)) );
        assertFalse( LogFilter.SILENT.accept(ev(OPS, INFO)) );

        assertFalse( LogFilter.SILENT.accept(ev(USER, FATAL)) );
        assertFalse( LogFilter.SILENT.accept(ev(USER, ERROR)) );
        assertFalse( LogFilter.SILENT.accept(ev(USER, WARN)) );
        assertFalse( LogFilter.SILENT.accept(ev(USER, INFO)) );

        assertFalse( LogFilter.SILENT.accept(ev(AUDIT, FATAL)) );
        assertFalse( LogFilter.SILENT.accept(ev(AUDIT, ERROR)) );
        assertFalse( LogFilter.SILENT.accept(ev(AUDIT, WARN)) );
        assertFalse( LogFilter.SILENT.accept(ev(AUDIT, INFO)) );
    }

    @Test
    public void testDebugWithTracer() {
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(evT(OPS, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(OPS, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(OPS, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(evT(OPS, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(evT(USER, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(USER, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(USER, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(evT(USER, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(evT(AUDIT, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(AUDIT, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(AUDIT, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(evT(AUDIT, INFO)) );
    }

    @Test
    public void testVerboseWithTracer() {
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(evT(OPS, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(OPS, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(OPS, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(evT(OPS, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(evT(USER, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(USER, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(USER, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(evT(USER, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(evT(AUDIT, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(AUDIT, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(AUDIT, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(evT(AUDIT, INFO)) );
    }

    @Test
    public void testNormalWithTracer() {
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(evT(DEV, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(evT(OPS, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(OPS, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(OPS, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(evT(OPS, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(evT(USER, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(USER, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(USER, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(evT(USER, INFO)) );

        assertTrue( LogFilter.DEBUG.accept(evT(AUDIT, FATAL)) );
        assertTrue( LogFilter.DEBUG.accept(evT(AUDIT, ERROR)) );
        assertTrue( LogFilter.DEBUG.accept(evT(AUDIT, WARN)) );
        assertTrue( LogFilter.DEBUG.accept(ev(AUDIT, INFO)) );
    }

    @Test
    public void testAuditWithTracer() {
        assertTrue( LogFilter.AUDIT.accept(evT(DEV, FATAL)) );
        assertTrue( LogFilter.AUDIT.accept(evT(DEV, ERROR)) );
        assertTrue( LogFilter.AUDIT.accept(evT(DEV, WARN)) );
        assertTrue( LogFilter.AUDIT.accept(evT(DEV, INFO)) );

        assertTrue( LogFilter.AUDIT.accept(evT(OPS, FATAL)) );
        assertTrue( LogFilter.AUDIT.accept(evT(OPS, ERROR)) );
        assertTrue( LogFilter.AUDIT.accept(evT(OPS, WARN)) );
        assertTrue( LogFilter.AUDIT.accept(evT(OPS, INFO)) );

        assertTrue( LogFilter.AUDIT.accept(evT(USER, FATAL)) );
        assertTrue( LogFilter.AUDIT.accept(evT(USER, ERROR)) );
        assertTrue( LogFilter.AUDIT.accept(evT(USER, WARN)) );
        assertTrue( LogFilter.AUDIT.accept(evT(USER, INFO)) );

        assertTrue( LogFilter.AUDIT.accept(evT(AUDIT, FATAL)) );
        assertTrue( LogFilter.AUDIT.accept(evT(AUDIT, ERROR)) );
        assertTrue( LogFilter.AUDIT.accept(evT(AUDIT, WARN)) );
        assertTrue( LogFilter.AUDIT.accept(evT(AUDIT, INFO)) );
    }

    @Test
    public void testErrorsOnlyWithTracer() {
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(DEV, FATAL)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(DEV, ERROR)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(DEV, WARN)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(DEV, INFO)) );

        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(OPS, FATAL)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(OPS, ERROR)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(OPS, WARN)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(OPS, INFO)) );

        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(USER, FATAL)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(USER, ERROR)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(USER, WARN)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(USER, INFO)) );

        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(AUDIT, FATAL)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(AUDIT, ERROR)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(AUDIT, WARN)) );
        assertTrue( LogFilter.ERRORS_ONLY.accept(evT(AUDIT, INFO)) );
    }

    @Test
    public void testSilentWithTracer() {
        assertTrue( LogFilter.SILENT.accept(evT(DEV, FATAL)) );
        assertTrue( LogFilter.SILENT.accept(evT(DEV, ERROR)) );
        assertTrue( LogFilter.SILENT.accept(evT(DEV, WARN)) );
        assertTrue( LogFilter.SILENT.accept(evT(DEV, INFO)) );

        assertTrue( LogFilter.SILENT.accept(evT(OPS, FATAL)) );
        assertTrue( LogFilter.SILENT.accept(evT(OPS, ERROR)) );
        assertTrue( LogFilter.SILENT.accept(evT(OPS, WARN)) );
        assertTrue( LogFilter.SILENT.accept(evT(OPS, INFO)) );

        assertTrue( LogFilter.SILENT.accept(evT(USER, FATAL)) );
        assertTrue( LogFilter.SILENT.accept(evT(USER, ERROR)) );
        assertTrue( LogFilter.SILENT.accept(evT(USER, WARN)) );
        assertTrue( LogFilter.SILENT.accept(evT(USER, INFO)) );

        assertTrue( LogFilter.SILENT.accept(evT(AUDIT, FATAL)) );
        assertTrue( LogFilter.SILENT.accept(evT(AUDIT, ERROR)) );
        assertTrue( LogFilter.SILENT.accept(evT(AUDIT, WARN)) );
        assertTrue( LogFilter.SILENT.accept(evT(AUDIT, INFO)) );
    }


    private LogMessage ev( TargetAudience audience, LogSeverity severity ) {
        return ev(audience, severity, false);
    }

    private LogMessage evT( TargetAudience audience, LogSeverity severity ) {
        return ev(audience, severity, true);
    }

    private LogMessage ev( TargetAudience audience, LogSeverity severity, boolean withTracer ) {
        return new LogMessage(
            new DTM(2020,5,26, 11,30),
            audience,
            severity,
            FP.emptyOption(),
            FP.emptyOption(),
            "template",
            FP.emptyOption(),
            FP.emptyOption(),
            FP.emptyMap(),
            withTracer
        );
    }
}
