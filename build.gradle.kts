plugins {
    `kotlin-dsl`
    id("com.github.ben-manes.versions").version("0.42.0")
}


repositories {
    gradlePluginPortal()
}

// Currently this test report sucks.  It only runs when all of the tests pass.
// See https://docs.gradle.org/current/userguide/java_testing.html#test_reporting.
// attempts to use finalisedBy have so far failed.
val testReportData by configurations.creating {
    isCanBeResolved = true
    isCanBeConsumed = false
    attributes {
        attribute(Category.CATEGORY_ATTRIBUTE, objects.named(Category.DOCUMENTATION))
        attribute(DocsType.DOCS_TYPE_ATTRIBUTE, objects.named("test-report-data"))
    }
}

dependencies {
    testReportData(project(":java-mosaic"))
    testReportData(project(":logging-mosaic"))
    testReportData(project(":application-mosaic"))
}

tasks.register<TestReport>("testReport") {
    destinationDir = layout.buildDirectory.dir("reports/junit").get().asFile

    (getTestResultDirs() as ConfigurableFileCollection).from(testReportData)
}

//tasks.named<Test>("test") {
////    finalizedBy(tasks.named<TestReport>("testReport"))
//    finalizedBy("testReport")
//}
