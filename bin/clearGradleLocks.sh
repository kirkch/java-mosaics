#!/bin/bash

set -eu


# use when gradle reports that it cannot proceed due to locked files (I get into this by running
# gradle outside of docker and then moving into docker and running gradle again)

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"

echo Stopping Gradle Daemons
$DIR/gradlew --stop

echo Deleting lock files...
find $DIR/.gradle -type f -name "*.lock" -delete
echo done
