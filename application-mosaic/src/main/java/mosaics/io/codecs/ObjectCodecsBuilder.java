package mosaics.io.codecs;

import mosaics.fp.collections.maps.FPMap;
import mosaics.fp.collections.RRBVector;
import mosaics.fp.collections.Tuple2;
import mosaics.io.codecs.gson.GsonObjectCodecBuilder;
import mosaics.io.codecs.html.HtmlObjectCodecBuilder;
import mosaics.io.codecs.text.StringObjectCodecBuilder;
import mosaics.io.resources.MimeType;
import mosaics.lang.functions.VoidFunction0;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Create a set of ObjectCodecs keyed by MimeType.
 */
public class ObjectCodecsBuilder {

    private RRBVector<ObjectCodecBuilder<?>> builders = RRBVector.emptyVector();

    private List<VoidFunction0> builderActions = new ArrayList<>();


    public ObjectCodecsBuilder supportsAll() {
        return supportsGson()
            .supportsHtml()
            .supportsText();
    }

    public ObjectCodecsBuilder supportsGson() {
        return supports( new GsonObjectCodecBuilder() );
    }

    public ObjectCodecsBuilder supportsHtml() {
        return supports( new HtmlObjectCodecBuilder() );
    }

    public ObjectCodecsBuilder supportsText() {
        return supports( new StringObjectCodecBuilder() );
    }

    public ObjectCodecsBuilder supports( ObjectCodecBuilder builder ) {
        builders = builders.add( builder );

        return this;
    }

    public ObjectCodecsBuilder identifyInheritanceTreeByClassName( Class<?> baseType ) {
        builderActions.add( () -> builders.forEach(builder -> builder.identifyInheritanceTreeByClassName(baseType)) );

        return this;
    }

    public ObjectCodecsBuilder identifyInheritanceTreeByClassName( Class<?> baseType, String propertyName ) {
        builderActions.add( () -> builders.forEach(builder -> builder.identifyInheritanceTreeByClassName(baseType, propertyName)) );

        return this;
    }

    public ObjectCodecsBuilder identifyInheritanceTreeByPreRegisteredIdentifiers( Class<?> baseType, Map<Class, String> mappings ) {
        builderActions.add( () -> builders.forEach(builder -> builder.identifyInheritanceTreeByPreRegisteredIdentifiers(baseType, mappings)) );

        return this;
    }

    public ObjectCodecsBuilder identifyInheritanceTreeByPreRegisteredIdentifiers( Class<?> baseType, String propertyName, Map<Class,String> mappings ) {
        builderActions.add( () -> builders.forEach(builder -> builder.identifyInheritanceTreeByPreRegisteredIdentifiers(baseType, propertyName, mappings)) );

        return this;
    }

    public ObjectCodecs create() {
        builderActions.forEach( VoidFunction0::invoke );

        FPMap<MimeType, ObjectCodec> codecs                     = builders.map( ObjectCodecBuilder::create ).toMap( codec -> new Tuple2<>(codec.getCodecMimeType(), codec) );
        RRBVector<MimeType>          mimeTypesInPreferenceOrder = builders.map(ObjectCodecBuilder::getCodecMimeType ).toRRBVector();
        ObjectCodec                  defaultCodec               = mimeTypesInPreferenceOrder.first().flatMap(codecs::get).orNull();

        return new ObjectCodecs( codecs, defaultCodec, mimeTypesInPreferenceOrder );
    }

}
