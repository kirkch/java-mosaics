package mosaics.io.codecs.gson;

import mosaics.io.codecs.ObjectCodecBuilder;
import mosaics.io.resources.MimeType;
import mosaics.json.JsonCodecBuilder;

import java.util.Map;


/**
 *
 */
@SuppressWarnings( "unchecked" )
public class GsonObjectCodecBuilder implements ObjectCodecBuilder<GsonObjectCodec> {

    private JsonCodecBuilder jsonBuilder = new JsonCodecBuilder();


    public MimeType getCodecMimeType() {
        return MimeType.JSON;
    }

    public GsonObjectCodecBuilder identifyInheritanceTreeByClassName( Class<?> baseType ) {
        this.jsonBuilder = jsonBuilder.identifyInheritanceTreeByClassName( baseType );

        return this;
    }

    public GsonObjectCodecBuilder identifyInheritanceTreeByClassName( Class<?> baseType, String propertyName ) {
        this.jsonBuilder = jsonBuilder.identifyInheritanceTreeByClassName( baseType, propertyName );

        return this;
    }

    public GsonObjectCodecBuilder identifyInheritanceTreeByPreRegisteredIdentifiers( Class<?> baseType, Map<Class,String> mappings ) {
        this.jsonBuilder = jsonBuilder.identifyInheritanceTreeByPreRegisteredIdentifiers( baseType, mappings );

        return this;
    }

    public GsonObjectCodecBuilder identifyInheritanceTreeByPreRegisteredIdentifiers( Class<?> baseType, String propertyName, Map<Class,String> mappings ) {
        this.jsonBuilder = jsonBuilder.identifyInheritanceTreeByPreRegisteredIdentifiers( baseType, propertyName, mappings );

        return this;
    }

    public GsonObjectCodec create() {
        return new GsonObjectCodec( jsonBuilder.create() );
    }
}


