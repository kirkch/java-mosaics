package mosaics.io.codecs.text;

import mosaics.io.codecs.ObjectCodecBuilder;
import mosaics.io.resources.MimeType;

import java.util.Map;


/**
 *
 */
public class StringObjectCodecBuilder implements ObjectCodecBuilder<StringObjectCodec> {

    public MimeType getCodecMimeType() {
        return MimeType.TEXT;
    }

    public StringObjectCodecBuilder identifyInheritanceTreeByClassName( Class<?> baseType ) {
        return this;
    }

    public StringObjectCodecBuilder identifyInheritanceTreeByClassName( Class<?> baseType, String propertyName ) {
        return this;
    }

    public StringObjectCodecBuilder identifyInheritanceTreeByPreRegisteredIdentifiers( Class<?> baseType, Map<Class, String> mappings ) {
        return this;
    }

    public StringObjectCodecBuilder identifyInheritanceTreeByPreRegisteredIdentifiers( Class<?> baseType, String propertyName, Map<Class, String> mappings ) {
        return this;
    }

    public StringObjectCodec create() {
        return StringObjectCodec.INSTANCE;
    }

}
