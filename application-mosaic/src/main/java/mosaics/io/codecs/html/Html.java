package mosaics.io.codecs.html;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Annotation used to override default HTML formatting of POJOs.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Html {
    /**
     * Specify the output format of the HTML.  When empty, a default rendering of the POJO will be
     * used.  When specified, this template will be used to generate the HTML.  See
     * HtmlObjectCodec_annotationOverrideTests for examples of the supported template syntax.
     *
     * Template syntax overview:
     *
     * <div>hello world</div>
     * <div>$propertyName</div>
     * <div>${prop1.prop2.prop3}</div>
     * #if $prop1 <div>$prop1</div> #elseif $prop3 <div>$prop3</div> #else <div>$prop2</div> #endif
     * #foreach $prop1 <div>$this</div> #endeach
     */
    public String[] template() default {};

    /**
     * Specify which properties on the POJO to include in the HTML.  When empty, reflection will
     * be used to find all of the properties declared on the POJO.
     */
    public String[] properties() default {};
}
