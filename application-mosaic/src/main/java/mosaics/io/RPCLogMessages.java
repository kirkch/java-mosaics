//package com.softwaremosaic.app;
//
//import com.softwaremosaic.logging.LogMessage;
//
//
//public class RPCLogMessages {
//
//    public static RPCStarted rpcStarted( String destination ) {
//        return new RPCStarted( destination );
//    }
//
//    public static RPCFailed rpcFailed( String destination, long durationMillis, String failureCode, String failureMessage ) {
//        return new RPCFailed( destination, durationMillis, failureCode, failureMessage, null );
//    }
//
//    public static RPCFailed rpcFailed( String destination, long durationMillis, String failureCode, String failureMessage, Throwable ex  ) {
//        return new RPCFailed( destination, durationMillis, failureCode, failureMessage, ex );
//    }
//
//    public static RPCSuccessful rpcSuccessful( String destination, long durationMillis, String resultCode ) {
//        return new RPCSuccessful( destination, durationMillis, resultCode );
//    }
//
//    public static LogMessage requestRetryScheduled( String desc, long inMillis, int attemptNum, int numRetriesLeft, Throwable ex ) {
//        return new RPCRetryScheduled(desc, inMillis, attemptNum, numRetriesLeft, ex);
//    }
//
//    public static LogMessage requestRetryStarted( String dest, int retryNum ) {
//        return new RequestRetryStarted(dest, retryNum);
//    }
//
//
//    public static class RPCStarted extends LogMessage.AuditInfo {
//        private String destination;
//
//        public RPCStarted( String destination ) {
//            super("Remote call to '$destination' started.");
//
//            this.destination = destination;
//        }
//    }
//
//    public static class RPCSuccessful extends LogMessage.AuditInfo {
//        private String destination;
//        private long   durationMillis;
//        private String resultCode;
//
//        public RPCSuccessful( String destination, long durationMillis, String resultCode ) {
//            super("Remote call to '$destination' completed successfully in ${durationMillis:duration}.  Result code = ${resultCode}.");
//
//            this.destination    = destination;
//            this.durationMillis = durationMillis;
//            this.resultCode     = resultCode;
//        }
//    }
//
//    public static class RPCFailed extends LogMessage.AuditError {
//        private String    destination;
//        private long      durationMillis;
//        private String    failureCode;
//        private String    failureMessage;
//        private Throwable cause;
//
//        public RPCFailed( String destination, long durationMillis, String failureCode, String failureMessage, Throwable cause ) {
//            super("Remote call to '$destination' failed after ${durationMillis:duration}.  Error code = ${resultCode}: ${failureMessage}. ${cause}.");
//
//            this.destination    = destination;
//            this.durationMillis = durationMillis;
//            this.failureCode    = failureCode;
//            this.failureMessage = failureMessage;
//            this.cause          = cause;
//        }
//    }
//
//    public static class RPCRetryScheduled extends LogMessage.AuditError {
//        private String    destination;
//        private long      inMillis;
//        private int       attemptNum;
//        private int       numRetriesLeft;
//        private Throwable ex;
//
//        public RPCRetryScheduled( String destination, long inMillis, int attemptNum, int numRetriesLeft, Throwable ex ) {
//            super("The request '$destination' errored, and will be retried again in ${inMillis:duration}.  There are ${numRetriesLeft} retries remaining before the request will be aborted.  $ex");
//
//            this.destination    = destination;
//            this.inMillis       = inMillis;
//            this.attemptNum     = attemptNum;
//            this.numRetriesLeft = numRetriesLeft;
//            this.ex             = ex;
//        }
//    }
//
//
//    public static class RequestRetryStarted extends LogMessage.AuditInfo {
//        private String dest;
//        private int    retryNum;
//
//        public RequestRetryStarted( String dest, int retryNum ) {
//            super("Retrying ${dest}.  Attempt ${retryNum}.");
//            this.dest = dest;
//            this.retryNum = retryNum;
//        }
//    }
//
//}
