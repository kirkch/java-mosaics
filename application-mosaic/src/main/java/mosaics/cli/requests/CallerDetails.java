package mosaics.cli.requests;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;


@With
@Value
@AllArgsConstructor
public class CallerDetails {
    /**
     * Who triggered the call, used for security and auditing.
     */
    private FPOption<String> userId;

    public CallerDetails() {
        this( FP.emptyOption() );
    }
}
