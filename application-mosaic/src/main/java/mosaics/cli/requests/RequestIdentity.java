package mosaics.cli.requests;


import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;
import mosaics.fp.collections.ConsList;


/**
 * Uniquely identifies the request and its ancestry.
 */
@With
@Value
@AllArgsConstructor
public class RequestIdentity {

    /**
     * When a request creates another request, this path tracks the requests ancestry.  The head
     * is always the originating request, then each child request follows until this request's id
     * next element.
     */
    private ConsList<String> requestIds;

    /**
     * A description of what the request is, eg GET /foo/bar or AccountService.getAccount.
     */
    private String requestDescription;

    public String getRequestId() {
        return requestIds.getHead();
    }
}
