package mosaics.cli.requests;

import lombok.AllArgsConstructor;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.QA;
import mosaics.lang.time.DTM;


@Value
@AllArgsConstructor
public class RequestTiming {
    private DTM           requestStartDTM;
    private FPOption<DTM> requestEndDTM;

    public RequestTiming( DTM requestStartDTM ) {
        this(requestStartDTM, FP.emptyOption());
    }


    public RequestTiming markCompleted( DTM endedAt ) {
        throwIfRequestHasAlreadyFinished();
        QA.argIsGTE(endedAt, requestStartDTM, "endedAt");

        return new RequestTiming( requestStartDTM, FP.option(endedAt) );
    }

    public boolean hasRequestFinished() {
        return getRequestEndDTM().hasValue();
    }

    public void throwIfRequestHasAlreadyFinished() {
        if ( hasRequestFinished() ) {
            throw new IllegalStateException( "Request has finished: " + this );
        }
    }
}
