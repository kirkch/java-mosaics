package mosaics.cli.requests;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;


/**
 * A snapshot of stats that describe what has happened during the request.  These values are an
 * immutable snapshot because these values can change over the life time of the request.
 */
@Value
@AllArgsConstructor
public class RequestMetrics {

    public RequestMetrics() {
        this(
            0,0,0,
            0,0,0,0
        );
    }

// LOG RELATED METRICS

    /**
     * A count of how many warning messages were logged during the request.
     */
    @With private int warningCount;

    /**
     * A count of how many error messages were logged during the request.
     */
    @With private int errorCount;

    /**
     * A count of how many fatal error messages were logged during the request.
     */
    @With private int fatalErrorCount;



// REMOTE CALL RELATED METRICS

    /**
     * A count of how many bytes has been sent because of this request.
     */
    @With private long remoteCallBytesSent;

    /**
     * A count of how many bytes has been received because of this request.
     */
    @With private long remoteCallBytesReceived;

    /**
     * The number of data rows transferred so far, if available.
     */
    @With private long remoteRowCount;

    /**
     * The number of remote calls made during this request so far.
     */
    @With private long remoteCallCount;

}
