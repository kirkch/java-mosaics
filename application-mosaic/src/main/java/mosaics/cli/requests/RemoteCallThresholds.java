package mosaics.cli.requests;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;


@With
@Value
@AllArgsConstructor
public class RemoteCallThresholds {
    private static final long DEFAULT_SLOW_REQUEST_THRESHOLD     = 1_000;
    private static final long DEFAULT_REMOTE_CALLCOUNT_THRESHOLD =    20;
    private static final long DEFAULT_REMOTE_ROWCOUNT_THRESHOLD  = 1_000;

    public static final RemoteCallThresholds DEFAULT = new RemoteCallThresholds(
        DEFAULT_SLOW_REQUEST_THRESHOLD,
        DEFAULT_REMOTE_CALLCOUNT_THRESHOLD,
        DEFAULT_REMOTE_ROWCOUNT_THRESHOLD
    );


    private final long slowRequestThreshold;
    private final long remoteCallCountThreshold;
    private final long remoteRowCountThreshold;
}
