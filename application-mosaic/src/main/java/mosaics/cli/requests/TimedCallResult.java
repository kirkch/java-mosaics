package mosaics.cli.requests;

public record TimedCallResult<T>(long durationMillis, T result) {
}
