package mosaics.cli;

import lombok.AllArgsConstructor;
import lombok.Getter;
import mosaics.cli.requests.Request;
import mosaics.lang.Backdoor;
import mosaics.lang.Copyable;
import mosaics.lang.Random;
import mosaics.lang.clock.Clock;
import mosaics.lang.clock.Clocks;
import mosaics.lang.functions.VoidFunction0WithThrows;
import mosaics.lang.lifecycle.StartStoppable;
import mosaics.lang.lifecycle.StartStoppableCallback;
import mosaics.lang.time.DTM;
import mosaics.logging.LogFilter;
import mosaics.logging.LogMessage;
import mosaics.logging.LogMessageFactory;
import mosaics.logging.LogMessageProxyFactory;
import mosaics.logging.Logger;
import mosaics.logging.loglevels.DevInfo;
import mosaics.logging.writers.LogWriter;
import mosaics.logging.writers.LogWriters;


/**
 * The runtime environment for the CLI application.  Env gives us a single place to gain access
 * to system resources and to mock them.  It also gives us a single place to control start and stop
 * all system resources used by the application.
 */
public class Env extends StartStoppable<Env> implements Copyable<Env>, Logger {

    public static Env createTestEnv() {
        return createTestEnv( new DTM(2020,3,20, 9,30) );
    }

    public static Env createTestEnv( DTM fixedTime ) {
        return createTestEnv( fixedTime, LogWriters.nullLogWriter() );
    }

    public static Env createTestEnv( LogWriter logWriter ) {
        return createTestEnv( new DTM(2020,3,20, 9,30), logWriter );
    }

    public static Env createTestEnv( DTM fixedTime, LogWriter logWriter ) {
        return new Env(
            "test-env",
            Clocks.newFixedTimeClock( fixedTime ),
            logWriter,
            LogFilter.DEBUG,
            new Random( 42L )
        );
    }

    public static Env createTestEnvWithRealClock() {
        return new Env(
            "test-env",
            Clocks.newRealTimeClock(),
            LogWriters.nullLogWriter(),
            LogFilter.SILENT,
            new Random( 42L )
        );
    }

    public static Env createTestEnvWithRealClock(LogWriter logWriter) {
        return new Env(
            "test-env",
            Clocks.newRealTimeClock(),
            logWriter,
            LogFilter.DEBUG,
            new Random( 42L )
        );
    }

    public static Env createLiveEnv( String envName ) {
        return createLiveEnv( envName, LogWriters.textLogWriter(System.out, true) );
    }

    public static Env createLiveEnv( String envName, LogWriter logWriter ) {
        return createEnv(
            envName,
            Clocks.newRealTimeClock(),
            logWriter,
            LogFilter.NORMAL,
            new Random()
        );
    }

    public static Env createEnv( String name, Clock clock, LogWriter logWriter, LogFilter logFilter, Random random ) {
        return new Env( name, clock, logWriter, logFilter, random );
    }


    @Getter private final Clock     clock;
    @Getter private final LogWriter logWriter;
    @Getter private final LogFilter logFilter;
            private final Random    random;


    private Env( String name, Clock clock, LogWriter logWriter, LogFilter logFilter, Random random ) {
        super( name );

        this.clock     = clock;
        this.logWriter = logWriter;
        this.logFilter = logFilter;
        this.random    = random;

        withCallback( new EnvServiceCallback(this) );
    }


// LOGGING

    public void log( LogMessage logMessage ) {
        if ( logFilter.accept(logMessage) ) {
            logWriter.write( logMessage.withWhen( currentDTM() ) );
        }
    }

    public Env withLogFilter( LogFilter newLogFilter ) {
        return new Env(getName(),clock,logWriter,newLogFilter,random);
    }

    public Env withLogWriter( LogWriter newLogWriter ) {
        return new Env(getName(),clock,newLogWriter,logFilter,random);
    }


// INFORMATION GETTERS

//    public SystemClock getClock() {
//        return clock;
//    }
//
    public long currentTimeMillis() {
        return clock.currentTimeMillis();
    }

    public long currentTimeNanos() {
        return clock.currentTimeNanos();
    }

    public DTM currentDTM() {
        return clock.currentDTM();
    }

    public double timeMillis( VoidFunction0WithThrows f ) {
        return clock.timeMillis( f );
    }

    //
//    public String getHostName() {
//        return NetUtils.getHostName();
//    }
//
//    public String getOSUserId() {
//        return RuntimeUtils.getOSUserName();
//    }
//
//    public String generateGUID() {
//        return NetUtils.generateGUID();
//    }


// RANDOM

    public long nextRandomLong() {
        return random.nextLong();
    }

    public long nextRandomLong( long maxValue ) {
        return Math.abs(random.nextLong()) % maxValue;
    }

    public String nextUUID() {
        return random.nextUUID();
    }


// REQUESTS

    public Request createRequest( String requestDescription ) {
        return new Request( this, requestDescription );
    }

// FILESYSTEM

//    public FileSystem getFileSystem() {
//        return fs;
//    }
//
//    public Directory getCurrentWorkingDirectory() {
//        return fs.getCurrentWorkingDirectory();
//    }

// THREAD RELATED

    public void sleep( long millis ) {
        Backdoor.sleep(millis);
    }

// INVOKE REQUESTS

//    public int getProcessId() {
//        VMManagement vmManagement = ReflectionUtils.getPrivateField( ManagementFactory.getRuntimeMXBean(), "jvm" );
//
//        return ReflectionUtils.invokePrivateMethod( vmManagement, "getProcessId" );
//    }

// SHUTDOWN HOOKS

//    public Subscription registerShutdownHook( VoidFunction0 onShutdown ) {
//        Thread shutdownThread = new Thread( onShutdown::invoke );
//
//        Runtime.getRuntime().addShutdownHook(shutdownThread);
//
//        return new Subscription( () -> Runtime.getRuntime().removeShutdownHook(shutdownThread) );
//    }


    @AllArgsConstructor
    private static class EnvServiceCallback implements StartStoppableCallback {
        private static final EnvServiceLogMessageFactory envServiceLMF = LogMessageProxyFactory.INSTANCE.createLogMessageFactory( EnvServiceLogMessageFactory.class );

        private Env env;


        public void starting( StartStoppable service ) {
            env.log( envServiceLMF.serviceStarting(service.getName()) );
        }

        public void started( StartStoppable service ) {
            env.log( envServiceLMF.serviceStarted(service.getName()) );
        }

        public void stopping( StartStoppable service ) {
            env.log( envServiceLMF.serviceStopping(service.getName()) );
        }

        public void stopped( StartStoppable service ) {
            env.log( envServiceLMF.serviceStopped(service.getName()) );
        }
    }

    private static interface EnvServiceLogMessageFactory extends LogMessageFactory {
        @DevInfo("$serviceName starting")
        public LogMessage serviceStarting( String serviceName );

        @DevInfo("$serviceName started")
        public LogMessage serviceStarted( String serviceName );

        /* -- These calls back were part of WorkerThread
        @OpsError("$serviceName threw an unexpected exception: $exception")
        public void serviceErrored( String serviceName, Throwable exception );

        @OpsFatal("$serviceName threw an unexpected exception that cannot be recovered from: $exception")
        public void serviceFatalError( String serviceName, Throwable exception );

        @DevInfo("$serviceName received a Thread interruption")
        public void serviceInterrupted( String serviceName );
*/
        @DevInfo("$serviceName stopping")
        public LogMessage serviceStopping( String serviceName );

        @DevInfo("$serviceName stopped")
        public LogMessage serviceStopped( String serviceName );
    }
}
