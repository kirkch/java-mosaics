package mosaics.cli;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;


/**
 * Use this annotation to provide a short name for public fields that are to be used as
 * a command line arg.
 */
@Target({TYPE, METHOD, CONSTRUCTOR, FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandLineArg {
    public char shortName() default ' ';
    public String longName() default "";
}

