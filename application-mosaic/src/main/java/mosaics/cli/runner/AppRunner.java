package mosaics.cli.runner;

import mosaics.cli.CommandLineException;
import mosaics.cli.Env;
import mosaics.cli.meta.AppFlagMeta;
import mosaics.cli.meta.AppHelpLogMessage;
import mosaics.cli.meta.AppMeta;
import mosaics.cli.meta.AppMetaReflectionFactory;
import mosaics.cli.meta.ArgMeta;
import mosaics.cli.meta.RunMethodMeta;
import mosaics.cli.requests.Request;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.Tuple2;
import mosaics.io.resources.ResourceLoader;
import mosaics.io.resources.URLLoader;
import mosaics.lang.ArrayUtils;
import mosaics.lang.lifecycle.Subscription;
import mosaics.lang.reflection.DI;
import mosaics.lang.reflection.DTOBuilder;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaMethod;
import mosaics.lang.reflection.ReflectionException;
import mosaics.logging.LogFilter;
import mosaics.logging.writers.LogWriter;
import mosaics.logging.writers.LogWriters;
import mosaics.strings.StringCodec;
import mosaics.strings.StringCodecs;
import mosaics.strings.StringUtils;
import mosaics.utils.SetUtils;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static mosaics.cli.runner.AppResult.MISSING_CONFIG_ERROR;
import static mosaics.cli.runner.AppResult.NO_RUN_METHOD_FOUND;
import static mosaics.cli.runner.AppResult.USER_ERROR;


/**
 * Manages the execution of a program triggered from the command line.<p/>
 * <p>
 * EXAMPLE:
 * <code>
 * /**
 * * Application docs here.  This documentation will be visible from the command line
 * * when the application is run with the flag --help.
 * *&#047;
 * public class App {
 * public static int main(String[] args) {
 * return new CommandLineRunner(App.cass).run(args);
 * }
 * <p>
 * /**
 * * Document the flag here.  The flag will be set to true when --flag1 is included on the
 * * command line.
 * *&#047;
 * public boolean flag1;
 * <p>
 * /**
 * * The CommandLineRunner will find this run method (by name) and will set the parameters
 * * based on args past into the main method.
 * *
 * * @param arg1 document arg 1
 * * @param arg2 document arg 2
 * *&#047;
 * public void run( String arg1, String arg2 ) {
 * // run app
 * }
 * }
 * </code>
 *
 * @see mosaics.cli.App
 * @param <T> The type of the app class being invoked
 */
@SuppressWarnings("unchecked")
public class AppRunner<T> {

    private static final Set<String> RESERVED_FLAG_NAMES = SetUtils.asImmutableSet(
        "help", "?", "v", "verbose", "d", "debug", "q", "quiet", "s", "silent"
    );


    private final AppMeta        appMeta;
    private final Env            env;
    private final StringCodecs   stringCodecs;
    private final ResourceLoader resourceLoader = new URLLoader().withDefaultProtocol( "file" );


    public AppRunner( Class<T> appClass, Env env ) {
        this( appClass, env, FP.emptyOption() );
    }

    public AppRunner( Class<T> appClass, Env env, FPOption<Class> configClass ) {
        this( JavaClass.of(appClass), env, configClass.map(JavaClass::of) );
    }

    public AppRunner( JavaClass appClass, Env env, FPOption<JavaClass> configClass ) {
        this( new AppMetaReflectionFactory().createAppMetaFrom(appClass, configClass), env, StringCodecs.DEFAULT_CODECS );

        validateAppMeta();
    }

    public AppRunner( AppMeta appMeta, Env env, StringCodecs stringCodecs ) {
        this.appMeta      = appMeta;
        this.env          = env;
        this.stringCodecs = stringCodecs;
    }


    public AppResult run( T app, String... args ) {
        return run( app, new DI(), args );
    }

    public AppResult run( T app, DI di, String... args ) {
        FPIterable<KVPair> allKVs = MainArgsParser.parse( args );

        Env configuredEnv = configureLogFilter( allKVs );

        Request request = configuredEnv.createRequest( appMeta.getAppName() );

        return run( request, app, di, allKVs );
    }

    private Env configureLogFilter( FPIterable<KVPair> flags ) {
        LogFilter logFilter = selectLogFilterFrom( flags );
        LogWriter logWriter = selectLogWriterFrom( flags );

        return env.withLogFilter( logFilter )
                  .withLogWriter( logWriter );
    }

    private LogWriter selectLogWriterFrom( FPIterable<KVPair> flags ) {
        LogWriter logWriter = env.getLogWriter();

        for ( KVPair flag : flags ) {
            if ( flag.getKey() != null)
            logWriter = switch ( flag.getKey() ) {
                case "logFormat" -> selectLogWriterFrom( flag.getValue() );
                default          -> logWriter;
            };
        }

        return logWriter;
    }

    private LogWriter selectLogWriterFrom( String value ) {
        if ( value == null ) {
            throw new CommandLineException( "Unrecognised value for logFormat '"+value+"'.  Valid values are TEXT or JSON.", AppResult.USER_ERROR );
        }

        return switch (value) {
            case "TEXT" -> LogWriters.textLogWriter(System.out, true);
            case "JSON" -> LogWriters.jsonLogWriter(System.out);
            default     -> throw new CommandLineException( "Unrecognised value for logFormat '"+value+"'.  Valid values are TEXT or JSON.", AppResult.USER_ERROR );
        };
    }

    private LogFilter selectLogFilterFrom( FPIterable<KVPair> flags ) {
        LogFilter logFilter = LogFilter.NORMAL;

        for ( KVPair flag : flags ) {
            if ( flag.getKey() != null ) {
                logFilter = switch ( flag.getKey() ) {
                    case "v", "verbose" -> LogFilter.VERBOSE;
                    case "d", "debug" -> LogFilter.DEBUG;
                    case "q", "quiet" -> LogFilter.ERRORS_ONLY;
                    case "s", "silent" -> LogFilter.SILENT;
                    default -> logFilter;
                };
            }
        }

        return logFilter;
    }

    //
//    public AppResult run( T app, LogHandler logHandler, Function1<DI,DI> initDI, String...args ) {
//        FPIterable<KVPair> allKVs = MainArgsParser.parse( args );
//
//        RequestFactoryConfig requestFactoryConfig = AppRunnerConfigFactory.createRequestFactoryConfig(allKVs, logHandler);
//        RequestFactory       requestFactory       = RequestFactory.createRequestFactory( requestFactoryConfig );
//        Request              request              = requestFactory.create();
//
//        return run( request, app, () -> initDI.invoke(new DI(requestFactory)), allKVs );
//    }
//
//    public AppResult run( T app, String...args ) {
//        FPIterable<KVPair> allKVs = MainArgsParser.parse( args );
//
//        RequestFactoryConfig requestFactoryConfig = AppRunnerConfigFactory.createRequestFactoryConfig(allKVs);
//        RequestFactory       requestFactory       = RequestFactory.createRequestFactory( requestFactoryConfig );
//        Request              request              = requestFactory.create();
//
//        return run( request, app, () -> new DI(requestFactory), args );
//    }
//
//    public AppResult run( Request initialRequest, T app, Function0<DI> originalDI, String...args ) {
//        FPIterable<KVPair> allKVs = MainArgsParser.parse( args );
//
//        return run( initialRequest, app, originalDI, allKVs );
//    }
//
    public AppResult run( Request req, T app, DI originalDI, FPIterable<KVPair> allKVs ) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                env.stop();
            }
        });

        Env reqEnv = req.getEnv();
        Subscription envRegistration = reqEnv.registerServicesAfter( app );
        reqEnv.start();

        //        int exitStatus;
//        int exitStatus = SUCCESS;
//
//        AppResult appResult = initialRequest.invoke( req -> {
        try {
//                req = configureRequest( req, app );

            FPIterable<KVPair> flags = handleOptionalConfigFlag( allKVs.filter(KVPair::hasKey) );
            FPIterable<KVPair> args  = allKVs.filterNot(KVPair::hasKey);

            Set<String> helpFlagNames = SetUtils.asSet( "help", "?" );
            boolean isHelpFlagSet = flags.map( KVPair::getKey ).filter( helpFlagNames::contains ).hasContents();
            if ( isHelpFlagSet ) {
                req.log( AppHelpLogMessage.createHelpMessage(reqEnv.currentDTM(),appMeta) );

                return AppResult.success();
            } else {
                FPOption<Object> configDTO = createConfigDTO(appMeta, flags);

                    DI di = new DI(originalDI)
                        .with( StringCodecs.class, stringCodecs )
                        .with( ResourceLoader.class, resourceLoader )
                        .with( Request.class, req )
                        .with( Env.class, reqEnv );

                    if ( configDTO.hasContents() ) {
                        di = di.with( appMeta.getAppConfigClass().get(), configDTO.get() );
                    }

                    di.populateFields( app );

                    configDTO.forEach( c -> logConfig(req,c) );

                    if ( detectMissingConfig(req,configDTO) ) {
                        req.log( AppHelpLogMessage.createHelpMessage(reqEnv.currentDTM(),appMeta) );

                        return AppResult.create( MISSING_CONFIG_ERROR );
                    }

//                        invokeSetupMethod();
//
                    return invokeRunMethod( di, req, app, args );

//                    }
            }
        } catch ( CommandLineException ex ) {
            req.log(AppLogMessageFactory.INSTANCE.userErrorMessage(StringUtils.nvl(ex.getMessage(),"")));

            return AppResult.create( ex.getExitStatus() );
        }finally {
            reqEnv.stop();

            envRegistration.cancel();
        }
    }

    private boolean detectMissingConfig( Request req, FPOption<Object> configDTO ) {
        if ( configDTO.isEmpty() ) {
            return false;
        }

        FPIterable<AppFlagMeta> missingConfig = appMeta.getFlags().filter(
            flag -> !flag.isSetOn(configDTO.get())
        );

        for ( AppFlagMeta cliFlagDeclaration : missingConfig ) {
            req.log( AppLogMessageFactory.INSTANCE.missingConfig(cliFlagDeclaration.getLongName()) );
        }

        return missingConfig.hasContents();
    }

    @SuppressWarnings("unchecked")
    private void logConfig( Request req, Object configDTO ) {
        Map<String,String> config = appMeta.getFlags().flatMap( flagMeta -> {
            return flagMeta.getValueFrom(configDTO).flatMap( v -> {
                String                key      = flagMeta.getLongName();
                FPOption<StringCodec> codecOpt = stringCodecs.getCodecFor( (Class) v.getClass() );

                FPOption<Tuple2<String, String>> map = codecOpt.map(codec -> new Tuple2<>(key,codec.encode(v)) );
                return map;
            });
        }).toJdkMap();

        if ( !config.isEmpty() ) {
            req.log( AppLogMessageFactory.INSTANCE.configLogMessage(config) );
        }
    }


    private AppResult invokeRunMethod( DI di, Request req, T app, FPIterable<KVPair> args ) {
        String[] params = args.map( KVPair::getValue ).toArray( String.class );

        Object result;

        try {
            JavaMethod runMethod = selectRunMethod( di, app, params );

            result = runMethod.invokeAgainst( app, (Object[]) params );
        } catch ( ReflectionException ex ) {
            req.log( AppLogMessageFactory.INSTANCE.userErrorMessage(StringUtils.nvl(ex.getMessage(),"")) );

            return AppResult.create( USER_ERROR );
        } catch ( CommandLineException ex ) {
            req.log( AppLogMessageFactory.INSTANCE.userErrorMessage(StringUtils.nvl(ex.getMessage(),"")) );

            return AppResult.create( ex.getExitStatus() );
        } catch ( Throwable ex ) {
            req.log( AppLogMessageFactory.INSTANCE.opsErrorMessage(StringUtils.nvl(ex.getMessage(),""), ex) );

            return AppResult.create( AppResult.EXCEPTION_THROWN );
        }

        int exitCode = 0;
        if ( result instanceof Number ) {
            exitCode = ((Number) result).intValue();
        }

        return AppResult.create( exitCode );
    }

//    private Object acquireLockFileAndThenInvoke( Function0 runner ) {
//        File         lockFile = getLockFile().get();
//        FileContents fc       = lockFile.openFile(FileModeEnum.READ_WRITE);
//
//        if ( fc.lockFile() ) {
//            try {
//                if ( fc.readLong(0,8) != 0 ) {
//                    recoverFromCrash();
//                }
//
//                String pid = Integer.toString( env.getProcessId() );  // todo log to ops
//                fc.resize(pid.length());
//                fc.writeUTF8StringUndemarcated(0, pid.length(), pid);
//
//                Subscription shutdownHook = env.registerShutdownHook( () -> {
//                    fc.unlockFile();
//                    lockFile.delete();
//                });
//
//                try {
//                    return runner.invoke();
//                } finally {
//                    shutdownHook.cancel();
//                }
//            } finally {
//                fc.unlockFile();
//
//                lockFile.delete();
//            }
//        } else {
//            env.log(new CLILogMessages.AppIsAlreadyRunning());
//
//            // NB no need to release fc lock as we failed to get it
//
//            return 1;
//        }
//    }
//
//
//
//    private void recoverFromCrash() {
//        Option<JavaMethod> m = new JavaClass(app.getClass()).findInstanceMethod(Void.TYPE, "recoverFromCrash", Env.class);
//
//        if ( m.hasValue() ) {
//            m.get().invokeAgainst(app, env);
//        }
//    }
//
//    private boolean requiresLockFile() {
//        if ( !(app instanceof App) ) {
//            return false;
//        }
//
//        App a = (App) app;
//        return a.requiresLockFile();
//    }
//
//    private Option<File> getLockFile() {
//        if ( !(app instanceof App) ) {
//            return Option.none();
//        }
//
//        App a = (App) app;
//        return a.lockFile();
//    }
//

//    private void setFlagsWithOptionalConfigLoading( AppMeta appMeta, FPIterable<KVPair> kvs ) {
//        FPOption<KVPair> configFlag = kvs.first( kv -> kv.getKey().equals("config") );
//        if ( configFlag.hasValue() ) {
//            String                       configURL  = configFlag.get().getValue();
//            FPOption<Map<String,String>> config     = resourceLoader.fetchProperties(configURL);
//
//            if ( config.isEmpty() ) {
//                throw new CommandLineException( "Unable to load missing configuration from '"+configURL+"'", USER_ERROR );
//            }
//
//            setConfigFlags( app, FPIterable.wrap(config.get().entrySet()).map(KVPair::new) );
//
//            kvs = kvs.filterNot(kv -> kv.getKey().equals("config"));
//        }
//
//        createConfigDTO(appMeta, kvs);
//    }

    private FPIterable<KVPair> handleOptionalConfigFlag( FPIterable<KVPair> initialFlags ) {
        FPIterable<KVPair> loadedFlags = initialFlags.first( kv -> Objects.equals(kv.getKey(),"config")  )
            .map( KVPair::getValue )
            .toFPList()
            .flatMap( this::loadConfigFrom );

        return loadedFlags
            .and( initialFlags )
            .filterNot( kv -> Objects.equals(kv.getKey(),"config") );
    }

    private FPIterable<KVPair> loadConfigFrom( String configURL ) {
        FPOption<Map<String,String>> config = resourceLoader.fetchProperties(configURL);

        if ( config.isEmpty() ) {
            throw new CommandLineException( "Unable to load missing configuration from '"+configURL+"'", USER_ERROR );
        }

        return FP.wrap( config.get().entrySet() ).map( KVPair::new );
    }

    private <C> FPOption<C> createConfigDTO( AppMeta appMeta, FPIterable<KVPair> kvs ) {
        return appMeta.getAppConfigClass().map( configClass -> {
            DTOBuilder<C> configDTOBuilder = kvs
                .filterNot( kv -> RESERVED_FLAG_NAMES.contains(kv.getKey()) )
                .fold(
                    DTOBuilder.createDTOBuilderFor( configClass.getJdkClass() ), // todo remove getJdkClass when new version of java-mosaic is released
                    (builder, kv) ->
                        appMeta
                            .getFlagByLabel(kv.getKey())
                            .map(flag -> {
                                if ( kv.getValue() == null && !flag.getValueType().isBoolean() ) {
                                    throw new CommandLineException( String.format("Flag '%s' requires a value",kv.getKey()), USER_ERROR );
                                }

                                try {
                                    return builder.withProperty( flag.getPropertyPath(), kv.getValue() );
                                } catch ( IllegalArgumentException ex ) {
                                    throw new CommandLineException(ex.getMessage(), USER_ERROR);
                                }
                            })
                            .orElse(builder)
                );

            return configDTOBuilder.build();
        });


//        for ( KVPair kv : kvs ) {
//            String key = kv.getKey();
//
//            if ( !RESERVED_FLAG_NAMES.contains(key) ) {
//                FPOption<AppFlagMeta2> flagOpt = appMeta.getKVByName(key);
//
//                if ( flagOpt.hasValue() ) {
//                    AppFlagMeta2 flag  = flagOpt.get();
//                    Object       value = decodeKVValue(kv, flag);
//
//                    flag.getJavaProperty().setValueOn( app, value );
//                }
//            }
//        }
    }

//    private Object decodeKVValue( KVPair kv, AppFlagMeta flag ) {
//        String key = kv.getKey();
//        if ( kv.getValue() == null ) {
//            if ( flag.getValueType().isBoolean() ) {
//                return true ;
//            } else {
//                throw new CommandLineException( String.format("Flag '%s' requires a value",kv.getKey()), USER_ERROR );
//            }
//        } else {
//            return decodeArg( "flag", key, kv.getValue(), flag.getValueType() );
//        }
//    }
//
//    private Object decodeArg( String paramTypeLabel, String name, String encodedArg, JavaClass targetType ) {
//        FPOption<StringCodec> codec         = stringCodecs.getCodecFor( targetType );
//        Tryable               resultTryable = codec.get().decode( encodedArg );
//
//        if ( resultTryable.hasFailure() ) {
//            String errorMessage = String.format("Malformed value '%s' for %s '%s'", encodedArg, paramTypeLabel, name);
//
//            throw new CommandLineException( errorMessage, USER_ERROR );
//        }
//
//        return resultTryable.getResult();
//    }

    private JavaMethod selectRunMethod( DI di, T app, String[] args ) {
        for ( RunMethodMeta m : appMeta.getRunMethods() ) {
            if ( m.acceptsArgs(di,args) ) {
                JavaMethod runMethod = selectRunMethodFrom(m, JavaClass.of(app));

                return stringCodecs.stringify( di.curry(runMethod) );
            }
        }

        throw new CommandLineException( "No run() method found supporting args: " + ArrayUtils.toString( args, "," ), NO_RUN_METHOD_FOUND );
    }

    private static JavaMethod selectRunMethodFrom( RunMethodMeta m, JavaClass c ) {
        return c.getMethod( m.getMethodName(), m.getArgs().map(arg -> arg.getValueType().getJdkClass()).toArray(Class.class) )
            .orElseThrow( () -> new IllegalArgumentException("Unable to find run method") );
    }
//
//    private Request configureRequest( Request req, T app ) {
//        return req.withCallerIp( HostUtils.getHostIp() )
//            .withCallerJvmInstanceId( FP.option( HostUtils.getRuntimeJvmId()) )
//            .withUserId( FP.option(System.getProperty( "user.name" )) );
//    }


    private void validateAppMeta() {
        validateFlags();
        validateRunMethods();
    }


    private void validateFlags() {
        for ( AppFlagMeta flag : appMeta.getFlags() ) {
            JavaClass flagType = flag.getValueType();
            FPOption<StringCodec> codec = stringCodecs.getCodecFor( flagType );

            if ( RESERVED_FLAG_NAMES.contains( flag.getLongName() ) ) {
                String errorMessage = String.format(
                    "%s is not valid.  Flag '%s' has already been reserved for another use.",
                    appMeta.getAppName(),
                    flag.getLongName()
                );

                throw new IllegalArgumentException( errorMessage );
            } else if ( flag.getShortName().hasValue() && RESERVED_FLAG_NAMES.contains( Character.toString( flag.getShortName().get() ) ) ) {
                String errorMessage = String.format(
                    "%s is not valid.  Flag '%s' has already been reserved for another use.",
                    appMeta.getAppName(),
                    flag.getShortName().get()
                );

                throw new IllegalArgumentException( errorMessage );
            }
        }
    }

    private void validateRunMethods() {
        FPIterable<RunMethodMeta> runMethods = appMeta.getRunMethods();
        long numRunMethods = runMethods.count();
        if ( numRunMethods == 0 ) {
            throw throwException(
                "%s is not valid.  There is no run method declared.  A valid run method is called run, returns int or void and can accept any arg that there is a valid StringCodec declared for its type.",
                appMeta.getAppName()
            );
        }

        runMethods.forEach( this::validateRunMethod );
    }

    private RuntimeException throwException( String msg, Object... args ) {
        throw new IllegalArgumentException( String.format( msg, args ) );
    }

    private void validateRunMethod( RunMethodMeta runMethod ) {
        for ( ArgMeta arg : runMethod.getArgs() ) {
            JavaClass flagType = arg.getValueType();

            if ( flagType.isInstanceOf(Request.class) || appMeta.isConfigDTOClass(flagType) ) { // is curryable via di

            } else if ( flagType.isOptional() ) {
                if ( flagType.getClassGenerics().count() == 0 ) {
                    throwException(
                        "%s is not valid, %s on run method has an optional with a parameter that is either missing or not @Capture'd",
                        appMeta.getAppName(),
                        arg.getName()
                    );
                }

                throwIfNoCodecFor( appMeta, arg, flagType.getClassGenerics().firstOrNull() );
            } else if ( !arg.isVarArg() ) {
                throwIfNoCodecFor( appMeta, arg, flagType );
            }
        }
    }

    private void throwIfNoCodecFor( AppMeta cliDescription, ArgMeta arg, JavaClass flagType ) {
        FPOption<StringCodec> codec = stringCodecs.getCodecFor( flagType );

        if ( codec.isEmpty() ) {
            throw throwException(
                "%s is not valid.  There is no StringCodec for type '%s', see param '%s' on the run method.",
                cliDescription.getAppName(),
                flagType.getShortName(),
                arg.getName()
            );
        }
    }

}
