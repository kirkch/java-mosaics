package mosaics.cli.meta;


import lombok.Value;
import mosaics.fp.collections.FPOption;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaDoc;
import mosaics.lang.reflection.JavaProperty;

import java.util.List;
import java.util.Objects;

import static mosaics.lang.Backdoor.cast;


@Value
public class AppFlagMeta implements Comparable<AppFlagMeta> {

    private final FPOption<Character> shortName;
    private final String              longName;

    private final String              propertyPath;
    private final JavaClass           valueType;
    private final FPOption<JavaDoc>   documentation;
    private final FPOption<?>         defaultValue;


    public int compareTo( AppFlagMeta o ) {
        return this.getLongName().compareTo( o.getLongName() );
    }

    public boolean matchesName( String candidateName ) {
        return matchesLongName(candidateName) || matchesShortName(candidateName);
    }

    public boolean isEnum() {
        return getValueType().isEnum();
    }

    public List<Object> getEnumValues() {
        return cast( getValueType().getEnumValues() );
    }

    public FPOption<Object> getValueFrom( Object configObj ) {
        return getJavaPropertyFor( configObj )
            .flatMap( p -> p.getValueFrom(configObj) );
    }

    public boolean isSetOn( Object configObj ) {
        return getJavaPropertyFor( configObj )
            .map( p -> p.isOptional() || p.isSetOn(configObj) )
            .orElse( false );
    }

    private FPOption<JavaProperty<Object,Object>> getJavaPropertyFor( Object configObj ) {
        return JavaClass.of(configObj).getProperty( valueType, propertyPath );
    }

    private boolean matchesLongName( String candidateName ) {
        return Objects.equals( candidateName, this.getLongName() );
    }

    private Boolean matchesShortName( String candidateName ) {
        return this.getShortName().map(
            sn -> candidateName.length() == 1 && candidateName.charAt( 0 ) == sn
        ).orElse( false );
    }
}
