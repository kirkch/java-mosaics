package mosaics.cli.meta;

import mosaics.fp.collections.DFS;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaDoc;
import mosaics.lang.reflection.JavaMethod;
import mosaics.lang.reflection.JavaProperty;
import mosaics.strings.StringCodec;
import mosaics.strings.StringCodecs;
import mosaics.utils.SetUtils;

import java.util.Set;


public class AppMetaReflectionFactory {
    private static final Set<String> excludeProperties = SetUtils.asImmutableSet("env");

    private final AppFlagMetaFactory appFlagFactoryMetaFactory = new AppFlagMetaFactory();


    public AppMeta createAppMetaFrom( Class<?> appClass, FPOption<Class> configClass ) {
        return createAppMetaFrom( JavaClass.of(appClass), configClass.map(JavaClass::of) );
    }


    public AppMeta createAppMetaFrom( JavaClass appClass, FPOption<JavaClass> configClass ) {
        String                    appName    = appClass.getShortName();
        FPOption<JavaDoc>         appDocs    = appClass.getJavaDoc();
        FPIterable<AppFlagMeta>   flags      = configClass.toFPList().flatMap( this::createFlagMetasFrom ).toFPList();
        FPIterable<RunMethodMeta> runMethods = createRunMethodMetasFrom(appClass).toFPList();

        return new AppMeta( appClass, appName, configClass, appDocs, flags, runMethods );
    }

    private FPIterable<AppFlagMeta> createFlagMetasFrom( JavaClass configClass ) {
        FPIterable<JavaProperty<Object,Object>> roots = configClass.getProperties().filter(this::filterFlag);

        return DFS.dfsMulti( roots, this::getCandidatePropertiesChildren )
            .filter( jp -> getCandidatePropertiesChildren(jp).isEmpty() )
            .flatMap( appFlagFactoryMetaFactory::createFlagFor );
    }

    private FPIterable<RunMethodMeta> createRunMethodMetasFrom( JavaClass appClass ) {
        return appClass.getAllPublicInstanceMethods( "run" )
            .filter( this::isValidRunMethod )
            .map( RunMethodMeta::of );
    }

    private boolean isValidRunMethod( JavaMethod runMethod ) {
        JavaClass returnType = runMethod.getReturnType();

        return returnType.isVoid() || returnType.isInteger();
    }


    private FPIterable<JavaProperty<Object, Object>> getCandidatePropertiesChildren( JavaProperty<Object, Object> p ) {
        FPOption<StringCodec> propertiesStringCodec = StringCodecs.DEFAULT_CODECS.getCodecFor( p.getValueType() );

        if ( propertiesStringCodec.hasValue() ) {
            return FPIterable.empty();
        } else {
            return Backdoor.cast( p.getProperties().filter(this::filterFlag) );
        }
    }

    private boolean filterFlag( JavaProperty p ) {
        boolean isRequestFactoryType = false; //Objects.equals(p.getJdkValueType(), RequestFactory.class);

        return !(isRequestFactoryType || excludeProperties.contains(p.getName()));
    }
}
