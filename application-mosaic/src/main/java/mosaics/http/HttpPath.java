package mosaics.http;

import mosaics.fp.FP;
import mosaics.fp.collections.RRBVector;
import mosaics.fp.collections.Tuple2;


public class HttpPath {
    private static final HttpPath EMPTY = new HttpPath(FP.emptyVector(), false);

    public static HttpPath empty() {
        return EMPTY;
    }

    public static HttpPath of(String path) {
        if ( path == null ) {
            return empty();
        }


        String modifiedPath = path.replaceFirst( "^/", "" ).replaceFirst( "/$", "" );
        if (modifiedPath.isBlank()) {
            return path.endsWith("/") ? new HttpPath(FP.emptyVector(),true) : HttpPath.empty();
        }

        return new HttpPath(
            FP.toVector(
                modifiedPath.split( "/", -1 )
            ).map(HttpParserUtils::decode).toRRBVector(),
            path.endsWith("/")
        );
    }

    public static HttpPath of(String...parts) {
        return parts == null ? EMPTY : new HttpPath(FP.toVector(parts), false);
    }


    private final RRBVector<String> parts;
    private final boolean           hasTrailingSlash;

    private HttpPath( RRBVector<String> parts, boolean hasTrailingSlash ) {
        this.parts = parts;
        this.hasTrailingSlash = hasTrailingSlash;
    }

    public HttpPath append( String part ) {
        return new HttpPath( parts.add(part), false );
    }

    public HttpPath hasTrailingSlash( boolean flag ) {
        if ( this.hasTrailingSlash == flag ) {
            return this;
        }

        return new HttpPath( parts, flag );
    }

    public boolean startsWith( HttpPath targetPrefix ) {
        if ( targetPrefix.parts.isEmpty() ) {
            return true;
        }

        if ( this.parts.size() < targetPrefix.parts.size() ) {
            return false;
        }

        return targetPrefix.parts.zip(this.parts).filterNot( p -> p.getFirst() == null ).isEvery( Tuple2::doAllValuesMatch );
    }

    public boolean equals( Object other ) {
        if ( other instanceof HttpPath o ) {
            return this.parts.equals(o.parts);
        }

        return false;
    }

    public int hashCode() {
        return parts.hashCode();
    }

    public String toString() {
        StringBuilder buf = new StringBuilder(200);

        appendTo( buf );

        return buf.toString();
    }

    public void appendTo( StringBuilder buf ) {
        if (parts.hasContents()) {
            buf.append( '/' );

            parts.mkString(buf, "/");

        }

        if ( hasTrailingSlash ) {
            buf.append( '/' );
        }
    }
}
