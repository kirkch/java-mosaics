package mosaics.http;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.QA;
import mosaics.net.HostUtils;
import mosaics.strings.StringUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@Value
@Builder
public class HttpURL {
    public static HttpURL of( String url ) {
        QA.argNotBlank(url, "url");

        try {
            URI uri = new URI( url );

            if ( StringUtils.isBlank( uri.getHost() ) ) {
                throw new IllegalArgumentException( "Host must be specified" );
            }

            HttpURL.HttpURLBuilder b = HttpURL.builder();
            int port = uri.getPort();

            b = b.protocol( HttpProtocol.of(uri.getScheme()) );
            b = b.host( uri.getHost() );
            b = b.port( port == -1 ? FP.emptyOption() : FP.option(port) );
            b = b.path( HttpPath.of(uri.getPath()) );
            b = b.queryParameters( HttpQueryParameters.of(uri.getRawQuery()) );
            b = b.fragment( FPOption.ofBlankableString(uri.getFragment()) );

            return b.build();
        } catch ( URISyntaxException ex ) {
            throw new IllegalArgumentException(ex.getMessage(),ex);
        }
    }

    public static HttpURL of( String protocol, String host ) {
        return new HttpURL( protocol, host );
    }

    public static HttpURL of( String protocol, String host, int port ) {
        return new HttpURL( protocol, host ).withPort( port );
    }

    public static HttpURL of( String protocol, String host, String path ) {
        return new HttpURL( protocol, host ).withPath( path );
    }

    public static HttpURL of( String protocol, String host, int port, String path ) {
        return new HttpURL( protocol, host ).withPort( port ).withPath( path );
    }

          private final HttpProtocol        protocol;
    @With private final String              host;
          private final FPOption<Integer>   port;
          private final HttpPath            path;
          private final FPOption<String>    fragment;
          private final HttpQueryParameters queryParameters;

    public HttpURL(String protocol, String host) {
        this(HttpProtocol.of(protocol),host, FP.emptyOption(),HttpPath.empty(), FP.emptyOption(), HttpQueryParameters.empty());
    }

    private HttpURL( HttpProtocol protocol, String host, FPOption<Integer> port, HttpPath path, FPOption<String> fragment, HttpQueryParameters queryParameters ) {
        this.protocol        = protocol;
        this.host            = host;
        this.port            = port;
        this.path            = path;
        this.fragment        = fragment;
        this.queryParameters = queryParameters;
    }

    public boolean isSecure() {
        return protocol.isSecure();
    }

    public HttpURL withPort( int port ) {
        FPOption<Integer> newPort = FP.option( port );

        return newPort.equals(this.port) ? this : new HttpURL( protocol, host, newPort, path, fragment, queryParameters );
    }

    public HttpURL withProtocol( String protocol ) {
        return withProtocol( HttpProtocol.of(protocol) );
    }

    public HttpURL withProtocol( HttpProtocol newProtocol ) {
        return newProtocol.equals(this.protocol) ? this : new HttpURL( newProtocol, host, port, path, fragment, queryParameters );
    }

    public HttpURL withPath( String newPath ) {
        return withPath( HttpPath.of(newPath) );
    }

    public HttpURL withPath( HttpPath newPath ) {
        if ( Objects.equals(this.path,newPath) ) {
            return this;
        }

        return new HttpURL( protocol, host, port, newPath, fragment, queryParameters );
    }

    public HttpURL appendQueryParameter( String k, String v ) {
        if ( queryParameters.hasParameter(k,v) ) {
            return this;
        }

        return new HttpURL(
            protocol, host, port, path, fragment,
            queryParameters.withParameter(k,v)
        );
    }

    public HttpURL appendQueryParameters( String...kvPairs ) {
        return new HttpURL(
            protocol, host, port, path, fragment,
            queryParameters.withParameters(kvPairs)
        );
    }

    public HttpURL withFragment( String newFragment ) {
        return withFragment( FP.option(newFragment) );
    }

    public HttpURL withFragment( FPOption<String> newFragment ) {
        if ( Objects.equals(this.fragment,newFragment) ) {
            return this;
        }

        return new HttpURL( protocol, host, port, path, newFragment, queryParameters );
    }

    public boolean equals( Object other ) {
        if ( other instanceof HttpURL o ) {
            return Objects.equals(this.protocol,o.protocol)
                && Objects.equals(this.host,o.host)
                && Objects.equals(this.port,o.port)
                && Objects.equals(this.path,o.path)
                && Objects.equals(this.fragment,o.fragment)
                && Objects.equals(this.queryParameters,o.queryParameters);
        }

        return false;
    }

    public int hashCode() {
        return Objects.hash(host.hashCode(), port, path, fragment, queryParameters);
    }

    public String toString() {
        StringBuilder buf = new StringBuilder(200);

        toString(buf);

        return buf.toString();
    }

    public void toString( StringBuilder buf ) {
        buf.append(protocol.getName());
        buf.append("://");
        buf.append(host);

        if ( port.hasValue() ) {
            buf.append(':');
            buf.append( port.get() );
        }

        path.appendTo(buf);

        if ( fragment.hasValue() ) {
            buf.append( '#' );
            buf.append( fragment.get() );
        }

        if ( queryParameters.hasParameters() ) {
            buf.append( '?' );
            queryParameters.toString( buf );
        }
    }

    public URI toURI() {
        return HostUtils.toURI( toString() );
    }
}
