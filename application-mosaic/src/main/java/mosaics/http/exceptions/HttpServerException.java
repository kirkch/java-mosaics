package mosaics.http.exceptions;

import mosaics.http.HttpResponse;


public class HttpServerException extends HttpException {
    public HttpServerException( HttpResponse resp ) {
        super( resp );
    }
}
