package mosaics.http.exceptions;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.http.HttpResponse;


public class HttpResponseErrorMessageExtractor {
    public FPOption<String> extractErrorMessage( HttpResponse response ) {
//        return response.getReasonPhrase();
        return FP.emptyOption();  // the challenge:  http2 has no reason phrase!
    }

    /* RESEARCH into how different apps report errors:

{
    "error": "auth-0001",
    "message": "Incorrect username and password",
    "detail": "Ensure that the username and password included in the request are correct",
        "help": "https://example.com/help/error/auth-0001"

}

{
    "type": "/errors/incorrect-user-pass",
    "title": "Incorrect username or password.",
    "status": 401,
    "detail": "Authentication failed due to incorrect username or password.",
    "instance": "/login/log/abc123"
}


{
    "errors": [
        {
            "code":215,
            "message":"Bad Authentication data."
        }
    ]
}

{
 "error": {
  "errors": [
   {
    "domain": "global",
    "reason": "invalidParameter",
    "message": "Invalid string value: 'asdf'. Allowed values: [mostpopular]",
    "locationType": "parameter",
    "location": "chart"
   }
  ],
  "code": 400,
  "message": "Invalid string value: 'asdf'. Allowed values: [mostpopular]"
 }
}

{
   "type": "https://example.net/validation-error",
   "title": "Your request parameters didn't validate.",
   "invalid-params": [ {
                         "name": "age",
                         "reason": "must be a positive integer"
                       },
                       {
                         "name": "color",
                         "reason": "must be 'green', 'red' or 'blue'"}
                     ]
   }

Why not just change the reason phrase? That's what it is there for.
 The "Bad Request" text is just the default. If you want to include more
 information then use the response body. The HTTP spec says you SHOULD include
 a response body with details of an error.

 Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF


 // 'Warning' http header


 */
}
