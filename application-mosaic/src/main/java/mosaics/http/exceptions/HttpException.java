package mosaics.http.exceptions;

import mosaics.http.HttpResponse;
import mosaics.lang.Copyable;
import mosaics.lang.reflection.JavaField;


public class HttpException implements Copyable<HttpException> {
    private static final HttpResponseErrorMessageExtractor errorMessageExtractor = new HttpResponseErrorMessageExtractor();
    private static final JavaField ERRORMSG_FIELD = JavaField.of( HttpException.class, "errorMessage" ).get();

    private final String              errorMessage;
    private final HttpResponse responseDetails;

    public HttpException( HttpResponse resp) {
        this( errorMessageExtractor.extractErrorMessage(resp).orElse(""), resp );
    }

    public HttpException( String errorMessage, HttpResponse resp) {
        this.errorMessage    = errorMessage;
        this.responseDetails = resp;
    }

    public HttpException withErrorMessage( String newErrorMessage ) {
        HttpException newMessage = this.copy();

        ERRORMSG_FIELD.setValueOn( newMessage, newErrorMessage );

        return newMessage;
    }
}
