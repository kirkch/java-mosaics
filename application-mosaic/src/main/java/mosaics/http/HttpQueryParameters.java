package mosaics.http;

import lombok.SneakyThrows;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.Tuple2;
import mosaics.fp.collections.maps.FPMap;
import mosaics.strings.StringUtils;

import java.net.URLEncoder;
import java.util.regex.Pattern;


public class HttpQueryParameters implements FPIterable<Tuple2<String,String>> {
    private static final HttpQueryParameters EMPTY = new HttpQueryParameters(FP.emptyMap());
    public static final Pattern QP_SEPARATER = Pattern.compile( "&" );

    public static HttpQueryParameters empty() {
        return EMPTY;
    }

    public static HttpQueryParameters of( String encodedStr ) {
        if ( StringUtils.isBlank(encodedStr) ) {
            return empty();
        }

        return HttpParserUtils.parseQueryParametersString( encodedStr );
    }

    public static HttpQueryParameters of( String...kvPairs ) {
        return new HttpQueryParameters( FP.toMap((Object[]) kvPairs) );
    }

    // wrapTuples
    // wrapMap
    // wrapFPMap
    // of(keyValuePairs)

    private final FPMap<String,String> queryParameters;


    HttpQueryParameters( FPMap<String, String> queryParameters ) {
        this.queryParameters = queryParameters;
    }


    public FPIterator<Tuple2<String, String>> iterator() {
        return queryParameters.getEntryTuples().iteratorX();
    }


    public int size() {
        return queryParameters.size();
    }

    public FPOption<String> getParameter( String parameterName ) {
        return queryParameters.get( parameterName );
    }

    public String getParameterMandatory( String parameterName ) {
        return queryParameters.getMandatory( parameterName );
    }

    public HttpQueryParameters withParameter( String parameterName, String parameterValue ) {
        FPMap<String, String> newQPMap = queryParameters.put( parameterName, parameterValue );

        return wrap( newQPMap );
    }

    public HttpQueryParameters withParameters( String...kvPairs ) {
        FPMap<String, String> newQPMap = FP.wrapAll( kvPairs )
            .pairs()
            .fold( this.queryParameters, FPMap::put );

        return wrap( newQPMap );
    }

    public HttpQueryParameters removeParameter( String parameterName ) {
        FPMap<String, String> newQPMap = queryParameters.remove( parameterName );

        return wrap( newQPMap );
    }

    public boolean hasParameter( String parameterName ) {
        return queryParameters.containsKey( parameterName );
    }

    public boolean hasParameter( String targetKey, String targetValue ) {
        return this.contains( new Tuple2<>(targetKey,targetValue) );
    }

    public boolean equals(Object other) {
        if (other instanceof HttpQueryParameters o) {
            return this.queryParameters.equals(o.queryParameters);
        }

        return false;
    }

    public int hashCode() {
        return queryParameters.hashCode();
    }

    public boolean hasParameters() {
        return queryParameters.hasContents();
    }

    public String toString() {
        StringBuilder buf = new StringBuilder(100);

        toString( buf );

        return buf.toString();
    }

    public void toString( StringBuilder buf ) {
        queryParameters.forEach( new ToStringAggregator(buf)::append );
    }


    private HttpQueryParameters wrap( FPMap<String, String> newQPMap ) {
        return newQPMap == queryParameters ? this : new HttpQueryParameters( newQPMap );
    }

    private static class ToStringAggregator {
        private final StringBuilder buf;
        private       boolean       incSeparator;

        private ToStringAggregator( StringBuilder buf ) {
            this.buf = buf;
        }

        @SneakyThrows
        public void append( String k, String v ) {
            if ( incSeparator ) {
                buf.append( '&' );
            } else {
                incSeparator = true;
            }

            buf.append(URLEncoder.encode(k, "UTF-8"));
            buf.append('=');
            buf.append(URLEncoder.encode(v, "UTF-8"));
        }
    }
}
