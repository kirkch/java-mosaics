package mosaics.http;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import mosaics.fp.collections.FPIterable;

import java.net.URI;


@With
@Value
@Builder
public class HttpRequest {
    public static HttpRequest GET(String url) {
        return HttpRequest.builder()
            .requestMethod(HttpRequestMethod.GET)
            .url(HttpURL.of(url))
            .build();
    }

    public static HttpRequest POST(String url) {
        return HttpRequest.builder()
            .requestMethod(HttpRequestMethod.POST)
            .url(HttpURL.of(url))
            .build();
    }

    public static HttpRequest PUT(String url) {
        return HttpRequest.builder()
            .requestMethod(HttpRequestMethod.PUT)
            .url(HttpURL.of(url))
            .build();
    }

    public static HttpRequest DELETE(String url) {
        return HttpRequest.builder()
            .requestMethod(HttpRequestMethod.DELETE)
            .url(HttpURL.of(url))
            .build();
    }

    public static HttpRequest HEAD(String url) {
        return HttpRequest.builder()
            .requestMethod(HttpRequestMethod.HEAD)
            .url(HttpURL.of(url))
            .build();
    }

    @Builder.Default private final HttpRequestMethod   requestMethod   = HttpRequestMethod.GET;
                     private final HttpURL             url;

    @Builder.Default private final HttpHeaders         headers         = HttpHeaders.empty();
    @Builder.Default private final HttpCookies         cookies         = HttpCookies.empty();
    @Builder.Default private final HttpPayload         body            = HttpPayload.empty();

    public boolean isSecure() {
        return url.isSecure();
    }

    public String getHost() {
        return url.getHost();
    }

    public HttpPath getPath() {
        return url.getPath();
    }

    public HttpRequest appendQueryParameter( String label, String value ) {
        return this.withUrl(
            this.url.appendQueryParameter( label, value )
        );
    }

    public HttpRequest appendQueryParameters( String...kvPairs ) {
        return this.withUrl(
            this.url.appendQueryParameters( kvPairs )
        );
    }

    public HttpRequest appendHeader( String label, String value ) {
        return this.withHeaders(
            this.headers.appendHeader( label, value )
        );
    }

    public HttpRequest appendCookie( String name, String value ) {
        return appendCookie( HttpCookie.of(name,value) );
    }

    public HttpRequest appendCookie( HttpCookie newCookie ) {
        return withCookies( cookies.appendCookie(newCookie) );
    }

    public HttpRequest appendCookies( FPIterable<HttpCookie> newCookies ) {
        return withCookies( cookies.appendCookies(newCookies) );
    }

    public String getShortDescription() {
        StringBuilder buf = new StringBuilder(200);

        requestMethod.toString(buf);
        buf.append( ' ' );
        url.toString( buf );

        return buf.toString();
    }

    public URI toURI() {
        return url.toURI();
    }

    public HttpRequest withJsonBody( String json ) {
        return this.withBody( HttpPayload.json(json) );
    }

    public <T> HttpRequest withJsonBody( T dto ) {
        return this.withBody( HttpPayload.json(dto) );
    }

    public HttpRequest withTextBody( String txt ) {
        return this.withBody( HttpPayload.plainText(txt) );
    }

    public HttpRequest appendHeaders( HttpHeaders extraHeaders ) {
        return withHeaders( this.headers.appendHeaders(extraHeaders) );
    }
}
