package mosaics.http;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.RRBVector;
import mosaics.fp.collections.Tuple2;
import mosaics.fp.collections.maps.FPMap;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.VoidFunction2WithThrows;
import mosaics.strings.StringUtils;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;


/**
 * Immutable collection of http headers.
 */
public class HttpHeaders implements FPIterable<Tuple2<String,String>> {
    private static final HttpHeaders EMPTY = new HttpHeaders();

    public static HttpHeaders wrapJdkMap( Map<String,String> headers ) {
        return wrapEntries( FP.wrap(headers.entrySet()) );
    }

    public static HttpHeaders wrapFPMap( FPMap<String,String> headers ) {
        return wrapEntries( headers.getEntries() );
    }

    public static HttpHeaders wrapJdkMapOfLists( Map<String, List<String>> jdkHeaders ) {
        return new HttpHeaders(
            FPMap.wrapMap( jdkHeaders ).mapValues( FP::toVector )
        );
    }

    public static HttpHeaders wrapTuples( FPIterable<Tuple2<String,String>> headers ) {
        FPMap<String, RRBVector<String>> fpHeaders = headers
            .filterNot(kv -> StringUtils.isBlank(kv.getSecond()) )
            .map( kv -> kv.mapFirst(String::toLowerCase) )
            .groupBy( Tuple2::getFirst )
            .mapValues( kvs -> kvs.map(Tuple2::getSecond).toRRBVector() )
            .toFPMap();

        return new HttpHeaders( fpHeaders );
    }

    public static HttpHeaders empty() {
        return EMPTY;
    }

    public static HttpHeaders of( String...headerValuePairs ) {
        return wrapTuples( FP.wrapAll(headerValuePairs).pairs() );
    }

    private static HttpHeaders wrapEntries( FPIterable<Map.Entry<String,String>> headers ) {
        return wrapTuples( headers.map(e -> new Tuple2<>(e.getKey(),e.getValue())) );
    }


    private final FPMap<String, RRBVector<String>> headers;


    private HttpHeaders() {
        this( FPMap.emptyMap() );
    }

    private HttpHeaders( FPMap<String, RRBVector<String>> headers ) {
        this.headers = headers;
    }


    public FPOption<String> getSingleHeader( String header ) {
        return headers.get(header.toLowerCase()).flatMap( FPIterable::first );
    }

    public FPIterable<String> getHeader( String header ) {
        return headers.get( header.toLowerCase() ).orElse( FP::emptyVector );
    }

    public String getHeaderMandatory( String header ) {
        return getSingleHeader( header ).get();
    }

    public boolean hasHeader( String header ) {
        return headers.contains( header.toLowerCase() );
    }

    public HttpHeaders appendHeader( Tuple2<String,String> kv ) {
        return appendHeader( kv.getFirst(), kv.getSecond() );
    }

    public HttpHeaders appendHeader( String header, String value ) {
        String headerLC = header.toLowerCase();
        FPOption<RRBVector<String>> current = headers.get( headerLC );

        RRBVector<String> updatedValues;
        if (current.isEmpty()) {
            updatedValues = FP.toVector(value);
        } else if (current.get().contains(value)) {
            return this;
        } else {
            updatedValues = current.get().add(value);
        }

        return withHeader( header, updatedValues );
    }

    public HttpHeaders appendHeaders( HttpHeaders extraHeaders ) {
        if ( this == extraHeaders ) {
            return this;
        }

        return extraHeaders.fold( this, HttpHeaders::appendHeader );
    }

    public HttpHeaders appendHeaders( String...kvPairs ) {
        return FP.wrapArray( kvPairs ).pairs().fold(
            this,
            HttpHeaders::appendHeader
        );
    }

    public HttpHeaders withHeader( String header, Iterable<String> updatedValues ) {
        String headerLC = header.toLowerCase();
        FPOption<RRBVector<String>> current = headers.get(headerLC);
        if (current.hasContents() && current.get().equals(updatedValues)) {
            return this;
        }

        if (updatedValues.iterator().hasNext()) {
            return new HttpHeaders( headers.put(headerLC,FP.toVector(updatedValues)) );
        } else {
            return removeHeader( header );
        }
    }

    public HttpHeaders removeHeader( String header ) {
        FPMap<String, RRBVector<String>> updatedHeaders = headers.remove( header.toLowerCase() );

        return updatedHeaders.isEmpty() ? HttpHeaders.empty() : new HttpHeaders( updatedHeaders);
    }

    public FPIterator<Tuple2<String, String>> iterator() {
        return headers.getEntryTuples().iteratorX()
            .flatMap( kv -> kv.getSecond().map(v -> new Tuple2<>(kv.getFirst(),v)) );
    }

    public void forEach( VoidFunction2WithThrows<String,String> f ) {
        iterator().forEachWithThrows(
            kv -> f.invoke( kv.getFirst(), kv.getSecond() )
        );
    }

    public boolean hasValues() {
        return !headers.isEmpty();
    }

    public int hashCode() {
        return headers.hashCode();
    }

    public boolean equals( Object other ) {
        if ( other instanceof HttpHeaders o ) {
            return this.headers.equals(o.headers);
        }

        return false;
    }

    public String toString() {
        return "HttpHeaders(" +
            headers.getEntries()
                   .map(kv->kv.getKey()+"->"+kv.getValue())
                   .mkString(",") +
            ")";
    }

    public void dump() {
        dumpTo( System.out );
    }

    public void dumpTo( PrintStream out ) {
        this.forEach( kv -> {
            out.print(kv.getFirst());
            out.print(": ");
            out.println(kv.getSecond());
        } );
    }

    public int size() {
        return headers.getValues().mapToInt(v -> Backdoor.toInt(v.size())).sum();
    }
}
