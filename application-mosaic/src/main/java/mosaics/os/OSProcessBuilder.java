package mosaics.os;

import io.reactivex.Flowable;
import lombok.AllArgsConstructor;
import lombok.Value;
import mosaics.cli.requests.Request;
import mosaics.concurrency.futures.Promise;
import mosaics.fp.ExceptionFailure;
import mosaics.fp.FP;
import mosaics.fp.Failure;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.VoidFunction0;
import mosaics.lang.lifecycle.Subscription;
import mosaics.lang.threads.ThreadUtils;
import mosaics.lang.threads.TimeoutException;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;


@Value
@AllArgsConstructor
public class OSProcessBuilder {

    private String            command;
    private RRBVector<String> args;
    private FPOption<File>    targetWorkingDirectory;


    public OSProcessBuilder() {
        this( null, RRBVector.emptyVector(), FP.emptyOption() );
    }

    public OSProcessBuilder withCommand( String command, String...args ) {
        return new OSProcessBuilder( command, RRBVector.toVector(args), targetWorkingDirectory );
    }

    public OSProcessBuilder withCommand( String command, List<String> args ) {
        return new OSProcessBuilder( command, RRBVector.toVector(args), targetWorkingDirectory );
    }

    public OSProcessBuilder withCommand( Class<?> classWithMainMethod, String...args ) {
        String        javaCmd         = new File(System.getProperty("java.home"), "/bin/java").getPath();
        String        classPath       = System.getProperty( "java.class.path" );
//        RuntimeMXBean runtimeMXBean   = ManagementFactory.getRuntimeMXBean();
//        List<String>  currentJVMFlags = runtimeMXBean.getInputArguments();

        RRBVector<String> combinedArgs = RRBVector.toVector(
            "--enable-preview", "-cp", classPath, classWithMainMethod.getName() )
            .addAll( args );

        return new OSProcessBuilder( javaCmd, combinedArgs, targetWorkingDirectory );
    }

    public OSProcessBuilder withArgs( String...args ) {
        return new OSProcessBuilder( command, RRBVector.toVector(args), targetWorkingDirectory );
    }

    public OSProcessBuilder withArgs( List<String> args ) {
        return new OSProcessBuilder( command, RRBVector.toVector(args), targetWorkingDirectory );
    }

    public OSProcessBuilder appendArgs( String...args ) {
        return new OSProcessBuilder( command, this.args.addAll(args), targetWorkingDirectory );
    }

    public OSProcessBuilder withWorkingDirectory( String d ) {
        return withWorkingDirectory( new File(d) );
    }

    public OSProcessBuilder withWorkingDirectory( File d ) {
        return new OSProcessBuilder( command, args, FP.option(d) );
    }


    public OSProcess run( Request req ) {
        ProcessBuilder b = new ProcessBuilder();

        String[] cmdWithArgs = RRBVector.toVector(command).addAll( args ).toArray(String.class);

        b.command( cmdWithArgs );

        File twd = targetWorkingDirectory.orElse( () -> new File(System.getProperty("user.dir")) );
        b.directory(twd);

        Promise<Integer> promise = new Promise<>(req);

        req.log( OSProcessBuilderLogMessageFactory.INSTANCE.startingNewChildProcess(twd,cmdWithArgs) );

        try {
            Process p   = b.start();
            long    pid = p.pid();

            req.log( OSProcessBuilderLogMessageFactory.INSTANCE.newChildProcessStarted(targetWorkingDirectory,cmdWithArgs,pid) );


            Subscription shutdownHookSubscription = registerShutdownHook( () -> {
                req.log( OSProcessBuilderLogMessageFactory.INSTANCE.killingChildProcessAsJVMIsExiting(pid) );

                promise.setFailure(new ExceptionFailure("JVM shutdown is in progress"));
            } );

            promise.onSuccess((r,v) -> shutdownHookSubscription.cancel());
            promise.onFailure( (r,f) -> {
                this.handleRequestToAbortChildProcess(r,pid,p,f);
                shutdownHookSubscription.cancel();
            } );


            Function0<PrintWriter>      stdinWriter     = () -> new PrintWriter(new OutputStreamWriter(p.getOutputStream()));
            Function0<Flowable<String>> stdoutRxSubject = () -> new JdkReaderToRxFlowableAdapter(p.getInputStream(), p::isAlive);
            Function0<Flowable<String>> stderrRxSubject = () -> new JdkReaderToRxFlowableAdapter(p.getErrorStream(), p::isAlive);



            OSProcess osProcess = new OSProcess( pid, promise, stdoutRxSubject, stderrRxSubject, stdinWriter, p::isAlive );

            ProcessMonitor.push( p, promise );

            return osProcess;
        } catch ( IOException ex ) {
            promise.setFailure(new ExceptionFailure(ex));

            return new OSProcess(-1, promise);
        }
    }

    private Subscription registerShutdownHook( VoidFunction0 onShutdown ) {
        Thread shutdownThread = new Thread( onShutdown::invoke );

        Runtime.getRuntime().addShutdownHook(shutdownThread);

        return new Subscription( () -> Runtime.getRuntime().removeShutdownHook(shutdownThread) );
    }

    private void handleRequestToAbortChildProcess( Request req, long pid, Process p, Failure f ) {
        if ( !p.isAlive() ) {
            return;
        }

        req.log( OSProcessBuilderLogMessageFactory.INSTANCE.requestToAbortChildProcessReceived(pid,f) );

        killProcess( req, pid, p );
    }


    // -2 == Ctrl-C
    // -3 == Ctrl-D
    // -9 == exit immediately with no cleanup
    private void killProcess( Request req, long pid, Process p ) {
        long startNanos = req.currentTimeNanos();

        req.log( OSProcessBuilderLogMessageFactory.INSTANCE.issueingInterruptToChildProcess(pid) );

        try {
            Runtime.getRuntime().exec( "kill -2 " + pid );

            ThreadUtils.spinUntilFalse( p::isAlive );
        } catch ( IOException ex ) {
            req.log( OSProcessBuilderLogMessageFactory.INSTANCE.interruptNotSupportedByOS(pid,ex) );
            req.log( OSProcessBuilderLogMessageFactory.INSTANCE.issuingKillToChildProcess(pid) );

            p.destroyForcibly();
        } catch ( TimeoutException ex ) {
            req.log( OSProcessBuilderLogMessageFactory.INSTANCE.interruptDidNotKillChildProcess(pid, ex.getTimeoutMillis()) );
            req.log( OSProcessBuilderLogMessageFactory.INSTANCE.issuingKillToChildProcess(pid) );

            p.destroyForcibly();
        }

        try {
            ThreadUtils.spinUntilFalse( p::isAlive );
        } catch ( TimeoutException ex ) {
            req.log( OSProcessBuilderLogMessageFactory.INSTANCE.killDidNotKillChildProcess(pid,ex.getTimeoutMillis()) );

            throw new IllegalStateException( "Failed to kill child process: " + pid );
        }

        double durationMillis = req.elapsedMillisSinceNanos(startNanos);
        req.log( OSProcessBuilderLogMessageFactory.INSTANCE.childProcessKilled(pid, durationMillis) );
    }

}
