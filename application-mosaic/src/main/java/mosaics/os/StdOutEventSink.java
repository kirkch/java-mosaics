package mosaics.os;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import mosaics.json.JsonCodec;
import mosaics.lang.threads.ThreadUtils;
import mosaics.logging.LogMessage;
import mosaics.logging.writers.TextLogWriter;

import java.util.Vector;


public class StdOutEventSink {

    private Vector<LogMessage> events = new Vector<>();

    StdOutEventSink( Flowable<String> stdout ) {
        JsonCodec codec = JsonCodec.ndJsonCodec;

        stdout.map(json -> codec.fromJson(json,LogMessage.class))
            .subscribeOn( Schedulers.newThread() )  // NB: this thread cleans itself up after the flowable receives an EOS
            .subscribe( logMessage -> {
                synchronized(events) {
                    events.add(logMessage);
                }
            } );
    }

    @SuppressWarnings("unchecked")
    public LogMessage spinUntilContains( String targetStr ) {
        ThreadUtils.spinUntilTrue( () -> {
            synchronized (events) {
                return events.stream().anyMatch( ev -> {
                    System.out.println(ev.getFormattedMessage());
                    return ev.getFormattedMessage().contains(targetStr);
                } );
            }
        } );

        synchronized (events) {
            return events.stream().filter( ev -> TextLogWriter.toString(ev).contains(targetStr) ).findFirst().get();
        }
    }

}
