package mosaics.concurrency;

public enum ThreadType {
    DAEMON, NON_DAEMON;

    public boolean isDaemon() {
        return this == DAEMON;
    }
}
