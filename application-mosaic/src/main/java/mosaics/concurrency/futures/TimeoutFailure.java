package mosaics.concurrency.futures;

import mosaics.cli.requests.Request;
import mosaics.fp.Failure;

import java.util.Objects;
import java.util.concurrent.TimeoutException;


public class TimeoutFailure implements Failure {

    private final TimeoutException ex = new TimeoutException();
    private final String message;
    private final Failure chainedFailure;

    public TimeoutFailure( Request request, Future future ) {
        this(request,future,null);
    }

    public TimeoutFailure( Request request, Future future, Failure chainedFailure ) {
        this.message = "Unable to continue future '"+future+"' as request has already completed";
        this.chainedFailure = chainedFailure;
    }


    public String getMessage() {
        return message;
    }

    public Throwable toException() {
        return ex;
    }

    public Failure getChainedFailure() {
        return chainedFailure;
    }

    public int hashCode() {
        return Objects.hash( message, chainedFailure, ex );
    }

    public String toString() {
        return getMessage();
    }

    public boolean equals( Object o ) {
        if ( !(o instanceof TimeoutFailure) ) {
            return false;
        } else if ( o == this ) {
            return true;
        }

        TimeoutFailure other = (TimeoutFailure) o;
        return Objects.equals(this.chainedFailure, other.chainedFailure)
            && Objects.equals(this.message, other.message )
            && equalsExceptions(this.ex, other.ex);
    }

    private boolean equalsExceptions( Throwable a, Throwable b ) {
        if ( a == b ) {
            return true;
        } else if ( a == null || b == null ) {
            return false;
        }

        return Objects.equals(a.getClass(), b.getClass() )
            && Objects.equals(a.getMessage(), b.getMessage() );
    }

}
