package mosaics.concurrency.futures;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import mosaics.cli.requests.Request;
import mosaics.concurrency.ThreadLogMessageFactory;
import mosaics.fp.ExceptionFailure;
import mosaics.fp.FP;
import mosaics.fp.Failure;
import mosaics.fp.MultipleFailures;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPIterable;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function0WithThrows;
import mosaics.lang.functions.Function2;
import mosaics.lang.functions.VoidFunction1;
import mosaics.lang.functions.VoidFunction2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;

import static mosaics.lang.Backdoor.cast;


/**
 * A future is a unit of work that will complete later.  Future is related to a Tryable, as both
 * may succeed or fail and they differ only in timing as tryables always complete immediately.
 */
@SuppressWarnings( "unchecked" )
public interface Future<T> {

    public static <T> Future<T> invokeNow( Request req, Function0WithThrows<T> f ) {
        try {
            return Future.successful( req, f.invoke() );
        } catch ( Throwable ex ) {
            return Future.failure( req, ex );
        }
    }

    public static <T> Future<T> successful( Request req, T v ) {
        return new SuccessfulFuture<>( req, v, null );
    }

    public static <T> Future<T> successful( Request req, T v, String description ) {
        return new SuccessfulFuture<>( req, v, description );
    }

    public static <T> Future<T> failure( Request req, Throwable ex ) {
        return new FailedFuture<>( req, new ExceptionFailure(ex), null );
    }

    public static <T> Future<T> failure( Request req, Failure f ) {
        return new FailedFuture<>( req, f, null );
    }

    public static <T> Future<T> failure( Request req, Failure f, String description ) {
        return new FailedFuture<>( req, f, description );
    }

    public static <T> Future<T> completed( Request req, Tryable<T> tryable ) {
        if ( tryable.hasResult() ) {
            return successful( req, tryable.getResult() );
        } else {
            return failure( req, tryable.getFailure() );
        }
    }

    public static <T> Future<T> completed( Request req, Tryable<T> tryable, String description ) {
        if ( tryable.hasResult() ) {
            return successful( req, tryable.getResult(), description );
        } else {
            return failure( req, tryable.getFailure(), description );
        }
    }

    public static <T> Future<T> fromJDKFuture( Request req, CompletionStage<T> jdkFuture ) {
        Promise<T> p = new Promise<>(req);

        jdkFuture
            .thenAcceptAsync( p::setResult )
            .exceptionally( ex -> {
                if ( ex instanceof CompletionException ) {
                    p.setFailure( new ExceptionFailure(ex.getCause()) );
                } else {
                    p.setFailure( new ExceptionFailure( ex ) );
                }

                return null;
            } );

        return p;
    }

    public static <T> Future<List<T>> joinFutures( Request req, Future<T>...futures ) {
        return joinFutures( req, FP.wrapAll(futures) );
    }

    public static <T> Future<List<T>> joinFutures( Request req, Iterable<Future<T>> futures ) {
        return joinFutures( req, FP.wrap(futures) );
    }

    public static <T> Future<List<T>> joinFutures( Request req, FPIterable<Future<T>> futures ) {
        int numFutures = Backdoor.toInt( futures.count() );

        if ( numFutures == 0 ) {
            return Future.successful( req, Collections.emptyList() );
        }

        FutureAggregation<T> aggregator = new FutureAggregation<>( req, numFutures );

        futures.forEachWithIndex( (i,future) -> future.onCompletion(
            (r,v)       -> aggregator.onSuccess(i,v),
            (r,failure) -> aggregator.onFailure(i,failure)
        ) );

        return aggregator.promise;
    }

    static class FutureAggregation<T> {
        private T[]              results;
        private List<Failure>    failures;

        private int              countDown;

        private Promise<List<T>> promise;


        public FutureAggregation( Request req, int numFutures ) {
            this.results   = cast(new Object[numFutures]);
            this.failures  = new ArrayList<>();
            this.countDown = numFutures;
            this.promise   = new Promise<>( req );
        }

        public synchronized void onSuccess( int i, T result ) {
            results[i] = result;

            countDown--;

            completeResultsPromiseIFFAllResultsAreIn();
        }

        public synchronized void onFailure( int i, Failure failure ) {
            failures.add(failure);

            countDown--;

            completeResultsPromiseIFFAllResultsAreIn();
        }

        private void completeResultsPromiseIFFAllResultsAreIn() {
            if ( countDown == 0 ) {
                if ( failures.isEmpty() ) {
                    promise.setResult( Arrays.asList(results) );
                } else  {
                    promise.setFailure( MultipleFailures.aggregateFailures(failures) );
                }
            }
        }
    }


    /**
     * Returns true if the future has a result or has been marked as failed.  False means that
     * the calculation is still in progress.
     */
    public boolean isComplete();

    /**
     * Returns true if the future contains a successful result.  False means
     * that the try contains a failure of some kind.
     *
     * @return true if the future has completed with a successful result
     * @throws IllegalStateException the future has not completed yet
     */
    public boolean hasResult();


    /**
     * Returns true if the try contains a failure.  A failure represents
     * that something went wrong, which may or may not be an exception.
     *
     * @return true if the future has completed with a failure
     * @throws IllegalStateException the future has not completed yet
     */
    public boolean hasFailure();


    /**
     * Specifies the default executor to use for any chained calls from this point forth.</p>
     *
     * Futures will by default use the executor used by the previously chained future.
     */
    public Future<T> withExecutor( Executor executor );

    /**
     * Describe the purpose of this future.  The description will remain with all futures
     * that are derived from this new future until a new description is provided.  The description
     * will be visible from toString() and is intended for diagnostic purposes.
     */
    public Future<T> withDescription( String description );


    public default Future<T> chainToPromise( Promise<T> promise ) {
        return onSuccess( (r,v) -> promise.setResult(v) )
            .onFailure( (r,f) -> promise.setFailure(f) );
    }

    /**
     * Creates a new Future that will contain the mapped result of this Future.  If this
     * Future has failed then this method will return this Future unmodified.
     */
    public <B> Future<B> mapResult( Function2<Request,T,B> mappingFunction );

    /**
     * Creates a new Future that will contain the mapped result of this Future.  If this
     * Future has failed then this method will return this Future unmodified.</p>
     *
     * Futures will by default use the executor used by the previously chained future.  This method
     * specifies that the supplied executor will be used instead.
     */
    public default <B> Future<B> mapResult( Executor executor, Function2<Request,T, B> mappingFunction ) {
        return withExecutor(executor).mapResult( mappingFunction );
    }

    /**
     * Creates a new Future that will contain the mapped result of this Future.  If this
     * Future has failed then this method will return this Future unmodified.
     */
    public <B> Future<B> mapResultToTryable( Function2<Request,T, Tryable<B>> mappingFunction );

    /**
     * Creates a new Future that will contain the mapped result of this Future.  If this
     * Future has failed then this method will return this Future unmodified.</p>
     *
     * Futures will by default use the executor used by the previously chained future.  This method
     * specifies that the supplied executor will be used instead.
     */
    public default <B> Future<B> mapResultToTryable( Executor executor, Function2<Request,T, Tryable<B>> mappingFunction ) {
        return withExecutor( executor ).mapResultToTryable( mappingFunction );
    }

    /**
     * Creates a new Future that will contain the mapped result of this Future.  If this
     * Future has failed then this method will return this Future unmodified.<p/>
     * <p>
     * This method differs from mapResult in that the mapping function returns
     * another Future.  This method will return that Future directly and will not
     * wrap it with another Future the way that mapResult would.
     */
    public <B> Future<B> flatMapResult( Function2<Request,T, Future<B>> mappingFunction );

    /**
     * Creates a new Future that will contain the mapped result of this Future.  If this
     * Future has failed then this method will return this Future unmodified.<p/>
     * <p>
     * This method differs from mapResult in that the mapping function returns
     * another Future.  This method will return that Future directly and will not
     * wrap it with another Future the way that mapResult would.</p>
     *
     * Futures will by default use the executor used by the previously chained future.  This method
     * specifies that the supplied executor will be used instead.
     */
    public default <B> Future<B> flatMapResult( Executor executor, Function2<Request,T, Future<B>> mappingFunction ) {
        return withExecutor( executor ).flatMapResult( mappingFunction );
    }


    /**
     * Offers the opportunity to recover from a failure. If this Future contains
     * a result then this instance will be returned unmodified.  Otherwise
     * on a failure the recovery function will be invoked and a new Future
     * created containing its result will be returned.<p/>
     *
     * The recoveryFunction will not be invoked when the underlying request has been completed.
     */
    public Future<T> recover( Function2<Request,Failure, T> recoveryFunction );

    /**
     * Offers the opportunity to recover from a failure. If this Future contains
     * a result then this instance will be returned unmodified.  Otherwise
     * on a failure the recovery function will be invoked and a new Future
     * created containing its result will be returned.</p>
     *
     * Futures will by default use the executor used by the previously chained future.  This method
     * specifies that the supplied executor will be used instead.
     */
    public default Future<T> recover( Executor executor, Function2<Request,Failure, T> recoveryFunction ) {
        return withExecutor( executor ).recover( recoveryFunction );
    }


    /**
     * Offers the opportunity to recover from a failure. If this Future contains
     * a result then this instance will be returned unmodified.  Otherwise
     * on a failure the recovery function will be invoked and its result returned
     * unmodified.
     */
    public Future<T> flatRecover( Function2<Request,Failure, Future<T>> recoveryFunction );

    /**
     * Offers the opportunity to recover from a failure. If this Future contains
     * a result then this instance will be returned unmodified.  Otherwise
     * on a failure the recovery function will be invoked and its result returned
     * unmodified.</p>
     *
     * Futures will by default use the executor used by the previously chained future.  This method
     * specifies that the supplied executor will be used instead.
     */
    public default Future<T> flatRecover( Executor executor, Function2<Request,Failure, Future<T>> recoveryFunction ) {
        return withExecutor( executor ).flatRecover( recoveryFunction );
    }


    /**
     * Offers the opportunity to modify the description of a failed task. If this
     * Future contains a result, then this instance will be returned unmodified.
     * On the other hand if the Future contains a failure, then the mappingFunction
     * will be invoked and its result will be wrapped in a new Future and returned.
     */
    public Future<T> mapFailure( Function2<Request,Failure, Failure> mappingFunction );

    /**
     * Offers the opportunity to modify the description of a failed task. If this
     * Future contains a result, then this instance will be returned unmodified.
     * On the other hand if the Future contains a failure, then the mappingFunction
     * will be invoked and its result will be wrapped in a new Future and returned.</p>
     *
     * Futures will by default use the executor used by the previously chained future.  This method
     * specifies that the supplied executor will be used instead.
     */
    public default Future<T> mapFailure( Executor executor, Function2<Request,Failure, Failure> mappingFunction ) {
        return withExecutor( executor ).mapFailure( mappingFunction );
    }


    /**
     * Offers the opportunity to modify the description of a failed task. If this
     * Future contains a result, then this instance will be returned unmodified.
     * On the other hand if the Future contains a failure, then the mappingFunction
     * will be invoked and its result will be returned unmodified.
     */
    public Future<T> flatMapFailure( Function2<Request,Failure, Future<Failure>> mappingFunction );


    /**
     * Offers the opportunity to modify the description of a failed task. If this
     * Future contains a result, then this instance will be returned unmodified.
     * On the other hand if the Future contains a failure, then the mappingFunction
     * will be invoked and its result will be returned unmodified.</p>
     *
     * Futures will by default use the executor used by the previously chained future.  This method
     * specifies that the supplied executor will be used instead.
     */
    public default Future<T> flatMapFailure( Executor executor, Function2<Request,Failure, Future<Failure>> mappingFunction ) {
        return withExecutor( executor ).flatMapFailure( mappingFunction );
    }


    /**
     * Registers a callback to be called when this future is marked with a value of T.<p/>
     * <p>
     * If registered before this future is completed, then the callback will be
     * called from the same thread that completed the future.  If the future is
     * already completed then the callback will be invoked immediately from the calling
     * thread before onResult returns.
     */
    public Future<T> onSuccess( VoidFunction2<Request,T> callback );

    /**
     * Registers a callback to be called when this future is marked with a value of T.<p/>
     * <p>
     * If registered before this future is completed, then the callback will be
     * called from the same thread that completed the future.  If the future is
     * already completed then the callback will be invoked immediately from the calling
     * thread before onResult returns.</p>
     *
     * Futures will by default use the executor used by the previously chained future.  This method
     * specifies that the supplied executor will be used instead.
     */
    public default Future<T> onSuccess( Executor executor, VoidFunction2<Request,T> callback ) {
        return withExecutor( executor ).onSuccess( callback );
    }

    /**
     * Registers a callback to be called when this future is marked with a Failure.<p/>
     * <p>
     * If registered before this future is completed, then the callback will be
     * called from the same thread that completed the future.  If the future is
     * already completed then the callback will be invoked immediately from the calling
     * thread before onResult returns.
     */
    public Future<T> onFailure( VoidFunction2<Request,Failure> callback );

    /**
     * Registers a callback to be called when this future is marked with a Failure.<p/>
     * <p>
     * If registered before this future is completed, then the callback will be
     * called from the same thread that completed the future.  If the future is
     * already completed then the callback will be invoked immediately from the calling
     * thread before onResult returns.</p>
     *
     * Futures will by default use the executor used by the previously chained future.  This method
     * specifies that the supplied executor will be used instead.
     */
    public default Future<T> onFailure( Executor executor, VoidFunction2<Request,Failure> callback ) {
        return withExecutor( executor ).onFailure( callback );
    }


    public default Future<T> onCompletion( VoidFunction2<Request,T> onSuccess, VoidFunction2<Request,Failure> onFailure ) {
        return onSuccess( onSuccess )
            .onFailure( onFailure );
    }

    public default Future<T> onCompletion( Executor executor, VoidFunction2<Request,T> onSuccess, VoidFunction2<Request,Failure> onFailure ) {
        return withExecutor( executor )
            .onSuccess(onSuccess)
            .onFailure(onFailure);
    }

}


@EqualsAndHashCode
@AllArgsConstructor
class SuccessfulFuture<T> implements Future<T> {
    private final Request request;
    private final T       value;
    private final String  description;


    public String toString() {
        return description == null ? "Future("+value+")" : description;
    }

    public boolean isComplete() {
        return true;
    }

    public boolean hasResult() {
        return true;
    }

    public boolean hasFailure() {
        return false;
    }

    public Future<T> withExecutor( Executor executor ) {
        return new SuccessfulDispatchingFuture<>( request, executor, value, description );
    }

    public Future<T> withDescription( String newDescription ) {
        if ( Objects.equals(this.description, newDescription) ) {
            return this;
        } else {
            return new SuccessfulFuture<>( request, value, newDescription );
        }
    }

    public <B> Future<B> mapResult( Function2<Request,T, B> mappingFunction ) {
        return safeInvoke( () -> new SuccessfulFuture<>(request,mappingFunction.invoke(request,value), description) );
    }

    public <B> Future<B> mapResultToTryable( Function2<Request,T, Tryable<B>> mappingFunction ) {
        return this.mapResult(mappingFunction).flatMapResult(Future::completed);
    }

    public <B> Future<B> flatMapResult( Function2<Request,T, Future<B>> mappingFunction ) {
        return safeInvoke( () -> mappingFunction.invoke(request,value) );
    }

    public Future<T> recover( Function2<Request,Failure, T> recoveryFunction ) {
        return this;
    }

    public Future<T> flatRecover( Function2<Request,Failure, Future<T>> recoveryFunction ) {
        return this;
    }

    public Future<T> mapFailure( Function2<Request,Failure, Failure> mappingFunction ) {
        return this;
    }

    public Future<T> flatMapFailure( Function2<Request,Failure, Future<Failure>> mappingFunction ) {
        return this;
    }

    public Future<T> onSuccess( VoidFunction2<Request,T> callback ) {
        try {
            callback.invoke( request, value );
        } catch ( Throwable ex ) {
            request.log( PromiseLogMessageFactory.INSTANCE.futureSideEffectErrored(ex) );
        }

        return this;
    }

    public Future<T> onFailure( VoidFunction2<Request,Failure> callback ) {
        return this;
    }

    private <B> Future<B> safeInvoke( Function0<Future<B>> f ) {
        if ( request.hasRequestFinished() ) {
            return Future.failure(request, new TimeoutFailure(request, this) );
        }

        try {
            Future<B> response = f.invoke();

            if ( description != null ) {
                response = response.withDescription( description );
            }

            return response;
        } catch ( Throwable ex ) {
            return new FailedFuture<>( request, new ExceptionFailure(ex), description );
        }
    }
}


@EqualsAndHashCode
@AllArgsConstructor
class FailedFuture<T> implements Future<T> {
    private final Request request;
    private final Failure failure;
    private final String  description;


    public boolean isComplete() {
        return true;
    }

    public boolean hasResult() {
        return false;
    }

    public boolean hasFailure() {
        return true;
    }


    public Future<T> withExecutor( Executor executor ) {
        return new FailedDispatchingFuture<>(request, executor, failure, description);
    }

    public Future<T> withDescription( String newDescription ) {
        if ( Objects.equals(this.description, newDescription) ) {
            return this;
        } else {
            return new FailedFuture<>( request, failure, newDescription );
        }
    }


    public <B> Future<B> mapResult( Function2<Request,T, B> mappingFunction ) {
        return Backdoor.cast(this);
    }

    public <B> Future<B> mapResultToTryable( Function2<Request,T, Tryable<B>> mappingFunction ) {
        return Backdoor.cast(this);
    }

    public <B> Future<B> flatMapResult( Function2<Request,T, Future<B>> mappingFunction ) {
        return Backdoor.cast(this);
    }

    public Future<T> recover( Function2<Request,Failure, T> recoveryFunction ) {
        return safeInvoke( () -> new SuccessfulFuture<>(request,recoveryFunction.invoke(request,failure), description) );
    }

    public Future<T> flatRecover( Function2<Request,Failure, Future<T>> recoveryFunction ) {
        return safeInvoke( () -> recoveryFunction.invoke(request,failure) );
    }

    public Future<T> mapFailure( Function2<Request,Failure, Failure> mappingFunction ) {
        return safeInvoke( () -> new FailedFuture<>(request,mappingFunction.invoke(request,failure),description) );
    }

    public Future<T> flatMapFailure( Function2<Request,Failure, Future<Failure>> mappingFunction ) {
        return safeInvoke( () -> cast(mappingFunction.invoke(request,failure)) );
    }

    public Future<T> onSuccess( VoidFunction2<Request,T> callback ) {
        return this;
    }

    public Future<T> onFailure( VoidFunction2<Request,Failure> callback ) {
        try {
            callback.invoke( request, failure );
        } catch ( Throwable ex ) {
            request.log( PromiseLogMessageFactory.INSTANCE.futureSideEffectErrored(ex) );
        }

        return this;
    }

    public String toString() {
        return description == null ? "FailedFuture("+failure+")" : description;
    }


    private <B> Future<B> safeInvoke( Function0<Future<B>> f ) {
        if ( request.hasRequestFinished() ) {
            return Future.failure(request, new TimeoutFailure(request, this, failure), description );
        }

        try {
            Future<B> response = f.invoke();

            return response.withDescription( description );
        } catch ( Throwable ex ) {
            return new FailedFuture<>( request, new ExceptionFailure(ex,failure), description );
        }
    }

}


@EqualsAndHashCode
@AllArgsConstructor
class SuccessfulDispatchingFuture<T> implements Future<T> {

    private final Request  request;
    private final Executor executor;
    private final T        value;
    private final String   description;


    public String toString() {
        return description == null ? "Future("+value+")" : description;
    }


    public boolean isComplete() {
        return true;
    }

    public boolean hasResult() {
        return true;
    }

    public boolean hasFailure() {
        return false;
    }

    public Future<T> withExecutor( Executor newExecutor ) {
        return newExecutor == this.executor ? this : new SuccessfulDispatchingFuture<>( request, newExecutor, value, description );
    }

    public Future<T> withDescription( String newDescription ) {
        if ( Objects.equals(this.description, newDescription) ) {
            return this;
        } else {
            return new SuccessfulDispatchingFuture<>( request, executor, value, newDescription );
        }
    }

    public <B> Future<B> mapResult( Function2<Request,T, B> mappingFunction ) {
        return dispatch( promise -> {
            B result = mappingFunction.invoke( request, value );

            promise.setResult( result );
        } );
    }

    public <B> Future<B> mapResultToTryable( Function2<Request,T, Tryable<B>> mappingFunction ) {
        return dispatch( promise -> {
            Tryable<B> result = mappingFunction.invoke( request, value );

            promise.complete( result );
        } );
    }

    public <B> Future<B> flatMapResult( Function2<Request,T, Future<B>> mappingFunction ) {
        return dispatch( promise -> {
            Future<B> result = mappingFunction.invoke( request, value );

            result.chainToPromise( promise );
        } );
    }

    public Future<T> recover( Function2<Request,Failure, T> recoveryFunction ) {
        return this;
    }

    public Future<T> flatRecover( Function2<Request,Failure, Future<T>> recoveryFunction ) {
        return this;
    }

    public Future<T> mapFailure( Function2<Request,Failure, Failure> mappingFunction ) {
        return this;
    }

    public Future<T> flatMapFailure( Function2<Request,Failure, Future<Failure>> mappingFunction ) {
        return this;
    }

    public Future<T> onSuccess( VoidFunction2<Request,T> callback ) {
        executor.execute( () -> {
            try {
                callback.invoke( request, value );
            } catch ( Exception ex ) {
                request.log( PromiseLogMessageFactory.INSTANCE.futureSideEffectErrored(ex) );
            }
        } );

        return this;
    }

    public Future<T> onFailure( VoidFunction2<Request,Failure> callback ) {
        return this;
    }


    private <B> Future<B> dispatch( VoidFunction1<Promise<B>> f ) {
        if ( request.hasRequestFinished() ) {
            return Future.failure( request, new TimeoutFailure(request,this) );
        }

        Promise<B> promise = new Promise<>(executor, request, description);

        executor.execute( () -> {
            if ( request.hasRequestFinished() ) {
                promise.setFailure( new TimeoutFailure(request,this) );

                return;
            }

            try {
                f.invoke(promise);
            } catch ( Exception ex ) {
                promise.setFailure( new ExceptionFailure(ex) );
            }
        } );

        return promise;
    }

}

@EqualsAndHashCode
@AllArgsConstructor
class FailedDispatchingFuture<T> implements Future<T> {

    private final Request  request;
    private final Executor executor;
    private final Failure  failure;
    private final String   description;



    public boolean isComplete() {
        return true;
    }

    public boolean hasResult() {
        return false;
    }

    public boolean hasFailure() {
        return true;
    }

    public Future<T> withExecutor( Executor newExecutor ) {
        return newExecutor == this.executor ? this : new FailedDispatchingFuture<>( request, newExecutor, failure, description );
    }

    public Future<T> withDescription( String newDescription ) {
        if ( Objects.equals(this.description, newDescription) ) {
            return this;
        } else {
            return new FailedDispatchingFuture<>( request, executor, failure, newDescription );
        }
    }

    public <B> Future<B> mapResult( Function2<Request,T, B> mappingFunction ) {
        return cast(this);
    }

    public <B> Future<B> mapResultToTryable( Function2<Request,T, Tryable<B>> mappingFunction ) {
        return cast(this);
    }

    public <B> Future<B> flatMapResult( Function2<Request,T, Future<B>> mappingFunction ) {
        return cast(this);
    }

    public Future<T> recover( Function2<Request,Failure, T> recoveryFunction ) {
        return dispatch( promise -> promise.setResult(recoveryFunction.invoke(request,failure)) );
    }

    public Future<T> flatRecover( Function2<Request,Failure, Future<T>> recoveryFunction ) {
        return dispatch( promise -> recoveryFunction.invoke(request,failure).chainToPromise(promise) );
    }

    public Future<T> mapFailure( Function2<Request,Failure, Failure> mappingFunction ) {
        return dispatch( promise -> promise.setFailure(mappingFunction.invoke(request,failure)) );
    }

    public Future<T> flatMapFailure( Function2<Request,Failure, Future<Failure>> mappingFunction ) {
        return dispatch( promise -> mappingFunction.invoke(request,failure).chainToPromise(cast(promise)) );
    }

    public Future<T> onSuccess( VoidFunction2<Request,T> callback ) {
        return this;
    }

    public Future<T> onFailure( VoidFunction2<Request,Failure> callback ) {
        executor.execute( () -> {
            try {
                callback.invoke( request, failure );
            } catch ( Exception ex ) {
                request.log( PromiseLogMessageFactory.INSTANCE.futureSideEffectErrored(ex) );
            }
        } );

        return this;
    }

    public String toString() {
        return description == null ? "FailedFuture("+failure+")" : description;
    }

    private <B> Future<B> dispatch( VoidFunction1<Promise<B>> f ) {
        if ( request.hasRequestFinished() ) {
            return Future.failure( request, new TimeoutFailure(request,this,failure) );
        }

        Promise<B> promise = new Promise<>(executor, request, this, description);

        executor.execute( () -> {
            try {
                f.invoke(promise);
            } catch ( Exception ex ) {
                promise.setFailure( new ExceptionFailure(ex,failure) );
            }
        } );

        return promise;
    }

}
