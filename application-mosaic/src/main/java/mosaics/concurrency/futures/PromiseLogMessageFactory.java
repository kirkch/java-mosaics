package mosaics.concurrency.futures;

import mosaics.fp.Failure;
import mosaics.logging.LogMessage;
import mosaics.logging.LogMessageFactory;
import mosaics.logging.LogMessageProxyFactory;
import mosaics.logging.loglevels.DevError;
import mosaics.logging.loglevels.DevWarn;


public interface PromiseLogMessageFactory extends LogMessageFactory {
    public static final PromiseLogMessageFactory INSTANCE = LogMessageProxyFactory.INSTANCE.createLogMessageFactory( PromiseLogMessageFactory.class );

    @DevError( "A future's side effect threw the following exception: $ex" )
    public LogMessage futureSideEffectErrored( Throwable ex );

    @DevError( "promise already completed" )
    public LogMessage promiseAlreadyCompleted();

    @DevWarn( "Discarded value: $value" )
    public LogMessage promiseResultDiscardedDueToTimeout( Object value );

    @DevWarn( "Discarded failure: $failure" )
    public LogMessage promiseFailureDiscardedDueToTimeout( Failure failure );

}
