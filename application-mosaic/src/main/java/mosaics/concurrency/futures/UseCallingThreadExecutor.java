package mosaics.concurrency.futures;

import java.util.concurrent.Executor;


/**
 * An executor that does not front a thread pool.  Instead it calls the command immediately.
 */
public class UseCallingThreadExecutor implements Executor {
    public static final Executor INSTANCE = new UseCallingThreadExecutor();


    public void execute( Runnable command ) {
        command.run();
    }
}
