package mosaics.concurrency;


import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.lang.ComparableMixin;
import mosaics.lang.QA;
import mosaics.lang.TryUtils;
import mosaics.lang.clock.Clock;
import mosaics.lang.functions.VoidFunction0WithThrows;
import mosaics.lang.lifecycle.StartStoppable;
import mosaics.lang.lifecycle.Subscription;
import mosaics.lang.threads.STWDetector;

import java.util.Objects;
import java.util.concurrent.ConcurrentSkipListSet;


public class SystemScheduler extends StartStoppable<SystemScheduler> implements Scheduler {

    private final Env                             env;
    private final ObjectMonitor                   monitor;
    private final WorkerThread                    workerThread;
    private final ConcurrentSkipListSet<TimerJob> timerJobs = new ConcurrentSkipListSet<>(TimerJob::compareTo);


    public SystemScheduler( Env env, String name ) {
        super(name );

        this.env          = env;
        this.monitor      = ObjectMonitor.createObjectMonitorFor( env.getClock() );
        this.workerThread = new WorkerThread(env, name, this::invokeReadyToRunTasks, monitor ).asDaemon();

        registerServicesAfter( workerThread );
    }


    public SystemScheduler withMaxIntervalBetweenPolls( long maxIntervalMillis ) {
        workerThread.withMaxIntervalBetweenPolls( maxIntervalMillis );

        return this;
    }

    public long currentTimeMillis() {
        return env.currentTimeMillis();
    }

    public void sleep( Request req, long millis ) {
        TryUtils.invokeAndRethrowException( () -> monitor.sleep(req,millis) );
    }

    public synchronized Subscription scheduleOnce( Request req, long delayMillis, VoidFunction0WithThrows callback ) {
        QA.argIsGTEZero(delayMillis, "delay");


        long     whenMillis = env.currentTimeMillis() + delayMillis;
        TimerJob job        = new TimerJob( req, whenMillis, callback);

        req.log( SystemSchedulerLogMessageFactory.INSTANCE.jobScheduled(callback.toString(),whenMillis) );

        timerJobs.add( job );
        workerThread.notifyWorkerThread( req );

        return new Subscription( () -> {
            job.cancel(env.getClock(), req);

            timerJobs.remove( job );
        } );
    }

    public void poke( Request req ) {
        workerThread.notifyWorkerThread( req );
    }

    protected long invokeReadyToRunTasks() {
        Request req = env.createRequest( getName() + " poll" );

        int  processedCount = 0;
        long t0             = env.currentTimeMillis();
        long stwT0          = STWDetector.global().getTotalDelaySoFarMillis();

        while ( !timerJobs.isEmpty() ) {
            TimerJob next = timerJobs.first();

            if ( next.isReadyAt(t0) ) {
                next.invoke();

                timerJobs.remove( next );

                processedCount += 1;
            } else {
                break;
            }
        }

        long durationMillis = req.currentTimeMillis() - t0;
        long stwMillis      = STWDetector.global().getTotalDelaySoFarMillis() - stwT0;

        req.log( SystemSchedulerLogMessageFactory.INSTANCE.schedulerRan(getName(),processedCount,durationMillis,stwMillis) );

        return 1_000;
    }


    public static class TimerJob implements ComparableMixin<TimerJob> {
        private final Request                 request;
        private final long                    whenMillis;
        private       VoidFunction0WithThrows task;
        private       boolean                 isCancelled;

        public TimerJob( Request request, long whenMillis, VoidFunction0WithThrows task ) {
            this.request    = request;
            this.whenMillis = whenMillis;
            this.task       = task;
        }

        public long getWhenMillis() {
            return whenMillis;
        }

        public synchronized void cancel( Clock clock, Request req ) {
            if ( task != null ) {
                long millisEarly = whenMillis - clock.currentTimeMillis();

                req.log( SystemSchedulerLogMessageFactory.INSTANCE.jobCancelled(task.toString(),millisEarly) );

                isCancelled = true;
                task        = null;
            }
        }

        public synchronized void invoke() {
            if ( isCancelled ) {
                return;
            }

            VoidFunction0WithThrows job = this.task;

            try {
                long t0                = request.currentTimeMillis();
                long stwT0             = STWDetector.global().getTotalDelaySoFarMillis();
                long durationMillis    = (long) request.timeMillis( job );
                long stwDurationMillis = STWDetector.global().getTotalDelaySoFarMillis() - stwT0;

                request.log( SystemSchedulerLogMessageFactory.INSTANCE.jobInvokedSuccessfully(Objects.toString(job), durationMillis,t0-whenMillis,stwDurationMillis) );
            } catch ( Throwable ex ) {
                request.log( SystemSchedulerLogMessageFactory.INSTANCE.jobFailed(Objects.toString(job),ex) );
            }

            this.task        = null;
            this.isCancelled = true;
        }

        public int compareTo( TimerJob o ) {
            if ( o == this ) {
                return 0;
            } else if ( this.getWhenMillis() < o.getWhenMillis() ) {
                return -1;
            } else if ( this.getWhenMillis() == o.getWhenMillis() ) {
                return Integer.compare( System.identityHashCode(this), System.identityHashCode(o) );
            }

            return 1;
        }

        public boolean isReadyAt( long nowMillis ) {
            return whenMillis <= nowMillis;
        }
    }
}
