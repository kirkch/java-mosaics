package mosaics.concurrency;

import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.concurrency.futures.Future;
import mosaics.concurrency.futures.TimeoutFailure;
import mosaics.fp.Failure;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;


public class TimeoutFailureTest {
    private Env     env     = Env.createTestEnv();
    private Request request = env.createRequest( "junit" );

//    @Test
//    public void equalsContract() {
//        // requires reflection permissions:  --add-opens java.base/java.lang=ALL-UNNAMED
//        EqualsVerifier.forClass( TimeoutFailure.class )
//            .suppress( Warning.STRICT_INHERITANCE )
//            .verify();
//    }

    @Test
    public void getChainedFailure() {
        Future<String> future = Future.successful( request, "a" );
        Failure chainedFailure = new MockFailure("foo");
        TimeoutFailure failure = new TimeoutFailure( request, future, chainedFailure );

        assertSame( chainedFailure, failure.getChainedFailure() );
    }

    @Test
    public void getException() {
        Future<String> future = Future.successful( request, "a" );
        TimeoutFailure failure = new TimeoutFailure( request, future );

        assertEquals( TimeoutException.class, failure.toException().getClass() );
    }

    @Test
    public void getMessage() {
        Future<String> future = Future.successful( request, "a" );
        TimeoutFailure failure = new TimeoutFailure( request, future );

        assertEquals( "Unable to continue future '"+future+"' as request has already completed", failure.getMessage() );
    }

    @Test
    public void testToString() {
        Future<String> future = Future.successful( request, "a" );
        TimeoutFailure failure = new TimeoutFailure( request, future );

        assertEquals( failure.toString(), failure.getMessage() );
    }

}
