//package com.softwaremosaic.lang.concurrency;
//
//import com.softwaremosaic.junit.JUnitMosaicRunner;
//import com.softwaremosaic.junit.annotations.Test;
//import com.softwaremosaic.lang.Subscription;
//import com.softwaremosaic.lang.clock.Clock;
//import com.softwaremosaic.lang.clock.FixedTimeClock;
//import com.softwaremosaic.lang.functions.VoidFunction0WithThrows;
//import com.softwaremosaic.logging.handlers.CaptureLogHandler;
//import com.softwaremosaic.requests.Request;
//import com.softwaremosaic.requests.RequestFactory;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.runner.RunWith;
//
//import java.time.Duration;
//import java.util.concurrent.atomic.AtomicBoolean;
//
//import static com.softwaremosaic.lang.threads.ThreadUtils.spinUntilTrue;
//import static org.junit.Assert.*;
//
//
//@RunWith(JUnitMosaicRunner.class)
//public class SystemSchedulerTest {
//
//    private CaptureLogHandler log            = new CaptureLogHandler();
//    private Clock             clock          = new FixedTimeClock();
//    private RequestFactory    requestFactory = new RequestFactory(clock, log);
//    private SystemScheduler   scheduler      = new SystemScheduler(requestFactory, "junit").withMaxIntervalBetweenPolls(10);
//    private Request           request        = requestFactory.create();
//
//
//    @Before
//    public void setup() {
//        scheduler.start(requestFactory.create("setup"));
//    }
//
//    @After
//    public void tearDown() {
//        scheduler.stop(requestFactory.create("tearDown"));
//    }
//
//
//// otherConstructors
//
//    @Test
//    public void constructor(){
//        new SystemScheduler( "j2" );
//    }
//
//// currentTimeMillis
//
//    @Test
//    public void currentTimeMillis() {
//        clock.setMillis( 100 );
//
//        assertEquals( 100, scheduler.currentTimeMillis() );
//
//        incTime( 10 );
//        assertEquals( 110, scheduler.currentTimeMillis() );
//    }
//
//
//// sleep
//
//    @Test
//    public void sleep() throws InterruptedException {
//        long startMillis = clock.currentTimeMillis();
//
//        new Thread(() -> {
//            spinUntilTrue( () -> log.getLoggedMessagesFP(ThreadGoingToSleepEvent.class).hasContents() );
//            clock.incMillis(6);
//        } ).start();
//
//        scheduler.sleep( request, 5 );
//
//        assertTrue( clock.currentTimeMillis() >= startMillis + 5 );
//    }
//
//
//// scheduleOnce behaviours
//
//    @Test
//    public void scheduleOnceInNegativeMS_expectException() {
//        AtomicBoolean flag = new AtomicBoolean( false );
//
//        try {
//            scheduler.scheduleOnce( request, -1, () -> flag.set( true ) );
//            fail("Expected IllegalArgumentException");
//        } catch ( IllegalArgumentException ex ) {
//            assertEquals("'delay' (-1) must be >= 0", ex.getMessage());
//        }
//    }
//
//    @Test(threadCheck=true)
//    public void scheduleOnceIn100ms_checkIn60ms_expectNoCall() {
//        AtomicBoolean flag = new AtomicBoolean( false );
//
//
//        scheduler.scheduleOnce( request, Duration.ofMillis(100), () -> flag.set( true ) );
//
//        incTime( 60 );
//
//        assertFalse( flag.get() );
//    }
//
//
//    @Test(threadCheck=true)
//    public void scheduleOnceIn100ms_checkIn100ms_expectCall() {
//        AtomicBoolean flag = new AtomicBoolean( false );
//
//        VoidFunction0WithThrows job = () -> flag.set(true);
//        scheduler.scheduleOnce( request, 100, job.withDescription("setFlagJob") );
//        incTime( 100 );
//
//        spinUntilTrue(flag::get);
//    }
//
//    @Test
//    public void scheduleOnceIn100ms_cancelThenCheckIn100ms_expectNoCall() {
//        AtomicBoolean flag = new AtomicBoolean( false );
//
//        Subscription sub = scheduler.scheduleOnce( request, 100, () -> flag.set(true) );
//        sub.cancel( request );
//
//        incTime( 100 );
//
//        assertFalse( flag.get() );
//    }
//
//    @Test
//    public void scheduleTwoTasksInTimeOrder_expectThemBothToRunInOrder() {
//        StringBuffer buf = new StringBuffer();
//
//        scheduler.scheduleOnce( request, 10, () -> buf.append( "A" ) );
//        scheduler.scheduleOnce( request, 20, () -> buf.append( "B" ) );
//
//        incTime( 100 );
//
//        spinUntilTrue( () -> buf.toString().equals("AB") );
//    }
//
//    @Test
//    public void scheduleTwoTasksInReverseTimeOrder_expectThemBothToRunInOrder() {
//        StringBuffer buf = new StringBuffer();
//
//        scheduler.scheduleOnce( request, 20, () -> buf.append( "B" ) );
//        scheduler.scheduleOnce( request, 10, () -> buf.append( "A" ) );
//
//        incTime( 100 );
//
//        spinUntilTrue( () -> buf.toString().equals("AB") );
//    }
//
//
//// TimerJob
//
//    @Test
//    public void invokeATimerJobThatHasBeenCancelled_expectItToNotRun() {
//        AtomicBoolean            flag = new AtomicBoolean();
//        SystemScheduler.TimerJob job  = new SystemScheduler.TimerJob( request, 100, () -> flag.set(true) );
//
//        job.cancel( clock, request );
//        job.invoke( clock );
//
//        assertFalse( flag.get() );
//    }
//
//
//// scheduleOnce logging
//
//    @Test
//    public void scheduleJob_expectJobScheduledMessage() {
//        VoidFunction0WithThrows job = () -> {};
//
//        scheduler.scheduleOnce( request, 100, job.withDescription("j1") );
//
//        JobScheduledEvent expected = new JobScheduledEvent("j1", clock.currentTimeMillis()+100);
//
//        assertEquals( expected, log.getLoggedMessages(JobScheduledEvent.class).get(0) );
//    }
//
//    @Test
//    public void cancelJob_expectJobCancelledMessage() {
//        VoidFunction0WithThrows job = () -> {};
//
//        Subscription subscription = scheduler.scheduleOnce( request, 100, job.withDescription("j1") );
//        subscription.cancel( request );
//
//        JobCancelledEvent expected = new JobCancelledEvent("j1", 100);
//
//        assertEquals( expected, log.getLoggedMessages(JobCancelledEvent.class).get(0) );
//    }
//
//    @Test
//    public void schedule3JobsAtSameTime_expectSchedulerRun3JobsMessage() {
//        VoidFunction0WithThrows job = () -> {};
//
//        scheduler.scheduleOnce( request, 10, job.withDescription("j1") );
//        scheduler.scheduleOnce( request, 10, job.withDescription("j2") );
//        scheduler.scheduleOnce( request, 10, job.withDescription("j3") );
//
//        incTime( 20 );
//
//        spinUntilTrue( () ->
//            log.getLoggedMessagesFP(SchedulerRanEvent.class)
//                .mapToInt(msg -> msg.getProcessedCount())
//                .sum() == 3
//        );
//
//        SchedulerRanEvent actual = log.getLoggedMessagesFP(SchedulerRanEvent.class)
//            .filter( msg -> msg.getProcessedCount() > 0 )
//            .firstOrNull();
//
//        assertEquals( "junit", actual.getSchedulerName() );
//        assertEquals( 0, actual.getDurationMillis() );
//        assertTrue( actual.getStwMillis() >= 0 );
//    }
//
//    @Test
//    public void afterSuccessfulJobHasRun_expectJobInvokedSuccessfullyMessage() {
//        VoidFunction0WithThrows job = () -> {};
//
//        scheduler.scheduleOnce( request, 10, job.withDescription("j1") );
//
//        incTime( 20 );
//
//        spinUntilTrue( () -> log.getLoggedMessagesFP(JobInvokedSuccessfullyEvent.class).hasContents() );
//
//        JobInvokedSuccessfullyEvent actual = log.getLoggedMessagesFP( JobInvokedSuccessfullyEvent.class ).firstOrNull();
//
//        assertEquals( "j1", actual.getJobDesc() );
//        assertEquals( 10, actual.getMillisLate() );
//        assertEquals( 0, actual.getTotalDurationMillis() );
//        assertTrue( actual.getStwMillis() >= 0 );
//    }
//
//    @Test
//    public void afterJobHasThrownAnException_expectJobFailedMessage() {
//        RuntimeException ex = new RuntimeException( "expected" );
//        VoidFunction0WithThrows job = () -> {throw ex;};
//
//        scheduler.scheduleOnce( request, 10, job.withDescription("j1") );
//
//        incTime( 20 );
//
//        spinUntilTrue( () -> log.getLoggedMessagesFP(JobFailedEvent.class).hasContents() );
//
//        JobFailedEvent expected = new JobFailedEvent("j1", ex);
//
//        assertEquals( ex, expected.getEx() );
//        assertEquals( expected, log.getLoggedMessagesFP( JobFailedEvent.class ).firstOrNull() );
//    }
//
//
//
//
//    private void incTime( long millis) {
//        clock.incMillis( millis );
//        scheduler.poke( request );  // the poke is needed incase the schedulers worker thread has been parked
//    }
//
//}
