package mosaics.concurrency.futures;

public class SuccessfulFutureTest extends BaseHasResultFutureTestCases {

    public SuccessfulFutureTest() {
        super( SuccessfulFuture.class );

        this.future = Future.successful( request, "hello" );
    }

}
