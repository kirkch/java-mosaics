package mosaics.concurrency.futures;

public class FailedFutureTest extends BaseHasFailureFutureTestCases {

    public FailedFutureTest() {
        super( FailedFuture.class );

        future = Future.failure( request, initialFailure );
    }

}
