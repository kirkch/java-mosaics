package mosaics.concurrency.futures;

import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.concurrency.MockFailure;
import mosaics.fp.ExceptionFailure;
import mosaics.logging.writers.CapturingLogWriter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;


public class PromiseTest extends BaseFutureTestSupport {
    private CapturingLogWriter capturingLogWriter = new CapturingLogWriter();
    private Env                env                = Env.createTestEnv(capturingLogWriter);
    private Request            request            = env.createRequest( "junit" );


    @Test
    public void constructorReqDesc() {
        Promise<String> p = new Promise<>( request, "desc" );

        assertEquals( "desc", p.toString() );
    }

    @Test
    public void withMatchingDescription_expectSamePromiseBack() {
        Promise<String> p = new Promise<>( request, "desc" );

        assertSame( p, p.withDescription("desc") );
    }

    @Test
    public void withMissmatchingDescription_expectDifferentPromiseBack() {
        Promise<String> p = new Promise<>( request, "desc" );

        assertSame( p, p.withDescription("abc") );
        assertSame( "abc", p.withDescription("abc").toString() );
    }

    @Test
    public void givenWaitingPromise_isComplete_expectFalse() {
        Promise<String> p = new Promise<>( request );

        assertFalse( p.isComplete() );
    }

    @Test
    public void givenWaitingPromise_hasResult_expectFalse() {
        Promise<String> p = new Promise<>( request );

        assertFalse( p.hasResult() );
    }

    @Test
    public void givenWaitingPromise_hasFailure_expectFalse() {
        Promise<String> p = new Promise<>( request );

        assertFalse( p.hasFailure() );
    }

    @Test
    public void givenOnSuccessCallbackThatErrorsBeforeThePromiseIsCompleted_expectItsErrorToBeLogged() {
        Promise<String> p = new Promise<>( request );

        NullPointerException ex = new NullPointerException( "bomb" );
        p.onSuccess( (r,v) -> {throw ex;} );

        p.setResult( "abc" );

        capturingLogWriter.assertContainsMessage( "A future's side effect threw the following exception: java.lang.NullPointerException: bomb" );
    }

    @Test
    public void givenOnFailureCallbackThatErrorsBeforeThePromiseIsCompleted_expectItsErrorToBeLogged() {
        Promise<String> p = new Promise<>( request );

        NullPointerException ex = new NullPointerException( "bomb" );
        p.onFailure( (r,f) -> {throw ex;} );

        p.setFailure(new MockFailure("abc") );

        capturingLogWriter.assertContainsMessage( "A future's side effect threw the following exception: java.lang.NullPointerException: bomb" );
    }

    @Test
    public void mapResultBeforePromiseCompleted_callbackErrors_expectPromiseToRecordAFailure() {
        Promise<String>      p  = new Promise<>( request );
        NullPointerException ex = new NullPointerException( "bomb" );
        Future<String> f  = p.mapResult( (r,v) -> {throw ex;} );

        p.setResult( "abc" );

        assertFailure( new ExceptionFailure(ex), f );
    }

    @Test
    public void mapFailureBeforePromiseCompleted_callbackErrors_expectPromiseToRecordAFailure() {
        Promise<String>      p  = new Promise<>( request );
        NullPointerException ex = new NullPointerException( "bomb" );
        Future<String>       f  = p.recover( (r,f2) -> {throw ex;} );

        MockFailure initialFailure = new MockFailure( "initial failure" );
        p.setFailure( initialFailure );

        assertFailure( new ExceptionFailure(ex,initialFailure), f );
    }

}
