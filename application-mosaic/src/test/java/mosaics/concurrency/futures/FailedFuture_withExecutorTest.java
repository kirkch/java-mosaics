package mosaics.concurrency.futures;

import mosaics.cli.requests.Request;
import mosaics.concurrency.MockFailure;
import mosaics.concurrency.SingleThreadedExecutor;
import mosaics.fp.ExceptionFailure;
import mosaics.fp.Failure;
import mosaics.fp.Try;
import mosaics.junit.JMThreadExtension;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Isolated;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


@Isolated
@JMThreadExtension.IgnoreThread("Attach Listener")
@ExtendWith(JMThreadExtension.class)
public class FailedFuture_withExecutorTest extends BaseFutureTestSupport {

    private SingleThreadedExecutor executor       = new SingleThreadedExecutor("junit");
    private MockFailure            initialFailure = new MockFailure( "initial failure" );
    private Future<String>         future         = Future.failure( request, initialFailure );



    @BeforeEach
    public void setup() {
        executor.start();
    }

    @AfterEach
    public void tearDown() {
        executor.stop();
    }


    @Test
    public void isComplete_expectTrue() {
        assertTrue( future.withExecutor(executor).isComplete() );
    }

    @Test
    public void hasResult_expectFalse() {
        assertFalse( future.withExecutor(executor).hasResult() );
    }

    @Test
    public void hasFailure_expectTrue() {
        assertTrue( future.withExecutor(executor).hasFailure() );
    }

    @Test
    public void withSameExecutorWillReturnTheSameFuture() {
        Future<String> dispatchingFuture = future.withExecutor(executor);

        assertSame( dispatchingFuture, dispatchingFuture.withExecutor(executor) );
    }

    @Test
    public void withDescription() {
        assertEquals( "foo", future.withDescription( "foo" ).withExecutor(executor).toString() );
        assertEquals( "foo", future.withExecutor(executor).withDescription( "foo" ).toString() );

        Future<String> f = future.withExecutor( executor ).withDescription( "foo" );
        assertSame( f, f.withDescription("foo") );
    }

// public <B> Future<B> mapResult( Function1<T, B> mappingFunction );

    @Test
    public void mapResult_expectCallbackToNotBeCalled() {
        Future<Integer> resultF = future.mapResult( executor, (r,v) -> {recordCallbackCall(); return v.length();} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenMapResult_expectDescriptionToCarryOver() {
        Future<Integer> resultF = future.withDescription( "abc" )
            .mapResult( executor, (r,v) -> {recordCallbackCall(); return v.length();} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void givenFinishedRequest_mapResult_expectCallbackToNotBeCalled() {
        request.markCompleted();

        Future<Integer> resultF = future.mapResult( executor, (r,v) -> {recordCallbackCall(); return v.length();} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

// public default <B> Future<B> mapResultToTryable( Function1<T, Tryable<B>> mappingFunction )

    @Test
    public void mapResultToTryable_expectCallbackToNotBeCalled() {
        Future<Integer> resultF = future.mapResultToTryable( executor, (r,str) -> {recordCallbackCall(); return Try.succeeded(str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void givenFinishedRequest_mapResultToTryable_expectCallbackToNotBeCalled() {
        request.markCompleted();

        Future<Integer> resultF = future.mapResultToTryable( executor, (r,str) -> {recordCallbackCall(); return Try.succeeded(str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionMapResultToTryable_expectDescriptionToCarryOVer() {
        Future<Integer> resultF = future.withDescription( "abc" )
            .mapResultToTryable( executor, (r,str) -> {recordCallbackCall(); return Try.succeeded(str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }


// public <B> Future<B> flatMapResult( Function1<T, Future<B>> mappingFunction );

    @Test
    public void flatMapResult_expectCallbackToNotBeCalled() {
        Future<Integer> resultF = future.flatMapResult( executor, (r,str) -> {recordCallbackCall(); return Future.successful(request,str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void givenFinishedRequest_flatMapResult_expectCallbackToNotBeCalled() {
        request.markCompleted();

        Future<Integer> resultF = future.flatMapResult( executor, (r,str) -> {recordCallbackCall(); return Future.successful(request,str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenFlatMapResult_expectDescriptionToCarryOver() {
        Future<Integer> resultF = future.withDescription( "abc" )
            .flatMapResult( executor, (r,str) -> {recordCallbackCall(); return Future.successful(request,str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }


// public Future<T> recover( Function1<Failure, T> recoveryFunction );

    @Test
    public void recover_succeed_expectFutureToHaveRecovered() {
        Future<String> resultF = future.recover( executor, (r,f) -> {recordCallbackCall(); return "recovered";} );

        assertResult( "recovered", resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void givenFinishedRequest_recover_expectFutureToHaveTimedout() {
        request.markCompleted();

        Failure expectation = new TimeoutFailure( request, future, initialFailure );
        Future<String> resultF = future.recover( executor, (r,f) -> {recordCallbackCall(); return "recovered";} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenRecover_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .recover( executor, (r,f) -> {recordCallbackCall(); return "recovered";} );

        assertResult( "recovered", resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void recover_threwException_expectChainedFailure() {
        Exception      ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.recover( executor, (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex,initialFailure), resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }


// public Future<T> flatRecover( Function1<Failure, Future<T>> recoveryFunction );

    @Test
    public void flatRecover_succeed_expectRecoveredResult() {
        Future<String> resultF = future.flatRecover( executor, (r,f) -> {recordCallbackCall(); return Future.successful(request,"recovered");} );

        assertResult( "recovered", resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void givenFinishedRequest_flatRecover_expectTimeout() {
        request.markCompleted();

        Failure expectation = new TimeoutFailure( request, future, initialFailure );
        Future<String> resultF = future.flatRecover( executor, (r,f) -> {recordCallbackCall(); return Future.successful(request,"recovered");} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionFlatRecover_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .flatRecover( executor, (r,f) -> {recordCallbackCall(); return Future.successful(request,"recovered");} );

        assertResult( "recovered", resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void flatRecover_fail_expectReplacement() {
        Failure        expectation = new MockFailure("replacement");
        Future<String> resultF     = future.flatRecover( executor, (r,f) -> {recordCallbackCall(); return Future.failure(request,expectation);} );

        assertFailure( expectation, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void flatRecover_threwException_expectChainedFailure() {
        Exception      ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.flatRecover( executor, (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex,initialFailure), resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }


// public Future<T> mapFailure( Function1<Failure, Failure> mappingFunction );

    @Test
    public void mapFailure_success_expectReplacedFailure() {
        Failure        expectation = new MockFailure("replacement");
        Future<String> resultF     = future.mapFailure( executor, (r,f) -> {recordCallbackCall(); return expectation;} );

        assertFailure( expectation, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void givenFinishedRequest_mapFailure_expectTimeout() {
        request.markCompleted();

        Failure        expectation = new TimeoutFailure( request, future, initialFailure );
        Future<String> resultF     = future.mapFailure( executor, (r,f) -> {recordCallbackCall(); return expectation;} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenMapFailure_expectDescriptionToCarryOver() {
        Failure        expectation = new MockFailure("replacement");
        Future<String> resultF     = future.withDescription( "abc" )
            .mapFailure( executor, (r,f) -> {recordCallbackCall(); return expectation;} );

        assertFailure( expectation, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void mapFailure_threwException_expectChainedFailure() {
        Exception      ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.mapFailure( executor, (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex,initialFailure), resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }


// public Future<T> flatMapFailure( Function1<Failure, Failure> mappingFunction );

    @Test
    public void flatMapFailure_returnsFailure_expectReplacedFailure() {
        Failure        expectation = new MockFailure("replacement");
        Future<String> resultF     = future.flatMapFailure( executor, (r,f) -> {recordCallbackCall(); return Future.failure(request,expectation);} );

        assertFailure( expectation, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void givenFinishedRequest_flatMapFailure_expectTimeout() {
        request.markCompleted();

        Failure        expectation = new TimeoutFailure( request, future, initialFailure );
        Future<String> resultF     = future.flatMapFailure( executor, (r,f) -> {recordCallbackCall(); return Future.failure(request,expectation);} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionFlatMapFailure_expectDescriptionToCarryOver() {
        Failure        expectation = new MockFailure("replacement");
        Future<String> resultF     = future.withDescription( "abc" )
            .flatMapFailure( executor, (r,f) -> {recordCallbackCall(); return Future.failure(request,expectation);} );

        assertFailure( expectation, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void flatMapFailure_throwsException_expectChainedException() {
        Exception      ex      = new IllegalArgumentException("ex");;
        Future<String> resultF = future.flatMapFailure( executor, (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex,initialFailure), resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }


// public Future<T> onSuccess( VoidFunction1<T> callback );

    @Test
    public void onSuccess_expectCallbackWasCalled() {
        Future<String> resultF = future.onSuccess( executor, (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void givenFinishedRequest_onSuccess_expectCallbackWasCalled() {
        request.markCompleted();

        Future<String> resultF = future.onSuccess( executor, (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenOnSuccess_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .onSuccess( executor, (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }


// public Future<T> onFailure( VoidFunction1<Failure> callback );

    @Test
    public void onFailure_expectCallbackWasCalled() {
        Future<String> resultF = future.onFailure( executor, (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void givenFinishedRequest_onFailure_expectCallbackWasCalled() {
        request.markCompleted();

        Future<String> resultF = future.onFailure( executor, (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void withDescriptionThenOnFailure_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .onFailure( executor, (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void onFailureThrowsException_expectErrorToBeLogged() {
        Exception       ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.onFailure( executor, (r,f) -> {recordCallbackCall(); throwException(ex);} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());

        capturingLogWriter.assertContainsMessage("A future's side effect threw the following exception: java.lang.IllegalArgumentException: ex");
    }



    @Test
    public void equalsContract() {
        EqualsVerifier.forClass( FailedDispatchingFuture.class)
            .suppress( Warning.STRICT_INHERITANCE)
            .withPrefabValues( Request.class, request, env.createRequest("equals-verifier") )
            .verify();
    }

    @Test
    public void testToString() {
        assertEquals( "FailedFuture(MockFailure(msg=initial failure))", future.withExecutor(executor).toString() );
    }

}
