package mosaics.concurrency.futures;

import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.concurrency.Await;
import mosaics.fp.Failure;
import mosaics.lang.Backdoor;
import mosaics.logging.writers.CapturingLogWriter;

import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;


public abstract class BaseFutureTestSupport {

    protected CapturingLogWriter capturingLogWriter = new CapturingLogWriter();
    protected Env                env                = Env.createTestEnvWithRealClock(capturingLogWriter);
    protected Request            request            = env.createRequest( "junit" );

    protected Await              await              = new Await(env);

    protected AtomicReference<Thread> threadWasCalledFrom = new AtomicReference<>();




    protected void recordCallbackCall() {
        threadWasCalledFrom.set( Thread.currentThread() );
    }

    protected void assertCallbackWasCalledFromCurrentThread() {
        assertCallbackWasCalledFrom( Thread.currentThread() );
    }

    protected void assertCallbackWasCalledFrom( Thread expectedThread ) {
        assertSame( expectedThread, threadWasCalledFrom.get() );
    }
    protected void assertCallbackWasNotCalled() {
        assertNull( threadWasCalledFrom.get() );
    }


    protected <T> void assertResult( T expectation, Future<T> f ) {
        assertEquals( expectation, await.result(request,f) );
    }

    protected <T> void assertFailure( Failure expectation, Future<T> resultF ) {
        Failure failure = await.failure( request, resultF );
        assertEquals( expectation, failure );
    }

    @SuppressWarnings("ThrowableNotThrown")
    protected <T> T throwException( Exception ex ) {
        Backdoor.throwException( ex );

        return null;
    }

}
