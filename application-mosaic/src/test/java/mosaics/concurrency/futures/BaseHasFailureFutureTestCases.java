package mosaics.concurrency.futures;

import mosaics.concurrency.MockFailure;
import mosaics.cli.requests.Request;
import mosaics.fp.ExceptionFailure;
import mosaics.fp.Failure;
import mosaics.fp.Try;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SuppressWarnings({"rawtypes", "unchecked"})
public abstract class BaseHasFailureFutureTestCases extends BaseFutureTestSupport {

    protected MockFailure    initialFailure = new MockFailure( "initial failure" );
    protected Future<String> future;
    private   Class          type;


    protected BaseHasFailureFutureTestCases( Class type ) {
        this.type = type;
    }


    @Test
    public void isComplete_expectTrue() {
        assertTrue( future.isComplete() );
    }

    @Test
    public void hasResult_expectFalse() {
        assertFalse( future.hasResult() );
    }

    @Test
    public void hasFailure_expectTrue() {
        assertTrue( future.hasFailure() );
    }


    @Test
    public void withDescription() {
        assertEquals( "foo", future.withDescription("foo").toString() );
    }


// public <B> Future<B> mapResult( Function1<T, B> mappingFunction );

    @Test
    public void mapResult_expectCallbackToNotBeCalled() {
        Future<Integer> resultF = future.mapResult( (r,v) -> {recordCallbackCall(); return v.length();} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void givenRequestHasFinished_mapResult_expectCallbackToNotBeCalledAndTheFutureToCarryOverTheInitialFailure() {
        request.markCompleted();

        Future<Integer> resultF = future.mapResult( (r,v) -> {recordCallbackCall(); return v.length();} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenmapResult_expectDescriptionToCarryOver() {
        Future<Integer> resultF = future.withDescription("abc").mapResult( (r,v) -> {recordCallbackCall(); return v.length();} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }

// public default <B> Future<B> mapResultToTryable( Function1<T, Tryable<B>> mappingFunction )

    @Test
    public void mapResultToTryable_expectCallbackToNotBeCalled() {
        Future<Integer> resultF = future.mapResultToTryable( (r,str) -> {recordCallbackCall(); return Try.succeeded(str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void givenRequestHasFinished_mapResultToTryable_expectCallbackToNotBeCalled() {
        request.markCompleted();

        Future<Integer> resultF = future.mapResultToTryable( (r,str) -> {recordCallbackCall(); return Try.succeeded(str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenmapResultToTryable_expectDescriptionToCarryOver() {
        Future<Integer> resultF = future.withDescription("abc").mapResultToTryable( (r,str) -> {recordCallbackCall(); return Try.succeeded(str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }


// public <B> Future<B> flatMapResult( Function1<T, Future<B>> mappingFunction );

    @Test
    public void givenRequestHasFinished_flatMapResult_expectCallbackToNotBeCalled() {
        request.markCompleted();

        Future<Integer> resultF = future.flatMapResult( (r,str) -> {recordCallbackCall(); return Future.successful(request,str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void flatMapResult_expectCallbackToNotBeCalled() {
        Future<Integer> resultF = future.flatMapResult( (r,str) -> {recordCallbackCall(); return Future.successful(request,str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenflatMapResult_expectDescriptionToCarryOver() {
        Future<Integer> resultF = future.withDescription( "abc" )
            .flatMapResult( (r,str) -> {recordCallbackCall(); return Future.successful(request,str.length());} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }


// public Future<T> recover( Function1<Failure, T> recoveryFunction );

    @Test
    public void recover_succeed_expectFutureToHaveRecovered() {
        Future<String> resultF = future.recover( (r,f) -> {recordCallbackCall(); return "recovered";} );

        assertResult( "recovered", resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void withDescriptionThenRecover_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .recover( (r,f) -> {recordCallbackCall(); return "recovered";} );

        assertResult( "recovered", resultF );
        assertCallbackWasCalledFromCurrentThread();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void recover_threwException_expectChainedFailure() {
        Exception      ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.recover( (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex,initialFailure), resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void givenRequestHasFinished_recover_expectCallbackToNotBeCalledA() {
        request.markCompleted();

        TimeoutFailure expectation = new TimeoutFailure( request, future, initialFailure );
        Exception      ex          = new IllegalArgumentException("ex");
        Future<String> resultF     = future.recover( (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }


// public Future<T> flatRecover( Function1<Failure, Future<T>> recoveryFunction );

    @Test
    public void flatRecover_succeed_expectRecoveredResult() {
        Future<String> resultF = future.flatRecover( (r,f) -> {recordCallbackCall(); return Future.successful(request,"recovered");} );

        assertResult( "recovered", resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void withDescriptionThenFlatRecover_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .flatRecover( (r,f) -> {recordCallbackCall(); return Future.successful(request,"recovered");} );

        assertResult( "recovered", resultF );
        assertCallbackWasCalledFromCurrentThread();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void flatRecover_fail_expectReplacement() {
        Failure expectation = new MockFailure("replacement");
        Future<String> resultF     = future.flatRecover( (r,f) -> {recordCallbackCall(); return Future.failure(request,expectation);} );

        assertFailure( expectation, resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void flatRecover_threwException_expectChainedFailure() {
        Exception      ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.flatRecover( (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex,initialFailure), resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void givenRequestHasFinished_flatRecover_expectCallbackToNotBeCalledAndTimeoutToBeIssued() {
        request.markCompleted();

        TimeoutFailure expectation = new TimeoutFailure( request, future, initialFailure );
        Exception      ex          = new IllegalArgumentException("ex");
        Future<String> resultF     = future.flatRecover( (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }


// public Future<T> mapFailure( Function1<Failure, Failure> mappingFunction );

    @Test
    public void mapFailure_success_expectReplacedFailure() {
        Failure        expectation = new MockFailure("replacement");
        Future<String> resultF     = future.mapFailure( (r,f) -> {recordCallbackCall(); return expectation;} );

        assertFailure( expectation, resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void withDescriptionThenMapFailure_expectDescriptionToCarryOver() {
        Failure        expectation = new MockFailure("replacement");
        Future<String> resultF     = future.withDescription( "abc" )
            .mapFailure( (r,f) -> {recordCallbackCall(); return expectation;} );

        assertFailure( expectation, resultF );
        assertCallbackWasCalledFromCurrentThread();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void mapFailure_threwException_expectChainedFailure() {
        Exception      ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.mapFailure( (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex,initialFailure), resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void givenRequestHasFinished_mapFailure_expectCallbackToNotBeCalledAndTimeoutToBeIssued() {
        request.markCompleted();

        TimeoutFailure expectation = new TimeoutFailure( request, future, initialFailure );
        Exception      ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.mapFailure( (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }


// public Future<T> flatMapFailure( Function1<Failure, Failure> mappingFunction );

    @Test
    public void flatMapFailure_returnsFailure_expectReplacedFailure() {
        Failure        expectation = new MockFailure("replacement");
        Future<String> resultF     = future.flatMapFailure( (r,f) -> {recordCallbackCall(); return Future.failure(request,expectation);} );

        assertFailure( expectation, resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void withDescriptionThenFlatMapFailure_expectDescriptionToCarryOver() {
        Failure        expectation = new MockFailure("replacement");
        Future<String> resultF     = future.withDescription( "abc" )
            .flatMapFailure( (r,f) -> {recordCallbackCall(); return Future.failure(request,expectation);} );

        assertFailure( expectation, resultF );
        assertCallbackWasCalledFromCurrentThread();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void flatMapFailure_throwsException_expectChainedException() {
        Exception      ex      = new IllegalArgumentException("ex");;
        Future<String> resultF = future.flatMapFailure( (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex,initialFailure), resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void givenRequestHasFinished_flatMapFailure_expectCallbackToNotBeCalledAndTimeoutToBeIssued() {
        request.markCompleted();

        TimeoutFailure expectation = new TimeoutFailure( request, future, initialFailure );
        Exception      ex          = new IllegalArgumentException("ex");;
        Future<String> resultF     = future.flatMapFailure( (r,f) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }


// public Future<T> onSuccess( VoidFunction1<T> callback );

    @Test
    public void onSuccess_expectCallbackWasCalled() {
        Future<String> resultF = future.onSuccess( (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenOnSuccess_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .onSuccess( (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void givenRequestHasFinished_onSuccess_expectCallbackWasCalled() {
        Future<String> resultF = future.onSuccess( (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasNotCalled();
    }

// public Future<T> onFailure( VoidFunction1<Failure> callback );

    @Test
    public void onFailure_expectCallbackWasCalled() {
        Future<String> resultF = future.onFailure( (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void withDescriptionThenOnFailure_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .onFailure( (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasCalledFromCurrentThread();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void onFailureThrowsException_expectErrorToBeLogged() {
        Exception       ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.onFailure( (r,f) -> {recordCallbackCall(); throwException(ex);} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasCalledFromCurrentThread();

        capturingLogWriter.assertContainsMessage( "A future's side effect threw the following exception: java.lang.IllegalArgumentException: ex" );
    }

    @Test
    public void givenRequestHasFinished_onFailure_expectCallbackWasCalled() {
        request.markCompleted();

        Future<String> resultF = future.onFailure( (r,f) -> recordCallbackCall() );

        assertFailure( initialFailure, resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void givenRequestHasFinished_onFailureThrowsException_expectErrorToBeLogged() {
        request.markCompleted();

        Exception       ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.onFailure( (r,f) -> {recordCallbackCall(); throwException(ex);} );

        assertFailure( initialFailure, resultF );
        assertCallbackWasCalledFromCurrentThread();

        capturingLogWriter.assertContainsMessage( "A future's side effect threw the following exception: java.lang.IllegalArgumentException: ex" );
    }

// public default Future<T> onCompletion( VoidFunction1<T> onSuccess, VoidFunction1<Failure> onFailure ) {

    @Test
    public void onCompletion_expectOnFailureToBeInvoked() {
        AtomicReference<String>  flag1 = new AtomicReference<>();
        AtomicReference<Failure> flag2 = new AtomicReference<>();

        future.onCompletion( (r,v) -> flag1.set(v), (r,f) -> flag2.set(f) );

        assertNull( flag1.get() );
        assertEquals( initialFailure, flag2.get() );
    }

    @Test
    public void withDescriptionThenOnCompletion_expectDescriptionToCarryOver() {
        AtomicReference<String>  flag1 = new AtomicReference<>();
        AtomicReference<Failure> flag2 = new AtomicReference<>();

        Future<String> resultF = future.withDescription( "abc" ).onCompletion( (r,v) -> flag1.set(v), (r,f) -> flag2.set(f) );

        assertNull( flag1.get() );
        assertEquals( initialFailure, flag2.get() );
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void requestHasFinished_onCompletion_expectOnFailureToBeInvoked() {
        request.markCompleted();

        AtomicReference<String>  flag1 = new AtomicReference<>();
        AtomicReference<Failure> flag2 = new AtomicReference<>();

        future.onCompletion( (r,v) -> flag1.set(v), (r,f) -> flag2.set(f) );

        assertNull( flag1.get() );
        assertEquals( initialFailure, flag2.get() );
    }

// public default Future<T> onCompletion( Executor executor, VoidFunction1<T> onSuccess, VoidFunction1<Failure> onFailure );

    @Test
    public void onCompletionWithExecutor_expectOnSuccessToBeInvoked() {
        AtomicReference<String>  flag1 = new AtomicReference<>();
        AtomicReference<Failure> flag2 = new AtomicReference<>();

        future.onCompletion( UseCallingThreadExecutor.INSTANCE, (r,v) -> flag1.set(v), (r,f) -> flag2.set(f) );

        assertNull( flag1.get() );
        assertEquals( initialFailure, flag2.get() );
    }


    @Test
    public void equalsContract() {
        if ( type != Promise.class ) {
            EqualsVerifier.forClass( type )
                .suppress( Warning.STRICT_INHERITANCE )
                .withPrefabValues( Request.class, request, env.createRequest("equals-verifier") )
                .verify();
        }
    }

    @Test
    public void testToString() {
        assertEquals( "FailedFuture(MockFailure(msg=initial failure))", future.toString() );
    }

}
