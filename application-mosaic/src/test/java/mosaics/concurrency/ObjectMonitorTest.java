package mosaics.concurrency;

import mosaics.concurrency.ObjectMonitor;
import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.lang.Backdoor;
import mosaics.lang.TryUtils;
import mosaics.lang.threads.ThreadUtils;
import mosaics.lang.time.DTM;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class ObjectMonitorTest {
    private ObjectMonitor monitor = ObjectMonitor.createObjectMonitor();
    private Env           env     = Env.createTestEnv(new DTM(2020,3,1));
    private Request       req     = new Request(env);

    
    @Test
    public void sleepFor5ms() throws InterruptedException {
        long startMillis = System.currentTimeMillis();

        monitor.sleep( req, 5 );

        assertTrue( System.currentTimeMillis() >= startMillis + 5 );
    }

    @Test
    public void waitFor5ms() throws InterruptedException {
        long startMillis = System.currentTimeMillis();

        monitor.wait( req, 5 );

        assertTrue( System.currentTimeMillis() >= startMillis + 5 );
    }

    @Test
    public void notifyWillWakeWaitEarly() throws InterruptedException {
        long startMillis = System.currentTimeMillis();

        new Thread(() -> {
            Backdoor.sleep(100); monitor.notify(req);} ).start();

        monitor.wait( req, 10_000 );

        assertTrue( System.currentTimeMillis() < startMillis + 10_000 );
    }

    @Test
    public void notifyAllWillWakeMultipleWaitEarly() throws InterruptedException {
        long startMillis = System.currentTimeMillis();
        int  numThreads  = 5;

        CountDownLatch startLatch    = new CountDownLatch( numThreads );
        CountDownLatch finishedLatch = new CountDownLatch( numThreads );

        for ( int i=0; i<numThreads; i++ ) {
            new Thread( () -> {
                startLatch.countDown();
                TryUtils.invokeAndRethrowException( () -> monitor.wait( req, 10_000 ) );
                finishedLatch.countDown();
            } ).start();
        }

        startLatch.await( 10, TimeUnit.SECONDS );

        Backdoor.sleep( 100 ); // give the last thread that called startLatch a bit of time to call wait before continuing
        monitor.notifyAll( req );

        finishedLatch.await( 10, TimeUnit.SECONDS );

        assertTrue( System.currentTimeMillis() < startMillis + 10_000 );
    }

    @Test
    public void mockNotify() {
        ObjectMonitor  monitor = ObjectMonitor.createObjectMonitorFor( env.getClock() );

        AtomicBoolean flag = new AtomicBoolean();

        new Thread(() -> {
            TryUtils.invokeAndRethrowException( () -> monitor.wait( req, 10_000 ) );
            flag.set( true );
        }).start();

        monitor.notify( new Request(env) );

        ThreadUtils.spinUntilTrue( flag::get );
    }

}
