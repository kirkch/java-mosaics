package mosaics.concurrency;


import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.junit.JMThreadExtension;
import mosaics.lang.Backdoor;
import mosaics.lang.threads.STWCounter;
import mosaics.lang.threads.STWDetector;
import mosaics.logging.writers.CapturingLogWriter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Isolated;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@Isolated
@SuppressWarnings("Duplicates")
@ExtendWith( JMThreadExtension.class )
@JMThreadExtension.IgnoreThread(value="Attach Listener")
public class WorkerThreadTest {

    private CapturingLogWriter capturingLogWriter = new CapturingLogWriter();
    private Env                env                = Env.createTestEnv( capturingLogWriter );
    private Request            request            = env.createRequest( "junit" );

    private STWDetector        stwDetector        = new STWDetector( "WorkerThreadTest" );

    @BeforeEach
    public void setup() {
        stwDetector.start();
    }

    @AfterEach
    public void tearDown() {
        stwDetector.stop();
    }


    @Test
    public void startWorkerThread_expectItToInvokeTheJobFunctionInANewThread() throws InterruptedException {
        AtomicReference<Thread> threadCapture = new AtomicReference<>();
        CountDownLatch latch = new CountDownLatch( 1 );

        WorkerThread workerThread = new WorkerThread(env, "junit", () -> {
            threadCapture.set( Thread.currentThread() );
            latch.countDown();

            return 0;
        }).start();

        try {
            latch.await( 10, TimeUnit.SECONDS );

            assertEquals( "junit", threadCapture.get().getName() );
            assertFalse( threadCapture.get().isDaemon() );
        } finally {
            workerThread.stop();
        }
    }

    @Test
    public void startWorkerThreadAsDaemon_ensureThatItIsADaemonThread() throws InterruptedException {
        AtomicReference<Thread> threadCapture = new AtomicReference<>();
        CountDownLatch latch = new CountDownLatch( 1 );

        WorkerThread workerThread = new WorkerThread(env, "junit", () -> {
            threadCapture.set( Thread.currentThread() );
            latch.countDown();

            return 0;
        }).asDaemon().start();

        try {
            latch.await( 10, TimeUnit.SECONDS );

            assertTrue( threadCapture.get().isDaemon() );
        } finally {
            workerThread.stop();
        }
    }

    @Test
    public void jobReturns100_expectSleepToBeCalledFor100() throws InterruptedException {
        long                        targetDelayMillis = 100;
        long                        toleranceMillis   = 100;
        AtomicLong                  startMillisRef    = new AtomicLong();
        AtomicReference<STWCounter> stwCounterRef     = new AtomicReference<>();
        AtomicLong                  repCounter        = new AtomicLong();
        CountDownLatch              latch             = new CountDownLatch( 1 );

        WorkerThread workerThread = new WorkerThread(env, "junit", () -> {
            long rep = repCounter.getAndIncrement();

            if ( rep == 0 ) {
                startMillisRef.set( System.currentTimeMillis() );
                stwCounterRef.set( stwDetector.stwCounter() );
            } else if ( rep == 1 ) {
                latch.countDown();
            }

            return targetDelayMillis;
        }).asDaemon().start();

        try {
            latch.await( 10, TimeUnit.SECONDS );

            long stwDelay = stwCounterRef.get().getSTWTotal();
            long delay   = System.currentTimeMillis() - startMillisRef.get() - stwDelay;

            String msg = delay + "ms delay (expected " + targetDelayMillis + ")";
            assertTrue( delay >= targetDelayMillis, msg );
            assertTrue( delay < targetDelayMillis+toleranceMillis, msg );
        } finally {
            workerThread.stop();
        }
    }

    @Test
    public void jobReturns1000_butMaxIsSetTo10_expectSleepToBeCalledFor10() throws InterruptedException {
        long                        targetDelayMillis = 10;
        long                        toleranceMillis   = 100;
        AtomicLong                  startMillisRef    = new AtomicLong();
        AtomicReference<STWCounter> stwCounterRef     = new AtomicReference<>();
        AtomicLong                  repCounter        = new AtomicLong();
        CountDownLatch              latch             = new CountDownLatch( 1 );

        WorkerThread workerThread = new WorkerThread(env, "junit", () -> {
            long rep = repCounter.getAndIncrement();

            if ( rep == 0 ) {
                startMillisRef.set( System.currentTimeMillis() );
                stwCounterRef.set( stwDetector.stwCounter() );
            } else if ( rep == 1 ) {
                latch.countDown();
            }

            return 10_000;
        }).asDaemon().withMaxIntervalBetweenPolls(targetDelayMillis).start();

        try {
            latch.await( 10, TimeUnit.SECONDS );

            long stwDelay = stwCounterRef.get().getSTWTotal();
            long delay = System.currentTimeMillis() - startMillisRef.get() - stwDelay;

            String msg = delay + "ms delay (expected " + targetDelayMillis + ")";
            assertTrue( delay >= targetDelayMillis, msg );
            assertTrue( delay < targetDelayMillis+toleranceMillis, msg );
        } finally {
            workerThread.stop();
        }
    }

    @Test
    public void jobReturns10_expectSleepToBeCalledFor100() throws InterruptedException {
        long                        targetDelayMillis = 10;
        long                        toleranceMillis   = 100;
        AtomicLong                  startMillisRef    = new AtomicLong();
        AtomicReference<STWCounter> stwCounterRef     = new AtomicReference<>();
        AtomicLong                  repCounter        = new AtomicLong();
        CountDownLatch              latch             = new CountDownLatch( 1 );

        WorkerThread workerThread = new WorkerThread(env, "junit", () -> {
            long rep = repCounter.getAndIncrement();

            if ( rep == 0 ) {
                startMillisRef.set( System.currentTimeMillis() );
                stwCounterRef.set( stwDetector.stwCounter() );

            } else if ( rep == 1 ) {
                latch.countDown();

                return WorkerThread.EXIT;
            }

            return targetDelayMillis;
        }).asDaemon().start();

        try {
            latch.await( 10, TimeUnit.SECONDS );

            long stwDelay = stwCounterRef.get().getSTWTotal();
            long delay = System.currentTimeMillis() - startMillisRef.get() - stwDelay;

            String msg = delay + "ms delay (expected " + targetDelayMillis + ")";
            assertTrue( delay >= targetDelayMillis, msg );
            assertTrue( delay < targetDelayMillis+toleranceMillis, msg );

            assertEquals(2, repCounter.get());
        } finally {
            workerThread.stop();
        }
    }

    @Test
    public void jobSleepsFor10s_wakeItUpEarly() throws InterruptedException {
        long                        toleranceMillis   = 200;
        AtomicLong                  startMillisRef    = new AtomicLong();
        AtomicReference<STWCounter> stwCounterRef     = new AtomicReference<>();
        AtomicLong                  repCounter        = new AtomicLong();
        CountDownLatch              rep1Latch         = new CountDownLatch( 1 );
        CountDownLatch              rep2Latch         = new CountDownLatch( 1 );

        WorkerThread workerThread = new WorkerThread(env, "junit", () -> {
            long rep = repCounter.incrementAndGet();

            if ( rep == 1 ) {
                startMillisRef.set( System.currentTimeMillis() );
                stwCounterRef.set( stwDetector.stwCounter() );
                rep1Latch.countDown();

                return 10_000;
            } else if ( rep == 2 ) {
                rep2Latch.countDown();
            }

            return 10;
        }).start();

        try {
            rep1Latch.await( 10, TimeUnit.SECONDS );

            assertEquals(1, repCounter.get());

            Backdoor.sleep( 100 ); // give the worker thread a chance to go to sleep

            workerThread.notifyWorkerThread( request );

            rep2Latch.await( 10, TimeUnit.SECONDS );
            assertEquals(2, repCounter.get());


            long stwDelay = stwCounterRef.get().getSTWTotal();
            long delay = System.currentTimeMillis() - startMillisRef.get() - stwDelay;

            String msg = delay + "ms delay";
            assertTrue( delay < toleranceMillis, msg );
        } finally {
            workerThread.stop();
        }
    }

    @Test
    public void jobErrors_expectLogMessageAndWorkerThreadToContinueRegardless() throws InterruptedException {
        AtomicLong                  repCounter        = new AtomicLong();
        CountDownLatch              latch             = new CountDownLatch( 1 );

        RuntimeException ex = new IllegalStateException( "junit" );
        WorkerThread workerThread = new WorkerThread(env, "junit", () -> {
            long rep = repCounter.incrementAndGet();

            if ( rep == 1 ) {
                throw ex;
            } else if ( rep == 2 ) {
                latch.countDown();
            }

            return 100_000;
        }).asDaemon().withMaxIntervalBetweenPolls(10).start();

        try {
            latch.await( 10, TimeUnit.SECONDS );

//            List<Event> expectedLogMessages = Collections.singletonList(
//                new ServiceErroredEvent( "junit", ex )
//            );
//
//            assertEquals( expectedLogMessages, capturingLogWriter.getLoggedMessages(ServiceErroredEvent.class) );
            assert( repCounter.get() >= 2 );
        } finally {
            workerThread.notifyWorkerThread( request );
            workerThread.stop();
        }
    }

}
