package mosaics.cli.meta;

import mosaics.fp.FP;
import mosaics.lang.reflection.JavaClass;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class AppMetaTest {
    @Test
    public void equalsContract() {
        EqualsVerifier.forClass( AppMeta.class )
            .withPrefabValues(
                AppMeta.class,
                new AppMetaReflectionFactory().createAppMetaFrom(AppMetaTest.class,FP.option(AppMetaTest.class)),
                new AppMetaReflectionFactory().createAppMetaFrom(AppHelpLogMessageTest.class,FP.option(AppHelpLogMessageTest.class))
            )
            .withPrefabValues( JavaClass.class, JavaClass.of(AppMetaTest.class), JavaClass.of(AppHelpLogMessageTest.class) )
            .verify();
    }

    @Test
    public void testToString() {
        assertEquals( "AppMetaTest", new AppMetaReflectionFactory().createAppMetaFrom(AppMetaTest.class,FP.option(AppMetaTest.class)).toString() );
    }
}
