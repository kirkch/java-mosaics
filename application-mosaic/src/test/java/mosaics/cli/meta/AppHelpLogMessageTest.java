package mosaics.cli.meta;

import mosaics.cli.CommandLineArg;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.time.DTM;
import mosaics.logging.LogMessage;
import mosaics.logging.LogSeverity;
import mosaics.logging.TargetAudience;
import mosaics.strings.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class AppHelpLogMessageTest {
    private static final DTM T0 = new DTM(2020,3,20,14,30);


    @Test
    public void testGetTargetAudience() {
        LogMessage msg = AppHelpLogMessage.createHelpMessage(T0,createAppMetaFor(NoArgApp.class));

        assertEquals( TargetAudience.USER, msg.getTargetAudience() );
    }

    @Test
    public void testGetLogSeverity() {
        LogMessage msg = AppHelpLogMessage.createHelpMessage(T0,createAppMetaFor(NoArgApp.class));

        assertEquals( LogSeverity.INFO, msg.getSeverity() );
    }

    @Test
    public void givenNoArgAppWithNoDefinedHelp_invokeHelp_expectTheAppToNotRunAndTheDefaultHelpToBeDisplayed() {
        assertAppHelp( NoArgApp.class );
    }

    @Test
    public void appWithClassDocumentation() {
        assertAppHelp(
            HasClassDocumentationApp.class,
            Collections.singletonList("This is a test application."),
            new ArrayList<>()
        );
    }

    @Test
    public void appWithFlagDocumentation() {
        assertAppHelp(
            HasFlagDocumentationApp.class,
            "arg1 arg2",
            Collections.singletonList("This is a test application."),
            Arrays.asList(
                "    arg1",
                "        this is arg 1",
                "",
                "    arg2",
                "        this is arg 2"
            ),
            Arrays.asList(
                "    --flag1",
                "        this is flag 1",
                "    --flag2",
                "    --flag3, -c",
                "        this is flag 3"
            )
        );
    }

    @Test
    public void appWithOptionalRunMethodArg() {
        assertAppHelp(
            HasOptionalArgOnRunMethod.class,
            "arg1 [arg2]",
            Collections.singletonList("This is a test application."),
            Arrays.asList(
                "    arg1",
                "        this is arg 1",
                "",
                "    arg2",
                "        this is arg 2"
            ),
            Collections.emptyList()
        );
    }

    @Test
    public void appWithOptionRunMethodArg() {
        assertAppHelp(
            HasOptionArgOnRunMethod.class,
            "arg1 [arg2]",
            Collections.singletonList("This is a test application."),
            Arrays.asList(
                "    arg1",
                "        this is arg 1",
                "",
                "    arg2",
                "        this is arg 2"
            ),
            Collections.emptyList()
        );
    }

    @Test
    public void appWithOutOfOrderOptionalArg_ExpectError() {
        try {
            createAppMetaFor(HasOptionalArgOutOfOrder.class);
            fail( "expected exception" );
        } catch ( IllegalArgumentException ex ) {
            assertEquals( "'HasOptionalArgOutOfOrder' is not valid.  The run method has mandatory arg 'name' after an optional arg.  All optional args must be declared last, order matters here.", ex.getMessage() );
        }
    }

    @Test
    public void appWithOutOfOrderOptionArg_ExpectError() {
        try {
            createAppMetaFor(HasOptionArgOutOfOrder.class);
            fail( "expected exception" );
        } catch ( IllegalArgumentException ex ) {
            assertEquals( "'HasOptionArgOutOfOrder' is not valid.  The run method has mandatory arg 'name' after an optional arg.  All optional args must be declared last, order matters here.", ex.getMessage() );
        }
    }


// ENUM HANDLING

    @Test
    public void appWithEnumArg() {
        assertAppHelp(
            HasEnumRunMethodApp.class,
            "arg1 arg2",
            Collections.singletonList("This is a test application."),
            Arrays.asList(
                "    arg1",
                "        this is arg 1",
                "",
                "    arg2 (OP1|OP2|OP3)",
                "        this is arg 2"
            ),
            Collections.emptyList()
        );
    }

    @Test
    public void appWithEnumFlag() {
        assertAppHelp(
            HasEnumFlagApp.class,
            Collections.singletonList("This is a test application."),
            Arrays.asList(
                "    --flag = OP1|OP2|OP3"
            )
        );
    }


// PARAMETERS LOADED INTO A COMPOSITE OBJECT

    @Test
    public void givenAppThatHasCompositeArg_ensureThatTheDocumentationIncludesTheArgsFromTheCompositeObject() {
        assertAppHelp(
            HasCompositeArgRunMethod.class,
            Collections.emptyList(),
            Arrays.asList(
                "    --args.arg1",
                "        A trusty String arg.",
                "    --args.arg2",
                "        A trusty boolean arg.",
                "    --args.arg3",
                "        A trusty integer arg.",
                "    --args.arg4",
                "        A trusty float arg.",
                "    --args.arg5",
                "        A trusty character arg."
            )
        );
    }

    @Test
    public void givenAppThatHasCompositeArgWithShortNames_ensureThatTheDocumentationIncludesTheArgsFromTheCompositeObject() {
        assertAppHelp(
            HasCompositeArgWithShortName.class,
            Collections.emptyList(),
            Arrays.asList(
                "    --args.arg1, -a",
                "        A trusty String arg."
            )
        );
    }

    @Test
    public void givenAppThatHasCompositeArgWithRemappedLongNames_ensureThatTheDocumentationIncludesTheArgsFromTheCompositeObject() {
        assertAppHelp(
            HasCompositeArgWithRemappedLongName.class,
            Collections.emptyList(),
            Arrays.asList(
                "    --name, -a",
                "        A trusty String arg."
            )
        );
    }


// DEFAULT VALUES IN HELP

    @Test
    public void appWithDefaultValues() {
        assertAppHelp(
            HasDefaultValuesApp.class,
            "",
            Collections.emptyList(),
            Collections.emptyList(),
            Arrays.asList(
                "    --flag1  (default=abc)",
                "    --flag2  (default=true)",
                "    --flag3"
            )
        );
    }


    private AppMeta createAppMetaFor(Class c) {
        return new AppMetaReflectionFactory().createAppMetaFrom( c, FP.option(c) );
    }

    private void assertAppHelp( Class<?> appClass ) {
        assertAppHelp( appClass, null, new ArrayList<>(), new ArrayList<>(), new ArrayList<>() );
    }

    private void assertAppHelp( Class<?> appClass, List<String> appDoc, List<String> appFlags ) {
        assertAppHelp( appClass, null, appDoc, new ArrayList<>(), appFlags );
    }

    private void assertAppHelp( Class<?> appClass, String argNames, List<String> appDoc, List<String> appArgs, List<String> appFlags ) {
        List<String> expectedLines = new ArrayList<>();

        expectedLines.add("#static");
        if ( appDoc.size() > 0 ) {
            expectedLines.addAll( appDoc );

            expectedLines.add( "" );
        }

        if ( StringUtils.isBlank(argNames) ) {
            expectedLines.add("Usage: " + appClass.getTypeName());
        } else {
            expectedLines.add("Usage: " + appClass.getTypeName() + " " + argNames);
        }
        expectedLines.add( "" );

        if ( appArgs.size() > 0 ) {
            expectedLines.addAll( appArgs );

            expectedLines.add( "" );
        }

        if ( appFlags.size() > 0 ) {
            expectedLines.addAll( appFlags );
        }

        expectedLines.add( "" );
        expectedLines.add( "    --help, -?" );
        expectedLines.add( "        Display this help summary." );
        expectedLines.add( "    --debug, -d" );
        expectedLines.add( "        Enable debug logging, which will display all fine grained logging suitable for developers." );
        expectedLines.add( "    --verbose, -v" );
        expectedLines.add( "        Enable verbose logging, which will display important usage events suitable for operational monitoring." );
        expectedLines.add( "    --quiet, -q" );
        expectedLines.add( "        Only errors will be logged." );
        expectedLines.add( "    --silent, -s" );
        expectedLines.add( "        Disable all logging." );
        expectedLines.add("    --logFormat (default=TEXT)");
        expectedLines.add("        Format the logged events either TEXT or JSON.");
        expectedLines.add( "    --config=<url>" );
        expectedLines.add( "        Specify where to load the configuration file from.  The URL supports file://<absolute path>, and" );
        expectedLines.add( "        classpath:<absolute path>.  The contents of the config file matches the same arguments described" );
        expectedLines.add( "        above.  The config file can hold an argument that may be specified on the command line, stored." );
        expectedLines.add( "        as a Java properties file.  For example to specify the verbose flag within the config file" );
        expectedLines.add( "        add the following line into the properties file 'verbose=true'." );
        expectedLines.add( "#endstatic" );


        String actualLines = renderDocsFor(appClass);
        assertEquals( expectedLines.stream().collect( Collectors.joining(Backdoor.NEWLINE) ), actualLines );
    }

    private String renderDocsFor( Class<?> appClass ) {
        AppMeta    appMeta = createAppMetaFor(appClass);
        LogMessage msg     = AppHelpLogMessage.createHelpMessage(T0,appMeta);

        return msg.getMessageTemplate();
//            .replaceFirst( "#endstatic", "" )
//            .replaceFirst( "#static", "" );
    }


    @SuppressWarnings("unused")
    public static class NoArgApp {
//        private final Env system;

        private boolean didRun = false;

//        public NoArgApp( Env system ) {
//            this.system = system;
//        }

        public void run() {
            this.didRun = true;
        }
    }

//    public static class OneArgApp {
//        private final Env system;
//
//        private String arg1;
//
//        public OneArgApp( Env system ) {
//            this.system = system;
//        }
//
//        public void run( String arg1 ) {
//            this.arg1 = arg1;
//        }
//    }
//
//    public static class NoSystemConstructorApp {
//        private boolean didRun = false;
//
//        public void run() {this.didRun = true;}
//    }
//
    /**
     * This is a test application.
     */
    public static class HasOptionArgOnRunMethod {
        /**
         *
         * @param arg1 this is arg 1
         * @param arg2 this is arg 2
         */
        public void run( String arg1, FPOption<Boolean> arg2) {

        }
    }

    /**
     * This is a test application.
     */
    public static class HasClassDocumentationApp {
        public void run() {}
    }

    /**
     * This is a test application.
     */
    public static class HasFlagDocumentationApp {
        /**
         * this is flag 1
         */
        public String  flag1;

        public boolean flag2;

        /**
         * this is flag 3
         */
        @CommandLineArg(shortName='c')
        public boolean flag3;

        /**
         *
         * @param arg1 this is arg 1
         * @param arg2 this is arg 2
         */
        public void run( String arg1, boolean arg2) {
        }
    }


    /**
     * This is a test application.
     */
    public static class HasOptionalArgOnRunMethod {
        /**
         *
         * @param arg1 this is arg 1
         * @param arg2 this is arg 2
         */
        public void run( String arg1, Optional<Boolean> arg2) {
        }
    }

    public static enum AppEnum {
        OP1, OP2, OP3
    }


    public static class HasOptionalArgOutOfOrder {
        public void run( Optional<Boolean> flag, String name ) {
        }
    }

    public static class HasOptionArgOutOfOrder {
        public void run( FPOption<Boolean> flag, String name ) {
        }
    }

//    public static class HasVoidReturnMethod {
//        public HasVoidReturnMethod( Env system ) {}
//
//        public void run() {
//        }
//    }
//
//    public static class HasIntReturnMethod {
//        private int returnValue;
//
//        public HasIntReturnMethod( Env system ) {}
//
//        public int run() {
//            return returnValue;
//        }
//    }
//
//    public static class HasFlags {
//        public String  flag1;
//        public boolean flag2;
//
//        @CommandLineArg(shortName='c')
//        public boolean flag3;
//
//        public HasFlags( Env system ) {}
//
//        public void run() {}
//    }
//
//    public static class HasBooleanFlag {
//        @CommandLineArg(shortName='c')
//        public boolean flag2;
//
//        public HasBooleanFlag( Env system ) {}
//
//        public void run() {}
//    }
//
//    public static class HasThreeBooleanFlags {
//        @CommandLineArg(shortName='a')
//        public boolean flag1;
//
//        @CommandLineArg(shortName='b')
//        public boolean flag2;
//
//        @CommandLineArg(shortName='c')
//        public boolean flag3;
//
//        public HasThreeBooleanFlags( Env system ) {}
//
//        public void run() {}
//    }
//
//    public static class HasMultiArgRunMethod {
//        public HasMultiArgRunMethod( Env system ) {}
//
//        private String  arg1;
//        private boolean arg2;
//        private int     arg3;
//        private float   arg4;
//        private char    arg5;
//
//        public void run( String arg1, boolean arg2, int arg3, float arg4, char arg5 ) {
//            this.arg1 = arg1;
//            this.arg2 = arg2;
//            this.arg3 = arg3;
//            this.arg4 = arg4;
//            this.arg5 = arg5;
//        }
//    }

    public static class HasCompositeArgRunMethod {
        public static class MyComposite {
            /**
             * A trusty String arg.
             */
            public String  arg1;

            /**
             * A trusty boolean arg.
             */
            public boolean arg2;

            /**
             * A trusty integer arg.
             */
            public int     arg3;

            /**
             * A trusty float arg.
             */
            public float   arg4;

            /**
             * A trusty character arg.
             */
            public char    arg5;
        }

        public MyComposite args;

        public void run() {}
    }

    public static class HasCompositeArgWithShortName {
        public static class MyComposite {
            /**
             * A trusty String arg.
             */
            @CommandLineArg(shortName='a')
            public String  arg1;
        }

        public MyComposite args;

        public void run() {}
    }

    public static class HasCompositeArgWithRemappedLongName {
        public static class MyComposite {
            /**
             * A trusty String arg.
             */
            @CommandLineArg(shortName='a', longName="name")
            public String  arg1;
        }

        public MyComposite args;

        public void run() {}
    }
//
//    public static class HasCompositeArgWithRemappedBaseName {
//        public static class MyOwnComposite {
//            public String arg1;
//        }
//
//        @CommandLineArg(longName="config")
//        public MyOwnComposite args;
//
//        public HasCompositeArgWithRemappedBaseName( Env system ) {}
//
//        public void run() {}
//    }

    public static class HasCompositeArgWithArgNamePostfixedWithConfig {
        public static class MyOwnComposite {
            public String arg1;
        }

        public MyOwnComposite argsConfig;

        public void run() {}
    }

//    public static class HasAppEnvOnRunMethod {
//        private Env env;
//
//        public void run( Env env ) {
//            this.env = env;
//        }
//    }
//
//    public static class HasIntArgOnRunMethod {
//        public HasIntArgOnRunMethod( Env system ) {}
//
//        private int intArg;
//
//        public void run( int intArg ) {
//            this.intArg = intArg;
//        }
//    }
//
//    public static class HasMultipleFlags {
//        public HasMultipleFlags( Env system ) {}
//
//        public String  stringFlag;
//        public boolean booleanFlag;
//        public int     intFlag;
//        public float   floatFlag;
//        public char    charFlag;
//
//        public void run() {}
//    }
//
//
//    public static class UnknownType {}
//
//    public static class HasFlagOfUnknownType {
//        public HasFlagOfUnknownType( Env system ) {}
//
//        public UnknownType unknownFlag;
//
//        public void run() {}
//    }
//
//    public static class HasArgOfUnknownType {
//        public HasArgOfUnknownType( Env system ) {}
//
//        public void run( UnknownType unknownArg ) {}
//    }
//
//    public static class HasRunMethodThatReturnsString {
//        public HasRunMethodThatReturnsString( Env system ) {}
//
//        public String run() {
//            return null;
//        }
//    }
//
//    public static class HasNoRunMethods {
//        public HasNoRunMethods( Env system ) {}
//    }
//
//    public static class HasTwoRunMethods {
//        public HasTwoRunMethods( Env system ) {}
//
//        public void run() {}
//        public void run(int a) {}
//    }
//
//    public static class HasPassword {
//        public HasPassword( Env system ) {}
//
//        public Secret password;
//
//        public void run() {}
//    }


    public static class HasDefaultValuesApp {
        public String  flag1 = "abc";

        public boolean flag2 = true;
        public boolean flag3 = false;

        public void run() {
        }
    }

    /**
     * This is a test application.
     */
    public static class HasEnumRunMethodApp {

        private AppEnum arg2;

        /**
         *
         * @param arg1 this is arg 1
         * @param arg2 this is arg 2
         */
        public void run( String arg1, AppEnum arg2) {
            this.arg2 = arg2;
        }
    }

    /**
     * This is a test application.
     */
    public static class HasEnumFlagApp {
        public AppEnum flag;

        public void run( ) { }
    }
}
