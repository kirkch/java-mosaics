//package com.softwaremosaic.mosaics.cli.internals;
//
//import com.softwaremosaic.app.Env;
//import com.softwaremosaic.app.LiveEnv;
//import com.softwaremosaic.fp.collections.IterableX;
//import com.softwaremosaic.fp.collections.Option;
//import com.softwaremosaic.io.FileUtils;
//import com.softwaremosaic.io.files.Directory;
//import com.softwaremosaic.io.files.File;
//import com.softwaremosaic.io.files.disk.LocalDirectory;
//import com.softwaremosaic.io.files.disk.LocalFileSystem;
//import com.softwaremosaic.io.os.OSProcess;
//import com.softwaremosaic.io.os.ProcessRunner;
//import com.softwaremosaic.junit.JUnitMosaic;
//import com.softwaremosaic.lang.time.Duration;
//import com.softwaremosaic.mosaics.cli.App;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.io.BufferedReader;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.RandomAccessFile;
//import java.util.ConcurrentModificationException;
//import java.util.Vector;
//
//import static org.junit.Assert.*;
//
//
//public class CLApp_fileLockChildProcessTests {
//
//    private Env          env;
//    private java.io.File dataDir;
//    private java.io.File lockFile;
//
//    Vector<String> processOutput1 = new Vector<>();
//    Vector<String> processOutput2 = new Vector<>();
//
//
//    @Before
//    public void setup() throws IOException {
//        this.dataDir  = FileUtils.makeTempDirectory("CLApp_fileLockChildProcessTests", ".dataDir");
//        this.lockFile = new java.io.File(dataDir, "LOCK");
//
//        this.env = new LiveEnv();
//        env.getFileSystem().setCurrentWorkingDirectory(new LocalDirectory((LocalFileSystem) env.getFileSystem(), dataDir));
//
//        env.start();
//    }
//
//    @After
//    public void tearDown() {
//        Directory dir = env.getCurrentWorkingDirectory();
//        dir.deleteAll();
//
//        env.stop();
//
//
//        System.out.println("process 1");
//        System.out.println("=========");
//        processOutput1.forEach(System.out::println);
//        System.out.println(".");
//        System.out.println("process 2");
//        System.out.println("=========");
//        processOutput2.forEach(System.out::println);
//        System.out.println(".");
//    }
//
//    private void spinUntilLogContains( Vector<String> log, String targetString ) {
//        JUnitMosaic.spinUntilTrue(() -> {
//            try {
//                return IterableX.wrap(log).filter(str -> str.contains(targetString)).first().hasValue();
//            } catch ( ConcurrentModificationException ex ) {
//                return false;
//            }
//        });
//    }
//
//    private void assertLogDoesNotContain( Vector<String> log, String targetString ) {
//        try {
//            boolean r = IterableX.wrap(log).filter(str -> str.contains(targetString)).first().hasValue();
//
//            assertFalse( r );
//        } catch ( ConcurrentModificationException ex ) {
//            assertLogDoesNotContain( log, targetString );
//        }
//    }
//
//    @Test
//    public void startTwoAppsInTheirOwnProcesses_expectSecondOneToNotStartDueToLock() throws IOException {
//        OSProcess process1 = ProcessRunner.runJavaProcess(dataDir.getAbsolutePath(), WaitForSignalFileApp.class, processOutput1::add, dataDir.getAbsolutePath());
//        spinUntilLogContains(processOutput1, "App has started");
//
//        OSProcess process2 = ProcessRunner.runJavaProcess(dataDir.getAbsolutePath(), WaitForSignalFileApp.class, processOutput2::add, dataDir.getAbsolutePath(), "-d");
//
//        spinUntilLogContains(processOutput2, "Application is already running, only one instance is allowed at a time.");
//
//        process2.getPromise().spinUntilComplete(3000);
//
//        assertTrue(process2.getPromise().isComplete());
//
//
//        signalChildProcessesToStop(dataDir);
//        spinUntilProcessesHaveStopped(process1, process2);
//        assertEquals(0, process1.getPromise().getResultNoBlock().intValue());
//        assertEquals(1, process2.getPromise().getResultNoBlock().intValue());
//    }
//
//    @Test
//    public void startTwoAppsOneAfterTheOtherHasFinishedInTheirOwnProcesses_expectBothToStart() throws IOException {
//        OSProcess process1 = ProcessRunner.runJavaProcess( dataDir.getAbsolutePath(), WaitForSignalFileApp.class, processOutput1::add, dataDir.getAbsolutePath() );
//        spinUntilLogContains( processOutput1, "App has started" );
//
//        signalChildProcessesToStop(dataDir);
//        process1.getPromise().spinUntilComplete(3000);
//
//        removeSignalForChildProcessesToStop(dataDir);
//
//
//        OSProcess process2 = ProcessRunner.runJavaProcess( dataDir.getAbsolutePath(), WaitForSignalFileApp.class, processOutput2::add, dataDir.getAbsolutePath() );
//        spinUntilLogContains(processOutput2, "App has started");
//
//
//        signalChildProcessesToStop( dataDir );
//        spinUntilProcessesHaveStopped( process1, process2 );
//
//
//        assertEquals( 0, process1.getPromise().getResultNoBlock().intValue() );
//        assertEquals( 0, process2.getPromise().getResultNoBlock().intValue() );
//    }
//
//    @Test
//    public void startTwoAppsInTheirOwnProcesses_killTheFirstProcessBeforeStartingTheSecond_expectTheSecondToTryToRecoverWhereTheDefaultImplIsToAbortTheApp() throws IOException {
//        OSProcess process1 = ProcessRunner.runJavaProcess( dataDir.getAbsolutePath(), WaitForSignalFileApp.class, processOutput1::add, dataDir.getAbsolutePath(), "-v" );
//        spinUntilLogContains(processOutput1, "App has started");
//
//        process1.killImmediately();
//        process1.getPromise().spinUntilComplete(3000);
//
//
//        OSProcess process2 = ProcessRunner.runJavaProcess( dataDir.getAbsolutePath(), WaitForSignalFileApp.class, processOutput2::add, dataDir.getAbsolutePath(), "-v" );
//        spinUntilLogContains(processOutput2, "A previous run of the app did not clean up after itself, manual recovery required. Aborting..");
//
//
//        signalChildProcessesToStop(dataDir);
//        spinUntilProcessesHaveStopped(process1, process2);
//
//
//        assertEquals( 10, process2.getPromise().getResultNoBlock().intValue() );
//    }
//
//
//    @Test
//    public void startTwoAppsInTheirOwnProcesses_cleanlyShutdownTheFirstProcessViaCtrlCBeforeStartingTheSecond_expectTheSecondToStartNormally() throws IOException {
//        OSProcess process1 = ProcessRunner.runJavaProcess( dataDir.getAbsolutePath(), WaitForSignalFileApp.class, processOutput1::add, dataDir.getAbsolutePath(), "-v" );
//        spinUntilLogContains(processOutput1, "App has started");
//
//        // whether abort() cleanly kills, or abruptly kills is jvm implementation dependent.
//        // MacOSX usually does it cleanly but not always.. edge cases are irritating, thus this test is
//        // fragile...  options?
//        // considering adding two flags to junit.  1) an OnError handler for reporting the logs, and 2) permit
//        // a retry on error upto a limit.. usually this is bad, but for this test it may be acceptable
//        process1.abort();
//        process1.getPromise().spinUntilComplete(3002);
//
//
//        spinUntilLogContains(processOutput1, "Forced shutdown.");
//        JUnitMosaic.spinUntilTrue(3010, () -> !new java.io.File(dataDir, "LOCK").exists());
//
//
//        OSProcess process2 = ProcessRunner.runJavaProcess( dataDir.getAbsolutePath(), WaitForSignalFileApp.class, processOutput2::add, dataDir.getAbsolutePath(), "-v" );
//
//        spinUntilLogContains(processOutput2, "App has started");
//        assertLogDoesNotContain(processOutput2, "Previous run did not shutdown cleanly, recovering");
//
//
//        signalChildProcessesToStop(dataDir);
//        spinUntilProcessesHaveStopped(process1, process2);
//
//
//        assertEquals( 0, process2.getPromise().getResultNoBlock().intValue() );
//    }
//
//    @Test
//    public void whenTryingToRecoverAnAbortedLock_recoverApp_expectAppToStart() throws IOException {
//        OSProcess process1 = ProcessRunner.runJavaProcess( dataDir.getAbsolutePath(), WaitForSignalFileApp.class, processOutput1::add, dataDir.getAbsolutePath() );
//        spinUntilLogContains(processOutput1, "App has started");
//
//        process1.killImmediately();
//        process1.getPromise().spinUntilComplete(3000);
//
//
//        OSProcess process2 = ProcessRunner.runJavaProcess( dataDir.getAbsolutePath(), RecoveryApp.class, processOutput2::add, "-v" );
//        spinUntilLogContains( processOutput2,  "Previous run did not shutdown cleanly, recovering" );
//        spinUntilLogContains(processOutput2, "App ran");
//
//
//        signalChildProcessesToStop( dataDir );
//        spinUntilProcessesHaveStopped( process1, process2 );
//
//
//        assertEquals( 0, process2.getPromise().getResultNoBlock().intValue() );
//    }
//
//    @Test
//    public void pidInLockFile() throws IOException {
//        Vector<String> processOutput1 = new Vector<>();
//
//        OSProcess process1 = ProcessRunner.runJavaProcess( dataDir.getAbsolutePath(), WaitForSignalFileApp.class, processOutput1::add, dataDir.getAbsolutePath() );
//        spinUntilLogContains( processOutput1, "App has started" );
//
//
//        java.io.File lockFile = new java.io.File(dataDir,".LOCK");
//        BufferedReader in = new BufferedReader( new InputStreamReader(new FileInputStream(lockFile)) );
//
//        String contents = in.readLine();
//
//        assertEquals(Integer.toString(process1.getPid()), contents);
//
//        signalChildProcessesToStop( dataDir );
//        spinUntilProcessesHaveStopped( process1, process1 );
//
//
//        assertEquals( 0, process1.getPromise().getResultNoBlock().intValue() );
//    }
//
//
//
//    private void spinUntilProcessesHaveStopped( OSProcess process1, OSProcess process2 ) {
//        process1.getPromise().spinUntilComplete(3005);
//        process2.getPromise().spinUntilComplete( 3006 );
//    }
//
//    private void signalChildProcessesToStop( java.io.File dataDir ) throws IOException {
//        java.io.File doneFileF = new java.io.File( dataDir, "DONE" );
//        doneFileF.getParentFile().mkdirs();
//
//        RandomAccessFile doneFile = new RandomAccessFile( doneFileF, "rw" );
//
//        doneFile.close();
//
//        System.out.println("doneFileF = " + doneFileF);
//    }
//
//    private void removeSignalForChildProcessesToStop( java.io.File dataDir ) {
//        java.io.File doneFile = new java.io.File(dataDir,"DONE");
//
//        doneFile.delete();
//    }
//
//    public static class WaitForSignalFileApp extends App {
//
//        protected void setup( Env env ) throws Exception {
//            super.setup(env);
//
//            setUseLockFileFlag(env, ".LOCK");
//        }
//
////        private final CLArgument<DirectoryX> dataDir = getOrCreateDirectoryArgument( "dir", "data directory" );
//
//        public int run( Env env, String dataDir ) throws Exception {
//            try {
//                Directory dir = new LocalDirectory((LocalFileSystem) env.getFileSystem(), new java.io.File(dataDir));
//
//                env.stdout("dataDirectory=" + dir.getFullPath());
//                env.stdout(env.getFileSystem().getClass().getName());
//                env.stdout("App has started");
//
//                pollForContinuationFile( dir );
//
//                env.stdout( "Continuation file detected, exiting" );
//            } catch ( Throwable ex ) {
//                ex.printStackTrace();
//            }
//
//            return 0;
//        }
//
//        private void pollForContinuationFile( Directory dir ) {
//            long startMillis = System.currentTimeMillis();
//            long maxMillis   = startMillis + Duration.seconds(10).getMillis();
//
//            Option<File> continuationFile = dir.getFileIfExists("DONE");
//
//            System.out.println("continuationFile = " + dir + "/DONE");
//            while ( !continuationFile.hasValue() ) {
//                if ( System.currentTimeMillis() >=  maxMillis ) {
//                    System.out.println("FAILURE - aborting after 10s");
//                    return;
//                }
//
//                continuationFile = dir.getFileIfExists("DONE");
//            }
//        }
//    }
//
//    public static class RecoveryApp extends App {
//
//        protected void setup( Env env ) throws Exception {
//            super.setup(env);
//
//            setUseLockFileFlag(env, ".LOCK");
//        }
//
//        public int run( Env env ) throws Exception {
//            env.stdout("App ran");
//
//            return 0;
//        }
//
//        protected void recoverFromCrash( Env env ) {
//            env.stdout("Previous run did not shutdown cleanly, recovering");
//        }
//    }
//
//}
