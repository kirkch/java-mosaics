package mosaics.cli.runner;

import mosaics.fp.collections.FPIterable;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class MainArgsParserTest {

    // -f            ShortNameFlag              KV
    // -f abc        ShortNameKV                ParamDescription
    // --long        LongNameFlag
    // --long abc    LongNameKV
    // --long=abc    LongNameKV
    // abc           ValueOnly
    // -? -h --help  HelpFlag
    // -lsh          ShortNamesConcatenatedTogether


    @Test
    public void givenNoArgs_expectNoKVs() {
        FPIterable<KVPair> actual = MainArgsParser.parse();

        assertTrue( actual.isEmpty() );
    }

    @Test
    public void givenTwoValueArgs_expectTwoValueOnlys() {
        FPIterable<KVPair> actual = MainArgsParser.parse("abc", "123 456");

        List<KVPair> expected = Arrays.asList(
            new KVPair(null, "abc"),
            new KVPair(null, "123 456")
        );

        assertEquals( expected, actual.toList() );
    }

    @Test
    public void givenShortNameFlag_expectKeyOnly() {
        FPIterable<KVPair> actual = MainArgsParser.parse("-f");

        List<KVPair> expected = Arrays.asList(
            new KVPair( "f", "true" )
        );

        assertEquals( expected, actual.toList() );
    }

    @Test
    public void givenLongNameFlag_expectKeyOnly() {
        FPIterable<KVPair> actual = MainArgsParser.parse("--tail");

        List<KVPair> expected = Arrays.asList(
            new KVPair("tail", "true")
        );

        assertEquals( expected, actual.toList() );
    }

    @Test
    public void givenShortNameWithValueFlag_expectSeparateKVs() {
        FPIterable<KVPair> actual = MainArgsParser.parse("-a", "123");

        List<KVPair> expected = Arrays.asList(
            new KVPair("a", "true"),
            new KVPair(null, "123")
        );

        assertEquals( expected, actual.toList() );
    }

    @Test
    public void givenLongNameWithValueFlag_expectKV() {
        FPIterable<KVPair> actual = MainArgsParser.parse("--abc", "123");

        List<KVPair> expected = Arrays.asList(
            new KVPair( "abc", "true" ),
            new KVPair( null, "123" )
        );

        assertEquals( expected, actual.toList() );
    }

    @Test
    public void givenLongNameWithValueSeparatedByEQ_expectKV() {
        FPIterable<KVPair> actual = MainArgsParser.parse("--abc=123");

        List<KVPair> expected = Arrays.asList(
            new KVPair( "abc", "123" )
        );

        assertEquals( expected, actual.toList() );
    }

    @Test
    public void givenTwoShortNamesInARow_expectTwoKeysOnly() {
        FPIterable<KVPair> actual = MainArgsParser.parse("-a", "-b");

        List<KVPair> expected = Arrays.asList(
            new KVPair( "a", "true" ),
            new KVPair( "b", "true" )
        );

        assertEquals( expected, actual.toList() );
    }

    @Test
    public void givenTwoLongNamesInARow_expectTwoKeysOnly() {
        FPIterable<KVPair> actual = MainArgsParser.parse("--aaa", "--bbb");

        List<KVPair> expected = Arrays.asList(
            new KVPair( "aaa", "true" ),
            new KVPair( "bbb", "true" )
        );

        assertEquals( expected, actual.toList() );
    }

    @Test
    public void givenOneShortKVfollowedByLongName_expectKVThenK() {
        FPIterable<KVPair> actual = MainArgsParser.parse("-a", "--bbb");

        List<KVPair> expected = Arrays.asList(
            new KVPair( "a", "true" ),
            new KVPair( "bbb", "true" )
        );

        assertEquals( expected, actual.toList() );
    }

    @Test
    public void givenKVFollowedByV_expectKVThenV() {
        FPIterable<KVPair> actual = MainArgsParser.parse("-a", "123", "bbb");

        List<KVPair> expected = Arrays.asList(
            new KVPair( "a", "true" ),
            new KVPair( null, "123" ),
            new KVPair( null, "bbb" )
        );

        assertEquals( expected, actual.toList() );
    }

    @Test
    public void givenThreeShortNamesConcatetedTogether() {
        FPIterable<KVPair> actual = MainArgsParser.parse("-lsh");

        List<KVPair> expected = Arrays.asList(
            new KVPair( "l", "true" ),
            new KVPair( "s", "true" ),
            new KVPair( "h", "true" )
        );

        assertEquals( expected, actual.toList() );
    }

}
