package mosaics.io.codecs.html;

import lombok.Value;
import mosaics.fp.collections.FPList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class HtmlObjectCodec_annotationOverrideTests {

    private HtmlObjectCodec codec = new HtmlObjectCodec();


// property overrides

    @Test
    public void givenClassWithThreePropertiesAndAnnotationThatSelectsOnlyOne_expectDefaultHtmlToIncludeOnlyTheSelectedProperty() {
        PojoWithPropertyOverride1 bean = new PojoWithPropertyOverride1("a","b","c");

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_PojoWithPropertyOverride1\">",
            "  <div class=\"jcprop jcprop_PojoWithPropertyOverride1_prop1\">",
            "    <div class=\"jcpropkey jckey_PojoWithPropertyOverride1_prop1\">prop1</div>",
            "    <div class=\"jcpropvalue jcvalue_PojoWithPropertyOverride1_prop1\">a</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void givenClassWithThreePropertiesAndAnnotationThatSelectsTwoOfThem_expectDefaultHtmlToIncludeOnlyTheSelectedProperty() {
        PojoWithPropertyOverride2 bean = new PojoWithPropertyOverride2("a","b","c");

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_PojoWithPropertyOverride2\">",
            "  <div class=\"jcprop jcprop_PojoWithPropertyOverride2_prop1\">",
            "    <div class=\"jcpropkey jckey_PojoWithPropertyOverride2_prop1\">prop1</div>",
            "    <div class=\"jcpropvalue jcvalue_PojoWithPropertyOverride2_prop1\">a</div>",
            "  </div>",
            "  <div class=\"jcprop jcprop_PojoWithPropertyOverride2_prop2\">",
            "    <div class=\"jcpropkey jckey_PojoWithPropertyOverride2_prop2\">prop2</div>",
            "    <div class=\"jcpropvalue jcvalue_PojoWithPropertyOverride2_prop2\">b</div>",
            "  </div>",
            "</div>"
        );
    }


// template override

    @Test
    public void givenSingleAttributeTemplate_provideValue_expectCustomHtmlOutput() {
        PojoWithSingleAttributeTemplate bean = new PojoWithSingleAttributeTemplate("a","b","c");

        assertHtmlEquals(
            bean,
            "<foobar wibble=\"c\"/>"
        );
    }

    @Test
    public void givenSingleAttributeTemplate_provideMissingValue_expectNoHtml() {
        PojoWithSingleAttributeTemplate bean = new PojoWithSingleAttributeTemplate("a","b","");

        assertHtmlEquals( bean );
    }

    @Test
    public void givenTwoAttributeTemplate_provideValues_expectCustomHtmlOutput() {
        PojoWithTwoAttributeTemplate bean = new PojoWithTwoAttributeTemplate("a","b");

        assertHtmlEquals(
            bean,
            "<foo bar=\"a\" baz=\"b\"/>"
        );
    }

    @Test
    public void givenTwoAttributeTemplate_provideOneValue_expectCustomHtmlWithOneAttributeOutput() {
        PojoWithTwoAttributeTemplate bean = new PojoWithTwoAttributeTemplate("a","");

        assertHtmlEquals(
            bean,
            "<foo bar=\"a\"/>"
        );
    }

    @Test
    public void givenTwoAttributeTemplate_provideNoValues_expectNoHtmlOutput() {
        PojoWithTwoAttributeTemplate bean = new PojoWithTwoAttributeTemplate("","");

        assertHtmlEquals( bean );
    }

    @Test
    public void givenSingleBodyTemplate_provideValue_expectCustomHtml() {
        PojoWithSingleBodyTemplate bean = new PojoWithSingleBodyTemplate("a","b","c");

        assertHtmlEquals(
            bean,
            "<foobar>c</foobar>"
        );
    }

    @Test
    public void givenSingleBodyTemplate_provideMissingValue_expectNoHtml() {
        PojoWithSingleBodyTemplate bean = new PojoWithSingleBodyTemplate("a","b","");

        assertHtmlEquals( bean );
    }


    @Value
    @Html(properties = "prop1")
    public static class PojoWithPropertyOverride1 {
        private String prop1;
        private String prop2;
        private String prop3;
    }

    @Value
    @Html(properties = {"prop1","prop2"})
    public static class PojoWithPropertyOverride2 {
        private String prop1;
        private String prop2;
        private String prop3;
    }

    @Value
    @Html(template = "<foobar wibble=$prop3/>")
    public static class PojoWithSingleAttributeTemplate {
        private String prop1;
        private String prop2;
        private String prop3;
    }

    @Value
    @Html(template = "<foo bar=$prop1 baz=$prop2/>")
    public static class PojoWithTwoAttributeTemplate {
        private String prop1;
        private String prop2;
    }

    @Value
    @Html(template = "<foobar>$prop3</foobar>")
    public static class PojoWithSingleBodyTemplate {
        private String prop1;
        private String prop2;
        private String prop3;
    }


    private void assertHtmlEquals( Object v, String...expectedHtml ) {
        if ( expectedHtml.length == 0 ) {
            String actual = codec.toString( v, v.getClass() );
            String expected = "<html>\n" +
                "  <body/>\n" +
                "</html>";

            assertEquals( expected, actual );

            return;
        }

        FPList<String> t = FPList.wrapArray(expectedHtml).map( line -> "    "+line).toFPList();

        FPList<String> prefix = FPList.wrapArray( "<html>", "  <body>" );
        FPList<String> postfix = FPList.wrapArray( "  </body>", "</html>" );


        String expected = prefix.and(t).and(postfix).mkString("\n");
        String actual   = codec.toString( v, v.getClass() );

        assertEquals( expected, actual );
    }
}
