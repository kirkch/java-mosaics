package mosaics.io.codecs;

/**
 * Example DTO interface used in unit tests to verify behaviour.
 */
public interface Animal {
    public static Animal parseString( String v ) {
        if ( v.startsWith("Dog") ) {
            return new Dog( v.substring(9, v.length()-1) );
        } else {
            return new Cow( v.substring(9, v.length()-1) );
        }
    }
}


