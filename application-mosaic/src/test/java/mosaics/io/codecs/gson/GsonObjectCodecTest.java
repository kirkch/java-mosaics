package mosaics.io.codecs.gson;

import mosaics.io.codecs.Animal;
import mosaics.io.codecs.Cow;
import mosaics.io.codecs.Dog;
import mosaics.io.codecs.ObjectCodec;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 *
 */
public class GsonObjectCodecTest {

    @Test
    public void dogDTORoundTrip() {
        ObjectCodec codec = new GsonObjectCodecBuilder().create();

        Dog origDog = new Dog( "dog1" );

        String dogJson = codec.toString( origDog, Dog.class );

        assertEquals(
            "{\"name\":\"dog1\"}",
            dogJson
        );

        Animal returnedDog = codec.fromString( dogJson, Dog.class ).getResult();

        assertEquals( origDog, returnedDog );
    }

    @Test
    public void cowAndDogDTOsRoundTrip() {
        ObjectCodec codec = new GsonObjectCodecBuilder()
            .identifyInheritanceTreeByClassName( Animal.class )
            .create();


        Cow origCow = new Cow( "cow1" );
        Dog origDog = new Dog( "dog1" );

        String cowJson = codec.toString( origCow, Animal.class );
        String dogJson = codec.toString( origDog, Animal.class );

        assertEquals(
            "{\"name\":\"cow1\",\"type\":\"mosaics.io.codecs.Cow\"}",
            cowJson
        );

        assertEquals(
            "{\"name\":\"dog1\",\"type\":\"mosaics.io.codecs.Dog\"}",
            dogJson
        );

        Animal returnedCow = codec.fromString( cowJson, Animal.class ).getResult();
        Animal returnedDog = codec.fromString( dogJson, Animal.class ).getResult();

        assertEquals( origCow, returnedCow );
        assertEquals( origDog, returnedDog );
    }

    @Test
    public void nullField() {
        ObjectCodec codec = new GsonObjectCodecBuilder()
            .identifyInheritanceTreeByClassName( Animal.class )
            .create();


        Cow origCow = new Cow( null );

        String cowJson = codec.toString( origCow, Animal.class );

        assertEquals(
            "{\"type\":\"mosaics.io.codecs.Cow\"}",
            cowJson
        );

        Animal returnedCow = codec.fromString( cowJson, Animal.class ).getResult();

        assertEquals( origCow, returnedCow );
    }

    @Test
    public void parseExplicitNullField() {
        ObjectCodec codec = new GsonObjectCodecBuilder()
            .identifyInheritanceTreeByClassName( Animal.class )
            .create();


        Cow origCow = new Cow( null );

        String cowJson = codec.toString( origCow, Animal.class );

        assertEquals(
            "{\"type\":\"mosaics.io.codecs.Cow\"}",
            cowJson
        );

        Animal returnedCow = codec.fromString( "{\"name\":null,\"type\":\"mosaics.io.codecs.Cow\"}", Animal.class ).getResult();

        assertEquals( origCow, returnedCow );
    }

    @Test
    public void classRoundTrip() {
        ObjectCodec codec = new GsonObjectCodecBuilder().create();

        String json = codec.toString( String.class, Class.class );

        assertEquals( "\"java.lang.String\"", json );

        Class roundTrip = codec.fromString( json, Class.class ).getResult();

        assertEquals( String.class, roundTrip );
    }

    @Test
    public void nullClassRoundTrip() {
        ObjectCodec codec = new GsonObjectCodecBuilder().create();

        String json = codec.toString( null, Class.class );

        assertEquals( "null", json );

        Class roundTrip = codec.fromString( "", Class.class ).getResult();

        assertEquals( null, roundTrip );
    }

    @Test
    public void blankClassRoundTrip() {
        ObjectCodec codec = new GsonObjectCodecBuilder().create();

        Class roundTrip = codec.fromString( "null", Class.class ).getResult();

        assertEquals( null, roundTrip );
    }

}
