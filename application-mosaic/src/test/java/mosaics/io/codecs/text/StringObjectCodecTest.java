package mosaics.io.codecs.text;

import mosaics.io.codecs.ObjectCodec;
import mosaics.io.codecs.Animal;
import mosaics.io.codecs.Dog;
import mosaics.lang.Backdoor;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class StringObjectCodecTest {

    private ObjectCodec codec = StringObjectCodec.INSTANCE;


// toString

    @Test
    public void toString_roundTripSingleLineString() {
        String v = codec.toString( "hello world", String.class );

        assertEquals( "hello world", codec.fromString(v, String.class).getResult() );
    }

    @Test
    public void toString_roundTripMultiLineString() {
        String v = codec.toString( "hello" + Backdoor.NEWLINE + "world", String.class );

        assertEquals( "hello" + Backdoor.NEWLINE + "world", codec.fromString(v, String.class).getResult() );
    }

    @Test
    public void toString_roundTripSingleLineInteger() {
        String v = codec.toString( 701, Integer.class );

        assertEquals( 701, codec.fromString(v, Integer.class).getResult().intValue() );
    }

    @Test
    public void toString_roundTripSingleLineAnimal_expectItToUseFactoryMethod() {
        String v = codec.toString( new Dog("Harvey"), Animal.class );

        assertEquals( new Dog("Harvey"), codec.fromString(v, Animal.class).getResult() );
    }


// usingReader

    @Test
    public void usingReader_roundTripSingleLineString() {
        StringWriter stringWriter = new StringWriter();
        codec.writeTo( "hello world", stringWriter, String.class );

        StringReader stringReader = new StringReader( stringWriter.toString() );
        assertEquals( "hello world", codec.fromReader( stringReader, String.class).getResult() );
    }

    @Test
    public void usingReader_roundTripMultiLineString() {
        StringWriter stringWriter = new StringWriter();
        codec.writeTo( "hello" + Backdoor.NEWLINE + "world", stringWriter, String.class );

        StringReader stringReader = new StringReader( stringWriter.toString() );
        assertEquals( "hello" + Backdoor.NEWLINE + "world", codec.fromReader( stringReader, String.class).getResult() );
    }

    @Test
    public void usingReader_roundTripSingleLineInteger() {
        StringWriter stringWriter = new StringWriter();
        codec.writeTo( 701, stringWriter, Integer.class );

        StringReader stringReader = new StringReader( stringWriter.toString() );
        assertEquals( 701, codec.fromReader( stringReader, Integer.class).getResult().intValue() );
    }

    @Test
    public void usingReader_roundTripSingleLineAnimal_expectItToUseFactoryMethod() {
        StringWriter stringWriter = new StringWriter();
        codec.writeTo( new Dog("Harvey"), stringWriter, Animal.class );

        StringReader stringReader = new StringReader( stringWriter.toString() );
        assertEquals( new Dog("Harvey"), codec.fromReader( stringReader, Animal.class).getResult() );
    }


// usingStream

    @Test
    public void usingStream_roundTripSingleLineString() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        codec.writeTo( "hello world", out, String.class );

        byte[] bytes = out.toByteArray();
        ByteArrayInputStream in = new ByteArrayInputStream( bytes );
        assertEquals( "hello world", codec.fromInputStream(in, String.class).getResult() );
    }

    @Test
    public void usingStream_roundTripMultiLineString() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        codec.writeTo( "hello" + Backdoor.NEWLINE + "world", out, String.class );

        byte[] bytes = out.toByteArray();
        ByteArrayInputStream in = new ByteArrayInputStream( bytes );
        assertEquals( "hello" + Backdoor.NEWLINE + "world", codec.fromInputStream(in, String.class).getResult() );
    }

    @Test
    public void usingStream_roundTripSingleLineInteger() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        codec.writeTo( 701, out, Integer.class );

        byte[] bytes = out.toByteArray();
        ByteArrayInputStream in = new ByteArrayInputStream( bytes );
        assertEquals( 701, codec.fromInputStream( in, Integer.class).getResult().intValue() );
    }

    @Test
    public void usingStream_roundTripSingleLineAnimal_expectItToUseFactoryMethod() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        codec.writeTo( new Dog("Harvey"), out, Animal.class );

        byte[] bytes = out.toByteArray();
        ByteArrayInputStream in = new ByteArrayInputStream( bytes );
        assertEquals( new Dog("Harvey"), codec.fromInputStream( in, Animal.class).getResult() );
    }

}
