package mosaics.io;

import mosaics.cli.App;
import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.junit.JMFileExtension;
import mosaics.lang.Backdoor;
import mosaics.lang.RandomUtils;
import mosaics.lang.lifecycle.Subscription;
import mosaics.logging.LogMessage;
import mosaics.logging.LogMessageFactory;
import mosaics.logging.LogMessageProxyFactory;
import mosaics.logging.loglevels.OpsInfo;
import mosaics.os.OSProcess;
import mosaics.os.OSProcessBuilder;
import mosaics.os.StdOutEventSink;
import mosaics.utils.OSUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


// NB these tests are for LockFile;  which is from the io module.   The test is currently in app so
// that it can make use of the OSProcessBuilder and App

@Disabled("deserializing json LogMessages currently fails -- todo add round trip tests")
@ExtendWith(JMFileExtension.class)
public class LockFileIntTest {

    private final Env env = Env.createTestEnvWithRealClock();
    private final Request request = env.createRequest( "junit" );

    public  File     dir;
    public  File     expectedUnderlyingFile;

    private String   name = RandomUtils.randomString( "^[a-z][a-z0-9]*$", 10 );
    private LockFile lock;
    private String   currentPID = Integer.toString( OSUtils.getCurrentPid() );

    private Subscription lockSubscription;


    @BeforeEach
    public void setup() {
        lock                   = new LockFile( dir, name );
        expectedUnderlyingFile = new File( dir, name+".lock" );
    }

    @AfterEach
    public void tearDown() {
        if ( lockSubscription != null ) {
            lockSubscription.cancel();

            lockSubscription = null;
        }
    }

    @Test
    public void givenLockFileHeldByAnotherProcess_tryToAcquireLock_expectFailure() {
        OSProcess proc = new OSProcessBuilder()
            .withCommand( AcquireLockMain.class, "-v", "--logFormat=JSON", dir.getAbsolutePath(), name )
            .run( request );

        try {
            StdOutEventSink stdoutAsEventsSink = proc.getStdoutAsEventsSink();

            assertNotNull( stdoutAsEventsSink.spinUntilContains("Request Started") );

            assertFalse( lock.isAvailable() );

            assertTrue( lock.tryLock().hasValue() );
        } finally {
            proc.killProcess();
        }
    }

    @Test
    public void givenLockFileHeldByAnotherProcess_killTheOwningProcess_expectLockToBecomeAvailable() {
        OSProcess proc = new OSProcessBuilder()
            .withCommand( AcquireLockMain.class, "-v", "--logFormat=JSON", dir.getAbsolutePath(), name )
            .run( request );

        StdOutEventSink stdoutAsEventsSink = proc.getStdoutAsEventsSink();

        assertNotNull( stdoutAsEventsSink.spinUntilContains("Request Started") );

        assertFalse( lock.isAvailable() );

        proc.killProcess();

        assertTrue( lock.isAvailable() );
    }


    public interface LockAppLogMessageFactory extends LogMessageFactory {
        public static final LockAppLogMessageFactory INSTANCE = LogMessageProxyFactory.INSTANCE.createLogMessageFactory( LockAppLogMessageFactory.class );

        @OpsInfo("Request Started")
        public LogMessage lockAcquired();
    }

    public static class AcquireLockMain extends App {
        public void run( Request req, File dir, String lockName ) {
            LockFile lockFile = new LockFile( dir, lockName );

            lockFile.lock();

            req.log( LockAppLogMessageFactory.INSTANCE.lockAcquired() );

            Backdoor.sleep( 20_000 );
        }
    }
}
