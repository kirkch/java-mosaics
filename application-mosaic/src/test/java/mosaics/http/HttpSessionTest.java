package mosaics.http;

import mosaics.cli.requests.Request;
import mosaics.fp.FP;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;


public class HttpSessionTest {
    private final HttpClient httpClientMock = Mockito.mock( HttpClient.class );
    private final Request     requestMock    = Mockito.mock( Request.class );

    @Test
    public void givenNewSession_makeRestCall_expectHeadersInRequestToBeUnaltered() throws IOException {
        HttpSession  session      = new HttpSession( httpClientMock );
        HttpRequest  httpRequest  = HttpRequest.GET("http://foo.bar");
        HttpResponse httpResponse = HttpResponse.of( 200 );

        Mockito.when( httpClientMock.invoke(requestMock,httpRequest) ).thenReturn( httpResponse );

        HttpSessionResponse actual = session.invoke( requestMock, httpRequest );

        assertSame( httpResponse, actual.getResponse() );
        assertSame( session, actual.getUpdatedSession() );
        Mockito.verify(httpClientMock).invoke( requestMock, httpRequest );
    }

    @Test
    public void givenSessionWithHeadersAlreadyInRestCall_expectHeadersInRequestToBeUnaltered() throws IOException {
        HttpSession  session      = new HttpSession( httpClientMock ).appendHeader( "A", "1" );
        HttpRequest  httpRequest  = HttpRequest.GET("http://foo.bar").appendHeader( "A", "1" );
        HttpResponse httpResponse = HttpResponse.of( 200 );

        Mockito.when( httpClientMock.invoke(requestMock,httpRequest) ).thenReturn( httpResponse );

        HttpSessionResponse actual = session.invoke( requestMock, httpRequest );

        assertSame( httpResponse, actual.getResponse() );
        assertSame( session, actual.getUpdatedSession() );
        Mockito.verify(httpClientMock).invoke( requestMock, httpRequest );
    }

    @Test
    public void givenSessionWithNewHeader_expectHeadersInRequestToBeAppendedTo() throws IOException {
        HttpSession  session      = new HttpSession( httpClientMock ).appendHeader( "A", "1" );
        HttpRequest  httpRequest  = HttpRequest.GET("http://foo.bar").appendHeader( "B", "2" );
        HttpResponse httpResponse = HttpResponse.of( 200 );
        HttpRequest  expectedHttpRequest  = HttpRequest.GET("http://foo.bar")
            .appendHeader( "A", "1" )
            .appendHeader( "B", "2" );

        Mockito.when( httpClientMock.invoke(requestMock,expectedHttpRequest) ).thenReturn( httpResponse );

        HttpSessionResponse actual = session.invoke( requestMock, httpRequest );

        assertSame( httpResponse, actual.getResponse() );
        assertSame( session, actual.getUpdatedSession() );
        Mockito.verify(httpClientMock).invoke( requestMock, expectedHttpRequest );
    }

    @Test
    public void givenSessionWithCookies_expectSessionCookiesToBeAddedToRequest() throws IOException {
        HttpSession  session      = new HttpSession( httpClientMock ).appendCookie( "a", "1" ).appendCookie( "b", "2" );
        HttpRequest  httpRequest  = HttpRequest.GET("http://foo.bar");
        HttpResponse httpResponse = HttpResponse.of( 200 );
        HttpRequest  expectedHttpRequest  = HttpRequest.GET("http://foo.bar")
            .appendCookie( "a", "1" )
            .appendCookie( "b", "2" );

        Mockito.when( httpClientMock.invoke(requestMock,expectedHttpRequest) ).thenReturn( httpResponse );

        HttpSessionResponse actual = session.invoke( requestMock, httpRequest );

        Mockito.verify(httpClientMock).invoke( requestMock, expectedHttpRequest );
        assertSame( httpResponse, actual.getResponse() );
        assertSame( session, actual.getUpdatedSession() );
    }

    @Test
    public void givenRequestReturnsCookies_expectThemToBeAutoAddedToSession() throws IOException {
        HttpSession  session      = new HttpSession( httpClientMock );
        HttpRequest  httpRequest  = HttpRequest.GET("http://foo.bar");
        HttpResponse httpResponse = HttpResponse.of( 200 )
            .appendHeader( "Set-Cookie", "a=1; Domain=foo.bar" )
            .appendHeader( "Set-Cookie", "b=2; Domain=foo.bar" );
        HttpRequest  expectedHttpRequest  = HttpRequest.GET("http://foo.bar");
        HttpSession  expectedUpdatedSession = session.appendCookies(
            HttpCookie.of("a", "1").withDomain( FP.option("foo.bar") ),
            HttpCookie.of("b", "2").withDomain( FP.option("foo.bar") )
        );

        Mockito.when( httpClientMock.invoke(requestMock,expectedHttpRequest) ).thenReturn( httpResponse );

        HttpSessionResponse actual = session.invoke( requestMock, httpRequest );

        assertSame( httpResponse, actual.getResponse() );
        assertEquals( expectedUpdatedSession, actual.getUpdatedSession() );
        Mockito.verify(httpClientMock).invoke( requestMock, expectedHttpRequest );
    }
}
