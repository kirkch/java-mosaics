package mosaics.http;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class HttpHeadersTest {
    @Nested
    public class HasHeaderTestCases {
        @Test
        public void givenEmpty_hasHeader_expectFalse() {
            HttpHeaders headers = HttpHeaders.empty();

            assertEquals( 0, headers.size() );
            assertFalse( headers.hasHeader( "A" ) );
        }

        @Test
        public void givenOneEntry_hasCorrectHeader_expectTrue() {
            HttpHeaders headers = HttpHeaders.of( "A", "1" );

            assertTrue( headers.hasHeader( "A" ) );
        }

        @Test
        public void givenOneEntry_hasDifferentHeader_expectFalse() {
            HttpHeaders headers = HttpHeaders.of( "A", "1" );

            assertFalse( headers.hasHeader( "B" ) );
        }

        @Test
        public void givenOneBlankValue_callHasHeader_expectFalse() {
            HttpHeaders headers = HttpHeaders.of( "A", "" );

            assertFalse( headers.hasHeader( "A" ) );
        }
    }

    @Nested
    public class GetSingleHeaderTestCases {
        @Test
        public void givenNoHeaders_callGetSingleHeader_expectNoValues() {
            HttpHeaders headers = HttpHeaders.empty();

            assertTrue( headers.getSingleHeader("A").isEmpty() );
        }

        @Test
        public void givenOneSingleHeaderValue_callWithMissingKey_expectNoValues() {
            HttpHeaders headers = HttpHeaders.of( "A", "1" );

            assertEquals( FP.emptyOption(), headers.getSingleHeader("B") );
        }

        @Test
        public void givenOneSingleHeaderValue_callGetSingleHeader_expectOneValue() {
            HttpHeaders headers = HttpHeaders.of( "A", "1" );

            assertEquals( FP.option("1"), headers.getSingleHeader("A") );
        }

        @Test
        public void givenOneMultiHeaderValue_callGetSingleHeader_expectValue() {
            HttpHeaders headers = HttpHeaders.of( "A", "1", "A", "2", "A", "3" );

            assertEquals( FP.option("1"), headers.getSingleHeader("A") );
        }

        @Test
        public void givenMultipleValues_callGetSingleHeader_expectValue() {
            HttpHeaders headers = HttpHeaders.of( "A", "1", "B", "2", "C", "3" );

            assertEquals( FP.option("1"), headers.getSingleHeader("A") );
            assertEquals( FP.option("2"), headers.getSingleHeader("B") );
            assertEquals( FP.option("3"), headers.getSingleHeader("C") );
            assertEquals( FP.emptyOption(), headers.getSingleHeader("D") );
        }
    }

    @Nested
    public class GetHeaderTestCases {
        @Test
        public void givenNoHeaders_callGetHeader_expectNoValues() {
            HttpHeaders headers = HttpHeaders.empty();

            assertEquals( FP.emptyList(), headers.getHeader("A") );
        }

        @Test
        public void givenOneSingleHeaderValue_callWithMissingKey_expectNoValues() {
            HttpHeaders headers = HttpHeaders.of("A", "1");

            assertEquals( FP.emptyList(), headers.getHeader("B") );
        }

        @Test
        public void givenOneSingleHeaderValue_callGetHeader_expectOneValue() {
            HttpHeaders headers = HttpHeaders.of("A", "1");

            assertEquals( FP.toList("1"), headers.getHeader("A") );
        }

        @Test
        public void givenOneMultiHeaderValue_callGetHeader_expectValues() {
            HttpHeaders headers = HttpHeaders.of("A", "1", "A", "2");

            assertEquals( FP.toList("1", "2"), headers.getHeader("A") );
        }

        @Test
        public void givenMultipleValues_callGetHeader_expectValue() {
            HttpHeaders headers = HttpHeaders.of("A", "1", "B", "2");

            assertEquals( FP.toList("1"), headers.getHeader("A") );
            assertEquals( FP.toList("2"), headers.getHeader("B") );
            assertEquals( FP.toList("2"), headers.getHeader("b") );
        }
    }

    @Nested
    public class WithHeaderTestCases {
        @Test
        public void givenNoHeaders_addOneHeader_expectNewHeaderToBeAdded() {
            HttpHeaders headers = HttpHeaders.empty().appendHeader( "A", "1" );

            assertEquals( FP.toList("1"), headers.getHeader("A") );
            assertEquals( 1, headers.size() );
        }

        @Test
        public void givenNoHeaders_specifyMultiHeader_expectNewHeaderToBeAdded() {
            HttpHeaders headers = HttpHeaders.empty()
                .withHeader( "A", List.of("1","2") );

            assertEquals( FP.toList("1","2"), headers.getHeader("A") );
            assertEquals( 2, headers.size() );
        }

        @Test
        public void givenOneHeader_addAnotherHeader_expectNewHeaderToBeAdded() {
            HttpHeaders headers = HttpHeaders.of("A", "1")
                .appendHeader( "B", "2" );

            assertEquals( FP.toList("1"), headers.getHeader("A") );
            assertEquals( FP.toList("2"), headers.getHeader("B") );
            assertEquals( 2, headers.size() );
        }

        @Test
        public void givenOneHeader_addAnotherValue_expectNewHeaderToBeAdded() {
            HttpHeaders headers = HttpHeaders.of("A", "1")
                .appendHeader( "A", "2" );

            assertEquals( FP.toList("1", "2"), headers.getHeader("A") );
            assertEquals( 2, headers.size() );
        }

        @Test
        public void givenMultiHeader_specifyNewMultiValue_expectOldValueToBeReplaced() {
            HttpHeaders headers = HttpHeaders.of("A", "1", "A", "2")
                .withHeader( "A", List.of("i") );

            assertEquals( FP.toList("i"), headers.getHeader("A") );
            assertEquals( 1, headers.size() );
            assertFalse( headers.isEmpty() );
            assertTrue( headers.hasValues() );
        }

        @Test
        public void givenMultiHeader_specifyEmptyMultiValue_expectOldValuesToBeRemoved() {
            HttpHeaders headers = HttpHeaders.of("A", "1", "A", "2")
                .withHeader( "A", FP.emptyVector() );

            assertEquals( FP.emptyList(), headers.getHeader("A") );
            assertEquals( 0, headers.size() );
            assertTrue( headers.isEmpty() );
            assertSame( HttpHeaders.empty(), headers );
        }
    }

    @Nested
    public class RemoveHeaderTestCases {
        @Test
        public void givenEmptyHeaders_callRemove_expectNoChange() {
            HttpHeaders headers = HttpHeaders.empty();

            assertTrue( headers == headers.removeHeader("A") );
        }

        @Test
        public void givenSingleHeader_callRemove_expectEmptyHeaders() {
            HttpHeaders headers = HttpHeaders.of("A", "1");

            assertSame( HttpHeaders.empty(), headers.removeHeader("A") );
        }

        @Test
        public void givenTwoHeaders_callRemove_expectOneHeaderLeft() {
            HttpHeaders headers = HttpHeaders.of("A", "1", "B", "2");

            assertEquals( HttpHeaders.of("B", "2"), headers.removeHeader("A") );
        }
    }

    @Nested
    public class AppendHttpHeadersTestCases {
        @Test
        public void givenNoHeaders_addOneHeader_expectNewHeaderToBeAdded() {
            HttpHeaders extraHeaders = HttpHeaders.of("A","1");
            HttpHeaders headers      = HttpHeaders.empty().appendHeaders( extraHeaders );

            assertEquals( FP.toList("1"), headers.getHeader("A") );
            assertEquals( 1, headers.size() );
        }

        @Test
        public void givenNoHeaders_addNoHeaders_expectNoChange() {
            HttpHeaders extraHeaders = HttpHeaders.empty();
            HttpHeaders initial      = HttpHeaders.empty();
            HttpHeaders actual       = initial.appendHeaders( extraHeaders );

            assertSame( initial, actual );
        }

        @Test
        public void givenOneHeader_addDupHeader_expectNoChange() {
            HttpHeaders extraHeaders = HttpHeaders.of("A","1");
            HttpHeaders initial      = HttpHeaders.of("A","1");
            HttpHeaders actual       = initial.appendHeaders( extraHeaders );

            assertSame( initial, actual );
        }

        @Test
        public void givenOneHeader_addTwoHeader_expectThreeHeaders() {
            HttpHeaders extraHeaders = HttpHeaders.of("B","2","C","3");
            HttpHeaders initial      = HttpHeaders.of("A","1");
            HttpHeaders actual       = initial.appendHeaders( extraHeaders );
            HttpHeaders expected     = HttpHeaders.of("A","1","B","2","C","3");

            assertEquals( expected, actual );
        }
    }
}
